DELETE
FROM wp_posts
WHERE post_type IN ('shop_order', 'product');

DELETE FROM wp_posts
WHERE post_parent > 0 AND post_parent NOT IN (SELECT ID FROM (SELECT * FROM wp_posts) t);

DELETE FROM wp_postmeta
WHERE post_id NOT IN (SELECT ID FROM wp_posts);

DELETE FROM wp_woocommerce_order_itemmeta;

DELETE FROM wp_woocommerce_order_items;

DELETE FROM wp_term_relationships WHERE object_id NOT IN (SELECT ID FROM wp_posts);