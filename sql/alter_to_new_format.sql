#######################

INSERT INTO `wp_usermeta` (`user_id`, `meta_key`, `meta_value`)
SELECT ID, 'billing_apartment', ''
FROM wp_users
WHERE ID NOT IN (SELECT user_id FROM wp_usermeta WHERE meta_key = 'billing_apartment');

INSERT INTO `wp_usermeta` (`user_id`, `meta_key`, `meta_value`)
SELECT ID, 'billing_entry_code', ''
FROM wp_users
WHERE ID NOT IN (SELECT user_id FROM wp_usermeta WHERE meta_key = 'billing_entry_code');

INSERT INTO `wp_usermeta` (`user_id`, `meta_key`, `meta_value`)
SELECT ID, 'billing_floor_no', ''
FROM wp_users
WHERE ID NOT IN (SELECT user_id FROM wp_usermeta WHERE meta_key = 'billing_floor_no');

INSERT INTO `wp_usermeta` (`user_id`, `meta_key`, `meta_value`)
SELECT ID, 'billing_phone2', ''
FROM wp_users
WHERE ID NOT IN (SELECT user_id FROM wp_usermeta WHERE meta_key = 'billing_phone2');

INSERT INTO `wp_usermeta` (`user_id`, `meta_key`, `meta_value`)
SELECT ID, 'billing_street', ''
FROM wp_users
WHERE ID NOT IN (SELECT user_id FROM wp_usermeta WHERE meta_key = 'billing_street');

INSERT INTO `wp_usermeta` (`user_id`, `meta_key`, `meta_value`)
SELECT ID, 'billing_street_no', ''
FROM wp_users
WHERE ID NOT IN (SELECT user_id FROM wp_usermeta WHERE meta_key = 'billing_street_no');


UPDATE wp_usermeta um1 INNER JOIN wp_usermeta um2 ON um1.user_id = um2.user_id
SET um1.meta_value = um2.meta_value
WHERE um1.meta_value = '' AND um1.meta_key = 'billing_apartment' AND um2.meta_key = 'apartment';

UPDATE wp_usermeta um1 INNER JOIN wp_usermeta um2 ON um1.user_id = um2.user_id
SET um1.meta_value = um2.meta_value
WHERE um1.meta_value = '' AND um1.meta_key = 'billing_entry_code' AND um2.meta_key = 'entry_code';

UPDATE wp_usermeta um1 INNER JOIN wp_usermeta um2 ON um1.user_id = um2.user_id
SET um1.meta_value = um2.meta_value
WHERE um1.meta_value = '' AND um1.meta_key = 'billing_phone2' AND um2.meta_key = 'phone ext';

UPDATE wp_usermeta um1 INNER JOIN wp_usermeta um2 ON um1.user_id = um2.user_id
SET um1.meta_value = TRIM(MID(MID(um2.meta_value, 1, CHAR_LENGTH(um2.meta_value) - LOCATE(' ', REVERSE(um2.meta_value)) + 1), 1, 64))
WHERE um1.meta_value = '' AND um1.meta_key = 'billing_street' AND um2.meta_key = 'billing_address_1';

UPDATE wp_usermeta um1 INNER JOIN wp_usermeta um2 ON um1.user_id = um2.user_id
SET um1.meta_value = TRIM(MID(MID(um2.meta_value, CHAR_LENGTH(um2.meta_value) - LOCATE(' ', REVERSE(um2.meta_value)) + 1), 1, 10))
WHERE um1.meta_value = '' AND um1.meta_key = 'billing_street_no' AND um2.meta_key = 'billing_address_1';

#######################

ALTER TABLE `customers`
DROP COLUMN `first_name`,
DROP COLUMN `last_name`,
DROP COLUMN `phone`,
DROP COLUMN `phone1_no`,
DROP COLUMN `phone1_ext`,
DROP COLUMN `phone2`,
DROP COLUMN `phone2_no`,
DROP COLUMN `phone2_ext`,
DROP COLUMN `house_no`,
DROP COLUMN `street`,
DROP COLUMN `street_number`,
DROP COLUMN `floor`,
DROP COLUMN `city`,
DROP COLUMN `country`,
DROP COLUMN `zip`,
DROP COLUMN `shipping_house_number`,
DROP COLUMN `shipping_street`,
DROP COLUMN `shipping_city`,
DROP COLUMN `shipping_country`,
DROP COLUMN `shipping_zip`,
DROP COLUMN `billing_house_number`,
DROP COLUMN `billing_street`,
DROP COLUMN `billing_city`,
DROP COLUMN `billing_country`,
DROP COLUMN `billing_zip`,
DROP COLUMN `house_entry_code`,
ADD COLUMN `floor_no` varchar(10) DEFAULT NULL AFTER `barcode`,
ADD COLUMN `apartment` varchar(10) DEFAULT NULL AFTER `barcode`,
ADD COLUMN `entry_code` varchar(10) DEFAULT NULL AFTER `barcode`,
ADD COLUMN `city` varchar(64) DEFAULT NULL AFTER `barcode`,
ADD COLUMN `street` varchar(64) DEFAULT NULL AFTER `barcode`,
ADD COLUMN `street_no` varchar(10) DEFAULT NULL AFTER `barcode`,
ADD COLUMN `phone2_ext` varchar(16) DEFAULT NULL AFTER `barcode`,
ADD COLUMN `phone2_no` varchar(64) DEFAULT NULL AFTER `barcode`,
ADD COLUMN `phone2` varchar(64) DEFAULT NULL AFTER `barcode`,
ADD COLUMN `phone_ext` varchar(16) DEFAULT NULL AFTER `barcode`,
ADD COLUMN `phone_no` varchar(64) DEFAULT NULL AFTER `barcode`,
ADD COLUMN `phone` varchar(64) DEFAULT NULL AFTER `barcode`,
MODIFY COLUMN `email` varchar(128) DEFAULT NULL AFTER `barcode`,
ADD COLUMN `last_name` varchar(64) DEFAULT NULL AFTER `barcode`,
ADD COLUMN `first_name` varchar(64) DEFAULT NULL AFTER `barcode`;

UPDATE customers c INNER JOIN wp_shookbook.wp_usermeta um ON c.wp_id = um.user_id
SET c.first_name = um.meta_value
WHERE um.meta_key = MID('billing_first_name', 1, 64);

UPDATE customers c INNER JOIN wp_shookbook.wp_usermeta um ON c.wp_id = um.user_id
SET c.last_name = um.meta_value
WHERE um.meta_key = MID('billing_last_name', 1, 64);

UPDATE customers c INNER JOIN wp_shookbook.wp_usermeta um ON c.wp_id = um.user_id
SET c.email = um.meta_value
WHERE um.meta_key = MID('billing_email', 1, 128);

UPDATE customers c INNER JOIN wp_shookbook.wp_usermeta wu ON c.wp_id = wu.user_id
SET phone = wu.meta_value,
    phone_no = right(wu.meta_value, 7),
    phone_ext = case when left(wu.meta_value, 2) = '05' then left(wu.meta_value,3) else left(wu.meta_value,2) end
WHERE wu.meta_key = 'billing_phone';

UPDATE customers c INNER JOIN wp_shookbook.wp_usermeta wu ON c.wp_id = wu.user_id
SET phone2 = wu.meta_value,
    phone2_no = right(wu.meta_value, 7),
    phone2_ext = case when left(wu.meta_value, 2) = '05' then left(wu.meta_value,3) else left(wu.meta_value,2) end
WHERE wu.meta_key = 'phone ext';

UPDATE customers c INNER JOIN wp_shookbook.wp_usermeta wu ON c.wp_id = wu.user_id
SET phone2 = wu.meta_value,
    phone2_no = right(wu.meta_value, 7),
    phone2_ext = case when left(wu.meta_value, 2) = '05' then left(wu.meta_value,3) else left(wu.meta_value,2) end
WHERE wu.meta_key = 'billing_phone2';

UPDATE customers c INNER JOIN wp_shookbook.wp_usermeta um ON c.wp_id = um.user_id
SET c.street_no = MID(MID(meta_value, CHAR_LENGTH(meta_value) - LOCATE(' ', REVERSE(meta_value)) + 1), 1, 10)
WHERE um.meta_key = 'billing_address_1';

UPDATE customers c INNER JOIN wp_shookbook.wp_usermeta um ON c.wp_id = um.user_id
SET c.street_no = MID(um.meta_value, 1, 10)
WHERE um.meta_key = 'billing_street_no';

UPDATE customers c INNER JOIN wp_shookbook.wp_usermeta um ON c.wp_id = um.user_id
SET c.street = MID(MID(meta_value, 1, CHAR_LENGTH(meta_value) - LOCATE(' ', REVERSE(meta_value)) + 1), 1, 64)
WHERE um.meta_key = 'billing_address_1';

UPDATE customers c INNER JOIN wp_shookbook.wp_usermeta um ON c.wp_id = um.user_id
SET c.street = MID(um.meta_value, 1, 64)
WHERE um.meta_key = 'billing_street';

UPDATE customers c INNER JOIN wp_shookbook.wp_usermeta um ON c.wp_id = um.user_id
SET c.city = MID(um.meta_value, 1, 64)
WHERE um.meta_key = 'billing_city';

UPDATE customers c INNER JOIN wp_shookbook.wp_usermeta um ON c.wp_id = um.user_id
SET c.entry_code = MID(um.meta_value, 1, 10)
WHERE um.meta_key = 'entry_code';

UPDATE customers c INNER JOIN wp_shookbook.wp_usermeta um ON c.wp_id = um.user_id
SET c.entry_code = MID(um.meta_value, 1, 10)
WHERE um.meta_key = 'billing_entry_code';

UPDATE customers c INNER JOIN wp_shookbook.wp_usermeta um ON c.wp_id = um.user_id
SET c.apartment = MID(um.meta_value, 1, 10)
WHERE um.meta_key = 'apartment';

UPDATE customers c INNER JOIN wp_shookbook.wp_usermeta um ON c.wp_id = um.user_id
SET c.apartment = MID(um.meta_value, 1, 10)
WHERE um.meta_key = 'billing_apartment';

UPDATE customers c INNER JOIN wp_shookbook.wp_usermeta um ON c.wp_id = um.user_id
SET c.floor_no = MID(um.meta_value, 1, 10)
WHERE um.meta_key = 'billing_floor_no';



ALTER TABLE `orders`
DROP COLUMN `shipping_first_name`,
DROP COLUMN `shipping_last_name`,
DROP COLUMN `shipping_house_number`,
DROP COLUMN `shipping_house_entry_code`,
DROP COLUMN `shipping_street`,
DROP COLUMN `shipping_city`,
DROP COLUMN `shipping_country`,
DROP COLUMN `shipping_zip`,
ADD COLUMN `floor_no` varchar(10) DEFAULT NULL AFTER `dhl_package_number`,
ADD COLUMN `apartment` varchar(10) DEFAULT NULL AFTER `dhl_package_number`,
ADD COLUMN `entry_code` varchar(10) DEFAULT NULL AFTER `dhl_package_number`,
ADD COLUMN `city` varchar(64) DEFAULT NULL AFTER `dhl_package_number`,
ADD COLUMN `street` varchar(64) DEFAULT NULL AFTER `dhl_package_number`,
ADD COLUMN `street_no` varchar(10) DEFAULT NULL AFTER `dhl_package_number`,
ADD COLUMN `last_name` varchar(64) DEFAULT NULL AFTER `dhl_package_number`,
ADD COLUMN `first_name` varchar(64) DEFAULT NULL AFTER `dhl_package_number`;


UPDATE orders o INNER JOIN wp_shookbook.wp_postmeta pm ON o.wp_id = pm.post_id
SET o.first_name = MID(pm.meta_value, 1, 64)
WHERE pm.meta_key = '_shipping_first_name';

UPDATE orders o INNER JOIN wp_shookbook.wp_postmeta pm ON o.wp_id = pm.post_id
SET o.last_name = MID(pm.meta_value, 1, 64)
WHERE pm.meta_key = '_shipping_last_name';

UPDATE orders o INNER JOIN wp_shookbook.wp_postmeta pm ON o.wp_id = pm.post_id
SET o.street_no = MID(MID(meta_value, CHAR_LENGTH(meta_value) - LOCATE(' ', REVERSE(meta_value)) + 1), 1, 10)
WHERE pm.meta_key = '_shipping_address_1';

UPDATE orders o INNER JOIN wp_shookbook.wp_postmeta pm ON o.wp_id = pm.post_id
SET o.street_no = MID(pm.meta_value, 1, 10)
WHERE pm.meta_key = '_shipping_street_no';

UPDATE orders o INNER JOIN wp_shookbook.wp_postmeta pm ON o.wp_id = pm.post_id
SET o.street = MID(MID(meta_value, 1, CHAR_LENGTH(meta_value) - LOCATE(' ', REVERSE(meta_value)) + 1), 1, 64)
WHERE pm.meta_key = '_shipping_address_1';

UPDATE orders o INNER JOIN wp_shookbook.wp_postmeta pm ON o.wp_id = pm.post_id
SET o.street = MID(pm.meta_value, 1, 64)
WHERE pm.meta_key = '_shipping_street';

UPDATE orders o INNER JOIN wp_shookbook.wp_postmeta pm ON o.wp_id = pm.post_id
SET o.city = MID(pm.meta_value, 1, 64)
WHERE pm.meta_key = '_shipping_city';

UPDATE orders o INNER JOIN wp_shookbook.wp_postmeta pm ON o.wp_id = pm.post_id
SET o.entry_code = MID(pm.meta_value, 1, 10)
WHERE pm.meta_key = '_shipping_door_code';

UPDATE orders o INNER JOIN wp_shookbook.wp_postmeta pm ON o.wp_id = pm.post_id
SET o.entry_code = MID(pm.meta_value, 1, 10)
WHERE pm.meta_key = '_shipping_entry_code';

UPDATE orders o INNER JOIN wp_shookbook.wp_postmeta pm ON o.wp_id = pm.post_id
SET o.apartment = MID(pm.meta_value, 1, 10)
WHERE pm.meta_key = '_shipping_apartment';

UPDATE orders o INNER JOIN wp_shookbook.wp_postmeta pm ON o.wp_id = pm.post_id
SET o.floor_no = MID(pm.meta_value, 1, 10)
WHERE pm.meta_key = '_shipping_floor_no';
