USE shookbook;

START TRANSACTION;

DROP TEMPORARY TABLE IF EXISTS tmp;
CREATE TEMPORARY TABLE tmp (
	`id` int(10) unsigned NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# IMPORT CUSTOMERS

DELETE FROM tmp;

INSERT INTO tmp
	SELECT u.ID
	FROM wp_shookbook.wp_users u INNER JOIN wp_shookbook.wp_usermeta um ON u.id = um.user_id AND um.meta_key = 'wp_capabilities'
	WHERE um.meta_value LIKE '%"customer"%' AND u.id NOT IN (SELECT wp_id FROM customers);

INSERT INTO customers (wp_id, email, discount, credit_points)
	SELECT wu.ID, wu.user_email, 1, 0
	FROM wp_shookbook.wp_users wu INNER JOIN tmp ON wu.ID = tmp.id;

DELETE FROM tmp;

INSERT INTO tmp
	SELECT wp_id
	FROM customers
	WHERE wp_id > 0;


UPDATE customers c INNER JOIN wp_shookbook.wp_usermeta um ON c.wp_id = um.user_id INNER JOIN tmp t ON um.user_id = t.id
SET c.first_name = MID(um.meta_value, 1, 64)
WHERE um.meta_key = 'billing_first_name';

UPDATE customers c INNER JOIN wp_shookbook.wp_usermeta um ON c.wp_id = um.user_id INNER JOIN tmp t ON um.user_id = t.id
SET c.last_name = MID(um.meta_value, 1, 64)
WHERE um.meta_key = 'billing_last_name';

UPDATE customers c INNER JOIN wp_shookbook.wp_usermeta um ON c.wp_id = um.user_id INNER JOIN tmp t ON um.user_id = t.id
SET c.email = MID(um.meta_value, 1, 128)
WHERE um.meta_key = 'billing_email';

UPDATE customers c INNER JOIN wp_shookbook.wp_usermeta um ON c.wp_id = um.user_id INNER JOIN tmp t ON um.user_id = t.id
SET phone = um.meta_value,
    phone_no = right(um.meta_value, 7),
    phone_ext = case when left(um.meta_value, 2) = '05' then left(um.meta_value,3) else left(um.meta_value,2) end
WHERE um.meta_key = 'billing_phone';

UPDATE customers c INNER JOIN wp_shookbook.wp_usermeta um ON c.wp_id = um.user_id INNER JOIN tmp t ON um.user_id = t.id
SET phone2 = um.meta_value,
    phone2_no = right(um.meta_value, 7),
    phone2_ext = case when left(um.meta_value, 2) = '05' then left(um.meta_value,3) else left(um.meta_value,2) end
WHERE um.meta_key = 'billing_phone2';

UPDATE customers c INNER JOIN wp_shookbook.wp_usermeta um ON c.wp_id = um.user_id INNER JOIN tmp t ON um.user_id = t.id
SET c.street_no = MID(um.meta_value, 1, 10)
WHERE um.meta_key = 'billing_street_no';

UPDATE customers c INNER JOIN wp_shookbook.wp_usermeta um ON c.wp_id = um.user_id INNER JOIN tmp t ON um.user_id = t.id
SET c.street = MID(um.meta_value, 1, 64)
WHERE um.meta_key = 'billing_street';

UPDATE customers c INNER JOIN wp_shookbook.wp_usermeta um ON c.wp_id = um.user_id INNER JOIN tmp t ON um.user_id = t.id
SET c.city = MID(um.meta_value, 1, 64)
WHERE um.meta_key = 'billing_city';

UPDATE customers c INNER JOIN wp_shookbook.wp_usermeta um ON c.wp_id = um.user_id INNER JOIN tmp t ON um.user_id = t.id
SET c.entry_code = MID(um.meta_value, 1, 10)
WHERE um.meta_key = 'billing_entry_code';

UPDATE customers c INNER JOIN wp_shookbook.wp_usermeta um ON c.wp_id = um.user_id INNER JOIN tmp t ON um.user_id = t.id
SET c.apartment = MID(um.meta_value, 1, 10)
WHERE um.meta_key = 'billing_apartment';

UPDATE customers c INNER JOIN wp_shookbook.wp_usermeta um ON c.wp_id = um.user_id INNER JOIN tmp t ON um.user_id = t.id
SET c.floor_no = MID(um.meta_value, 1, 10)
WHERE um.meta_key = 'billing_floor_no';


# IMPORT ORDERS

DELETE FROM tmp;

INSERT INTO tmp
	SELECT ID
	FROM wp_shookbook.wp_posts
	WHERE post_type = 'shop_order' AND (ID NOT IN (SELECT wp_id FROM orders) AND post_status = 'wc-completed');

INSERT INTO orders (wp_id, barcode, customer_id, pointer, shipping_date, sum, status, vat)
	SELECT p.ID, p.ID, 0, 0, NOW(), 0, '1', 0
	FROM wp_shookbook.wp_posts p INNER JOIN tmp t ON p.ID = t.id;

UPDATE orders o INNER JOIN wp_shookbook.wp_postmeta p ON o.wp_id = p.`post_id` INNER JOIN tmp t ON p.post_id = t.id INNER JOIN customers c ON c.wp_id = p.meta_value
SET customer_id = c.id
WHERE p.meta_key = '_customer_user';

UPDATE orders o INNER JOIN wp_shookbook.wp_postmeta p ON o.wp_id = p.`post_id` INNER JOIN tmp t ON p.post_id = t.id
SET sum = p.meta_value
WHERE p.meta_key = '_order_total';

UPDATE orders o INNER JOIN wp_shookbook.wp_postmeta p ON o.wp_id = p.`post_id` INNER JOIN tmp t ON p.post_id = t.id
SET discount_sum = p.meta_value
WHERE p.meta_key = '_cart_discount';

UPDATE orders o INNER JOIN wp_shookbook.wp_postmeta p ON o.wp_id = p.`post_id` INNER JOIN tmp t ON p.post_id = t.id
SET clearing_token = p.meta_value
WHERE p.meta_key = 'billing_Token';

UPDATE orders o INNER JOIN wp_shookbook.wp_postmeta p ON o.wp_id = p.`post_id` INNER JOIN tmp t ON p.post_id = t.id
SET clearing_customer_id = p.meta_value
WHERE p.meta_key = 'billing_CardOwnerID';

UPDATE orders o INNER JOIN wp_shookbook.wp_postmeta p ON o.wp_id = p.`post_id` INNER JOIN tmp t ON p.post_id = t.id
SET clearing_month = p.meta_value
WHERE p.meta_key = 'billing_CardValidityMonth';

UPDATE orders o INNER JOIN wp_shookbook.wp_postmeta p ON o.wp_id = p.`post_id` INNER JOIN tmp t ON p.post_id = t.id
SET clearing_year = p.meta_value
WHERE p.meta_key = 'billing_CardValidityYear';

UPDATE orders o INNER JOIN wp_shookbook.wp_postmeta p ON o.wp_id = p.`post_id` INNER JOIN tmp t ON p.post_id = t.id
SET clearing_four_digits = p.meta_value
WHERE p.meta_key = 'cc_number';

UPDATE orders o INNER JOIN wp_shookbook.wp_postmeta p ON o.wp_id = p.`post_id` INNER JOIN tmp t ON p.post_id = t.id
SET clearing_card_holder = p.meta_value
WHERE p.meta_key = 'cc_holdername';

UPDATE orders o INNER JOIN wp_shookbook.wp_postmeta p ON o.wp_id = p.`post_id` INNER JOIN tmp t ON p.post_id = t.id
SET clearing_card_type = p.meta_value
WHERE p.meta_key = 'billing_CardTypeCode60';

UPDATE orders o INNER JOIN wp_shookbook.wp_postmeta p ON o.wp_id = p.`post_id` INNER JOIN tmp t ON p.post_id = t.id
SET clearing_card_mutag = p.meta_value
WHERE p.meta_key = 'billing_Mutag24';

UPDATE orders o INNER JOIN wp_shookbook.wp_postmeta pm ON o.wp_id = pm.post_id INNER JOIN tmp t ON pm.post_id = t.id
SET o.first_name = MID(pm.meta_value, 1, 64)
WHERE pm.meta_key = '_shipping_first_name';

UPDATE orders o INNER JOIN wp_shookbook.wp_postmeta pm ON o.wp_id = pm.post_id INNER JOIN tmp t ON pm.post_id = t.id
SET o.last_name = MID(pm.meta_value, 1, 64)
WHERE pm.meta_key = '_shipping_last_name';

UPDATE orders o INNER JOIN wp_shookbook.wp_postmeta pm ON o.wp_id = pm.post_id INNER JOIN tmp t ON pm.post_id = t.id
SET o.street_no = MID(pm.meta_value, 1, 10)
WHERE pm.meta_key = '_shipping_street_no';

UPDATE orders o INNER JOIN wp_shookbook.wp_postmeta pm ON o.wp_id = pm.post_id INNER JOIN tmp t ON pm.post_id = t.id
SET o.street = MID(pm.meta_value, 1, 64)
WHERE pm.meta_key = '_shipping_street';

UPDATE orders o INNER JOIN wp_shookbook.wp_postmeta pm ON o.wp_id = pm.post_id INNER JOIN tmp t ON pm.post_id = t.id
SET o.city = MID(pm.meta_value, 1, 64)
WHERE pm.meta_key = '_shipping_city';

UPDATE orders o INNER JOIN wp_shookbook.wp_postmeta pm ON o.wp_id = pm.post_id INNER JOIN tmp t ON pm.post_id = t.id
SET o.entry_code = MID(pm.meta_value, 1, 10)
WHERE pm.meta_key = '_shipping_entry_code';

UPDATE orders o INNER JOIN wp_shookbook.wp_postmeta pm ON o.wp_id = pm.post_id INNER JOIN tmp t ON pm.post_id = t.id
SET o.apartment = MID(pm.meta_value, 1, 64)
WHERE pm.meta_key = '_shipping_apartment';

UPDATE orders o INNER JOIN wp_shookbook.wp_postmeta pm ON o.wp_id = pm.post_id INNER JOIN tmp t ON pm.post_id = t.id
SET o.floor_no = MID(pm.meta_value, 1, 64)
WHERE pm.meta_key = '_shipping_floor_no';

UPDATE orders o INNER JOIN wp_shookbook.wp_postmeta pm ON o.wp_id = pm.post_id INNER JOIN tmp t ON pm.post_id = t.id
SET o.delivery_date = STR_TO_DATE(meta_value, '%Y-%m-%d')
WHERE pm.meta_key = 'shipping_delivery_date';

UPDATE orders o INNER JOIN
	(SELECT *
	FROM
		(SELECT *
		FROM orders
		WHERE clearing_token IS NOT NULL
		ORDER BY id DESC) t
	GROUP BY customer_id) t ON o.customer_id = t.customer_id
SET o.clearing_token = t.clearing_token, o.clearing_customer_id = t.clearing_customer_id, o.clearing_month = t.clearing_month, o.clearing_year = t.clearing_year, o.clearing_four_digits = t.clearing_four_digits, o.clearing_card_holder = t.clearing_card_holder
WHERE o.clearing_token IS NULL;

INSERT INTO order_product (order_id, product_id, quantity_needed, quantity, unit_price)
	SELECT o.id, p.id, oim2.meta_value, 0, oim3.meta_value / oim2.meta_value
	FROM wp_shookbook.wp_woocommerce_order_items oi INNER JOIN tmp t ON oi.order_id = t.id
		INNER JOIN wp_shookbook.wp_woocommerce_order_itemmeta oim1 ON oi.order_item_id = oim1.order_item_id AND oim1.meta_key = '_product_id'
		INNER JOIN wp_shookbook.wp_woocommerce_order_itemmeta oim2 ON oi.order_item_id = oim2.order_item_id AND oim2.meta_key = '_qty'
		INNER JOIN wp_shookbook.wp_woocommerce_order_itemmeta oim3 ON oi.order_item_id = oim3.order_item_id AND oim3.meta_key = '_line_subtotal'
		INNER JOIN orders o ON o.wp_id = t.id
		INNER JOIN products p ON p.wp_id = oim1.meta_value;

DROP TEMPORARY TABLE IF EXISTS tmp;

# EXPORT ORDER status

INSERT INTO wp_shookbook.wp_postmeta
(post_id, meta_key, meta_value)
SELECT wp_id, '_sb_status', status
FROM orders
WHERE wp_id NOT IN (SELECT DISTINCT post_id FROM wp_shookbook.wp_postmeta WHERE meta_key = '_sb_status');

UPDATE wp_shookbook.wp_postmeta wppm INNER JOIN orders o ON wppm.post_id = o.wp_id
SET wppm.meta_value = o.status
WHERE wppm.meta_key = '_sb_status';

# UPDATE USERS

UPDATE wp_shookbook.wp_postmeta wpp INNER JOIN products p ON wpp.post_id = p.wp_id
SET meta_value = p.supplier_names
WHERE meta_key = 'supplier_names';

INSERT INTO wp_shookbook.wp_usermeta (user_id, meta_key, meta_value)
SELECT wp_id, 'shookbook_status', ''
FROM customers WHERE wp_id NOT IN (SELECT user_id FROM wp_shookbook.wp_usermeta WHERE meta_key = 'shookbook_status') AND wp_id IN (SELECT ID FROM wp_shookbook.wp_users) AND wp_id > 0;

# UPDATE POINTS AND STATUS

UPDATE wp_shookbook.wp_usermeta wpu INNER JOIN customers c ON wpu.user_id = c.wp_id
SET meta_value = CASE   WHEN c.type = '0' THEN 'כסף'
								 WHEN c.type = '1' THEN 'זהב'
								 WHEN c.type = '2' THEN 'פלטינה'
								 END
WHERE meta_key = 'shookbook_status';

INSERT INTO wp_shookbook.wp_usermeta (user_id, meta_key, meta_value)
SELECT wp_id, 'shookbook_points', ''
FROM customers WHERE wp_id NOT IN (SELECT user_id FROM wp_shookbook.wp_usermeta WHERE meta_key = 'shookbook_points') AND wp_id IN (SELECT ID FROM wp_shookbook.wp_users) AND wp_id > 0;

UPDATE wp_shookbook.wp_usermeta wpu INNER JOIN customers c ON wpu.user_id = c.wp_id
SET meta_value = c.credit_points
WHERE meta_key = 'shookbook_points';

UPDATE orders o INNER JOIN wp_shookbook.wp_postmeta wppm ON o.wp_id = wppm.post_id
SET o.delivery_date = wppm.meta_value
WHERE o.delivery_date IS NULL AND o.status = 1 AND wppm.meta_key = 'shipping_delivery_date';

COMMIT;
