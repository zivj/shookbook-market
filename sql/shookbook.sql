# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.6.21)
# Database: shookbook
# Generation Time: 2016-04-20 12:23:59 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `wp_id` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;

INSERT INTO `categories` (`id`, `wp_id`, `name`)
VALUES
	(1,7,'ירקות'),
	(2,8,'פירות'),
	(3,9,'עלים ותבלינים'),
	(4,10,'פטריות ונבטים'),
	(5,26,'פירות יבשים'),
	(6,11,'מיוחדים');

/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table customers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `customers`;

CREATE TABLE `customers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `wp_id` int(10) unsigned NOT NULL DEFAULT '0',
  `barcode` varchar(20) DEFAULT NULL,
  `first_name` varchar(32) DEFAULT NULL,
  `last_name` varchar(32) DEFAULT NULL,
  `day_of_birth` date DEFAULT NULL,
  `company_name` varchar(50) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `phone2` varchar(32) DEFAULT NULL,
  `house_no` varchar(8) DEFAULT NULL,
  `street` varchar(32) DEFAULT NULL,
  `city` varchar(32) DEFAULT NULL,
  `country` varchar(32) DEFAULT NULL,
  `zip` varchar(16) DEFAULT NULL,
  `credit_card_token` varchar(100) DEFAULT NULL,
  `discount` float unsigned NOT NULL,
  `credit_points` smallint(5) unsigned NOT NULL,
  `type` char(1) NOT NULL DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_deleted` char(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table globals
# ------------------------------------------------------------

DROP TABLE IF EXISTS `globals`;

CREATE TABLE `globals` (
  `key_name` varchar(32) NOT NULL DEFAULT '',
  `value_string` varchar(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`key_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `globals` WRITE;
/*!40000 ALTER TABLE `globals` DISABLE KEYS */;

INSERT INTO `globals` (`key_name`, `value_string`)
VALUES
	('unit_depreciation_percent','1'),
	('vat_rate','1.17'),
	('weight_depreciation_percent','1');

/*!40000 ALTER TABLE `globals` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `items`;

CREATE TABLE `items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `supplier_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `barcode` varchar(20) NOT NULL DEFAULT '',
  `inventory` smallint(4) unsigned NOT NULL,
  `gross_profit` float unsigned NOT NULL,
  `buying_cost` float unsigned NOT NULL,
  `unit_weight` float unsigned NOT NULL,
  `is_deleted` char(1) CHARACTER SET ascii NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `barcode` (`barcode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;

INSERT INTO `items` (`id`, `supplier_id`, `product_id`, `barcode`, `inventory`, `gross_profit`, `buying_cost`, `unit_weight`, `is_deleted`)
VALUES
	(1,5,1,'02-0005-06-1',0,0,10,1,'0'),
	(2,22,2,'02-0022-04-6',0,0,10,1,'0'),
	(3,24,3,'02-0024-07-3',0,0,10,1,'0'),
	(4,8,4,'02-0008-02-8',0,0,10,1,'0'),
	(5,9,5,'01-0009-01-9',0,0,10,1,'0'),
	(6,15,6,'02-0015-02-4',0,0,10,1,'0'),
	(7,7,7,'01-0007-08-4',0,0,10,1,'0'),
	(8,22,8,'02-0022-06-4',0,0,10,1,'0'),
	(9,7,9,'01-0007-06-0',0,0,10,1,'0'),
	(10,7,10,'01-0007-07-1',0,0,10,1,'0'),
	(11,17,11,'01-0017-07-0',0,0,10,1,'0'),
	(12,7,12,'03-0007-09-3',0,0,10,1,'0'),
	(13,22,13,'02-0022-01-3',0,0,10,1,'0'),
	(14,17,14,'03-0017-02-7',0,0,10,1,'0'),
	(15,17,15,'03-0017-05-0',0,0,10,1,'0'),
	(16,17,16,'03-0017-04-1',0,0,10,1,'0'),
	(17,17,17,'03-0017-01-4',0,0,10,1,'0'),
	(18,12,18,'01-0012-01-3',0,0,10,1,'0'),
	(19,6,19,'01-0006-01-6',0,0,10,1,'0'),
	(20,6,20,'01-0006-02-5',0,0,10,1,'0'),
	(21,3,21,'01-0003-02-0',0,0,10,1,'0'),
	(22,3,22,'01-0003-01-3',0,0,10,1,'0'),
	(23,22,23,'03-0022-11-3',0,0,10,1,'0'),
	(24,16,25,'02-0016-01-4',0,0,10,1,'0'),
	(25,19,26,'05-0019-16-0',0,0,10,1,'0'),
	(26,2,27,'03-0002-05-4',0,0,10,1,'0'),
	(27,19,28,'05-0019-11-3',0,0,10,1,'0'),
	(28,20,29,'04-0020-01-7',0,0,10,1,'0'),
	(29,2,30,'03-0002-08-9',0,0,10,1,'0'),
	(30,28,31,'01-0028-02-9',0,0,10,1,'0'),
	(31,19,32,'05-0019-12-4',0,0,10,1,'0'),
	(32,10,33,'03-0010-16-5',0,0,10,1,'0'),
	(33,7,34,'01-0007-05-3',0,0,10,1,'0'),
	(34,19,35,'05-0019-17-1',0,0,10,1,'0'),
	(35,5,36,'01-0005-04-0',0,0,10,1,'0'),
	(36,2,37,'03-0002-06-7',0,0,10,1,'0'),
	(37,5,38,'01-0005-03-7',0,0,10,1,'0'),
	(38,19,39,'05-0019-09-4',0,0,10,1,'0'),
	(39,19,40,'05-0019-20-5',0,0,10,1,'0'),
	(40,2,41,'03-0002-09-8',0,0,10,1,'0'),
	(41,2,42,'03-0002-14-4',0,0,10,1,'0'),
	(42,2,43,'03-0002-16-6',0,0,10,1,'0'),
	(43,10,44,'04-0010-09-2',0,0,10,1,'0'),
	(44,19,45,'05-0019-19-5',0,0,10,1,'0'),
	(45,19,46,'05-0019-14-8',0,0,10,1,'0'),
	(46,5,47,'01-0005-05-1',0,0,10,1,'0'),
	(47,2,48,'03-0002-04-5',0,0,10,1,'0'),
	(48,10,49,'04-0010-13-7',0,0,10,1,'0'),
	(49,10,50,'04-0010-11-5',0,0,10,1,'0'),
	(50,10,51,'04-0010-15-1',0,0,10,1,'0'),
	(51,10,52,'04-0010-12-6',0,0,10,1,'0'),
	(52,10,53,'04-0010-14-0',0,0,10,1,'0'),
	(53,10,54,'04-0010-10-4',0,0,10,1,'0'),
	(54,2,55,'03-0002-12-2',0,0,10,1,'0'),
	(55,5,56,'01-0005-01-5',0,0,10,1,'0'),
	(56,2,57,'03-0002-10-0',0,0,10,1,'0'),
	(57,10,58,'04-0010-05-0',0,0,10,1,'0'),
	(58,10,59,'04-0010-04-1',0,0,10,1,'0'),
	(59,10,60,'04-0010-03-6',0,0,10,1,'0'),
	(60,2,62,'03-0002-13-3',0,0,10,1,'0'),
	(61,10,63,'04-0010-06-3',0,0,10,1,'0'),
	(62,19,64,'05-0019-04-9',0,0,10,1,'0'),
	(63,19,65,'05-0019-13-5',0,0,10,1,'0'),
	(64,5,66,'01-0005-02-6',0,0,10,1,'0'),
	(65,1,67,'01-0001-01-1',0,0,10,1,'0'),
	(66,1,68,'01-0001-02-2',0,0,10,1,'0'),
	(67,19,69,'05-0019-05-8',0,0,10,1,'0'),
	(68,19,70,'05-0019-06-1',0,0,10,1,'0'),
	(69,2,71,'03-0002-07-6',0,0,10,1,'0'),
	(70,2,72,'03-0002-03-2',0,0,10,1,'0'),
	(71,2,73,'03-0002-23-0',0,0,10,1,'0'),
	(72,19,74,'05-0019-15-9',0,0,10,1,'0'),
	(73,2,75,'03-0002-02-3',0,0,10,1,'0'),
	(74,2,76,'03-0002-01-0',0,0,10,1,'0'),
	(75,19,77,'05-0019-07-0',0,0,10,1,'0'),
	(76,20,78,'04-0020-02-4',0,0,10,1,'0'),
	(77,10,79,'04-0010-08-3',0,0,10,1,'0'),
	(78,10,80,'04-0010-07-2',0,0,10,1,'0'),
	(79,2,81,'03-0002-11-1',0,0,10,1,'0'),
	(80,10,82,'04-0010-02-7',0,0,10,1,'0'),
	(81,10,83,'04-0010-01-4',0,0,10,1,'0'),
	(82,22,85,'02-0022-08-0',0,0,10,1,'0'),
	(83,19,86,'05-0019-03-4',0,0,10,1,'0'),
	(84,19,87,'05-0019-18-4',0,0,10,1,'0'),
	(85,19,88,'05-0019-10-2',0,0,10,1,'0'),
	(86,23,89,'02-0023-01-2',0,0,10,1,'0'),
	(87,13,90,'02-0013-01-1',0,0,10,1,'0'),
	(88,19,91,'05-0019-02-5',0,0,10,1,'0'),
	(89,19,92,'05-0019-01-2',0,0,10,1,'0'),
	(90,7,93,'01-0007-02-4',0,0,10,1,'0'),
	(91,7,94,'01-0007-04-2',0,0,10,1,'0'),
	(92,7,95,'01-0007-03-5',0,0,10,1,'0'),
	(93,7,96,'01-0007-01-7',0,0,10,1,'0'),
	(94,19,97,'05-0019-08-5',0,0,10,1,'0'),
	(95,2,98,'03-0002-15-5',0,0,10,1,'0'),
	(96,2,99,'03-0002-21-2',0,0,10,1,'0'),
	(97,17,100,'03-0017-06-3',0,0,10,1,'0'),
	(98,8,101,'01-0008-01-8',0,0,10,1,'0'),
	(99,8,102,'02-0008-03-9',0,0,10,1,'0'),
	(100,28,103,'01-0028-01-0',0,0,10,1,'0'),
	(101,1,104,'01-0001-03-3',0,0,10,1,'0'),
	(102,28,105,'03-0028-03-0',0,0,10,1,'0'),
	(103,24,106,'02-0024-03-7',0,0,10,1,'0'),
	(104,22,107,'02-0022-03-1',0,0,10,1,'0'),
	(105,4,108,'01-0004-01-4',0,0,10,1,'0'),
	(106,4,109,'01-0004-04-1',0,0,10,1,'0'),
	(107,4,110,'01-0004-03-6',0,0,10,1,'0'),
	(108,4,111,'01-0004-02-7',0,0,10,1,'0'),
	(109,11,112,'02-0011-01-3',0,0,10,1,'0'),
	(110,2,113,'03-0002-22-1',0,0,10,1,'0'),
	(111,17,114,'03-0017-03-6',0,0,10,1,'0'),
	(112,18,115,'01-0018-01-9',0,0,10,1,'0'),
	(113,22,116,'01-0022-16-6',0,0,10,1,'0'),
	(114,3,117,'01-0003-03-1',0,0,10,1,'0'),
	(115,22,118,'02-0022-15-6',0,0,10,1,'0'),
	(116,21,119,'01-0021-02-0',0,0,10,1,'0'),
	(117,21,120,'01-0021-01-3',0,0,10,1,'0'),
	(118,15,121,'02-0015-01-7',0,0,10,1,'0'),
	(119,15,122,'02-0015-05-3',0,0,10,1,'0'),
	(120,3,123,'03-0003-04-4',0,0,10,1,'0'),
	(121,24,124,'02-0024-06-2',0,0,10,1,'0'),
	(122,24,125,'02-0024-05-1',0,0,10,1,'0'),
	(123,23,126,'12-0023-02-0',0,0,10,1,'0'),
	(124,2,127,'03-0002-20-3',0,0,10,1,'0'),
	(125,2,128,'03-0002-18-8',0,0,10,1,'0'),
	(126,2,129,'03-0002-19-9',0,0,10,1,'0'),
	(127,2,130,'03-0002-17-7',0,0,10,1,'0'),
	(128,15,131,'02-0015-03-5',0,0,10,1,'0'),
	(129,22,132,'02-0022-02-0',0,0,10,1,'0'),
	(130,24,133,'02-0024-04-0',0,0,10,1,'0'),
	(131,19,134,'05-0019-21-6',0,0,10,1,'0'),
	(132,36,135,'06-2001-01-4',0,0,10,1,'0'),
	(133,36,136,'06-2001-02-7',0,0,10,1,'0'),
	(134,31,137,'06-0031-01-5',0,0,10,1,'0'),
	(135,31,138,'06-0031-02-6',0,0,10,1,'0'),
	(136,36,139,'03-2001-03-3',0,0,10,1,'0');

/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table order_product
# ------------------------------------------------------------

DROP TABLE IF EXISTS `order_product`;

CREATE TABLE `order_product` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `item_id` int(10) unsigned DEFAULT NULL,
  `quantity_needed` float NOT NULL,
  `quantity` float NOT NULL,
  `unit_price` float NOT NULL,
  `status` char(1) CHARACTER SET ascii NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table orders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `orders`;

CREATE TABLE `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `wp_id` int(10) unsigned NOT NULL DEFAULT '0',
  `type` char(1) CHARACTER SET ascii NOT NULL DEFAULT '0',
  `barcode` varchar(1000) NOT NULL DEFAULT '',
  `customer_id` int(10) NOT NULL,
  `pointer` tinyint(4) NOT NULL,
  `shipping_date` datetime NOT NULL,
  `estimated_packing_date` datetime DEFAULT NULL,
  `start_packing_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_packing_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sum` float NOT NULL,
  `vat` float NOT NULL,
  `discount_code` varchar(50) DEFAULT NULL,
  `discount_sum` float DEFAULT NULL,
  `total_cost` float DEFAULT NULL,
  `status` char(1) CHARACTER SET ascii NOT NULL DEFAULT '0',
  `boxes` tinyint(4) NOT NULL DEFAULT '1',
  `refrigerated` char(1) CHARACTER SET ascii NOT NULL DEFAULT '0',
  `packing_worker_id` int(10) unsigned DEFAULT '0',
  `dispatcher_worker_id` int(10) unsigned DEFAULT '0',
  `tracking_no` varchar(50) DEFAULT '',
  `clearing_token` varchar(40) DEFAULT NULL,
  `clearing_customer_id` char(10) DEFAULT NULL,
  `clearing_month` char(2) DEFAULT NULL,
  `clearing_year` char(4) DEFAULT NULL,
  `clearing_status` char(1) CHARACTER SET ascii NOT NULL DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_deleted` char(1) CHARACTER SET ascii NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  FULLTEXT KEY `barcode` (`barcode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table products
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `wp_id` int(10) unsigned NOT NULL DEFAULT '0',
  `category_id` int(10) unsigned NOT NULL DEFAULT '1',
  `name` varchar(64) NOT NULL DEFAULT '',
  `name_heb` varchar(64) NOT NULL,
  `image_url` varchar(256) NOT NULL DEFAULT '',
  `unit_type` float NOT NULL,
  `unit_weight` float NOT NULL,
  `price` float NOT NULL,
  `packing_weight_code` tinyint(4) NOT NULL,
  `refrigerated` char(1) CHARACTER SET ascii NOT NULL DEFAULT '0',
  `key_words` varchar(256) NOT NULL,
  `supplier_names` varchar(128) NOT NULL,
  `status` char(1) CHARACTER SET ascii NOT NULL DEFAULT '0',
  `vat` char(1) CHARACTER SET ascii NOT NULL DEFAULT '0',
  `total_ordered` int(10) unsigned NOT NULL,
  `package_type` char(1) CHARACTER SET ascii NOT NULL DEFAULT '0',
  `units_sold` int(10) unsigned NOT NULL,
  `is_deleted` char(1) CHARACTER SET ascii NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;

INSERT INTO `products` (`id`, `wp_id`, `category_id`, `name`, `name_heb`, `image_url`, `unit_type`, `unit_weight`, `price`, `packing_weight_code`, `refrigerated`, `key_words`, `supplier_names`, `status`, `vat`, `total_ordered`, `package_type`, `units_sold`, `is_deleted`)
VALUES
	(1,0,2,'אבוקדו זן ריד, יחידה כ-400 גרם','אבוקדו זן ריד, יחידה כ-400 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,8.9,2,'0','','','0','0',0,'0',0,'0'),
	(2,0,2,'אגס, יחידה כ-175 גרם','אגס, יחידה כ-175 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,2.5,4,'0','','','0','0',0,'0',0,'0'),
	(3,0,2,'אפרסמון, יחידה כ-200 גרם','אפרסמון, יחידה כ-200 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1,6,'0','','','0','0',0,'0',0,'0'),
	(4,0,2,'אפרסק לבן, יחידה כ-130 גרם','אפרסק לבן, יחידה כ-130 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1,4,'0','','','0','0',0,'0',0,'0'),
	(5,0,1,'ארטישוק, יחידה כ-300 גרם','ארטישוק, יחידה כ-300 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1.9,3,'0','','','0','0',0,'0',0,'0'),
	(6,0,2,'אשכולית אדומה 1 ק״ג','אשכולית אדומה 1 ק״ג','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,2.7,2,'0','','','0','0',0,'0',0,'0'),
	(7,0,1,'בטטה, יחידה כ-350 גרם','בטטה, יחידה כ-350 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1,1,'0','','','0','0',0,'0',0,'0'),
	(8,0,2,'בננה 1 ק״ג','בננה 1 ק״ג','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,2.5,4,'0','','','0','0',0,'0',0,'0'),
	(9,0,1,'בצל יבש 1 ק״ג','בצל יבש 1 ק״ג','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,2.2,1,'0','','','0','0',0,'0',0,'0'),
	(10,0,1,'בצל סגול 1 ק״ג','בצל סגול 1 ק״ג','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,2.2,1,'0','','','0','0',0,'0',0,'0'),
	(11,0,1,'ברוקולי, יחידה כ-500 גרם','ברוקולי, יחידה כ-500 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,2.9,3,'0','','','0','0',0,'0',0,'0'),
	(12,0,3,'גבעולי סלרי, יחידה כ-500 גרם','גבעולי סלרי, יחידה כ-500 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,0.9,1,'0','','','0','0',0,'0',0,'0'),
	(13,0,2,'גרנד סמית, יחידה כ-200 גרם','גרנד סמית, יחידה כ-200 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,2.5,2,'0','','','0','0',0,'0',0,'0'),
	(14,0,3,'חסה אייסברג, יחידה','חסה אייסברג, יחידה','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,2,5,'0','','','0','0',0,'0',0,'0'),
	(15,0,3,'חסה מסולסלת סגולה, יחידה','חסה מסולסלת סגולה, יחידה','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,2,5,'0','','','0','0',0,'0',0,'0'),
	(16,0,3,'חסה מסולסלת, יחידה','חסה מסולסלת, יחידה','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,2,5,'0','','','0','0',0,'0',0,'0'),
	(17,0,3,'חסה ערבית, יחידה','חסה ערבית, יחידה','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,2,5,'0','','','0','0',0,'0',0,'0'),
	(18,0,1,'חציל, יחידה כ-400 גרם','חציל, יחידה כ-400 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1.2,3,'0','','','0','0',0,'0',0,'0'),
	(19,0,1,'כרוב לבן, יחידה 1.3-1.5 ק״ג','כרוב לבן, יחידה 1.3-1.5 ק״ג','http://www.lynguru.com/shookbook/no_image_available.jpg',1,5,2.4,1,'0','','','0','0',0,'2',0,'0'),
	(20,0,1,'כרוב סגול, יחידה 1.3-1.5 ק״ג','כרוב סגול, יחידה 1.3-1.5 ק״ג','http://www.lynguru.com/shookbook/no_image_available.jpg',1,3,2.4,1,'0','','','0','0',0,'2',0,'0'),
	(21,0,1,'כרובית אלמוג , 600-800 גרם','כרובית אלמוג , 600-800 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,2.8,3,'0','','','0','0',0,'0',0,'0'),
	(22,0,1,'כרובית, יחידה 0.9-1.2 ק״ג','כרובית, יחידה 0.9-1.2 ק״ג','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,2.8,1,'0','','','0','0',0,'0',0,'0'),
	(23,0,3,'כרשה, יחידה כ-500 גרם','כרשה, יחידה כ-500 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1,6,'0','','','0','0',0,'0',0,'0'),
	(25,0,2,'לימון, יחידה כ-150 גרם','לימון, יחידה כ-150 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,0.7,2,'0','','','0','0',0,'0',0,'0'),
	(26,0,5,'מארז אגוזי מלך כ-250 גרם','מארז אגוזי מלך כ-250 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1,5,'0','','','0','0',0,'0',0,'0'),
	(27,0,3,'מארז אורגנו 30 גרם','מארז אורגנו 30 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1.7,6,'0','','','0','0',0,'0',0,'0'),
	(28,0,5,'מארז אננס טיבעי כ-200 גרם','מארז אננס טיבעי כ-200 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1,5,'0','','','0','0',0,'0',0,'0'),
	(29,0,4,'מארז ארנג׳י כ-200 גרם','מארז ארנג׳י כ-200 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1.9,6,'0','','','0','0',0,'0',0,'0'),
	(30,0,3,'מארז בזיליקום 50 גרם','מארז בזיליקום 50 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1.7,6,'0','','','0','0',0,'0',0,'0'),
	(31,0,1,'מארז בייבי ארטישוק כ-500 גרם','מארז בייבי ארטישוק כ-500 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,10,6,'0','','','0','0',0,'0',0,'0'),
	(32,0,5,'מארז ג׳וגי ברי כ-150 גרם','מארז ג׳וגי ברי כ-150 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1,5,'0','','','0','0',0,'0',0,'0'),
	(33,0,3,'מארז ג׳ינג׳ר כ-200 גרם','מארז ג׳ינג׳ר כ-200 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1.9,3,'0','','','0','0',0,'0',0,'0'),
	(34,0,1,'מארז גזר 1.5 ק״ג','מארז גזר 1.5 ק״ג','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,2.2,1,'0','','','0','0',0,'0',0,'0'),
	(35,0,5,'מארז גינגר דל סוכר כ-300 גרם','מארז גינגר דל סוכר כ-300 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1,5,'0','','','0','0',0,'0',0,'0'),
	(36,0,1,'מארז הבנרו כ-150 גרם','מארז הבנרו כ-150 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,2.3,6,'0','','','0','0',0,'0',0,'0'),
	(37,0,3,'מארז זעתר 30 גרם','מארז זעתר 30 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1.7,6,'0','','','0','0',0,'0',0,'0'),
	(38,0,1,'מארז חלפניו כ-200 גרם','מארז חלפניו כ-200 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,2.3,6,'0','','','0','0',0,'0',0,'0'),
	(39,0,5,'מארז חמוציות כ-300 גרם','מארז חמוציות כ-300 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1,5,'0','','','0','0',0,'0',0,'0'),
	(40,0,5,'מארז חמוציות ללא סוכר כ-300 גרם','מארז חמוציות ללא סוכר כ-300 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1,5,'0','','','0','0',0,'0',0,'0'),
	(41,0,3,'מארז טרגון 30 גרם','מארז טרגון 30 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1.7,6,'0','','','0','0',0,'0',0,'0'),
	(42,0,3,'מארז כוסברה 100 גרם','מארז כוסברה 100 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1.7,6,'0','','','0','0',0,'0',0,'0'),
	(43,0,3,'מארז לובץ 100 גרם','מארז לובץ 100 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1.7,6,'0','','','0','0',0,'0',0,'0'),
	(44,0,4,'מארז מיקס פטריות כ-500 גרם','מארז מיקס פטריות כ-500 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1.9,6,'0','','','0','0',0,'0',0,'0'),
	(45,0,5,'מארז מישמש אוזבקי כ-300 גרם','מארז מישמש אוזבקי כ-300 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1,5,'0','','','0','0',0,'0',0,'0'),
	(46,0,5,'מארז מישמש כ-300 גרם','מארז מישמש כ-300 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1,5,'0','','','0','0',0,'0',0,'0'),
	(47,0,1,'מארז מלפפון בייבי כ-400 גרם','מארז מלפפון בייבי כ-400 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,2.3,3,'0','','','0','0',0,'0',0,'0'),
	(48,0,3,'מארז מרווה 30 גרם','מארז מרווה 30 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1.7,6,'0','','','0','0',0,'0',0,'0'),
	(49,0,4,'מארז נבטוטים','מארז נבטוטים','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1.9,6,'0','','','0','0',0,'0',0,'0'),
	(50,0,4,'מארז נבטי אלפלפא','מארז נבטי אלפלפא','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1.9,6,'0','','','0','0',0,'0',0,'0'),
	(51,0,4,'מארז נבטי ברוקולי','מארז נבטי ברוקולי','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1.9,6,'0','','','0','0',0,'0',0,'0'),
	(52,0,4,'מארז נבטי חמניה','מארז נבטי חמניה','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1.9,6,'0','','','0','0',0,'0',0,'0'),
	(53,0,4,'מארז נבטי צנונית','מארז נבטי צנונית','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1.9,6,'0','','','0','0',0,'0',0,'0'),
	(54,0,4,'מארז נבטים סינים','מארז נבטים סינים','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1.9,6,'0','','','0','0',0,'0',0,'0'),
	(55,0,3,'מארז נענע 100 גרם','מארז נענע 100 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1.7,6,'0','','','0','0',0,'0',0,'0'),
	(56,0,1,'מארז פלפל סוויט בייט כ400- גרם','מארז פלפל סוויט בייט כ400- גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,2.2,5,'0','','','0','0',0,'0',0,'0'),
	(57,0,3,'מארז עירית 30 גרם','מארז עירית 30 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1.7,6,'0','','','0','0',0,'0',0,'0'),
	(58,0,4,'מארז פורטבלו ג׳מבו','מארז פורטבלו ג׳מבו','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1.9,6,'0','','','0','0',0,'0',0,'0'),
	(59,0,4,'מארז פורטבלו כ-400 גרם','מארז פורטבלו כ-400 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1.9,6,'0','','','0','0',0,'0',0,'0'),
	(60,0,4,'מארז פורטבלו מיני כ-250','מארז פורטבלו מיני כ-250','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1.9,6,'0','','','0','0',0,'0',0,'0'),
	(62,0,3,'מארז פטרוזיליה 100 גרם','מארז פטרוזיליה 100 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1.7,6,'0','','','0','0',0,'0',0,'0'),
	(63,0,4,'מארז פטריות יער','מארז פטריות יער','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1.9,6,'0','','','0','0',0,'0',0,'0'),
	(64,0,5,'מארז פקאן טבעי כ-250 גרם','מארז פקאן טבעי כ-250 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1,5,'0','','','0','0',0,'0',0,'0'),
	(65,0,5,'מארז צ׳יה כ-250 גרם','מארז צ׳יה כ-250 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1,5,'0','','','0','0',0,'0',0,'0'),
	(66,0,1,'מארז צ׳ילי אדום כ-80 גרם','מארז צ׳ילי אדום כ-80 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1.5,6,'0','','','0','0',0,'0',0,'0'),
	(67,0,1,'מארז צ׳רי אשכול כ-500 גרם','מארז צ׳רי אשכול כ-500 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,2.4,4,'0','','','0','0',0,'0',0,'0'),
	(68,0,1,'מארז צ׳רי נועה כ600- גרם','מארז צ׳רי נועה כ600- גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,2.4,4,'0','','','0','0',0,'0',0,'0'),
	(69,0,5,'מארז צימוק בהיר כ-300 גרם','מארז צימוק בהיר כ-300 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1,5,'0','','','0','0',0,'0',0,'0'),
	(70,0,5,'מארז צימוק כהה כ-300 גרם','מארז צימוק כהה כ-300 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1,5,'0','','','0','0',0,'0',0,'0'),
	(71,0,3,'מארז קורנית 30 גרם','מארז קורנית 30 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1.7,6,'0','','','0','0',0,'0',0,'0'),
	(72,0,3,'מארז קורנית לימון 30 גרם','מארז קורנית לימון 30 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1.7,6,'0','','','0','0',0,'0',0,'0'),
	(73,0,3,'מארז קייל 200 גרם','מארז קייל 200 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1.7,6,'0','','','0','0',0,'0',0,'0'),
	(74,0,5,'מארז קשיו טבעי כ-300 גרם','מארז קשיו טבעי כ-300 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1,5,'0','','','0','0',0,'0',0,'0'),
	(75,0,3,'מארז רוזמרין 30 גרם','מארז רוזמרין 30 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1.7,6,'0','','','0','0',0,'0',0,'0'),
	(76,0,3,'מארז רוקט 150 גרם','מארז רוקט 150 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1.7,6,'0','','','0','0',0,'0',0,'0'),
	(77,0,5,'מארז שזיף מגולען כ-400 גרם','מארז שזיף מגולען כ-400 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1,5,'0','','','0','0',0,'0',0,'0'),
	(78,0,4,'מארז שיטאקי כ-200 גרם','מארז שיטאקי כ-200 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1.9,6,'0','','','0','0',0,'0',0,'0'),
	(79,0,4,'מארז שימאגי חומות כ-250 גרם','מארז שימאגי חומות כ-250 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1.9,6,'0','','','0','0',0,'0',0,'0'),
	(80,0,4,'מארז שימאגי כ-250 גרם','מארז שימאגי כ-250 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1.9,6,'0','','','0','0',0,'0',0,'0'),
	(81,0,3,'מארז שמיר 100 גרם','מארז שמיר 100 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1.7,6,'0','','','0','0',0,'0',0,'0'),
	(82,0,4,'מארז שמפיניון ג׳מבו כ-550 גרם','מארז שמפיניון ג׳מבו כ-550 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1.9,6,'0','','','0','0',0,'0',0,'0'),
	(83,0,4,'מארז שמפיניון כ-250 גרם','מארז שמפיניון כ-250 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1.9,6,'0','','','0','0',0,'0',0,'0'),
	(85,0,2,'מארז שסק, כ-300 גרם','מארז שסק, כ-300 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,3,4,'0','','','0','0',0,'0',0,'0'),
	(86,0,5,'מארז שקד טיבעי כ-300 גרם','מארז שקד טיבעי כ-300 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1,5,'0','','','0','0',0,'0',0,'0'),
	(87,0,5,'מארז תאנים אורגניות כ-300 גרם','מארז תאנים אורגניות כ-300 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1,5,'0','','','0','0',0,'0',0,'0'),
	(88,0,5,'מארז תאנים כ-350 גרם','מארז תאנים כ-350 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1,5,'0','','','0','0',0,'0',0,'0'),
	(89,0,2,'מארז תות שדה כ-450 גרם','מארז תות שדה כ-450 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,2.5,6,'0','','','0','0',0,'0',0,'0'),
	(90,0,2,'מארז תות שדה כ-500 גרם','מארז תות שדה כ-500 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,2.5,6,'0','','','0','0',0,'0',0,'0'),
	(91,0,5,'מארז תמר דקל נור כ-500 גרם','מארז תמר דקל נור כ-500 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1,5,'0','','','0','0',0,'0',0,'0'),
	(92,0,5,'מארז תמר מגול בינוני כ-500 גרם','מארז תמר מגול בינוני כ-500 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1,5,'0','','','0','0',0,'0',0,'0'),
	(93,0,1,'מארז תפו״א אדום 2 ק״ג','מארז תפו״א אדום 2 ק״ג','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,2.2,1,'0','','','0','0',0,'0',0,'0'),
	(94,0,1,'מארז תפו״א בייבי אדום 1 ק״ג','מארז תפו״א בייבי אדום 1 ק״ג','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,2.2,1,'0','','','0','0',0,'0',0,'0'),
	(95,0,1,'מארז תפו״א בייבי לבן 1 ק״ג','מארז תפו״א בייבי לבן 1 ק״ג','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,2.2,1,'0','','','0','0',0,'0',0,'0'),
	(96,0,1,'מארז תפו״א לבן 2 ק״ג','מארז תפו״א לבן 2 ק״ג','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,2.2,1,'0','','','0','0',0,'0',0,'0'),
	(97,0,5,'מארז תפוח עץ כ-200 גרם','מארז תפוח עץ כ-200 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1,5,'0','','','0','0',0,'0',0,'0'),
	(98,0,3,'מארז תרד ניוזילנדי 500 גרם','מארז תרד ניוזילנדי 500 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1.7,6,'0','','','0','0',0,'0',0,'0'),
	(99,0,3,'מיורם','מיורם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1.7,6,'0','','','0','0',0,'0',0,'0'),
	(100,0,3,'מיקס חסה מסולסלת','מיקס חסה מסולסלת','http://www.lynguru.com/shookbook/no_image_available.jpg',1,0.5,2,5,'0','','','0','0',0,'0',0,'0'),
	(101,0,1,'מלפפון 0.5 ק״ג','מלפפון 0.5 ק״ג','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1,2,'0','','','0','0',0,'1',0,'0'),
	(102,0,2,'נקטרינה','נקטרינה','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1,4,'0','','','0','0',0,'0',0,'0'),
	(103,0,1,'סלק, יחידה כ-250 גרם','סלק, יחידה כ-250 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,10,6,'0','','','0','0',0,'0',0,'0'),
	(104,0,1,'עגבניה 0.5 ק״ג','עגבניה 0.5 ק״ג','http://www.lynguru.com/shookbook/no_image_available.jpg',1,0.5,2,4,'0','','','0','0',0,'1',0,'0'),
	(105,0,3,'עלי מגגולד כ-500 גרם','עלי מגגולד כ-500 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,10,6,'0','','','0','0',0,'0',0,'0'),
	(106,0,2,'אפרסק צהוב, יחידה כ-130 גרם','אפרסק צהוב, יחידה כ-130 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1,4,'0','','','0','0',0,'0',0,'0'),
	(107,0,2,'פינק ליידי, יחידה כ-200 גרם','פינק ליידי, יחידה כ-200 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,2.5,2,'0','','','0','0',0,'0',0,'0'),
	(108,0,1,'פלפל אדום, יחידה כ-200 גרם','פלפל אדום, יחידה כ-200 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,0.6,3,'0','','','0','0',0,'0',0,'0'),
	(109,0,1,'פלפל ירוק, יחידה כ-200 גרם','פלפל ירוק, יחידה כ-200 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,0.6,3,'0','','','0','0',0,'0',0,'0'),
	(110,0,1,'פלפל כתום, יחידה כ-200 גרם','פלפל כתום, יחידה כ-200 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,0.6,3,'0','','','0','0',0,'0',0,'0'),
	(111,0,1,'פלפל צהוב, יחידה כ-200 גרם','פלפל צהוב, יחידה כ-200 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,0.6,3,'0','','','0','0',0,'0',0,'0'),
	(112,0,2,'פסיפלורה','פסיפלורה','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1,3,'0','','','0','0',0,'0',0,'0'),
	(113,0,3,'צמד בוק צ׳וי','צמד בוק צ׳וי','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1.7,6,'0','','','0','0',0,'0',0,'0'),
	(114,0,3,'צמד לבבות חסה','צמד לבבות חסה','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,2,5,'0','','','0','0',0,'0',0,'0'),
	(115,0,1,'צנונית 500 גרם','צנונית 500 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,2,5,'0','','','0','0',0,'0',0,'0'),
	(116,0,1,'צרור בצל ירוק','צרור בצל ירוק','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1,6,'0','','','0','0',0,'0',0,'0'),
	(117,0,1,'קולורבי, יחידה כ-350 גרם','קולורבי, יחידה כ-350 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1,1,'0','','','0','0',0,'0',0,'0'),
	(118,0,2,'קיווי, יחידה כ-100 גרם','קיווי, יחידה כ-100 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,0.4,6,'0','','','0','0',0,'0',0,'0'),
	(119,0,1,'קישוא','קישוא','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1,6,'0','','','0','0',0,'0',0,'0'),
	(120,0,1,'קישוא עגול, יחידה כ-100 גרם','קישוא עגול, יחידה כ-100 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1.5,6,'0','','','0','0',0,'0',0,'0'),
	(121,0,2,'קלמנטינה זן אור 1 ק״ג','קלמנטינה זן אור 1 ק״ג','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,2.7,3,'0','','','0','0',0,'0',0,'0'),
	(122,0,2,'קלמנטינה טופז למיץ 1 ק״ג','קלמנטינה טופז למיץ 1 ק״ג','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,3,3,'0','','','0','0',0,'0',0,'0'),
	(123,0,3,'שומר, יחידה כ-400 גרם','שומר, יחידה כ-400 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1,2,'0','','','0','0',0,'0',0,'0'),
	(124,0,2,'שזיף אדום','שזיף אדום','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1,6,'0','','','0','0',0,'0',0,'0'),
	(125,0,2,'שזיף ירוק','שזיף ירוק','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1,6,'0','','','0','0',0,'0',0,'0'),
	(126,0,12,'תות בתפזורת ארוז כ-80 גרם','תות בתפזורת ארוז כ-80 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1,6,'0','','','0','0',0,'0',0,'0'),
	(127,0,3,'תערובת לבשר','תערובת לבשר','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1.7,6,'0','','','0','0',0,'0',0,'0'),
	(128,0,3,'תערובת למרק','תערובת למרק','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1.7,6,'0','','','0','0',0,'0',0,'0'),
	(129,0,3,'תערובת לפסטה','תערובת לפסטה','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1.7,6,'0','','','0','0',0,'0',0,'0'),
	(130,0,3,'תערובת לתה','תערובת לתה','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1.7,6,'0','','','0','0',0,'0',0,'0'),
	(131,0,2,'תפוז 1 ק״ג','תפוז 1 ק״ג','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,2.7,2,'0','','','0','0',0,'0',0,'0'),
	(132,0,2,'תפוח עץ זהוב, כ-175 גרם','תפוח עץ זהוב, כ-175 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,2.5,2,'0','','','0','0',0,'0',0,'0'),
	(133,0,2,'תפוח עץ אנה','תפוח עץ אנה','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1,6,'0','','','0','0',0,'0',0,'0'),
	(134,0,5,'מארז תמר מגול מובחר כ-250 גרם','מארז תמר מגול מובחר כ-250 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,1,5,'0','','','0','0',0,'0',0,'0'),
	(135,0,6,'מארז מהודר לפסח','מארז מהודר לפסח','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,15,6,'0','','','0','0',0,'0',0,'0'),
	(136,0,6,'מארז מהודר לפסח בתוספת יין','מארז מהודר לפסח בתוספת יין','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,15,6,'0','','','0','0',0,'0',0,'0'),
	(137,0,6,'קברנה סוביניון 750 מ״ל','קברנה סוביניון 750 מ״ל','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,5,6,'0','','','0','0',0,'0',0,'0'),
	(138,0,6,'מרלו 750 מ״ל','מרלו 750 מ״ל','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,5,6,'0','','','0','0',0,'0',0,'0'),
	(139,0,3,'חזרת, יחידה כ-400 גרם','חזרת, יחידה כ-400 גרם','http://www.lynguru.com/shookbook/no_image_available.jpg',1,1,2,1,'0','','','0','0',0,'0',0,'0');

/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table supplier_order_item
# ------------------------------------------------------------

DROP TABLE IF EXISTS `supplier_order_item`;

CREATE TABLE `supplier_order_item` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `supplier_order_id` int(10) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `quantity` float NOT NULL,
  `unit_price` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table supplier_orders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `supplier_orders`;

CREATE TABLE `supplier_orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `supplier_id` int(10) unsigned NOT NULL,
  `barcode` varchar(20) NOT NULL,
  `shipping_date` timestamp NULL DEFAULT NULL,
  `acceptance_date` timestamp NULL DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sum` float NOT NULL,
  `vat` float NOT NULL,
  `status` char(1) NOT NULL DEFAULT '0',
  `is_auto_generated` char(1) NOT NULL DEFAULT '0',
  `is_deleted` char(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table suppliers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `suppliers`;

CREATE TABLE `suppliers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` varchar(32) DEFAULT NULL,
  `name` varchar(64) NOT NULL DEFAULT '',
  `phone` varchar(32) DEFAULT NULL,
  `phone2` varchar(32) DEFAULT NULL,
  `email` varchar(32) DEFAULT NULL,
  `house_no` varchar(8) DEFAULT NULL,
  `street` varchar(32) DEFAULT NULL,
  `city` varchar(32) DEFAULT NULL,
  `country` varchar(32) DEFAULT NULL,
  `zip` varchar(16) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` char(1) CHARACTER SET ascii NOT NULL DEFAULT '0',
  `positive_notes` text,
  `negative_notes` text,
  `bank_no` varchar(8) DEFAULT NULL,
  `bank_branch_no` varchar(8) DEFAULT NULL,
  `bank_account_no` varchar(16) DEFAULT NULL,
  `bank_account_owner` varchar(32) DEFAULT NULL,
  `is_deleted` char(1) CHARACTER SET ascii NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `suppliers` WRITE;
/*!40000 ALTER TABLE `suppliers` DISABLE KEYS */;

INSERT INTO `suppliers` (`id`, `company_id`, `name`, `phone`, `phone2`, `email`, `house_no`, `street`, `city`, `country`, `zip`, `created_date`, `status`, `positive_notes`, `negative_notes`, `bank_no`, `bank_branch_no`, `bank_account_no`, `bank_account_owner`, `is_deleted`)
VALUES
	(1,'558128781','פ.פ. חקלאות','052-8887600','','gilp@shook-book.com',NULL,'קדש ברנע 34','ד.נ. חלוצה','Israel','8551300','2016-04-20 13:57:55','0',NULL,NULL,'12','749','668814','פ.פ. חקלאות','0'),
	(2,'514449446','עימ חקלאות בע״מ','054-3094411','054-3094412','shaviti@orange.net.il',NULL,'משק 115','מושב כמהין ד.נ. חלוצה','Israel','8551100','2016-04-20 13:57:55','0',NULL,NULL,'14','363','618000','עימ חקלאות בע״מ','0'),
	(3,'514945591','משק מוניץ בע״מ','052-7202350','','munitz1@gmail.com',NULL,'משק 54','מושב ניצני סיני','Israel','85513','2016-04-20 13:57:55','0',NULL,NULL,'12','631','543533','משק מוניץ בעמ','0'),
	(4,'27371392','יובל כוכב','052-4260236','','cochavfarm@gmail.com',NULL,'יובל כוכב','מושב כמהין','Israel','85511','2016-04-20 13:57:55','0',NULL,NULL,'11','517','41339','יובל כוכב','0'),
	(5,'52892585','צ׳ילי זהבי','054-7775197','','chillizahavi@gmail.com',NULL,'משעול הסירה 1','זיקים','Israel','','2016-04-20 13:57:55','0',NULL,NULL,'12','645','299946','צ׳ילי זהבי','0'),
	(6,'51406999','חסן טוני','053-2441009','054-6673713','eitan.hasan38@gmail.com',NULL,'משעול כוכב הים 3','ניצן ב','Israel','','2016-04-20 13:57:55','0',NULL,NULL,'20','491','175720','חסן טוני','0'),
	(7,'512436684','גלי משה בוקר שיווק ואריזה בע״מ','052-6067301','','liat@gmv.co.il',NULL,'הפלמ״ח 10','אבן יהודה','Israel','40500','2016-04-20 13:57:55','0',NULL,NULL,'12','167','33956','גלי משה בוקר שיווק ואריזה בע״מ','0'),
	(8,'1269547','רן שורר','054-4442233','','shorer@zahav.net.il',NULL,'','תלמי יחיאל','Israel','7981000','2016-04-20 13:57:55','0',NULL,NULL,'','','','','0'),
	(9,'','צחי כצנלסון','','','',NULL,'','באר טוביה','Israel','','2016-04-20 13:57:55','0',NULL,NULL,'','','','','0'),
	(10,'512830266','מרינה פטריות הגליל בע״מ','04-6666451','050-4058052','ididia24@gmail.com',NULL,'ד.נ. גליל מערבי','ינוב','Israel','','2016-04-20 13:57:55','0',NULL,NULL,'','','','','0'),
	(11,'','אורי כהן','','','',NULL,'','עין הבסור','Israel','','2016-04-20 13:57:55','0',NULL,NULL,'','','','','0'),
	(12,'515178689','שיווק משק מילוא בע״מ','050-3263046','050-3077214','farmyariv@gmail.com',NULL,'משק מילוא','מושב כמהין ד.נ. חלוצה','Israel','85511','2016-04-20 13:57:55','0',NULL,NULL,'10','921','5180093','שיווק משק מילוא בע״מ','0'),
	(13,'34405183','איתן רונן','08-6524737','','eitan883@gmail.com',NULL,'משק 41','ניצני סיני ד.נ. חלוצה','Israel','8551300','2016-04-20 13:57:55','0',NULL,NULL,'12','564','451789','איתן רונן','0'),
	(14,'22587869','יאיר טייץ','052-6199844','','yair_teitz@walla.co.il',NULL,'משק 53','ניצני סיני ד.נ. חלוצה','Israel','85513','2016-04-20 13:57:55','0',NULL,NULL,'12','680','178422','יאיר טייץ','0'),
	(15,'56787922','רונן חרודי','054-4345160','','ronenh1@013net.net',NULL,'','נס ציונה','Israel','70422','2016-04-20 13:57:55','0',NULL,NULL,'10','933','8840045','רונן חרודי','0'),
	(16,'24273492','מיכה פרטוש','052-4771055','','wollf38@gmail.com',NULL,'משק 65','משוב בסלול','Israel','','2016-04-20 13:57:55','0',NULL,NULL,'12','633','406937','מיכה פרטוש','0'),
	(17,'514429166','ש.צ. זנזורי ובניו בעמ','050-9080905','','tsippi5@walla.com',NULL,'משק 83','שדה עוזיה','Israel','79260','2016-04-20 13:57:55','0',NULL,NULL,'','','','ש.צ. זנזורי ובניו בעמ','0'),
	(18,'','רוביסקו','','','',NULL,'','','Israel','','2016-04-20 13:57:55','0',NULL,NULL,'','','','','0'),
	(19,'514786938','א.ש. בוסתן בע״מ','03-6037818','052-3234040','bustan4us@gmail.com',NULL,'המלאכה 20','ראש העין','Israel','4809154','2016-04-20 13:57:55','0',NULL,NULL,'11','125','67117','א.ש. בוסתן בע״מ','0'),
	(20,'62542790','משק טופז קפלן','050-7519692','','hilayagil@gmail.com',NULL,'משפחת קפלן','קדש ברנע','Israel','85513','2016-04-20 13:57:55','0',NULL,NULL,'12','668','139767','טופז קפלן','0'),
	(21,'34793521','דרור בן גל','054-4340150','','drorbengal@gmail.com',NULL,'משק 124','כמהין ד.נ. חלוצה','Israel','81551','2016-04-20 13:57:55','0',NULL,NULL,'14','363','409055630','דרור בן גל','0'),
	(22,'39688205','איתמר חותה','052-6788455','','itamarhuta@gmail.com',NULL,'מזי 76','תל אביב','Israel','','2016-04-20 13:57:55','0',NULL,NULL,'10','817','40252159','איתמר חותה','0'),
	(23,'557147055','אהוד נחשון משק 6','504679823','','meira01@inter.net.il',NULL,'רחוב הוותיקים 6','מושב גאולים','Israel','42820','2016-04-20 13:57:55','0',NULL,NULL,'12','652','43410','אהוד נחשון','0'),
	(24,'58488057','כהן עמוס ערוגות','052-3860776','052-4460900','',NULL,'משק 15','מושב ערוגות','Israel','79864','2016-04-20 13:57:55','0',NULL,NULL,'12','648','319512','כהן צביקה ועמוס','0'),
	(25,'','לירון מורבר','','','',NULL,'','','Israel','','2016-04-20 13:57:55','0',NULL,NULL,'','','','','0'),
	(26,'514934868','סנסיני בע\"מ','054-4745174','','amnongamliel18@gmail.com',NULL,'שיזף 18','מושב צופר','Israel','86830','2016-04-20 13:57:55','0',NULL,NULL,'12','722','570775','אמנון גמליאל (סנסיני)','0'),
	(27,'513384057','אדום יוקיי בע״מ','','','',NULL,'יגאל אלון 65','תל אביב','Israel','','2016-04-20 13:57:55','0',NULL,NULL,'12','159','121597224','אדום יוקיי בע״מ','0'),
	(28,'514854140','כהן חקלאות שדה בע״מ','054-5653364','054-5653373','anat_co6@walla.com',NULL,'קפלן 9','יהוד','Israel','56250','2016-04-20 13:57:55','0',NULL,NULL,'10','837','18610038','כהן חקלאות שדה בע״מ','0'),
	(29,'512120940','משקו שיווק (1995) בע״מ','054-4553070','03-9346883','mushco95@gmail.com',NULL,'','פ״ת','Israel','49002','2016-04-20 13:57:55','0',NULL,NULL,'12','527','441449','משקו שיווק (1995) בע״מ','0'),
	(30,'','אלדן תחבורה בע״מ','077-2700276','052-5695273','eranl@eldan.co.il',NULL,'','','Israel','','2016-04-20 13:57:55','0',NULL,NULL,'12','63','5565','אלדן תחבורה בע״מ','0'),
	(31,'','ייקבי נגב','','','',NULL,'','','Israel','','2016-04-20 13:57:55','0',NULL,NULL,'','','','','0'),
	(32,'','שלומי אלבז','','','',NULL,'','','Israel','','2016-04-20 13:57:55','0',NULL,NULL,'','','','','0'),
	(33,'511161325','בסט קרטון בעמ','054-4597928','','rami@bestcarton.co.il',NULL,'האשל 32','פרק התעשיה קיסרה','Israel','3088900','2016-04-20 13:57:55','0',NULL,NULL,'10','639','61600/66','בסט קרטון בעמ','0'),
	(34,'65546780','מיקי גנות','050-5291400','050-2666951','mikiganot18@gmail.com',NULL,'מושב אשלים','ד.נ. חלוצה','Israel','85512','2016-04-20 13:57:55','0',NULL,NULL,'20','464','551008','מיקי גנות','0'),
	(35,'513046615','אף. סי. (פליינג קרגו) תובלה בינלאומית בע״מ','03-9719080','03-9719342','Ortal.Mordechai@dhl.com',NULL,'מסוף המטענים סוויספורט','דרך חטיבה שמונה נתב״ג','Israel','7010000','2016-04-20 13:57:55','0',NULL,NULL,'','','','','0'),
	(36,'','ספק מזדמן','','','',NULL,'','','Israel','','2016-04-20 13:57:55','0',NULL,NULL,'','','','','0');

/*!40000 ALTER TABLE `suppliers` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table worker_hours
# ------------------------------------------------------------

DROP TABLE IF EXISTS `worker_hours`;

CREATE TABLE `worker_hours` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `worker_id` int(10) unsigned NOT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table workers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `workers`;

CREATE TABLE `workers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL DEFAULT '',
  `password` varchar(50) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `first_name` varchar(50) NOT NULL DEFAULT '',
  `last_name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `workers` WRITE;
/*!40000 ALTER TABLE `workers` DISABLE KEYS */;

INSERT INTO `workers` (`id`, `username`, `password`, `type`, `first_name`, `last_name`)
VALUES
	(1,'mor','*A4B6157319038724E3560894F7F932C8886EBFCF',0,'Mor','Mauda'),
	(2,'rani','*A4B6157319038724E3560894F7F932C8886EBFCF',0,'Rani','Rimon'),
	(3,'ori','*A4B6157319038724E3560894F7F932C8886EBFCF',0,'Ori','Rimon'),
	(4,'ziv','*A4B6157319038724E3560894F7F932C8886EBFCF',0,'Ziv','Jacoby');

/*!40000 ALTER TABLE `workers` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
