package com.shookbook.types;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.shookbook.utils.U;

import java.util.HashMap;
import java.util.Map;

public class Result
{
    private final String errorDescription;
    private final Map<String, Object> map;

    public Result(String error)
    {
        map = null;
        errorDescription = error;
    }

    public Result(Map<String, Object> map)
    {
        this.map = map;
        errorDescription = null;
    }

    public Result(String name, Object object)
    {
        this(name, object, "Undefined error");
    }

    public Result(String name, Object object, String errorDescription)
    {
        map = new HashMap<>();
        if (U.var.isEmptyStr(name) || object == null)
        {
            this.errorDescription = errorDescription;
        }
        else
        {
            this.errorDescription = null;
            map.put(name, object);
        }
    }

    public Result(Object... array)
    {
        map = new HashMap<>();
        int l = (array.length / 2) * 2;
        for (int i = 0 ; i < l ; i += 2)
        {
            if (U.var.isEmptyStr(array[i]))
            {
                this.errorDescription = l == array.length ? "Undefined error" : array[array.length - 1].toString();
                return;
            }
        }
        this.errorDescription = null;
        for (int i = 0 ; i < l ; i += 2)
        {
            map.put(array[i].toString(), array[i + 1]);
        }
    }

    public boolean isError()
    {
        return !U.var.isEmptyStr(errorDescription);
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getErrorDescription()
    {
        return errorDescription;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public Map getObjects()
    {
        return map;
    }
}
