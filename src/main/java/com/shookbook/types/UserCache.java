package com.shookbook.types;

import com.shookbook.dao.shookbook.WorkerHoursDao;
import com.shookbook.dao.shookbook.WorkersDao;
import com.shookbook.entities.shookbook.Worker;
import com.shookbook.entities.shookbook.WorkerHours;
import com.shookbook.utils.U;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class UserCache
{
    public final static int CACHE_TIMEOUT = 10 * 60 * 1000;
    public final static String SESSION_EXPIRED = "Session expired";

    @Autowired
    public UserCache(WorkersDao workersDao, WorkerHoursDao workerHoursDao)
    {
        UserCache.workersDao = workersDao;
        UserCache.workerHoursDao = workerHoursDao;
        worker = null;
    }

    private static WorkersDao workersDao;
    private static WorkerHoursDao workerHoursDao;

    private final static Map<String, UserCache> workersByToken = new HashMap<>();
    private final static Map<Integer, UserCache> workersByIds = new HashMap<>();

    private String token;
    private final Worker worker;
    private long lastAccess;

    private UserCache(String token, Worker worker)
    {
        this.token = token;
        this.worker = worker;
    }

    public static synchronized UserCache create(Worker worker)
    {
        UserCache userCache = workersByIds.get(worker.getId());
        if (userCache == null)
        {
            workerHoursDao.closeAllByWorker(worker.getId());
            WorkerHours workerHours = new WorkerHours();
            workerHours.setId(worker.getId());
            workerHours.setStartTime(new Date(System.currentTimeMillis()));
            workerHoursDao.save(workerHours);
            userCache = new UserCache(UUID.randomUUID().toString(), worker);
            workersByToken.put(userCache.token, userCache);
            workersByIds.put(userCache.worker.getId(), userCache);
        }
        else
        {
            workersByToken.remove(userCache.getToken());
            userCache.token = UUID.randomUUID().toString();
            workersByToken.put(userCache.token, userCache);
        }
        userCache.lastAccess = System.currentTimeMillis();
        return userCache;
    }

    public static class SessionNotFoundException extends Exception
    {
        public final long lastAccess;

        public SessionNotFoundException(String s, long lastAccess)
        {
            super(s);
            this.lastAccess = lastAccess;
        }
    }

    public static synchronized UserCache get(String token) throws SessionNotFoundException
    {
        UserCache userCache = workersByToken.get(token);
        if (userCache == null)
        {
            if ("TEST".equals(token))
            {
                userCache = create(workersDao.findOne(1));
            }
            else
            {
                throw new SessionNotFoundException(token + " not found", System.currentTimeMillis());
            }
        }
        userCache.lastAccess = System.currentTimeMillis();
        return userCache;
    }

    public void remove()
    {
        workersByToken.remove(token);
        workersByIds.remove(getWorker().getId());
        workerHoursDao.closeAllByWorker(getWorker().getId());
    }

    public String getToken()
    {
        return token;
    }

    public Worker getWorker()
    {
        return worker;
    }

    static
    {
        U.thread.runOnExecutor(new Runnable()
        {
            @Override
            public void run()
            {
                while (true)
                {
                    U.thread.sleep(10000);
                    for (Iterator<UserCache> i = workersByIds.values().iterator(); i.hasNext(); )
                    {
                        UserCache userCache = i.next();
                        if (userCache.lastAccess + CACHE_TIMEOUT < System.currentTimeMillis())
                        {
                            workersByToken.remove(userCache.worker.getId());
                            i.remove();
                            workerHoursDao.closeAllByWorker(userCache.worker.getId());
                        }
                    }
                }
            }
        });
    }
}
