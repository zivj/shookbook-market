package com.shookbook.types;

import java.util.HashSet;

public class HashSetIgnoresNulls<E> extends HashSet<E>
{
    @Override
    public boolean add(E e)
    {
        if (e != null)
        {
            return super.add(e);
        }
        return false;
    }
}
