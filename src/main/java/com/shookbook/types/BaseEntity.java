package com.shookbook.types;

import com.shookbook.utils.U;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@MappedSuperclass
@EqualsAndHashCode
public class BaseEntity<T extends BaseEntity> implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Getter
    @Setter
    private Integer id;

    public List<T> asList()
    {
        return (List<T>) U.var.asList(this);
    }

    protected void collectIds(Class<BaseEntity> clazz, Set<Integer> ids)
    {
    }

    protected void collectIds(String property, Set<Integer> ids)
    {
    }

    public static <E extends BaseEntity> Set<Integer> toIdSet(List<E> list)
    {
        Set<Integer> results = new HashSetIgnoresNulls<>();
        for (E entity : list)
        {
            results.add(entity.getId());
        }
        return results;
    }

    public static <E extends BaseEntity> Map<Integer, E> toMap(List<E> list)
    {
        Map<Integer, E> results = new HashMap<Integer, E>();
        for (E entity : list)
        {
            results.put(entity.getId(), entity);
        }
        return results;
    }

    public static <E extends BaseEntity> Set<Integer> collectIds(List<E> entities, Class clazz)
    {
        return collectIds(entities, clazz, new HashSetIgnoresNulls<>());
    }

    public static <E extends BaseEntity> Set<Integer> collectIds(List<E> entities, Class clazz, Set<Integer> ids)
    {
        for (BaseEntity entity : entities)
        {
            entity.collectIds(clazz, ids);
        }
        return ids;
    }

    public static <E extends BaseEntity> Set<Integer> collectIds(List<E> entities, String property)
    {
        return collectIds(entities, property, new HashSetIgnoresNulls<>());
    }

    public static <E extends BaseEntity> Set<Integer> collectIds(List<E> entities, String property, Set<Integer> ids)
    {
        for (BaseEntity entity : entities)
        {
            entity.collectIds(property, ids);
        }
        return ids;
    }
}
