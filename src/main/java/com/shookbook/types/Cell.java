package com.shookbook.types;

import com.shookbook.entities.shookbook.Customer;
import com.shookbook.entities.shookbook.Item;
import com.shookbook.entities.shookbook.Product;
import com.shookbook.entities.shookbook.Order;
import com.shookbook.entities.shookbook.OrderProduct;
import com.shookbook.entities.shookbook.Supplier;
import com.shookbook.entities.shookbook.Worker;
import com.shookbook.entities.shookbook.WorkerHours;

import java.util.Arrays;
import java.util.List;

public class Cell
{

    private final int id;
    private final String type;

    public Cell(int id, Class clazz)
    {
        this.id = id;
        this.type = clazz.getSimpleName().replaceAll("(.)(\\p{Lu})", "$1_$2").toUpperCase();
    }

    public int getId()
    {
        return id;
    }

    public String getType()
    {
        return type;
    }

    public static List<Cell> fromList(List list)
    {
        if (list == null)
        {
            return Arrays.asList(new Cell[0]);
        }
        Cell[] results = new Cell[list.size()];
        for (int i = 0 ; i < results.length ; i++)
        {
            results[i] = fromEntity(list.get(i));
        }
        return Arrays.asList(results);
    }

    public static List<Cell> listFromEntity(Object entity)
    {
        return Arrays.asList(new Cell[]{fromEntity(entity)});
    }

    private static Cell fromEntity(Object entity)
    {
        if (entity instanceof Customer)
        {
            return new Cell(((Customer)entity).getId(), Customer.class);
        }
        else if (entity instanceof Product)
        {
            return new Cell(((Product)entity).getId(), Product.class);
        }
        else if (entity instanceof Order)
        {
            return new Cell(((Order)entity).getId(), Order.class);
        }
        else if (entity instanceof OrderProduct)
        {
            return new Cell(((OrderProduct)entity).getId(), OrderProduct.class);
        }
        else if (entity instanceof Worker)
        {
            return new Cell(((Worker)entity).getId(), Worker.class);
        }
        else if (entity instanceof WorkerHours)
        {
            return new Cell(((WorkerHours)entity).getId(), WorkerHours.class);
        }
        else if (entity instanceof Supplier)
        {
            return new Cell(((Supplier)entity).getId(), Supplier.class);
        }
        else if (entity instanceof Item)
        {
            return new Cell(((Item)entity).getId(), Item.class);
        }
        return null;
    }
}
