package com.shookbook;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.boot.orm.jpa.hibernate.SpringImplicitNamingStrategy;
import org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.HashMap;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "wordpressEntityManagerFactory", basePackages = {"com.shookbook.dao.wordpress"}, transactionManagerRef = "wordpressTransactionManager")
public class WordpressConfig {
    @Bean
    @ConfigurationProperties(prefix = "wordpress.datasource")
    public DataSource wordpressDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean wordpressEntityManagerFactory(EntityManagerFactoryBuilder builder, @Qualifier("wordpressDataSource") DataSource dataSource) {
        return builder.dataSource(dataSource).packages("com.shookbook.entities.wordpress").
                properties(new HashMap<String, Object>() {
                    {
                        put("hibernate.physical_naming_strategy", SpringPhysicalNamingStrategy.class.getName());
                        put("hibernate.implicit_naming_strategy", SpringImplicitNamingStrategy.class.getName());
                    }
                }).persistenceUnit("wordpress").build();
    }

    @Bean
    public PlatformTransactionManager wordpressTransactionManager(@Qualifier("wordpressEntityManagerFactory") EntityManagerFactory entityManagerFactory) {
        return new JpaTransactionManager(entityManagerFactory);
    }
}