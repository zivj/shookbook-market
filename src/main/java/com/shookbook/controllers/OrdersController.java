package com.shookbook.controllers;

import com.shookbook.admin.CustomersAdmin;
import com.shookbook.dao.shookbook.*;
import com.shookbook.entities.shookbook.*;
import com.shookbook.types.Cell;
import com.shookbook.types.HashSetIgnoresNulls;
import com.shookbook.types.Result;
import com.shookbook.types.UserCache;
import com.shookbook.utils.U;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;

@RestController
public class OrdersController {
    private static final Logger logger = Logger.getLogger(CustomersAdmin.class);

    @Autowired
    private OrdersDao ordersDao;
    @Autowired
    private OrderProductDao orderProductDao;
    @Autowired
    private ItemsDao itemsDao;
    @Autowired
    private ProductsDao productsDao;
    @Autowired
    private CustomersDao customersDao;
    @Autowired
    private InventoryDao inventoryDao;
    @Autowired
    private InventoryItemDao inventoryItemDao;
    @Autowired
    private GlobalsDao globalsDao;

    private Object get(Order[] orders) {
        if (orders.length > 0 && orders[0] != null) {
            List<OrderProduct> orderProducts = orderProductDao.findAllByOrderIdWithoutMissing(orders[0].getId());
            Set<Integer> productIds = new HashSetIgnoresNulls<>();
            for (OrderProduct orderProduct : orderProducts) {
                productIds.add(orderProduct.getProductId());
            }
            orders[0].setOrderProducts(Cell.fromList(orderProducts));
            return new Result("cells", Cell.fromList(Arrays.asList(orders)), "orders", orders, "orderProducts", orderProducts, "items", itemsDao.findAllByProductIds(productIds), "products", productsDao.findAll(productIds), "customers", new Customer[]{customersDao.findOne(orders[0].getCustomerId())});
        } else {
            return new Result("cells", new Cell[]{});
        }
    }

    @RequestMapping("/orders/get")
    public Object get(@RequestParam(value = "sessionToken") String sessionToken,
                      @RequestParam(value = "barcode") String barcode) {
        try {
            UserCache userCache = UserCache.get(sessionToken);

            logger.info("SB: " + userCache.getWorker().getUsername() + ": Get order by application - " + barcode);
            //Order[] orders = new Order[]{ordersDao.findOneByBarcode(barcode + "%")};
            if (barcode.length() > 11) {
                barcode = barcode.substring(barcode.length() - 11);
                barcode = "" + U.var.parseInt(barcode);
                logger.info("SB: " + userCache.getWorker().getUsername() + ": Changed to - " + barcode);
            }
            Order[] orders = new Order[]{ordersDao.findOneByBarcode(barcode)};
            //if (orders.length > 0 && orders[0] != null && orders[0].getStartPackingTimestamp() == null) {
            if (orders.length > 0 && orders[0] != null) {
                if (orders[0].getStatus().ordinal() != Order.OrderStatuses.IN_ORDER.ordinal()) {
                    System.out.format("   ### Got Error in order: %s %d %s Orginal order status: %s%n", barcode, orders[0].getId(), userCache.getWorker().getSbId(), orders[0].getStatus());
                } else {
                    orders[0].setStartPackingTimestamp(new Date(System.currentTimeMillis()));
                    orders[0].setPackingWorkerId(userCache.getWorker().getId());
                    ordersDao.save(orders[0]);
                }
            }
            return get(orders);
        } catch (UserCache.SessionNotFoundException ex) {
            return new Result(UserCache.SESSION_EXPIRED);
        }
    }

    @RequestMapping("/orders/update")
    public Object update(@RequestParam(value = "sessionToken") String sessionToken,
                         @RequestParam(value = "id") Integer id,
                         @RequestParam(value = "pointer") Short pointer,
                         @RequestParam(value = "boxes") Short boxes,
                         @RequestParam(value = "status") String statusString) {
        try {
            UserCache userCache = UserCache.get(sessionToken);
            Order[] orders = new Order[]{ordersDao.findOne(id)};
            if (orders[0].getStatus() == Order.OrderStatuses.SHIPMENT) {
                System.out.format("   ### Error: Trying to set order: %d   Pointer: %d   Boxes: %d    Status: %s%n", id, pointer, boxes, statusString);
            }
            orders[0].setPointer(pointer);
            orders[0].setBoxes(boxes);
            Order.OrderStatuses status = Order.OrderStatuses.valueOf(statusString);
            if (status != orders[0].getStatus()) {
                orders[0].setStatus(status);
                if (status == Order.OrderStatuses.SHIPMENT) {
                    orders[0].setPackingWorkerId(userCache.getWorker().getId());
                    orders[0].setLastPackingTimestamp(new Date(System.currentTimeMillis()));
                }
            }

            ordersDao.save(orders[0]);
            return get(orders);
        } catch (UserCache.SessionNotFoundException ex) {
            return new Result(UserCache.SESSION_EXPIRED);
        }
    }

    @RequestMapping("/ordersProduct/update")
    public Object update(@RequestParam(value = "sessionToken") String sessionToken,
                         @RequestParam(value = "id") Integer id,
                         @RequestParam(value = "itemId") Integer itemId,
                         @RequestParam(value = "quantity") Float quantity,
                         @RequestParam(value = "status") String status) {
        try {
            UserCache userCache = UserCache.get(sessionToken);
            OrderProduct orderProduct = orderProductDao.findOne(id);
            logger.info("SB: " + userCache.getWorker().getUsername() + ": Update product by application - Product id: " + id + " Order: " + orderProduct.getOrderId() + " Quantity: " + quantity);
            if (orderProduct != null) {
                float itemQuantity = Math.min(quantity, orderProduct.getQuantityNeeded());
                orderProduct.setQuantity(itemQuantity);
                orderProduct.setStatus(OrderProduct.OrderProductStatuses.valueOf(status));
                orderProduct.setItemId(itemId);
                if (U.var.parseInt(globalsDao.findOne("modules_enable_inventory").getValue()) == 1) {
                    if (U.var.parseInt(status) != 2) {
                        Float resedu = itemQuantity;
                        InventoryItem inventoryItem = inventoryItemDao.findLastByItemID(itemId);
                        if (inventoryItem != null) {   // Check ordered item
                            Float stock = inventoryItem.getReceived() - inventoryItem.getDelivered();
                            if (stock >= itemQuantity) {
                                inventoryItem.substructFromReceived(itemQuantity);
                                inventoryItem.addToDelivered(itemQuantity);
                                resedu = U.var.parseFloat(0);
                            } else if (stock > 0) {
                                inventoryItem.addToDelivered(stock);
                                resedu -= stock;
                            }
                            inventoryItemDao.save(inventoryItem);
                        }

                        if (resedu > 0) {   // Check other items
                            List<Item> items = itemsDao.findAllByProductIdWithParam(orderProduct.getProductId());
                            for (Item item : items) {
                                inventoryItem = inventoryItemDao.findLastByItemID(item.getId());
                                if (inventoryItem != null) {
                                    Float stock = inventoryItem.getReceived() - inventoryItem.getDelivered();
                                    if (stock >= itemQuantity) {
                                        inventoryItem.substructFromReceived(itemQuantity);
                                        inventoryItem.addToDelivered(itemQuantity);
                                        resedu = U.var.parseFloat(0);
                                    } else if (stock > 0) {
                                        inventoryItem.addToDelivered(stock);
                                        resedu -= stock;
                                    }
                                    inventoryItemDao.save(inventoryItem);
                                }
                                if (resedu == 0) {
                                    break;
                                }
                            }
                        }
                    }
                }
                orderProductDao.save(orderProduct);
                return new Result("cells", Cell.listFromEntity(orderProduct), "orderProducts", new OrderProduct[]{orderProduct});
            } else {
                return new Result("cells", new Cell[]{});
            }
        } catch (UserCache.SessionNotFoundException ex) {
            return new Result(UserCache.SESSION_EXPIRED);
        }
    }
}
