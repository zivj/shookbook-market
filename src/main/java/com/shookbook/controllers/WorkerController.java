package com.shookbook.controllers;

import com.shookbook.dao.shookbook.GlobalsDao;
import com.shookbook.dao.shookbook.ProductsDao;
import com.shookbook.dao.shookbook.WorkerHoursDao;
import com.shookbook.dao.shookbook.WorkersDao;
import com.shookbook.entities.shookbook.Worker;
import com.shookbook.entities.shookbook.WorkerHours;
import com.shookbook.types.Cell;
import com.shookbook.types.Result;
import com.shookbook.types.UserCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
public class WorkerController {
    @Autowired
    private WorkersDao workersDao;

    @Autowired
    private WorkerHoursDao workerHoursDao;

    @Autowired
    private ProductsDao productsDao;

    @Autowired
    private GlobalsDao globalsDao;

    @RequestMapping("/workers/get")
    public Object get(@RequestParam(value = "sessionToken") String sessionToken,
                      @RequestParam(value = "id") int id) {
        try {
            UserCache userCache = UserCache.get(sessionToken);
            Worker worker = workersDao.findOne(id);
            List<WorkerHours> workerHoursList = workerHoursDao.findAllByWorker(id);
            worker.setWorkerHours(Cell.fromList(workerHoursList));
            List<Worker> workers = Arrays.asList(new Worker[]{worker});
            return new Result("cells", Cell.fromList(workers), "workers", workers, "workerHours", workerHoursList);
        } catch (UserCache.SessionNotFoundException ex) {
            return new Result(UserCache.SESSION_EXPIRED);
        }
    }

    @RequestMapping("/workers/login")
    public Object login(@RequestParam(value = "username", required = true) String username,
                        @RequestParam(value = "password", required = false) String password) {
        Worker worker = workersDao.login(username, password);
        if (worker != null) {
            UserCache userCache = UserCache.create(worker);
            worker.setSessionToken(userCache.getToken());
            List<WorkerHours> workerHoursList = workerHoursDao.findAllByWorker(worker.getId());
            worker.setWorkerHours(Cell.fromList(workerHoursList));
            List<Worker> workers = Arrays.asList(new Worker[]{worker});
            return new Result("cells", Cell.fromList(workers), "workers", workers, "workerHours", workerHoursList, "products", productsDao.findAll(), "globals", globalsDao.findAll());
        }
        return new Result("Fail to login");
    }

    @RequestMapping("/workers/logout")
    public Object logout(@RequestParam(value = "sessionToken") String sessionToken) {
        try {
            UserCache userCache = UserCache.get(sessionToken);
            List<Worker> workers = Arrays.asList(new Worker[]{userCache.getWorker()});
            userCache.remove();
            return new Result("cells", Cell.fromList(workers), "workers", workers);
        } catch (UserCache.SessionNotFoundException ex) {
            return new Result(UserCache.SESSION_EXPIRED);
        }
    }
}
