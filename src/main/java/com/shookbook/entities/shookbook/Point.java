package com.shookbook.entities.shookbook;

import com.shookbook.types.BaseEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Entity
@Table(name = "points")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString
public class Point extends BaseEntity<Point> {

    @Getter
    @Setter
    private int customerId;
    @Getter
    @Setter
    private short points;
    @Getter
    @Setter
    private Character type;
    @Getter
    @Setter
    private float money;
    @Temporal(TemporalType.TIMESTAMP)
    @Getter
    @Setter
    private Date timestamp;

    public Point(int customerId, short points, Character type, float money, Date timestamp)
    {
        this.customerId = customerId;
        this.points = points;
        this.type = type;
        this.money = money;
        this.timestamp = timestamp;
    }
}
