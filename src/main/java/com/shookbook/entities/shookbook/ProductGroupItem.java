package com.shookbook.entities.shookbook;

import com.shookbook.types.BaseEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

@Entity
@Table(name = "product_group_item")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString
public class ProductGroupItem extends BaseEntity<ProductGroupItem> {
    public enum ItemTypes {
        PRODUCT("מוצר"), CONTENT("תוכן"), PROMOTION("קידום"), BANNER("באנר");
        public final String heb;

        ItemTypes(String heb) {
            this.heb = heb;
        }
    }

    public enum ItemStatus {
        AVAILABLE("זמין"), OUT_OF_STOCK("אזל"), MUST("חייב");
        public final String heb;

        ItemStatus(String heb) {
            this.heb = heb;
        }
    }

    @Enumerated(EnumType.ORDINAL)
    @Getter
    @Setter
    private ItemTypes type;
    @Enumerated(EnumType.ORDINAL)
    @Getter
    @Setter
    private ItemStatus status;
    @Getter
    @Setter
    private int productId;
    @Getter
    @Setter
    private int productGroupId;
    @Getter
    @Setter
    private Character isDeleted;

}
