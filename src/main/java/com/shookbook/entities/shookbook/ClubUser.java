package com.shookbook.entities.shookbook;

import com.shookbook.types.BaseEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Entity
@Table(name = "club_user")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString
public class ClubUser extends BaseEntity<ClubUser> {
    private static final long serialVersionUID = 1L;
    @Getter
    @Setter
    private int userId;
    @Getter
    @Setter
    private int clubId;
    @Getter
    @Setter
    private int userRegistrationId;
    @Temporal(TemporalType.TIMESTAMP)
    @Getter
    @Setter
    private Date join_date;
}
