package com.shookbook.entities.shookbook;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.shookbook.types.BaseEntity;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "products")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString
public class Product extends BaseEntity<Product> {
    public enum ProductStatuses {
        OUT_OF_STOCK("אזל"), AVAILABLE("זמין"), SPECIAL("במבצע"), NEW("חדש"), RECOMMENDED("מומלץ");
        public final String heb;

        ProductStatuses(String heb) {
            this.heb = heb;
        }
    }

    public enum PackageTypes {
        UNIT("יחידה"), QUANTA("קוונטה"), MULTI("מרובה"), PACK("מארז");
        public final String heb;

        PackageTypes(String heb) {
            this.heb = heb;
        }
    }

    public enum PackingAction {
        NONE(""), QUANTA("קוונטה"), MULTI("מרובה");
        public final String heb;

        PackingAction(String heb) {
            this.heb = heb;
        }
    }

    public enum PackingRules {
        NONE(""), GREEN("אריזה ירוקה");
        public final String heb;

        PackingRules(String heb) {
            this.heb = heb;
        }
    }

    public enum Stamp {
        NONE("ללא"), RECOMMANDED("מומלץ"), ON_SALE("במבצע"), ORGANIC("אורגני"), HYDROPHONIC("הידרופוני"), MILD("חריף"), NO_YEAST("ללא שמרים"), BREAD("חמץ") ;
        public final String heb;

        Stamp(String heb) {
            this.heb = heb;
        }
    }

    public enum Handling {
        NONE("ללא"), COOLING("צינון"), REFRIGERATED("קירור"), FRIZING("הקפאה");
        public final String heb;

        Handling(String heb) {
            this.heb = heb;
        }
    }

    @Getter
    @Setter
    private int wpId;
    @Getter
    @Setter
    private int categoryId;
    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private String nameHeb;
    @Getter
    @Setter
    private String nameThai;
    @Getter
    @Setter
    private String nameWeight;
    @Getter
    @Setter
    private String imageUrl;
    @Getter
    @Setter
    private float unitType;
    @Getter
    @Setter
    private float unitWeight;
    @Getter
    @Setter
    private float price;
    @Getter
    @Setter
    private short packingWeightCode;
    @Getter
    @Setter
    @Enumerated(EnumType.ORDINAL)
    private Handling refrigerated;
    @Getter
    @Setter
    private String keyWords;
    @Getter
    @Setter
    private String supplierNames;
    @Getter
    @Setter
    @Enumerated(EnumType.ORDINAL)
    private ProductStatuses status;
    @Getter
    @Setter
    private Character vat;
    @Getter
    @Setter
    private int totalOrdered;
    @Getter
    @Setter
    @Enumerated(EnumType.ORDINAL)
    private PackageTypes packageType;
    @Getter
    @Setter
    @Enumerated(EnumType.ORDINAL)
    private PackingAction packingAction;
    @Getter
    @Setter
    @Enumerated(EnumType.ORDINAL)
    private PackingRules packingRule;
    @Getter
    @Setter
    @Enumerated(EnumType.ORDINAL)
    private Stamp stamp;
    @Getter
    @Setter
    private int unitsSold;
    @Getter
    @Setter
    private Character isMissing;
    @Getter
    @Setter
    private Character isDeleted;
    @Getter
    @Setter
    private Character moreInfo;
    @Getter
    @Setter
    private String info;

    @JsonIgnore
    @Transient
    private float tmpQuantity = 0;

    @JsonIgnore
    @Transient
    public float getTmpQuantity() {
        return tmpQuantity;
    }

    @JsonIgnore
    @Transient
    public void setTmpQuantity(float tmpQuantity) {
        this.tmpQuantity = tmpQuantity;
    }

    @JsonIgnore
    @Transient
    public void addTmpCount(float tmpQuantity) {
        this.tmpQuantity += tmpQuantity;
    }

    @JsonIgnore
    @Transient
    private int tmpItemId = 0;

    @JsonIgnore
    @Transient
    public int getTmpItemId() {
        return tmpItemId;
    }

    @JsonIgnore
    @Transient
    public void setTmpItemId(int tmpItemId) {
        this.tmpItemId = tmpItemId;
    }

    @JsonIgnore
    @Transient
    public boolean hasVat() {
        return this.vat == '1';
    }
}
