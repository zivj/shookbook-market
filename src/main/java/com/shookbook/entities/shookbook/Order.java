package com.shookbook.entities.shookbook;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.shookbook.types.BaseEntity;
import com.shookbook.types.Cell;
import com.shookbook.utils.U;
import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "orders")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString
public class Order extends BaseEntity<Order> {
    public enum OrderTypes
    {
        NORMAL(10), RECURRING_WEEKLY_BEGINNING(11), RECURRING_WEEKLY_ENDING(12), EXTRNAL_YAROK(99);
        public final int code;
        OrderTypes(int code)
        {
            this.code = code;
        }
        public int getOrderTypeCode() {return code;}
    }

    public enum OrderStatuses
    {
        CLOSED("סגורה"),
        OPEN("פתוחה"),
        IN_ORDER("בטיפול"),
        SHIPMENT("נשלחה"),
        CANCELED("בוטלה"),
        HARVEST("בקטיף"),
        JOINED("אוחדה"),
        DELIVERED("נמסרה");
        public final String heb;
        OrderStatuses(String heb)
        {
            this.heb = heb;
        }
    }

    public enum ClearStatus
    {
        UNCLEAR("לא נסלק"),
        SUCCEED("הצליח"),
        FAILED("נכשל"),
        MANUAL("נגבה ידנית"),
        CANCELED("בוטל"),
        INVOICE_ONLY("חשבונית בלבד"),
        PENDING("בבדיקה"),
        NO_CHARGE("לא לגביה");
        public final String heb;
        ClearStatus(String heb)
        {
            this.heb = heb;
        }
    }

    @Getter
    @Setter
    private int wpId;
    @Enumerated(EnumType.ORDINAL)
    @Getter
    @Setter
    private OrderTypes type;
    @Getter
    @Setter
    private String barcode;
    @Getter
    @Setter
    private int customerId;
    @Getter
    @Setter
    private short pointer;
    @Getter
    @Setter
    private String dhlPackageNumber;
    @Temporal(TemporalType.TIMESTAMP)
    @Getter
    @Setter
    private Date shippingDate;
    @Temporal(TemporalType.TIMESTAMP)
    @Getter
    @Setter
    private Date estimatedDeliveryTime;
    @Temporal(TemporalType.TIMESTAMP)
    @Getter
    @Setter
    private Date startPackingTimestamp;
    @Temporal(TemporalType.TIMESTAMP)
    @Getter
    @Setter
    private Date lastPackingTimestamp;
    @Getter
    @Setter
    private Date deliveryDate;
    @Getter
    @Setter
    private Short deliveryWindow;
    @Getter
    @Setter
    private float sum;
    @Getter
    @Setter
    private float sumCharged;
    @Getter
    @Setter
    private float vat;
    @Getter
    @Setter
    private String discountCode;
    @Getter
    @Setter
    private Float discountSum;
    @Getter
    @Setter
    private Float totalCost;
    @Enumerated(EnumType.ORDINAL)
    @Getter
    @Setter
    private OrderStatuses status;
    @Getter
    @Setter
    private short boxes;
    @Getter
    @Setter
    private Character refrigerated;
    @Getter
    @Setter
    private Integer packingWorkerId;
    @Getter
    @Setter
    private Integer dispatcherWorkerId;
    @Getter
    @Setter
    private String trackingNo;
    @Getter
    @Setter
    private String clearingToken;
    @Getter
    @Setter
    private Date clearingTokenValidThrough;
    @Getter
    @Setter
    private String clearingProfile;
    @Getter
    @Setter
    private String clearingCustomerId;
    @Getter
    @Setter
    private String clearingMonth;
    @Getter
    @Setter
    private String clearingYear;
    @Getter
    @Setter
    private String clearingFourDigits;
    @Getter
    @Setter
    private String clearingCardHolder;
    @Getter
    @Setter
    private Short clearingCardType;
    @Getter
    @Setter
    private Short clearingCardMutag;
    @Getter
    @Setter
    private Integer clearingDealNumber;
    @Getter
    @Setter
    private Integer clearingInvoiceNumber;
    @Enumerated(EnumType.ORDINAL)
    @Getter
    @Setter
    private ClearStatus clearingStatus;
    @Getter
    @Setter
    private Character certificated;
    @Getter
    @Setter
    private String remarks;
    @Temporal(TemporalType.TIMESTAMP)
    @Getter
    @Setter
    private Date createdDate;
    @Getter
    @Setter
    private Character isDeleted;
    @Getter
    @Setter
    private String floorNo;
    @Getter
    @Setter
    private String apartment;
    @Getter
    @Setter
    private String entryCode;
    @Getter
    @Setter
    private String city;
    @Getter
    @Setter
    private String street;
    @Getter
    @Setter
    private String streetNo;
    @Getter
    @Setter
    private String lastName;
    @Getter
    @Setter
    private String firstName;
    @Getter
    @Setter
    private Integer orderGenerationId;
    @Getter
    @Setter
    private Integer smsCode;

    @Transient
    private List<Cell> orderProducts = null;

    @Transient
    public List<Cell> getOrderProducts()
    {
        return orderProducts;
    }

    @Transient
    public void setOrderProducts(List<Cell> orderProducts)
    {
        this.orderProducts = orderProducts;
    }

    @Transient
    private Object extra;

    @Transient
    @JsonIgnore
    public Object getExtra()
    {
        return extra;
    }

    @Transient
    @JsonIgnore
    public void setExtra(Object extra)
    {
        this.extra = extra;
    }

    @Transient
    @JsonIgnore
    public String getCardTypeString()
    {
        switch (U.var.parseInt(clearingCardType))
        {
        case 1:
            return "ישראכרט";
        case 2:
            return "ויזה כאל";
        case 3:
            return "דיינרס";
        case 4:
            return "אמריקן אקספרס";
        case 6:
            return "לאומיקארד";
        default:
            return "לא ידוע";
        }
    }

    @Transient
    @JsonIgnore
    public String getCardMutagString()
    {
        if (U.var.parseInt(clearingCardType) == 0)
        {
            return "לא ידוע";
        }
        switch (U.var.parseInt(clearingCardMutag))
        {
        case 0:
            return getCardTypeString();
        case 1:
            return "מסטרקארד";
        case 2:
            return "ויזה";
        case 5:
            return "ישראכרט";
        default:
            return "לא ידוע";
        }
    }
}
