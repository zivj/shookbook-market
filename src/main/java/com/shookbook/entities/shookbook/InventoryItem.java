/**
 * Created by zivj on 3/11/17.
 */

package com.shookbook.entities.shookbook;

import com.shookbook.types.BaseEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Entity
@Table(name = "inventory_item")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString
public class InventoryItem extends BaseEntity<InventoryItem>
{
    @Getter
    @Setter
    private Integer inventoryId;
    @Getter
    @Setter
    private Integer itemId;
    @Getter
    @Setter
    private Integer supplierOrderItemId;
    @Getter
    @Setter
    private float ordered;
    @Getter
    @Setter
    private float received;
    @Getter
    @Setter
    private float delivered;
    @Temporal(TemporalType.TIMESTAMP)
    @Getter
    @Setter
    private Date lastReceived;
    @Temporal(TemporalType.TIMESTAMP)
    @Getter
    @Setter
    private Date createdDate;
    @Getter
    @Setter
    private Character isDeleted;

    public void addToOrdered (Float qty)
    {
        this.ordered += qty;
    }

    public void substructFromOrdered (Float qty)
    {
        this.ordered -= qty;
    }

    public void addToReceived (Float qty)
    {
        this.received += qty;
    }

    public void substructFromReceived (Float qty)
    {
        this.received -= qty;
    }

    public void addToDelivered (Float qty)
    {
        this.delivered += qty;
    }

    public void substructFromDelivered (Float qty)
    {
        this.delivered -= qty;
    }
}
