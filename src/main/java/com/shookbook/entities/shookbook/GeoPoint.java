package com.shookbook.entities.shookbook;

import com.shookbook.types.BaseEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "geo_points")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString
public class GeoPoint extends BaseEntity<GeoPoint> {
    private static final long serialVersionUID = 1L;
    @Getter
    @Setter
    private int customerId;
    @Getter
    @Setter
    private String city;
    @Getter
    @Setter
    private String street;
    @Getter
    @Setter
    private String streetNumber;
    @Getter
    @Setter
    private Double latitude;
    @Getter
    @Setter
    private Double longitude;
    @Getter
    @Setter
    private String details;
    @Getter
    @Setter
    private String timeConstraints;
}
