package com.shookbook.entities.shookbook;

import com.shookbook.types.BaseEntity;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "subscriptions")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString
public class Subscription extends BaseEntity<Subscription>{

    public enum OrderStatuses
    {
        SUCCESS("הצלחה"),
        CREDIT_PROBLEM("בעיית אשראי");
        public final String heb;
        OrderStatuses(String heb)
        {
            this.heb = heb;
        }
    }

    public enum OrderFrequency
    {
        DOUWEEK("פעמיים בשבוע"),
        WEEKLY("שבועי"),
        BIWEEKLY("פעם בשבועיים"),
        MONTHLY("חודשי");
        public final String heb;
        OrderFrequency(String heb)
        {
            this.heb = heb;
        }
    }

    @Getter
    @Setter
    private int customerId;
    @Getter
    @Setter
    private Character dayOfDelivery;
    @Getter
    @Setter
    private Short deliveryWindow;
    @Enumerated(EnumType.ORDINAL)
    @Getter
    @Setter
    private OrderFrequency frequency;
    @Getter
    @Setter
    private String cart;
    @Getter
    @Setter
    private String floorNo;
    @Getter
    @Setter
    private String apartment;
    @Getter
    @Setter
    private String entryCode;
    @Getter
    @Setter
    private String city;
    @Getter
    @Setter
    private String street;
    @Getter
    @Setter
    private String streetNo;
    @Getter
    @Setter
    private String lastName;
    @Getter
    @Setter
    private String firstName;
    @Temporal(TemporalType.TIMESTAMP)
    @Getter
    @Setter
    private Date createdDate;
    @Getter
    @Setter
    private String description;
    @Getter
    @Setter
    private Character isDeleted;
    @Getter
    @Setter
    private Character valid;

}
