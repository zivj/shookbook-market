package com.shookbook.entities.shookbook;

import com.shookbook.types.BaseEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;


@Entity
@Table(name = "worker_hours")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString
public class WorkerHours extends BaseEntity<WorkerHours> {

    @Getter
    @Setter
    private int workerId;
    @Temporal(TemporalType.TIMESTAMP)
    @Getter
    @Setter
    private Date startTime;
    @Temporal(TemporalType.TIMESTAMP)
    @Getter
    @Setter
    private Date endTime;
}
