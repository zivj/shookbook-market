package com.shookbook.entities.shookbook;

import com.shookbook.types.BaseEntity;
import lombok.*;
import org.json.JSONObject;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;

@Entity
@Table(name = "suppliers")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString
public class Supplier extends BaseEntity<Supplier> {
    @Getter
    @Setter
    private String companyId;
    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private String contact;
    @Getter
    @Setter
    private String contact2;
    @Getter
    @Setter
    private String phone;
    @Getter
    @Setter
    private String phone2;
    @Getter
    @Setter
    private String fax;
    @Getter
    @Setter
    private String email;
    @Getter
    @Setter
    private String houseNo;
    @Getter
    @Setter
    private String street;
    @Getter
    @Setter
    private String city;
    @Getter
    @Setter
    private String country;
    @Getter
    @Setter
    private String zip;
    @Getter
    @Setter
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Getter
    @Setter
    private Character status;
    @Getter
    @Setter
    @Lob
    private String positiveNotes;
    @Getter
    @Setter
    @Lob
    private String negativeNotes;
    @Getter
    @Setter
    @Lob
    private String orderInventoryFactorJson;
    @Getter
    @Setter
    @Lob
    private String orderWeightFactorJson;
    @Getter
    @Setter
    private String bankNo;
    @Getter
    @Setter
    private String bankBranchNo;
    @Getter
    @Setter
    private String bankAccountNo;
    @Getter
    @Setter
    private String bankAccountOwner;
    @Getter
    @Setter
    private Character isDeleted;

    public void resetOrderInventoryFactorJson()
    {
        orderInventoryFactorJson = null;
    }

    @Transient
    public JSONObject getOrderInventoryFactorJsonAsJson() {
        try {
            return new JSONObject(orderInventoryFactorJson);
        } catch (Exception ex) {
            return new JSONObject().put("enable", false).put("day1", 1d).put("day2", 1d).put("day3", 1d).put("day4", 1d).put("day5", 1d).put("day6", 1d).put("day7", 1d);
        }
    }

    @Transient
    public void setOrderInventoryFactor(String key, Object value) {
        if (!key.equals("enable") && !key.startsWith("day")) {
            throw new RuntimeException("Invalid key");
        }
        orderInventoryFactorJson = getOrderInventoryFactorJsonAsJson().put(key, value).toString();
    }

    @Transient
    public Object getOrderInventoryFactor(String key) {
        if (!key.equals("enable") && !key.startsWith("day")) {
            throw new RuntimeException("Invalid key");
        }
        return getOrderInventoryFactorJsonAsJson().get(key);
    }

    @Transient
    public Object getTodayOrderInventoryFactor() {
        return getOrderInventoryFactorJsonAsJson().get("day" + Calendar.getInstance().get(Calendar.DAY_OF_WEEK));
    }

    @Transient
    public void setTodayOrderInventoryFactor(Double factor) {
        orderInventoryFactorJson = getOrderInventoryFactorJsonAsJson().put("day" + Calendar.getInstance().get(Calendar.DAY_OF_WEEK), factor).toString();
    }

    public void resetOrderWeightFactorJson()
    {
        orderWeightFactorJson = null;
    }

    @Transient
    public JSONObject getOrderWeightFactorJsonAsJson() {
        try {
            return new JSONObject(orderWeightFactorJson);
        } catch (Exception ex) {
            return new JSONObject().put("enable", false).put("day1", 1d).put("day2", 1d).put("day3", 1d).put("day4", 1d).put("day5", 1d).put("day6", 1d).put("day7", 1d);
        }
    }

    @Transient
    public void setOrderWeightFactor(String key, Object value) {
        if (!key.equals("enable") && !key.startsWith("day")) {
            throw new RuntimeException("Invalid key");
        }
        orderWeightFactorJson = getOrderWeightFactorJsonAsJson().put(key, value).toString();
    }

    @Transient
    public Object getOrderWeightFactor(String key) {
        if (!key.equals("enable") && !key.startsWith("day")) {
            throw new RuntimeException("Invalid key");
        }
        return getOrderWeightFactorJsonAsJson().get(key);
    }

    @Transient
    public Object getTodayOrderWeightFactor() {
        return getOrderWeightFactorJsonAsJson().get("day" + Calendar.getInstance().get(Calendar.DAY_OF_WEEK));
    }

    @Transient
    public void setTodayOrderWeightFactor(Double factor) {
        orderWeightFactorJson = getOrderWeightFactorJsonAsJson().put("day" + Calendar.getInstance().get(Calendar.DAY_OF_WEEK), factor).toString();
    }
}
