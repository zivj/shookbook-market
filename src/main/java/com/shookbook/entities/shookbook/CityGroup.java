package com.shookbook.entities.shookbook;

import com.shookbook.types.BaseEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "city_groups")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString
public class CityGroup extends BaseEntity<CityGroup>
{
    private static final long serialVersionUID = 1L;
    @Getter
    @Setter
    private Short hasOffset;
    @Getter
    @Setter
    private Short sunday;
    @Getter
    @Setter
    private Short monday;
    @Getter
    @Setter
    private Short tuesday;
    @Getter
    @Setter
    private Short wednesday;
    @Getter
    @Setter
    private Short thursday;
    @Getter
    @Setter
    private Short friday;
    @Getter
    @Setter
    private Short saturday;
    @Getter
    @Setter
    private String description;
}
