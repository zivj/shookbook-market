/*
  Created by zivj on 25/9/17.
 */

package com.shookbook.entities.shookbook;

import com.shookbook.types.BaseEntity;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "coupons")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString
public class Coupon extends BaseEntity<Coupon>
{
    public enum CouponTypes {
        DISCOUNT("הנחה"),
        PERCENT("אחוז"),
        POINTS("נקודות"),
        PRODUCT("מוצר"),
        GIFT("מתנה"),
        COMMERCIAL("פרסום"),
        BRAND("מותג"),
        CANCELLED("בוטל"),
        COMBINED("מאוחד");
        public final String heb;
        CouponTypes(String heb) { this.heb = heb; }
    }

    public enum CouponCause {
        BAD_PRODUCT("מוצר פגום"),
        DELIVERY("משלוח"),
        COMPANSATION("פיצוי"),
        GENERAL("כללי"),
        GROUP_DISCOUNT("הנחה קבוצתית"),
        GROUP("קבוצה"),
        PACKING("אריזה");
        public final String heb;
        CouponCause(String heb) { this.heb = heb; }
    }

    private static final long serialVersionUID = 1L;
    @Getter
    @Setter
    private int wpId;
    @Getter
    @Setter
    private Integer orderId;
    @Getter
    @Setter
    @Enumerated(EnumType.ORDINAL)
    private Coupon.CouponTypes type;
    @Getter
    @Setter
    @Enumerated(EnumType.ORDINAL)
    private Coupon.CouponCause cause;
    @Getter
    @Setter
    private float amount;
    @Getter
    @Setter
    private int customerId;
    @Getter
    @Setter
    private int leadId;
    @Getter
    @Setter
    private String title;
    @Getter
    @Setter
    private String details;
    @Getter
    @Setter
    private Integer random;
    @Getter
    @Setter
    private Integer couponUsage;
    @Getter
    @Setter
    private Integer couponGroup;
    @Getter
    @Setter
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Getter
    @Setter
    private Character isDeleted;
}
