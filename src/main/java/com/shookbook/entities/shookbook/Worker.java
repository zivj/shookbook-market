package com.shookbook.entities.shookbook;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.shookbook.types.BaseEntity;
import com.shookbook.types.Cell;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "workers")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString
public class Worker extends BaseEntity<Worker> {
    public enum Types {
        PACKER, SUPPLIER, CUSTOMER, MANAGER, CUSTOMER_SUPPORT, LOGISTICS, ADMIN, DELIVERY
    }

    @Getter
    @Setter
    private String username;
    @Getter
    @Setter
    private String password;
    @Enumerated(EnumType.ORDINAL)
    @Getter
    @Setter
    private Types type;
    @Getter
    @Setter
    private String firstName;
    @Getter
    @Setter
    private String lastName;
    @Getter
    @Setter
    private String sbId;
    @Getter
    @Setter
    private String isrId;
    @Getter
    @Setter
    private String email;
    @Getter
    @Setter
    private Long imei;
    @Getter
    @Setter
    private String assignedCar;

    public Worker(String username, String password, Types type, String firstName, String lastName, String sbId, String isrId, String email) {
        this.username = username;
        this.password = password;
        this.type = type;
        this.firstName = firstName;
        this.lastName = lastName;
        this.sbId = sbId;
        this.isrId = isrId;
        this.email = email;
    }

    @Transient
    private List<Cell> workerHoursList = null;

    @Transient
    public List<Cell> getWorkerHours() {
        return workerHoursList;
    }

    @Transient
    public void setWorkerHours(List<Cell> workerHoursList) {
        this.workerHoursList = workerHoursList;
    }

    @Transient
    private String sessionToken = null;

    @Transient
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getSessionToken() {
        return sessionToken;
    }

    @Transient
    public void setSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
    }
}
