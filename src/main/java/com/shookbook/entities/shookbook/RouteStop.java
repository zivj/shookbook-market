package com.shookbook.entities.shookbook;

import com.shookbook.types.BaseEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Entity
@Table(name = "route_stop")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString
public class RouteStop extends BaseEntity<RouteStop> {
    private static final long serialVersionUID = 1L;
    @Getter
    @Setter
    private int geopointId;
    @Getter
    @Setter
    private int routeId;
    @Getter
    @Setter
    private int orderId;
    @Temporal(TemporalType.TIMESTAMP)
    @Getter
    @Setter
    private Date estimatedDelivery;
    @Temporal(TemporalType.TIMESTAMP)
    @Getter
    @Setter
    private Date actualDelivery;
    @Getter
    @Setter
    private Character isDeleted;
}
