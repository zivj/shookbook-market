package com.shookbook.entities.shookbook;

import com.shookbook.types.BaseEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Entity
@Table(name = "routes")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString
public class Routes extends BaseEntity<Routes> {
    private static final long serialVersionUID = 1L;
    @Getter
    @Setter
    private int direction;
    @Getter
    @Setter
    private int visible;
    @Getter
    @Setter
    private int deliveryWindow;
    @Getter
    @Setter
    private int type;
    @Getter
    @Setter
    private String driverName;
    @Getter
    @Setter
    private int assignedCar;
    @Getter
    @Setter
    private Character prefix;
    @Temporal(TemporalType.TIMESTAMP)
    @Getter
    @Setter
    private Date estimatedStart;
    @Temporal(TemporalType.TIMESTAMP)
    @Getter
    @Setter
    private Date actualStart;
    @Temporal(TemporalType.TIMESTAMP)
    @Getter
    @Setter
    private Date deliveryDate;
    @Temporal(TemporalType.TIMESTAMP)
    @Getter
    @Setter
    private Date createdDate;
    @Getter
    @Setter
    private Character isDeleted;
}
