package com.shookbook.entities.shookbook;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "globals")
@NoArgsConstructor
@ToString
public class Global
{
    private static final long serialVersionUID = 1L;
    @Id
    @Getter
    @Setter
    @Column(name = "`key_name`")
    private String key;
    @Getter
    @Setter
    @Column(name = "`value_string`")
    private String value;
    @Getter
    @Setter
    private char category;
    @Getter
    @Setter
    private String details;

}
