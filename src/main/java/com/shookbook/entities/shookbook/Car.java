package com.shookbook.entities.shookbook;

import com.shookbook.types.BaseEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "cars")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString
public class Car extends BaseEntity<Car> {
    private static final long serialVersionUID = 1L;

    @Getter
    @Setter
    private String number;

    @Getter
    @Setter
    private String type;
}