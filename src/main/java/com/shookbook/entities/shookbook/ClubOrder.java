package com.shookbook.entities.shookbook;

import com.shookbook.types.BaseEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Entity
@Table(name = "club_order")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString
public class ClubOrder extends BaseEntity<ClubOrder> {
    private static final long serialVersionUID = 1L;
    @Getter
    @Setter
    private int wpOrderId;
    @Getter
    @Setter
    private int wpUserId;
    @Getter
    @Setter
    private int clubId;
    @Temporal(TemporalType.TIMESTAMP)
    @Getter
    @Setter
    private Date createdDate;
}
