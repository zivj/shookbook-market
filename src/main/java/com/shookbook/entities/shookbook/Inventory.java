/**
 * Created by zivj on 3/11/17.
 */

package com.shookbook.entities.shookbook;

import com.shookbook.types.BaseEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Entity
@Table(name = "inventory")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString
public class Inventory extends BaseEntity<Inventory>
{
    @Getter
    @Setter
    private Integer productId;
    @Getter
    @Setter
    private String productName;
    @Getter
    @Setter
    private float threshold;
    @Temporal(TemporalType.TIMESTAMP)
    @Getter
    @Setter
    private Date lastUpdate;
    @Getter
    @Setter
    private Character isDeleted;
}
