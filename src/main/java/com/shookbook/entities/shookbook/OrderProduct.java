package com.shookbook.entities.shookbook;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.shookbook.types.BaseEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

@Entity
@Table(name = "order_product")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString
public class OrderProduct extends BaseEntity<OrderProduct> {

    public enum OrderProductStatuses
    {
        UNSCANNED, SCANNING, SCANNED, MISSING
    }

    @Getter
    @Setter
    private int orderId;
    @Getter
    @Setter
    private int productId;
    @Getter
    @Setter
    private Integer itemId;
    @Getter
    @Setter
    private float quantityNeeded;
    @Getter
    @Setter
    private float quantity;
    @Getter
    @Setter
    @JsonIgnore
    private float quantityDelivered;
    @Getter
    @Setter
    private float unitPrice;
    @Enumerated(EnumType.ORDINAL)
    @Getter
    @Setter
    private OrderProductStatuses status;

    public OrderProduct(int orderId, int productId, float quantityNeeded, float unitPrice) {
        this.orderId = orderId;
        this.productId = productId;
        this.quantityNeeded = quantityNeeded;
        this.unitPrice = unitPrice;
        this.quantity = 0;
        this.status = OrderProduct.OrderProductStatuses.UNSCANNED;
    }
}
