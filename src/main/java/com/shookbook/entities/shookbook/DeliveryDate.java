package com.shookbook.entities.shookbook;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "delivery_dates")
@NoArgsConstructor
@ToString
public class DeliveryDate
{
    @Id
    @Temporal(TemporalType.TIMESTAMP)
    @Getter
    @Setter
    private Date date;

    public DeliveryDate(Date date)
    {
        this.date = date;
    }
}
