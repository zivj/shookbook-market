package com.shookbook.entities.shookbook;

import com.shookbook.types.BaseEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

@Entity
@Table(name = "mobile_notifications")
@NoArgsConstructor
@ToString
public class MobileNotification extends BaseEntity<Coupon> {
    public enum notificationType {
        REMAINDER("תזכורת"),
        PACKING("אריזה"),
        DELIVERY("משלוח"),
        GROUP("קבוצה"),
        INFORMATION("מידע"),
        GENERAL("כללי");
        public final String heb;
        notificationType(String heb) { this.heb = heb; }
    }

    @Getter
    @Setter
    private Integer groupId;
    @Getter
    @Setter
    private String text;
    @Getter
    @Setter
    @Enumerated(EnumType.ORDINAL)
    private MobileNotification.notificationType type;
    @Getter
    @Setter
    private Character isDeleted;
}
