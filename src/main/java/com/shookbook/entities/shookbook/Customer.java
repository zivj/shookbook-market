package com.shookbook.entities.shookbook;

import com.shookbook.types.BaseEntity;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "customers")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString
public class Customer extends BaseEntity<Customer>
{
    public enum CustomerTypes
    {
        //SILVER, GOLD, PLATINUM
        SILVER("כסף"),
        GOLD("זהב"),
        PLATINUM("פלטינה");

        public final String heb;
        CustomerTypes(String heb)
        {
            this.heb = heb;
        }
    }

    public enum AlertTypes
    {
        NONE("ללא"),
        DELIVERY("משלוח"),
        PACKING("אריזה"),
        FOLLOW("במעקב");

        public final String heb;
        AlertTypes(String heb)
        {
            this.heb = heb;
        }
    }

    private static final long serialVersionUID = 1L;
    @Getter
    @Setter
    private int wpId;
    @Getter
    @Setter
    private String barcode;
    @Getter
    @Setter
    private String firstName;
    @Getter
    @Setter
    private String lastName;
    public String getFullName()
    {
        return firstName + " " + lastName;
    }
    @Getter
    @Setter
    private String email;
    @Getter
    @Setter
    private String phone;
    @Getter
    @Setter
    private String phone2;
    @Getter
    @Setter
    private String phoneExt;
    @Getter
    @Setter
    @Column(name = "phone2_ext")
    private String phone2Ext;
    @Getter
    @Setter
    private String phoneNo;
    @Getter
    @Setter
    @Column(name = "phone2_no")
    private String phone2No;
    @Getter
    @Setter
    private String streetNo;
    @Getter
    @Setter
    private String street;
    @Getter
    @Setter
    private String city;
    @Getter
    @Setter
    private String entryCode;
    @Getter
    @Setter
    private String apartment;
    @Getter
    @Setter
    private String floorNo;
    @Getter
    @Setter
    private Integer forceGeotag;
    @Getter
    @Setter
    private Integer couponGroup;
    @Getter
    @Setter
    @Temporal(TemporalType.DATE)
    private Date dayOfBirth;
    @Getter
    @Setter
    private String companyName;
    @Getter
    @Setter
    private String companyId;
    @Getter
    @Setter
    private String creditCardToken;
    @Getter
    @Setter
    private float discount;
    @Getter
    @Setter
    private short creditPoints;
    @Getter
    @Setter
    @Enumerated(EnumType.ORDINAL)
    private CustomerTypes type;
    @Getter
    @Setter
    @Temporal(TemporalType.TIMESTAMP)
    private Date typeDate;
    @Getter
    @Setter
    private String remarks;
    @Getter
    @Setter
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Getter
    @Setter
    private Character isDeleted;
    @Basic(optional = false)
    @Getter
    @Setter
    private Character creditCardHide;
    @Getter
    @Setter
    private Character invoiceOnly;
    @Getter
    @Setter
    private Character Gender;
    @Getter
    @Setter
    private Character allowSimilarProducts;
    @Getter
    @Setter
    private Character environmentFriendly;
    @Getter
    @Setter
    @Enumerated(EnumType.ORDINAL)
    private AlertTypes attentionFlag;
    @Getter
    @Setter
    private String attentionCause;
    @Getter
    @Setter
    @Transient
    private String houseNo;
    @Getter
    @Setter
    @Transient
    private String country;
    @Getter
    @Setter
    @Transient
    private String zip;
}
