package com.shookbook.entities.shookbook;

import com.shookbook.types.BaseEntity;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "cities")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString

public class City extends BaseEntity<City>
{
    private static final long serialVersionUID = 1L;
    @Getter
    @Setter
    @Column(name = "`name`")
    private String city;
    @Getter
    @Setter
    private Integer cityGroupId;
}
