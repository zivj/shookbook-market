/*
  Created by zivj on 25/9/17.
 */

package com.shookbook.entities.shookbook;

import com.shookbook.types.BaseEntity;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "refunds")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString
public class Refund extends BaseEntity<Refund>
{
    public enum Types {
        CHARGE("חיוב"),
        COUPON("קופון"),
        CUSTOMER("זיכוי לקוח");
        public final String heb;
        Types(String heb) { this.heb = heb; }
    }


    public enum Cause {
        QUALITY("איכות"),
        HANDLING("אריזה"),
        DELIVERY("משלוח"),
        COMPANSATION("פיצוי"),
        MISC("אחר");
        public final String heb;
        Cause(String heb) { this.heb = heb; }
    }

    public enum RootCause {
        BAD_PRODUCT("מוצר פגום"),
        NO_DELIVERY("לא הגיע ליעד"),
        COMPANSATION("פיצוי"),
        GENERAL("כללי"),
        GROUP_DISCOUNT("הנחה קבוצתית"),
        GROUP("קבוצה"),
        WRONG_PRODUCT("מוצר שגוי"),
        WEIGHT("משקל"),
        MISSING("חסר"),
        DISORDERED("ניזוק במשלוח"),
        DELIVERY_BOY("התנהגות השליח"),
        BAD_BOX("מצב הארגז");
        public final String heb;
        RootCause(String heb) { this.heb = heb; }
    }

    private static final long serialVersionUID = 1L;
    @Getter
    @Setter
    private Integer orderId;
    @Getter
    @Setter
    @Enumerated(EnumType.ORDINAL)
    private Refund.Types type;
    @Getter
    @Setter
    @Enumerated(EnumType.ORDINAL)
    private Refund.Cause cause;
    @Getter
    @Setter
    @Enumerated(EnumType.ORDINAL)
    private Refund.RootCause rootCause;
    @Getter
    @Setter
    private float amount;
    @Getter
    @Setter
    private float quantity;
    @Getter
    @Setter
    private int customerId;
    @Getter
    @Setter
    private int workerId;
    @Getter
    @Setter
    private int couponId;
    @Getter
    @Setter
    private String details;
    @Getter
    @Setter
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
}