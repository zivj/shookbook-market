package com.shookbook.entities.shookbook;

import com.shookbook.types.BaseEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

@Entity
@Table(name = "items")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString
public class Item extends BaseEntity<Item> {

    public enum OrderUnits {
        UNIT("יחידה"), KG("קג");
        public final String heb;

        OrderUnits(String heb) {
            this.heb = heb;
        }
    }

    @Getter
    @Setter
    private int supplierId;
    @Getter
    @Setter
    private int productId;
    @Getter
    @Setter
    private String barcode;
    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private short inventory;
    @Getter
    @Setter
    private float grossProfit;
    @Getter
    @Setter
    private float buyingCost;
    @Getter
    @Setter
    private float unitWeight;
    @Enumerated(EnumType.ORDINAL)
    @Getter
    @Setter
    private Item.OrderUnits orderUnit;
    @Getter
    @Setter
    private Character isDeleted;
}
