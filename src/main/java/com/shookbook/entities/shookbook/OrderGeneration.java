package com.shookbook.entities.shookbook;

import com.shookbook.types.BaseEntity;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Entity
@Table(name = "order_generations")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString
public class OrderGeneration extends BaseEntity<OrderGeneration> {

    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
}
