package com.shookbook.entities.shookbook;

import com.shookbook.types.BaseEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

@Entity
@Table(name = "supplier_order_item")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString
public class SupplierOrderItem extends BaseEntity<SupplierOrderItem> {

    public enum ItemUnit {
        UNIT("יחידה"), KG("קג");
        public final String heb;

        ItemUnit(String heb) {
            this.heb = heb;
        }
    }

    @Getter
    @Setter
    private int supplierOrderId;
    @Getter
    @Setter
    private int itemId;
    @Getter
    @Setter
    private float quantity;
    @Getter
    @Setter
    private float unitPrice;
    @Getter
    @Setter
    @Enumerated(EnumType.ORDINAL)
    private SupplierOrderItem.ItemUnit itemUnit;
}
