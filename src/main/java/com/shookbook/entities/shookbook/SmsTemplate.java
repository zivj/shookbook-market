package com.shookbook.entities.shookbook;

import com.shookbook.types.BaseEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "sms_templates")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString
public class SmsTemplate extends BaseEntity<SmsTemplate> {
    private static final long serialVersionUID = 1L;

    @Getter
    @Setter
    private String message;
    @Getter
    @Setter
    private Integer type;
    @Getter
    @Setter
    private Integer cityGroupId;
    @Getter
    @Setter
    private Integer couponGroup;
    @Getter
    @Setter
    private Integer enable;
}

