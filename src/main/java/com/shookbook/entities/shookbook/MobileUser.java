package com.shookbook.entities.shookbook;

import com.shookbook.types.BaseEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "mobile_users")
@NoArgsConstructor
@ToString
public class MobileUser extends BaseEntity<Coupon> {

    private static final long serialVersionUID = 1L;
    @Getter
    @Setter
    private Integer customerId;
    @Getter
    @Setter
    private Integer groupId;
    @Getter
    @Setter
    private Character isDeleted;
}
