package com.shookbook.entities.shookbook;

import com.shookbook.types.BaseEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Entity
@Table(name = "club")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString
public class Club extends BaseEntity<Club> {
    private static final long serialVersionUID = 1L;
    @Getter
    @Setter
    private String password;
    @Getter
    @Setter
    private String description;
    @Getter
    @Setter
    private int clubManager;
    @Temporal(TemporalType.TIMESTAMP)
    @Getter
    @Setter
    private Date createdDate;
    @Getter
    @Setter
    private int clubCoupon;
    @Getter
    @Setter
    private String name;
}