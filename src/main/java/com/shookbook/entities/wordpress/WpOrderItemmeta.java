package com.shookbook.entities.wordpress;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "wp_woocommerce_order_itemmeta")
@NoArgsConstructor
@EqualsAndHashCode
@ToString

public class WpOrderItemmeta {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Getter
    @Setter
    @Id
    private Long metaId;
    @Getter
    @Setter
    private Long orderItemId;
    @Getter
    @Setter
    private String metaKey;
    @Getter
    @Setter
    @Lob
    private String metaValue;

    public WpOrderItemmeta(Long orderItemId, String metaKey, String metaValue) {
        this.orderItemId = orderItemId;
        this.metaKey = metaKey;
        this.metaValue = metaValue;
    }
}

