package com.shookbook.entities.wordpress;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "wp_users")
@NoArgsConstructor
@EqualsAndHashCode
@ToString

public class WpUser {
    private static final long serialVersionUID = 1L;

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Getter
    @Setter
    @Id
    private Long id;
    @Getter
    @Setter
    private String userLogin;
    @Getter
    @Setter
    private String userPass;
    @Getter
    @Setter
    private String userNicename;
    @Getter
    @Setter
    private String userEmail;
    @Getter
    @Setter
    private String userUrl;
    @Getter
    @Setter
    @Temporal(TemporalType.TIMESTAMP)
    private Date userRegistered;
    @Getter
    @Setter
    private String userActivationKey;
    @Getter
    @Setter
    private int userStatus;
    @Getter
    @Setter
    private String displayName;
}
