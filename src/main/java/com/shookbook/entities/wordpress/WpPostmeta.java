package com.shookbook.entities.wordpress;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "wp_postmeta")
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class WpPostmeta {
    private static final long serialVersionUID = 1L;

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Getter
    @Setter
    @Id
    private Long metaId;
    @Getter
    @Setter
    private long postId;
    @Getter
    @Setter
    private String metaKey;
    @Getter
    @Setter
    @Lob
    private String metaValue;

    public WpPostmeta(long postId, String metaKey, String metaValue) {
        this.postId = postId;
        this.metaKey = metaKey;
        this.metaValue = metaValue;
    }
}
