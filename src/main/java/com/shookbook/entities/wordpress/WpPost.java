package com.shookbook.entities.wordpress;

import com.shookbook.entities.shookbook.Coupon;
import com.shookbook.entities.shookbook.Product;
import lombok.*;

import javax.persistence.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@Entity
@Table(name = "wp_posts")
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class WpPost {

    private static final long serialVersionUID = 1L;

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Getter
    @Setter
    @Id
    private Long id;
    @Getter
    @Setter
    private long postAuthor;
    @Getter
    @Setter
    @Temporal(TemporalType.TIMESTAMP)
    private Date postDate;
    @Getter
    @Setter
    @Temporal(TemporalType.TIMESTAMP)
    private Date postDateGmt;
    @Getter
    @Setter
    @Lob
    private String postContent;
    @Getter
    @Setter
    @Lob
    private String postTitle;
    @Getter
    @Setter
    @Lob
    private String postExcerpt;
    @Getter
    @Setter
    private String postStatus;
    @Getter
    @Setter
    private String commentStatus;
    @Getter
    @Setter
    private String pingStatus;
    @Getter
    @Setter
    private String postPassword;
    @Getter
    @Setter
    private String postName;
    @Getter
    @Setter
    @Lob
    private String toPing;
    @Getter
    @Setter
    @Lob
    private String pinged;
    @Getter
    @Setter
    @Temporal(TemporalType.TIMESTAMP)
    private Date postModified;
    @Getter
    @Setter
    @Temporal(TemporalType.TIMESTAMP)
    private Date postModifiedGmt;
    @Getter
    @Setter
    @Lob
    private String postContentFiltered;
    @Getter
    @Setter
    private long postParent;
    @Getter
    @Setter
    private String guid;
    @Getter
    @Setter
    private int menuOrder;
    @Getter
    @Setter
    private String postType;
    @Getter
    @Setter
    private String postMimeType;
    @Getter
    @Setter
    private long commentCount;

    public WpPost(Product product, boolean isImage) {
        postAuthor = 1;
        postContent = "";
        if (isImage) {
            postParent = product.getWpId();
            guid = product.getImageUrl();
            postType = "attachment";
            postMimeType = "image/jpeg";
            postStatus = "inherit";
            postTitle = "";
            postExcerpt = "";
            postName = "";
        } else {
            guid = "";
            postType = "product";
            postMimeType = "";
            postStatus = "publish";
            postTitle = product.getNameHeb();
            postExcerpt = product.getKeyWords();
            postName = product.getName().replace(' ', '_');
        }
        commentStatus = "closed";
        pingStatus = "closed";
        toPing = "";
        pinged = "";
        postPassword = "";
        postContentFiltered = "";
        postDate = new Date(System.currentTimeMillis());
        postDateGmt = postDate;
        postModified = postDate;
        postModifiedGmt = postDate;
    }

    public WpPost(Coupon coupon) {
        postAuthor = 1;
        postContent = "";
        guid = "";
        postType = "shop_coupon";
        postMimeType = "";
        postStatus = "publish";
        postTitle = coupon.getTitle();
        postExcerpt = "";
        postName = coupon.getTitle();
        commentStatus = "closed";
        pingStatus = "closed";
        toPing = "";
        pinged = "";
        postPassword = "";
        postContentFiltered = "";
        postDate = new Date(System.currentTimeMillis());
        postDateGmt = postDate;
        postModified = postDate;
        postModifiedGmt = postDate;
    }

    private static final Locale heLocale = new Locale("he");
    private static final DateFormat postTitleDateFormat = new SimpleDateFormat("MMMM d, yyyy @ hh:mm a", heLocale);
    private static final DateFormat postNameDateFormat = new SimpleDateFormat("MMM-dd-yyyy-hhmm-a");

    public WpPost(String postUnique, String lastPostName, int commentCount) {
        this.postAuthor = 1;
        this.postDate = new Date(System.currentTimeMillis());
        this.postDateGmt = this.postDate;
        this.postContent = "";
        this.postTitle = "Order &ndash; " + postTitleDateFormat.format(this.postDate);
        this.postExcerpt ="";
        this.postStatus = "wc-completed";
        this.commentStatus = "open";
        this.pingStatus = "closed";
        this.postPassword = postUnique;
        this.postName = postNameDateFormat.format(this.postDate);
        if (lastPostName.contains(this.postName)) {
            //TODO
        }
        this.toPing = "";
        this.pinged = "";
        this.postModified = this.postDate;
        this.postModifiedGmt = this.postDate;
        this.postContentFiltered = "";
        this.postParent = 0;
        this.guid = "";
        this.menuOrder = 0;
        this.postType = "shop_order";
        this.postMimeType = "";
        this.commentCount = commentCount;
    }
}
