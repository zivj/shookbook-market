package com.shookbook.entities.wordpress;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "wp_woocommerce_order_items")
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class WpOrderItem {
    private static final long serialVersionUID = 1L;

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Getter
    @Setter
    @Id
    private Long orderItemId;
    @Getter
    @Setter
    private String orderItemName;
    @Getter
    @Setter
    private String orderItemType;
    @Getter
    @Setter
    private Long orderId;

    public WpOrderItem(String itemName, String itemType, long orderId) {
        this.orderItemName = itemName;
        this.orderItemType = itemType;
        this.orderId = orderId;
    }
}
