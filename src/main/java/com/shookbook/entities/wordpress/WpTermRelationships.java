package com.shookbook.entities.wordpress;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "wp_term_relationships")
@NoArgsConstructor
@EqualsAndHashCode
@ToString

public class WpTermRelationships implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Getter
    @Setter
    @Basic(optional = false)
    @Column(name = "object_id")
    private Long objectId;
    @Basic(optional = false)
    @Getter
    @Setter
    @Column(name = "term_taxonomy_id")
    private Long termTaxonomyId;
    @Basic(optional = false)
    @Getter
    @Setter
    @Column(name = "term_order")
    private Integer termOrder;

    public WpTermRelationships(long objectId, long termTaxonomyId, Integer termOrder) {
        this.setObjectId(objectId);
        this.setTermTaxonomyId(termTaxonomyId);
        this.setTermOrder(termOrder);
    }
}

