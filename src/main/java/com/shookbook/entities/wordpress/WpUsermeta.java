package com.shookbook.entities.wordpress;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "wp_usermeta")
@NoArgsConstructor
@EqualsAndHashCode
@ToString

public class WpUsermeta {

    private static final long serialVersionUID = 1L;

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)

    @Getter
    @Setter
    @Id
    private Long umetaId;
    @Getter
    @Setter
    private long userId;
    @Getter
    @Setter
    private String metaKey;
    @Getter
    @Setter
    @Lob
    private String metaValue;
}
