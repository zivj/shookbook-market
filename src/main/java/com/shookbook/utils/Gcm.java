package com.shookbook.utils;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONObject;

import java.io.IOException;

public class Gcm
{
    public static void send(String key, String message, int type, String... targets)
    {
        try
        {
            HttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost("https://android.googleapis.com/gcm/send");
            post.addHeader("Authorization", "key=" + key);
            post.addHeader("Content-Type", "application/json");
            JSONObject json = new JSONObject();
            json.put("registration_ids", targets);
            JSONObject data = new JSONObject();
            data.put("message", message);
            data.put("type", type);
            json.put("data", data);
            post.setEntity(new ByteArrayEntity(json.toString().getBytes("UTF8")));
            HttpResponse response = client.execute(post);
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
    }
}
