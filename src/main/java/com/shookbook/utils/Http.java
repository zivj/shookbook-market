package com.shookbook.utils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.HttpClients;

import java.io.IOException;
import java.net.URI;
import java.util.List;

public class Http
{
    private final static int BUFFER_SIZE = 4096;
    private static int defaultTimeout = 0;

    public static void setDefaultTimeout(int defaultTimeout)
    {
        Http.defaultTimeout = defaultTimeout;
    }

    public static byte[] bytesFromUrl(String url)
    {
        return bytesFromUrl(url, defaultTimeout);
    }

    public static byte[] bytesFromUrl(String url, int timeout)
    {
        try
        {
            HttpClient client = HttpClients.createDefault();
            HttpGet request = new HttpGet(url);
            if (timeout > 0)
            {
                request.setConfig(RequestConfig.custom().setSocketTimeout(timeout).setConnectTimeout(timeout).setConnectionRequestTimeout(timeout).build());
            }
            HttpResponse response = client.execute(request);
            HttpEntity entity = response.getEntity();
            return U.var.readFully(entity.getContent(), BUFFER_SIZE);
        } catch (IOException ex)
        {
            return null;
        }
    }

    public static String stringFromUrl(String url)
    {
        return stringFromUrl(url, defaultTimeout);
    }

    public static String stringFromUrl(String url, int timeout)
    {
        byte[] b = bytesFromUrl(url);
        return b == null ? null : new String(b);
    }

    public static String getFinalRedirect(String link)
    {
        List<URI> redirectLocations = null;
        CloseableHttpResponse response = null;

        try
        {
            HttpClientContext context = HttpClientContext.create();
            HttpGet httpGet = new HttpGet(link);
            response = HttpClients.createDefault().execute(httpGet, context);

            redirectLocations = context.getRedirectLocations();
        } catch (IOException ex)
        {
            return null;
        } finally
        {
            if (response != null)
            {
                try
                {
                    response.close();
                } catch (IOException ex)
                {
                }
            }
        }

        return redirectLocations.get(redirectLocations.size() - 1).toString();
    }
}
