package com.shookbook.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Config
{
    private final static Properties properties = new Properties();
    static
    {
        reload();
    }

    public static void reload()
    {
        reload("config.properties");
    }

    public static void reload(String fileName)
    {
        try
        {
            properties.load(new FileInputStream(fileName));
        }
        catch (IOException ex)
        {

        }
    }

    public static String get(String key)
    {
        return properties.getProperty(key);
    }

    public static int getInt(String key)
    {
        return U.var.parseInt(properties.get(key));
    }

    public static boolean getBoolean(String key)
    {
        return U.var.parseBoolean(properties.get(key));
    }
}
