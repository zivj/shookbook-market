package com.shookbook.utils;

public class U
{
    public final class config extends Config
    {}

    public final class gcm extends Gcm{}

    public final class apns extends Apns{}

    public final class http extends Http{}

    public final class thread extends Thread
    {}

    public final class var extends Var{}
}
