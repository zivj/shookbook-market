package com.shookbook.utils;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Thread
{
    private final static ExecutorService threadPoolExecutor = Executors.newCachedThreadPool();

    public static void runOnExecutor(Runnable runnable)
    {
        threadPoolExecutor.execute(runnable);
    }

    public static List<Runnable> shutdownNow()
    {
        return threadPoolExecutor.shutdownNow();
    }


    public static void sleep(int milliseconds)
    {
        try
        {
            java.lang.Thread.sleep(milliseconds);
        }
        catch (InterruptedException ex)
        {
        }
    }

    public static void wait(Object o)
    {
        synchronized(o)
        {
            try
            {
                o.wait();
            }
            catch (InterruptedException ex)
            {
            }
        }
    }

    public static void notify(Object o)
    {
        synchronized(o)
        {
            o.notify();
        }
    }
}
