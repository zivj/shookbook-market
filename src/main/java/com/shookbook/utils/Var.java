package com.shookbook.utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class Var
{
    public static Random rand = new Random();

    public static float parseFloat(Object o)
    {
        if (o instanceof Number)
        {
            return ((Number)o).floatValue();
        }
        try
        {
            return Float.parseFloat(o.toString());
        }
        catch(NumberFormatException ex)
        {
            return 0;
        }
        catch(NullPointerException ex)
        {
            return 0;
        }
    }

    public static double parseDouble(Object o)
    {
        if (o instanceof Number)
        {
            return ((Number)o).doubleValue();
        }
        try
        {
            return Double.parseDouble(o.toString());
        }
        catch(NumberFormatException ex)
        {
            return 0;
        }
        catch(NullPointerException ex)
        {
            return 0;
        }
    }

    public static long parseLong(Object o)
    {
        if (o instanceof Number)
        {
            return ((Number)o).longValue();
        }
        try
        {
            return Long.parseLong(o.toString());
        }
        catch(NumberFormatException ex)
        {
            return 0;
        }
        catch(NullPointerException ex)
        {
            return 0;
        }
    }

    public static Integer[] parseIntArray(Object[] o)
    {
        Integer[] ret = new Integer[o.length];
        for (int i = 0 ; i < ret.length ; i++)
        {
            ret[i] = parseInt(o[i]);
        }
        return ret;
    }

    public static Long[] parseLongArray(Object[] o)
    {
        Long[] ret = new Long[o.length];
        for (int i = 0 ; i < ret.length ; i++)
        {
            ret[i] = parseLong(o[i]);
        }
        return ret;
    }

    public static int parseInt(Object o)
    {
        if (o instanceof Number)
        {
            return ((Number)o).intValue();
        }
        try
        {
            return Integer.parseInt(o.toString());
        }
        catch(NumberFormatException ex)
        {
            return 0;
        }
        catch(NullPointerException ex)
        {
            return 0;
        }
    }

    public static byte parseByte(Object o)
    {
        if (o instanceof Number)
        {
            return ((Number)o).byteValue();
        }
        try
        {
            return Byte.parseByte(o.toString());
        }
        catch(NumberFormatException ex)
        {
            return 0;
        }
        catch(NullPointerException ex)
        {
            return 0;
        }
    }

    public static boolean parseBoolean(Object o)
    {
        if (o instanceof Boolean)
        {
            return (Boolean)o;
        }
        try
        {
            return Boolean.parseBoolean(o.toString());
        }
        catch(NumberFormatException ex)
        {
            return false;
        }
        catch(NullPointerException ex)
        {
            return false;
        }
    }

    public static boolean isEmptyStr(Object str)
    {
        return str == null || str.toString().length() == 0 || str.toString().trim().length() == 0;
    }

    public static String ignoreNull(Object str)
    {
        if (isEmptyStr(str))
        {
            return "";
        }
        return str.toString();
    }

    public static Map<String, Object> json2Map(JSONObject json)
    {
        Map<String, Object> map = new HashMap<String, Object>();
        for (Object k : json.keySet())
        {
            Object o = json.get(k.toString());
            if (o instanceof JSONArray)
            {
                JSONArray ja = (JSONArray)o;
                Object[] arr = new Object[ja.length()];
                for (int i = 0 ; i < arr.length ; i++)
                {
                    o = ja.get(i);
                    if (o instanceof JSONObject)
                    {
                        arr[i] = json2Map((JSONObject)o);
                    }
                    else if (o != null)
                    {
                        arr[i] = o;
                    }
                }
                map.put(k.toString(), arr);
            }
            else if (o instanceof JSONObject)
            {
                map.put(k.toString(), json2Map((JSONObject)o));
            }
            else if (o != null)
            {
                map.put(k.toString(), o);
            }
        }
        return map;
    }

    public static Map objArr2Map(Object... args)
    {
        Map map = new HashMap();
        for (int i = 0 ; i < args.length ; i += 2)
        {
            map.put(args[i], args[i + 1]);
        }
        return map;
    }

    public static byte[] readFully(InputStream inputStream, int bufferSize) throws IOException
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream(bufferSize);
        final byte[] buffer = new byte[bufferSize];
        for (int i = inputStream.read(buffer) ; i > 0 ; i = inputStream.read(buffer))
        {
            baos.write(buffer, 0, i);
        }
        return baos.toByteArray();
    }

    public static String getResourceAsString(String resource)
    {
        try
        {
            return new String(readFully(resource.getClass().getResourceAsStream(resource), 1024));
        }
        catch (IOException ex)
        {
            return null;
        }
    }

    private final static DateFormat dateOnly = new SimpleDateFormat("dd/MM/yyyy");
    private final static DateFormat dateTime = new SimpleDateFormat("dd/MM/yyyy hh:mm");

    public static String formatDateOnly(long timeMillis)
    {
        return dateOnly.format(new Date(timeMillis));
    }

    public static String formatDateTime(long timeMillis)
    {
        return dateTime.format(new Date(timeMillis));
    }

    public static String urlEncode(String str)
    {
        try
        {
            return URLEncoder.encode(str, "UTF-8");
        }
        catch (UnsupportedEncodingException ex)
        {
            return "";
        }
    }

    public static <E> List<E> asList(E i)
    {
        E[] e = (E[]) Array.newInstance(i.getClass(), 1);
        e[0] = i;
        return Arrays.asList(e);
    }
}
