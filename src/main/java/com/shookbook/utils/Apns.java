package com.shookbook.utils;

import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;
import com.notnoop.apns.PayloadBuilder;

import java.util.HashMap;
import java.util.Map;

public class Apns
{
    private final static Map<String, ApnsService> apnsServiceMap = new HashMap<>();
    public static void send(String certPath, String password, boolean withSandbox, String message, int type, int userId, String target)
    {
        send(certPath, password, withSandbox, message, type, userId, target, null, "default");
    }

    public static void send(String certPath, String password, boolean withSandbox, String message, int type, int userId, String target, Integer badge)
    {
        send(certPath, password, withSandbox, message, type, userId, target, badge, "default");
    }

    public static void send(String certPath, String password, boolean withSandbox, String message, int type, int userId, String target, String sound)
    {
        send(certPath, password, withSandbox, message, type, userId, target, null, sound);
    }

    public static void send(String certPath, String password, boolean withSandbox, String message, int type, int userId, String target, Integer badge, String sound)
    {
        ApnsService service = apnsServiceMap.get("certPath");
        if (service == null)
        {
            if (withSandbox)
            {
                service = APNS.newService().withCert(certPath, password).withSandboxDestination().build();
            }
            else
            {
                service = APNS.newService().withCert(certPath, password).withProductionDestination().build();
            }
            apnsServiceMap.put(certPath, service);
        }

        PayloadBuilder payloadBuilder  = APNS.newPayload().alertBody(message).customField("type", "" + type).customField("userId", "" + userId).sound(sound);
        if (badge != null)
        {
            payloadBuilder = payloadBuilder.badge(badge);
        }
        service.push(target, payloadBuilder.build());
    }
}
