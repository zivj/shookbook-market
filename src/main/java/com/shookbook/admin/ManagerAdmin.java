package com.shookbook.admin;

import com.shookbook.Application;
import com.shookbook.dao.shookbook.*;
import com.shookbook.dao.wordpress.WpPostmetaDao;
import com.shookbook.dao.wordpress.WpPostsDao;
import com.shookbook.dao.wordpress.WpUsermetaDao;
import com.shookbook.dao.wordpress.WpUsersDao;
import com.shookbook.entities.shookbook.*;
import com.shookbook.entities.wordpress.WpPost;
import com.shookbook.entities.wordpress.WpUser;
import com.shookbook.utils.U;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

//import sun.jvm.hotspot.debugger.linux.sparc.LinuxSPARCThreadContext;

@Controller
@SessionAttributes("thought")
public class ManagerAdmin {
    private static final Logger logger = Logger.getLogger(ManagerAdmin.class);
    @Autowired
    private Common common;
    @Autowired
    private LogisticsAdmin logisticsAdmin;
    @Autowired
    private GlobalsDao globalsDao;
    @Autowired
    private CustomersDao customersDao;
    @Autowired
    private OrdersDao ordersDao;
    @Autowired
    private ProductsDao productsDao;
    @Autowired
    private WorkersDao workersDao;
    @Autowired
    private CarsDao carsDao;
    @Autowired
    private GeoPoinsDao geoPoinsDao;
    @Autowired
    private DeliveryDatesDao deliveryDatesDao;
    @Autowired
    private CitiesDao citiesDao;
    @Autowired
    private RoutesDao routesDao;
    @Autowired
    private RouteStopDao routeStopDao;
    @Autowired
    private WpPostmetaDao wpPostmetaDao;
    @Autowired
    private WpPostsDao wpPostsDao;
    @Autowired
    private WpUsermetaDao wpUsermetaDao;
    @Autowired
    private WpUsersDao wpUsersDao;
    @Autowired
    private MobileNotificationsDao mobileNotificationsDao;

    @RequestMapping(value = "/admin/manager/globals", method = RequestMethod.GET)
    public Object globals(Model model, HttpServletRequest request) {
        if (Integer.parseInt(request.getSession().getAttribute("admin").toString()) != Worker.Types.ADMIN.ordinal()) {
            return "redirect:/admin/login";
        }

        List<Map<String, String>> columns = new LinkedList<>();
        for (Global global : globalsDao.findAll()) {
            Map<String, String> column = new HashMap<>();
            column.put("key", global.getKey());
            column.put("value", global.getValue());
            char category = global.getCategory();
            column.put("category", Character.toString(category));
            column.put("details", global.getDetails());

            columns.add(column);
        }
        model.addAttribute("columns", columns);
        Map<String, Object> globals = new HashMap<>();
        globals.put("modules_enable_coupons", globalsDao.findOne("modules_enable_coupons").getValue());
        globals.put("modules_enable_invoice", globalsDao.findOne("modules_enable_invoice").getValue());
        globals.put("modules_enable_logistics", globalsDao.findOne("modules_enable_logistics").getValue());
        globals.put("modules_enable_packing", globalsDao.findOne("modules_enable_packing").getValue());
        Object logedWorker = request.getSession().getAttribute("userId");
        Worker worker = workersDao.getOne(Integer.parseInt(logedWorker.toString()));
        globals.put("worker_name", worker.getFirstName() + " " + worker.getLastName());
        model.addAttribute("globals", globals);
        model.addAttribute("admin", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.ADMIN.ordinal() ? 1 : 0);


        return "Globals";
    }


    @RequestMapping(value = "/admin/manager/submitGlobals", method = RequestMethod.POST)
    public Object submitGlobals(HttpServletRequest request) {
        if (!U.var.parseBoolean(request.getSession().getAttribute("login"))) {
            return "redirect:/admin/login";
        }
        List<Global> globalsList = globalsDao.findAll();
        for (Global global : globalsList) {
            if (request.getParameter(global.getKey()) != null) {
                global.setValue(request.getParameter(global.getKey()).toString());
            }
        }
        globalsDao.save(globalsList);
        return "redirect:/admin/manager/globals";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/admin/fileUpload")
    public String handleFileUpload(@RequestParam("file") MultipartFile[] fileUpload,
                                   RedirectAttributes redirectAttributes) {
        if (fileUpload != null && fileUpload.length > 0) {
            for (MultipartFile file : fileUpload) {

                if (file.getOriginalFilename().contains("/")) {
                    redirectAttributes.addFlashAttribute("message", "Folder separators not allowed");
                    return "redirect:upload";
                }
                if (file.getOriginalFilename().contains("/")) {
                    redirectAttributes.addFlashAttribute("message", "Relative pathnames not allowed");
                    return "redirect:upload";
                }

                if (!file.isEmpty()) {
                    try {
                        BufferedOutputStream stream = new BufferedOutputStream(
                                new FileOutputStream(new File(Application.imageRoot + "/" + file.getOriginalFilename())));
                        FileCopyUtils.copy(file.getInputStream(), stream);
                        stream.close();
                        redirectAttributes.addFlashAttribute("message",
                                "You successfully uploaded " + file.getOriginalFilename() + "!");
                    } catch (Exception e) {
                        redirectAttributes.addFlashAttribute("message",
                                "You failed to upload " + file.getOriginalFilename() + " => " + e.getMessage());
                    }
                } else {
                    redirectAttributes.addFlashAttribute("message",
                            "You failed to upload " + file.getOriginalFilename() + " because the file was empty");
                }
            }
        }
        return "redirect:" + "/admin/manager";
    }

    DateFormat deliveryDateFormat = new SimpleDateFormat("dd/MM/yyyy");

    @RequestMapping(method = RequestMethod.POST, value = "/admin/manager/uploadDeliveryDates")
    public String uploadDeliveryDates(@RequestParam("file") MultipartFile[] fileUpload, RedirectAttributes redirectAttributes) {
        BufferedReader br = null;
        if (fileUpload != null && fileUpload.length > 0) {
            for (MultipartFile file : fileUpload) {

                if (file.getOriginalFilename().contains("/")) {
                    redirectAttributes.addFlashAttribute("message", "Folder separators not allowed");
                    return "redirect:upload";
                }
                if (file.getOriginalFilename().contains("/")) {
                    redirectAttributes.addFlashAttribute("message", "Relative pathnames not allowed");
                    return "redirect:upload";
                }

                if (!file.isEmpty()) {
                    List<DeliveryDate> deliveryDates = new LinkedList<>();
                    try {
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(file.getInputStream()));
                        String line = bufferedReader.readLine();
                        while (line != null) {
                            if (line.charAt(0) > 256) {
                                line = line.substring(1);
                            }
                            deliveryDates.add(new DeliveryDate(deliveryDateFormat.parse(line.trim())));
                            line = bufferedReader.readLine();
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    deliveryDatesDao.deleteAll();
                    deliveryDatesDao.save(deliveryDates);
                } else {
                    redirectAttributes.addFlashAttribute("message",
                            "You failed to upload " + file.getOriginalFilename() + " because the file was empty");
                }
            }
        }
        return "redirect:" + "/admin/manager";
    }

    @ResponseBody
    @RequestMapping(value = "/admin/manager/downloadDeliveryDates", method = RequestMethod.GET)
    public Object downloadDeliveryDates(HttpServletResponse response) {
        StringBuilder stringBuilder = new StringBuilder();

        for (DeliveryDate deliveryDate : deliveryDatesDao.findAll()) {
            stringBuilder.append(deliveryDateFormat.format(deliveryDate.getDate())).append('\n');
        }

        response.setHeader("Content-Disposition", "attachment; filename=\"validDeliveryDates.csv\"");
        response.setContentLength(stringBuilder.length());
        response.setContentType("text/csv");
        return stringBuilder.toString();
    }

    @ResponseBody
    @RequestMapping(value = "/admin/manager/downloadLogisticsFile", method = RequestMethod.GET)
    public Object downloadLogisticsFile(HttpServletResponse response) {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("ShibAzmnNo,ShibNo,IMIE_No,Customer_Name,Sug_Tavla,Type_of_Cargo,")
                .append("ShibFromPalce,Destination,Quantity,Container_Number,Certificate_Number,")
                .append("Manifest_Number,Weight,Container_Type,s_meter,startdate,enddate")
                .append("fromtime,totime,DATE,TIME,Rem,PaidBy\n");

        for (Order order : ordersDao.findAllPendingOrder()) {
            stringBuilder.append(order.getId()).append(',').append('\n');
        }

        response.setHeader("Content-Disposition", "attachment; filename=\"logistics.csv\"");
        response.setContentLength(stringBuilder.length());
        response.setContentType("text/csv");
        return stringBuilder.toString();
    }

    @RequestMapping(method = RequestMethod.POST, value = "/admin/manager/updateCityGroups")
    public String updateCityGroups(@RequestParam("file") MultipartFile[] fileUpload, RedirectAttributes redirectAttributes) {
        BufferedReader br = null;
        if (fileUpload != null && fileUpload.length > 0) {
            for (MultipartFile file : fileUpload) {

                if (file.getOriginalFilename().contains("/")) {
                    redirectAttributes.addFlashAttribute("message", "Folder separators not allowed");
                    return "redirect:upload";
                }
                if (file.getOriginalFilename().contains("/")) {
                    redirectAttributes.addFlashAttribute("message", "Relative pathnames not allowed");
                    return "redirect:upload";
                }

                String line = "";
                String cvsSplitBy = ",";

                if (!file.isEmpty()) {
                    List<DeliveryDate> deliveryDates = new LinkedList<>();
                    try {
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(file.getInputStream()));
                        line = bufferedReader.readLine();

                        while (line != null) {
                            String [] elements = line.split(cvsSplitBy);
                            City city = citiesDao.findOneByCity(elements[0]);
                            System.out.format("   City: %s   Group: %d%n", elements[0], U.var.parseInt(elements[1]));
                            if (city != null) {
                                city.setCityGroupId(U.var.parseInt(elements[1]));
                                citiesDao.save(city);
                            } else {
                                City newCity = new City();
                                newCity.setCity(elements[0]);
                                newCity.setCityGroupId(U.var.parseInt(elements[1]));
                                citiesDao.save(newCity);
                            }
                            line = bufferedReader.readLine();
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } else {
                    redirectAttributes.addFlashAttribute("message",
                            "You failed to upload " + file.getOriginalFilename() + " because the file was empty");
                }
            }
        }
        return "redirect:" + "/admin/manager";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/admin/order/uploadCsv")
    public String uploadCsv(@RequestParam("file") MultipartFile[] fileUpload,
                            RedirectAttributes redirectAttributes) {
        System.out.format("   ### I dont want to be here%n;");
        BufferedReader br = null;
        if (fileUpload != null && fileUpload.length > 0) {
            for (MultipartFile file : fileUpload) {

                if (file.getOriginalFilename().contains("/")) {
                    redirectAttributes.addFlashAttribute("message", "Folder separators not allowed");
                    return "redirect:upload";
                }
                if (file.getOriginalFilename().contains("/")) {
                    redirectAttributes.addFlashAttribute("message", "Relative pathnames not allowed");
                    return "redirect:upload";
                }

                if (!file.isEmpty()) {
                    Map<Integer, String> orders = new HashMap<>();
                    List<Integer> ordersIds = new LinkedList<>();
                    try {
                        BufferedOutputStream stream = new BufferedOutputStream(
                                new FileOutputStream(new File(Application.csvRoot + "/" + file.getOriginalFilename())));
                        FileCopyUtils.copy(file.getInputStream(), stream);
                        stream.close();
                        redirectAttributes.addFlashAttribute("message",
                                "You successfully uploaded " + file.getOriginalFilename() + "!");

                        String csvFile = Application.csvRoot + "/" + file.getOriginalFilename();
                        String line = "";
                        String cvsSplitBy = ",";

                        br = new BufferedReader(new InputStreamReader(new FileInputStream(csvFile), "UTF-8"));

                        boolean ignoreFirstLine = true;
                        while ((line = br.readLine()) != null) {
                            //Ignore csv header line
                            if (!ignoreFirstLine) {

                                // use comma as separator
                                String[] order = line.split(cvsSplitBy);
                                //put in map the order id + dhl number
                                orders.put(Integer.parseInt(order[0]), order[11]);
                                System.out.format("   Proccessed order: %d   %s", Integer.parseInt(order[0]), order[11]);
                                ordersIds.add(Integer.parseInt(order[0]));

                                System.out.println("[code= " + order[0]
                                        + " , name=" + order[11] + "]");
                            }
                            ignoreFirstLine = false;


                        }


                    } catch (Exception e) {
                        redirectAttributes.addFlashAttribute("message",
                                "You failed to upload " + file.getOriginalFilename() + " => " + e.getMessage());
                    } finally {
                        if (br != null) {
                            try {
                                br.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    List<Order> ordersList = ordersDao.findAll(ordersIds);
                    for (Order order : ordersList) {
                        order.setDhlPackageNumber(orders.get(order.getId()));
                        order.setStatus(Order.OrderStatuses.IN_ORDER);
                        if (order.getType().getOrderTypeCode() < 80) {  // No external orders
                            wpPostmetaDao.updateByIdAndKey((long) order.getWpId(), "_sb_status", "" + order.getStatus().ordinal());
                        }
                    }
                    ordersDao.save(ordersList);
                } else {
                    redirectAttributes.addFlashAttribute("message",
                            "You failed to upload " + file.getOriginalFilename() + " because the file was empty");
                }
            }
        }

        boolean clearProductsOnLoad = U.var.parseInt(globalsDao.findOne("packing_clear_missing_products").getValue()) == 0 ? false : true;
        if (clearProductsOnLoad) { // Clear all missing products on csv load
            List<Product> products = productsDao.findAllMissing();
            for (Product product : products) {
                product.setIsMissing('0');
                productsDao.save(product);
            }
        }
        return "redirect:" + "/admin/manager";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/admin/order/uploadMapoCsv")
    public String uploadMapoCsv(@RequestParam("file") MultipartFile[] fileUpload,
                                RedirectAttributes redirectAttributes, HttpServletRequest request, Model model) {

        BufferedReader br = null;
        Map<Integer, String> header = new HashMap<>();
        Map<Integer, String[]> lines = new HashMap<>();
        Integer deliveryWindowName = 0;

        int count = 0;
        if (fileUpload != null && fileUpload.length > 0) {
            for (MultipartFile file : fileUpload) {

                Map<String, Long> cars = new HashMap<>();

                for (Worker worker : workersDao.findAll()) {
                    if (!worker.getAssignedCar().equals("0")) {
                        cars.put(worker.getAssignedCar(), worker.getImei());
                    }
                }

                if (file.getOriginalFilename().contains("/")) {
                    redirectAttributes.addFlashAttribute("message", "Folder separators not allowed");
                    return "redirect:upload";
                }
                if (file.getOriginalFilename().contains("/")) {
                    redirectAttributes.addFlashAttribute("message", "Relative pathnames not allowed");
                    return "redirect:upload";
                }

                if (!file.isEmpty()) {
                    Map<Integer, String> orders = new HashMap<>();

                    List<Integer> ordersIds = new LinkedList<>();
                    try {
                        BufferedOutputStream stream = new BufferedOutputStream(
                                new FileOutputStream(new File(Application.csvRoot + "/" + file.getOriginalFilename())));
                        FileCopyUtils.copy(file.getInputStream(), stream);
                        stream.close();
                        redirectAttributes.addFlashAttribute("message",
                                "You successfully uploaded " + file.getOriginalFilename() + "!");

                        String csvFile = Application.csvRoot + "/" + file.getOriginalFilename();
                        String line = "";
                        String cvsSplitBy = ",";

                        br = new BufferedReader(new InputStreamReader(new FileInputStream(csvFile), "UTF-8"));

                        boolean firstLine = true;
                        while ((line = br.readLine()) != null) {
                            if (firstLine) {
                                firstLine = false;
                                String[] headerElements = line.split(cvsSplitBy);
                                int index = 0;
                                System.out.format("   ### Index: %d%n", index);
                                for (String headerElement : headerElements) {
                                    System.out.format("   ### Index inside: %d%n", index);
                                    header.put(index, headerElement);
                                    System.out.format("   ### Element inside: %s%n", headerElement);
                                    index++;
                                }
                            }  else {
                                String[] lineElements = mySplit(line, cvsSplitBy.charAt(0));
                                String[] lineDetails = new String[header.size()];
                                for (int collumn = 0; collumn < header.size(); collumn++) {
                                    lineDetails[collumn] = lineElements[collumn];
                                }
                                lines.put(count, lineDetails);
                                count++;
                            }
                        }
                        br.close();
                    } catch (Exception e) {
                        redirectAttributes.addFlashAttribute("message",
                                "You failed to upload " + file.getOriginalFilename() + " => " + e.getMessage());
                    }

                    Character morningDelivery = 'A';
                    Character eveningDelivery = 'P';
                    Character pallete = 'A';
                    Character darom_2Pallete = 'Z';
                    Integer orderIndex = 1;
                    boolean firstPallete = true;
                    boolean contantPallete = false;
                    boolean firstOrderInRoute = true;
                    Integer lastRouteId = 0;

                    for (int index = 0; index < lines.size(); index++) {
                        Integer orderNumberInLine = lines.get(index)[4].equals("") ? 9999 : U.var.parseInt(lines.get(index)[4]);
                        Integer orderId = U.var.parseInt(lines.get(index)[18]);
                        String  orderRoute = lines.get(index)[2];
                        String  vehicle = lines.get(index)[3];
                        String  deliveryZone = lines.get(index)[29];
                        String  pointType = lines.get(index)[5];
                        String  timeOfArrival = lines.get(index)[8];

                        if (!pointType.equals("store") && orderId > 0 && orderRoute.length() > 0) {
                            String[] elements = lines.get(index);
                            StringBuilder orderTag = new StringBuilder();
                            Order order = ordersDao.findOne(orderId);
                            Short deliveryWindow = order.getDeliveryWindow();
                            Customer customer = customersDao.findOne(order.getCustomerId());
                            if (deliveryZone.equals("Darom-3")) {
                                contantPallete = true;
                            }
                            if (contantPallete) {
                                if (deliveryZone.equals("Darom-3")) {
                                    orderTag.append(darom_2Pallete);
                                    System.out.format("   ## %s%n", deliveryZone);
                                } else {
                                    orderTag.append(pallete);
                                }
                            } else {
                                orderTag.append(pallete);
                            }

                            if (order.getDeliveryWindow() == 0) {
                                orderTag.append(morningDelivery);
                            } else {
                                orderTag.append(eveningDelivery);
                            }

                            String orderIndexString = String.format("%04d", orderIndex);
                            orderTag.append(" - ").append(orderIndexString);

                            elements[2] = orderTag.toString();
                            lines.put(index, elements);
                            orderIndex++;
                            if (order.getDeliveryWindow() == 1) { // Evening
                                order.setDhlPackageNumber(orderTag.toString());
                            } else {
                                order.setDhlPackageNumber(orderTag.toString());
                            }
                            order.setStatus(Order.OrderStatuses.IN_ORDER);
                            Date estimatedArrivalTime = new Date();
                            try {
                                estimatedArrivalTime = new SimpleDateFormat("HH:mm").parse(timeOfArrival);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                            order.setEstimatedDeliveryTime(estimatedArrivalTime);

                            Long imei;
                            Worker driver = workersDao.findOneByName(vehicle);
                            String driverName = driver.getSbId();
                            if (driver == null) {
                                imei = 0L;
                            } else {
                                imei = driver.getImei();
                            }
                            System.out.format("   ### Driver name: %s IMIE: %s%n", vehicle, imei.toString());

                            String fullName = order.getFirstName() + " " + order.getLastName();
                            fullName = fullName.substring(0, Math.min(fullName.length(), 39));  // Limit name to 39 chars - SimpleMind request

                            System.out.format("   ### Time %s%n", estimatedArrivalTime);
                            String writeResult = logisticsAdmin.setBlogicOrder(orderId, imei, driverName, timeOfArrival, 0, 0, 0);
                            if (writeResult.equals("OK")) {
                                ordersDao.save(order);
                                if (firstOrderInRoute) {
                                    Routes route = routesDao.findOne(lastRouteId);
                                    Date deliveryDate = order.getDeliveryWindow() == 1 ? new DateTime(order.getDeliveryDate()).minusDays(1).toDate() : order.getDeliveryDate();
                                    route.setDeliveryDate(deliveryDate);
                                    if (deliveryZone.equals("Darom-3")) {
                                        route.setPrefix(darom_2Pallete);
                                    }
                                    routesDao.save(route);
                                    firstOrderInRoute = false;
                                }
                                RouteStop stop = new RouteStop();
                                stop.setRouteId(lastRouteId);
                                stop.setOrderId(orderId);
                                List<GeoPoint> geoPoints = geoPoinsDao.findAllByCustomerId(order.getCustomerId());
                                for (GeoPoint geoPoint : geoPoints) {
                                    boolean sameAddress = true;
                                    if (!order.getCity().equals(geoPoint.getCity())) {
                                        sameAddress = false;
                                    }
                                    if (!order.getStreet().equals(geoPoint.getStreet())) {
                                        sameAddress = false;
                                    }
                                    if (!order.getStreetNo().equals(geoPoint.getStreetNumber())) {
                                        sameAddress = false;
                                    }

                                    if (sameAddress) {
                                        stop.setGeopointId(geoPoint.getId());
                                    }
                                }
                                stop.setEstimatedDelivery(estimatedArrivalTime);
                                stop.setIsDeleted('0');
                                routeStopDao.save(stop);

                                logisticsAdmin.sendSms(orderId, 1);
                            }
                            System.out.format("   ### %s %d %d %s %s%n", pallete, orderNumberInLine, orderId, lines.get(index)[2], deliveryZone);
                        } else if (orderNumberInLine == 0) {
                            if (!firstPallete) {
                                do {
                                    System.out.format("   ### Changing pallete %s%n", pallete);
                                    int nextPallete = (int) pallete + 1; // find the int value plus 1
                                    pallete = (char) nextPallete;
                                    contantPallete = false;
                                } while (pallete == darom_2Pallete);
                            }
                            firstPallete = false;
                            orderIndex = 1;
                            if (pointType.equals("store") && orderNumberInLine == 0) {
                                Routes route = new Routes();
                                route.setPrefix(pallete);
                                route.setDirection(0);
                                route.setType(0);
                                route.setDriverName(orderRoute);
                                Integer hour = U.var.parseInt(timeOfArrival.split(":")[0]);
                                Date estimatedArrivalTime = new Date();
                                try {
                                    estimatedArrivalTime = new SimpleDateFormat("HH:mm").parse(timeOfArrival);
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                                if (hour > 12) {
                                    route.setDeliveryWindow(1);
                                } else {
                                    route.setDeliveryWindow(0);
                                }
                                route.setEstimatedStart(estimatedArrivalTime);
                                route.setIsDeleted('0');
                                routesDao.save(route);
                                lastRouteId = route.getId();
                                firstOrderInRoute = true;
                            }
                        }
                    }
                } else {
                    redirectAttributes.addFlashAttribute("message",
                            "You failed to upload " + file.getOriginalFilename() + " because the file was empty");
                }
            }
        }

        boolean clearProductsOnLoad = U.var.parseInt(globalsDao.findOne("packing_clear_missing_products").getValue()) == 0 ? false : true;
        if (clearProductsOnLoad) { // Clear all missing products on csv load
            List<Product> products = productsDao.findAllMissing();
            for (Product product : products) {
                product.setIsMissing('0');
                productsDao.save(product);
            }
        }

        Map<String, Object> globals = new HashMap<>();
        globals.put("modules_enable_coupons", globalsDao.findOne("modules_enable_coupons").getValue());
        globals.put("modules_enable_invoice", globalsDao.findOne("modules_enable_invoice").getValue());
        globals.put("modules_enable_logistics", globalsDao.findOne("modules_enable_logistics").getValue());
        globals.put("modules_enable_packing", globalsDao.findOne("modules_enable_packing").getValue());
        Object logedWorker = request.getSession().getAttribute("userId");
        Worker worker = workersDao.getOne(Integer.parseInt(logedWorker.toString()));
        globals.put("worker_name", worker.getFirstName() + " " + worker.getLastName());
        model.addAttribute("globals", globals);
        model.addAttribute("admin", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.ADMIN.ordinal() ? 1 : 0);

        return "Manager";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/admin/manager")
    public String provideUploadInfo(Model model, HttpServletRequest request) {
        if (Integer.parseInt(request.
                getSession().getAttribute("admin").toString()) != Worker.Types.ADMIN.ordinal()) {
            return "Back";
        }
        File rootFolder = new File(Application.imageRoot);
        List<String> fileNames = Arrays.stream(rootFolder.listFiles())
                .map(f -> f.getName())
                .collect(Collectors.toList());

        model.addAttribute("files",
                Arrays.stream(rootFolder.listFiles())
                        .sorted(Comparator.comparingLong(f -> -1 * f.lastModified()))
                        .map(f -> f.getName())
                        .collect(Collectors.toList())
        );

        Map<String, Object> globals = new HashMap<>();
        globals.put("modules_enable_coupons", globalsDao.findOne("modules_enable_coupons").getValue());
        globals.put("modules_enable_invoice", globalsDao.findOne("modules_enable_invoice").getValue());
        globals.put("modules_enable_logistics", globalsDao.findOne("modules_enable_logistics").getValue());
        globals.put("modules_enable_packing", globalsDao.findOne("modules_enable_packing").getValue());
        Object logedWorker = request.getSession().getAttribute("userId");
        Worker worker = workersDao.getOne(Integer.parseInt(logedWorker.toString()));
        globals.put("worker_name", worker.getFirstName() + " " + worker.getLastName());
        model.addAttribute("globals", globals);
        model.addAttribute("admin", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.ADMIN.ordinal() ? 1 : 0);

        return "Manager";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/admin/manager/workers")
    public String workers(Model model, HttpServletRequest request) {
        if (!U.var.parseBoolean(request.getSession().getAttribute("login"))) {
            return "redirect:/admin/login";
        }
        model.addAttribute("workers", workersDao.findAll());
        model.addAttribute("cars", carsDao.findAll());
        Map<String, Object> globals = new HashMap<>();
        globals.put("modules_enable_coupons", globalsDao.findOne("modules_enable_coupons").getValue());
        globals.put("modules_enable_invoice", globalsDao.findOne("modules_enable_invoice").getValue());
        globals.put("modules_enable_logistics", globalsDao.findOne("modules_enable_logistics").getValue());
        globals.put("modules_enable_packing", globalsDao.findOne("modules_enable_packing").getValue());
        Object logedWorker = request.getSession().getAttribute("userId");
        Worker worker = workersDao.getOne(Integer.parseInt(logedWorker.toString()));
        globals.put("worker_name", worker.getFirstName() + " " + worker.getLastName());
        model.addAttribute("globals", globals);
        model.addAttribute("admin", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.ADMIN.ordinal() ? 1 : 0);
        return "Workers";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/admin/submit/worker")
    public String submitWorker(@RequestParam(value = "id") Integer id,
                               @RequestParam(value = "username") String username,
                               @RequestParam(value = "type") Integer type,
                               @RequestParam(value = "firstName") String firstName,
                               @RequestParam(value = "lastName") String lastName,
                               @RequestParam(value = "sbId") String sbId,
                               @RequestParam(value = "isrId") String isrId,
                               @RequestParam(value = "email") String email,
                               @RequestParam(value = "imei", required = false) Long imei,
                               @RequestParam(value = "car", required = false) String car,
                               HttpServletRequest request) {
        if (!U.var.parseBoolean(request.getSession().getAttribute("login"))) {
            return "redirect:/admin/login";
        }
        Worker worker = workersDao.findOne(id);
        if (worker != null) {
            worker.setUsername(username);
            worker.setType(Worker.Types.values()[type]);
            worker.setFirstName(firstName);
            worker.setLastName(lastName);
            worker.setSbId(sbId);
            worker.setIsrId(isrId);
            worker.setEmail(email);
            if (type == Worker.Types.DELIVERY.ordinal()) {
                worker.setImei(imei);
                worker.setAssignedCar(car);
            } else {
                worker.setImei(0L);
                worker.setAssignedCar("0");
            }
            workersDao.save(worker);
        }
        return "redirect:/admin/manager/workers";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/admin/submit/addWorker")
    public String submitAddWorker() {
        workersDao.save(new Worker("", "", Worker.Types.PACKER, "", "", "", "", ""));
        return "redirect:/admin/manager/workers";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/admin/submit/password")
    public String submitPassword(@RequestParam(value = "passwordId") Integer passwordId,
                                 @RequestParam(value = "password") String password,
                                 @RequestParam(value = "password2") String password2) {
        if (password.equals(password2)) {
            workersDao.updatePassword(passwordId, password);
        }
        return "redirect:/admin/manager/workers";
    }

    @RequestMapping(value = "/admin/manager/globals/submit/globalValue", method = RequestMethod.POST)
    public Object updateGlobalsValue(@RequestParam(value = "globalKey") String globalKey, @RequestParam(value = "newValue") String newValue, HttpServletRequest request) {
        Global myGlobal = globalsDao.findOne(globalKey);
        myGlobal.setValue(newValue);
        globalsDao.save(myGlobal);
        return "redirect:/admin/manager/globals";
    }

    @RequestMapping(value = "/admin/exportCron", method = RequestMethod.GET)
    public Object exportCron(HttpServletRequest request) {
        try {
            Process p = Runtime.getRuntime().exec(U.config.get("shookbook.sync.sh"));
            p.waitFor();
        } catch (Exception ex) {
            logger.info("error run sync", ex);
        }

        List<Customer> customers = customersDao.findAllWithoutBarcode();
        for (Customer customer : customers) {
            customer.setBarcode(common.createShookbookID());
        }
        customersDao.save(customers);
        logger.info("SB: cron: sync ");
        return "Back";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/admin/manager/uploadExternalOrders")
    public String uploadExternalOrders(@RequestParam("file") MultipartFile[] fileUpload,
                                       RedirectAttributes redirectAttributes) {

        System.out.format("   ### Loading external orders%n");
        BufferedReader br = null;
        Map<Integer, String> header = new HashMap<>();
        Map<Integer, String[]> lines = new HashMap<>();

        int count = 0;
        if (fileUpload != null && fileUpload.length > 0) {
            for (MultipartFile file : fileUpload) {

                if (file.getOriginalFilename().contains("/")) {
                    redirectAttributes.addFlashAttribute("message", "Folder separators not allowed");
                    return "redirect:upload";
                }
                if (file.getOriginalFilename().contains("/")) {
                    redirectAttributes.addFlashAttribute("message", "Relative pathnames not allowed");
                    return "redirect:upload";
                }

                if (!file.isEmpty()) {
                    Map<Integer, String> orders = new HashMap<>();

                    List<Integer> ordersIds = new LinkedList<>();
                    try {
                        BufferedOutputStream stream = new BufferedOutputStream(
                                new FileOutputStream(new File(Application.csvRoot + "/" + file.getOriginalFilename())));
                        FileCopyUtils.copy(file.getInputStream(), stream);
                        stream.close();
                        redirectAttributes.addFlashAttribute("message",
                                "You successfully uploaded " + file.getOriginalFilename() + "!");

                        String csvFile = Application.csvRoot + "/" + file.getOriginalFilename();
                        String line = "";
                        String cvsSplitBy = ",";

                        br = new BufferedReader(new InputStreamReader(new FileInputStream(csvFile), "UTF-8"));

                        boolean firstLine = true;
                        while ((line = br.readLine()) != null) {
                            System.out.format("   ### Line: %s%n", line);
                            if (firstLine) {
                                System.out.format(" ### I was here%n");
                                firstLine = false;
                                String[] headerElements = line.split(cvsSplitBy);
                                int index = 0;
                                for (String headerElement : headerElements) {
                                    header.put(index, headerElement);
                                    index++;
                                }
                            }  else {
                                String[] lineElements = mySplit(line, cvsSplitBy.charAt(0));
                                System.out.format("  ### Size: %d%n", lineElements.length);
                                String[] lineDetails = new String[header.size()];
                                for (int collumn = 0; collumn < header.size(); collumn++) {
                                    lineDetails[collumn] = lineElements[collumn];
                                    System.out.format("  ### Element: %s%n", lineElements[collumn]);
                                }
                                lines.put(count, lineDetails);
                                count++;
                                System.out.format(" ### I was here %d%n", count);
                            }
                        }
                        br.close();
                    } catch (Exception e) {
                        redirectAttributes.addFlashAttribute("message",
                                "You failed to upload " + file.getOriginalFilename() + " => " + e.getMessage());
                    }

                    System.out.format("   ### Got %d orders to load%n", lines.size());
                    for (int index = 0; index < lines.size(); index++) {
                        Integer externalOrderId = U.var.parseInt(lines.get(index)[3]);
                        String customerEmail = lines.get(index)[0];

                        Customer customer = customersDao.findOneByEmail(customerEmail);
                        if (customer == null) { // create new customer
                            System.out.format("   ### Got new customer %s%n", lines.get(index)[0]);
                            customer = new Customer();
                            customer.setWpId(0);
                            customer.setFirstName(lines.get(index)[1]);
                            customer.setLastName(lines.get(index)[2]);
                            customer.setEmail(lines.get(index)[0]);
                            customer.setPhoneNo(lines.get(index)[23]);
                            customer.setPhoneExt(lines.get(index)[22]);
                            customer.setPhone(lines.get(index)[22] + '-' + lines.get(index)[23]);
                            customer.setPhone2No(lines.get(index)[25]);
                            customer.setPhone2Ext(lines.get(index)[24]);
                            customer.setPhone2(lines.get(index)[24] + '-' + lines.get(index)[25]);
                            customer.setCity(lines.get(index)[10]);
                            customer.setStreet(lines.get(index)[11]);
                            customer.setStreetNo(lines.get(index)[12]);
                            customer.setApartment(lines.get(index)[13]);
                            customer.setFloorNo(lines.get(index)[14]);
                            customer.setEntryCode(lines.get(index)[15]);
                            customer.setBarcode(common.createShookbookID());
                            customer.setAttentionFlag(Customer.AlertTypes.NONE);
                            customer.setCreditCardHide('0');
                            customer.setForceGeotag(0);
                            customer.setInvoiceOnly('0');
                            customer.setIsDeleted('0');
                            customer.setCouponGroup(0);
                            customer.setEnvironmentFriendly('0');
                            customer.setType(Customer.CustomerTypes.SILVER);
                            customersDao.save(customer);
                        }

                        System.out.format("   ### Adding order number %d%n", externalOrderId);
                        Order order = new Order();
                        order.setWpId(externalOrderId);
                        order.setCustomerId(customer.getId());
                        order.setBarcode(externalOrderId.toString());
                        order.setSum(U.var.parseFloat(lines.get(index)[7]));
                        order.setDiscountSum(0F);
                        Integer boxes = U.var.parseInt(lines.get(index)[8]);
                        order.setBoxes(boxes.shortValue());
                        order.setRemarks(lines.get(index)[9]);
                        order.setType(Order.OrderTypes.EXTRNAL_YAROK);
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                        SimpleDateFormat externalDateFormat = new SimpleDateFormat("dd/MM/yy");
                        Date deliveryDate = new Date();
                        try {
                            deliveryDate = externalDateFormat.parse(lines.get(index)[5]);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        System.out.format("   # Delivery: %s%n", deliveryDate);
                        order.setDeliveryDate(deliveryDate);
                        order.setShippingDate(deliveryDate);
                        Integer deliveryWindow = U.var.parseInt(lines.get(index)[6]);
                        order.setDeliveryWindow(deliveryWindow.shortValue());
                        order.setStatus(Order.OrderStatuses.HARVEST);
                        order.setFirstName(customer.getFirstName());
                        order.setLastName(customer.getLastName());
                        if (lines.get(index)[16] == "") {
                            order.setCity(lines.get(index)[10]);
                            order.setStreet(lines.get(index)[11]);
                            order.setStreetNo(lines.get(index)[12]);
                            order.setApartment(lines.get(index)[13]);
                            order.setFloorNo(lines.get(index)[14]);
                            order.setEntryCode(lines.get(index)[15]);
                        } else {
                            order.setCity(lines.get(index)[16]);
                            order.setStreet(lines.get(index)[17]);
                            order.setStreetNo(lines.get(index)[18]);
                            order.setApartment(lines.get(index)[19]);
                            order.setFloorNo(lines.get(index)[20]);
                            order.setEntryCode(lines.get(index)[21]);
                        }
                        order.setCertificated('1');
                        order.setClearingStatus(Order.ClearStatus.NO_CHARGE);
                        order.setIsDeleted('0');
                        order.setCreatedDate(new Date(System.currentTimeMillis()));
                        order.setRefrigerated('0');
                        order.setVat(0);
                        order.setPackingWorkerId(0);
                        order.setDispatcherWorkerId(0);
                        order.setSmsCode(0);

                        ordersDao.save(order);
                    }
                } else {
                    redirectAttributes.addFlashAttribute("message",
                            "You failed to upload " + file.getOriginalFilename() + " because the file was empty");
                }
            }
        }

        boolean clearProductsOnLoad = U.var.parseInt(globalsDao.findOne("packing_clear_missing_products").getValue()) == 0 ? false : true;
        if (clearProductsOnLoad) { // Clear all missing products on csv load
            List<Product> products = productsDao.findAllMissing();
            for (Product product : products) {
                product.setIsMissing('0');
                productsDao.save(product);
            }
        }
        return "redirect:" + "/admin/manager";
    }

    @RequestMapping(value = "/admin/export", method = RequestMethod.GET)
    public Object export(HttpServletRequest request) {
//        U.http.stringFromUrl(U.config.get("shookbook.sync.url"));
        try {
            Process p = Runtime.getRuntime().exec(U.config.get("shookbook.sync.sh"));
            p.waitFor();
        } catch (Exception ex) {
            logger.info("error run sync", ex);
        }

        List<Customer> customers = customersDao.findAllWithoutBarcode();
        for (Customer customer : customers) {
            customer.setBarcode(common.createShookbookID());
        }
        customersDao.save(customers);
        logger.info("SB: " + request.getSession().getAttribute("userName") + ": sync ");
        return "Back";
    }

    @RequestMapping(value = "/admin/checkMobileOrdersCron", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public void checkMobileOrdersCron(HttpServletRequest request) {
        //System.out.format("   Sending...%n");
        Float delta = U.var.parseFloat(globalsDao.findOne("app_order_notification_threshold").getValue());
        List<WpPost> mobilePendingOrders = wpPostsDao.findAllMobilePending(delta);
        MobileNotification notification = mobileNotificationsDao.findOneByGroupId(999);
        String text = notification.getText();
        String deviceToken = "";
        for (WpPost order : mobilePendingOrders) {
            Long userId = U.var.parseLong(wpPostmetaDao.findOneByIdAndKey(order.getId(), "_customer_user").getMetaValue());
            WpUser user = wpUsersDao.findOne(userId);
            //System.out.format("%n   *** Ziv is trying to send %s to %d %d%n", text, U.var.parseInt(userId), user.getId());
            try {
                //System.out.format("   *** Sending to: %d $d ...%n%n", U.var.parseInt(userId), user.getId());
                deviceToken = wpUsermetaDao.findOneByIdAndKey(user.getId(), "app_device_token").getMetaValue();
                common.sendMobileNotification(deviceToken, text);
            } catch (Exception ex) {
                deviceToken = "";
                System.out.format("%n *** Error in sending to user ID: %d%n", userId);
            }

            //System.out.format("   Sending: %s   To: %s%n", text, deviceToken);

        }
        logger.info("SB: cron: Notify " + mobilePendingOrders.size() + " orders upon order not completed");
    }

    @RequestMapping(value = "/admin/updateCouponGroupCron", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public void updateCouponGroupCron(HttpServletRequest request) {

        List<Customer> customers = customersDao.findAllMidrasha();
        for (Customer customer : customers) {
            customer.setForceGeotag(2);
            customer.setCouponGroup(2);
            customersDao.save(customer);
        }

        logger.info("SB: cron: Added coupon group to " + customers.size() + " customers");
    }

    public String[] mySplit(String line, Character seperator) {
        Map<Integer, String> elements = new HashMap<>();
        int count = 0;
        boolean ignoreSeperation = false;
        String element = "";
        for (int index = 0; index < line.length(); index++) {
            if (line.charAt(index) == '"') {
                ignoreSeperation = !ignoreSeperation;
            }
            if ((line.charAt(index) == seperator) && !ignoreSeperation) {
                elements.put(count, element);
                element = "";
                count++;
            } else {
                element += line.charAt(index);
            }
        }

        if (line.charAt(line.length()-1) == seperator) {
            elements.put(count, "");
        }

        String[] result = new String[elements.size()];
        for (int index = 0; index < elements.size(); index++) {
            result[index] = elements.get(index);
        }

        return result;
    }
}

