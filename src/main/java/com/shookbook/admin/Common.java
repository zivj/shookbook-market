package com.shookbook.admin;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClient;
import com.amazonaws.services.simpleemail.model.*;
import com.shookbook.dao.shookbook.GlobalsDao;
import com.shookbook.dao.shookbook.OrderProductDao;
import com.shookbook.dao.shookbook.OrdersDao;
import com.shookbook.dao.shookbook.ProductsDao;
import com.shookbook.entities.shookbook.OrderProduct;
import com.shookbook.utils.U;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.BodyPart;
import javax.mail.Session;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.ByteBuffer;
import java.text.DecimalFormat;
import java.util.*;

@Component
public class Common {

    @Autowired
    private ProductsDao productsDao;
    @Autowired
    private OrdersDao ordersDao;
    @Autowired
    private OrderProductDao orderProductDao;
    @Autowired
    private GlobalsDao globalsDao;

    final static int SUPPLIER_CODE = 30;
    final static int CERTIFICATE_CODE = 20;
    final static int CUSTOMER_CODE = 10;

    /*
    static int enableCouponModule = 0;
    static int enableInvoiceModule = 0;
    static int enableLogisticsModule = 0;
    static int enablePackingModule = 0;
    static String userName = "";
    static int customerSupportPrivilege = 0;
    static int adminPrivilege = 0;
    static String interfaceLanguage = "IL";
    static String languageDir = "rtl";
*/
    public String createShookbookID() {
        //create customer id :
        StringBuilder barcode = new StringBuilder();
        StringBuilder secondPart = new StringBuilder();
        double d = (Math.round(Math.random() * 89999) + 10000);
        String nnnnn = new DecimalFormat("#####").format(d);
        barcode.append(nnnnn);
        Calendar calendar = new GregorianCalendar();
        Date trialTime = new Date();
        calendar.setTime(trialTime);
        //the week number of the year with two digits
        int woy = calendar.get(Calendar.WEEK_OF_YEAR);
        String yy = String.format("%02d", woy);
        secondPart.append(yy);
        //area zone
        int zz = 30;
        secondPart.append(zz);
        //The day of the week the customer enlisted
        int x = calendar.get(Calendar.DAY_OF_WEEK);
        secondPart.append(x);
        int m = (Integer.parseInt(barcode.toString()) ^ Integer.parseInt(secondPart.toString())) % 10;
        barcode.append(secondPart + "-" + m);

        return barcode.toString();
    }

    public String createOrderCode(int supplierId) {
        StringBuilder orderCode = new StringBuilder();
        StringBuilder tmp = new StringBuilder();
        orderCode.append(CUSTOMER_CODE + "-" + String.format("%010d", supplierId) + "-");
        tmp.append(CUSTOMER_CODE + String.format("%010d", supplierId));
        int res = Integer.parseInt(tmp.substring(0, 1));
        for (int i = 1; i < tmp.length(); i++) {
            res = (Integer.parseInt(tmp.substring(i, i + 1))) ^ res;
        }
        res = res % 10;
        orderCode.append(res);
        return orderCode.toString();
    }

    public String createCertificateCode(int orderId) {
        StringBuilder orderCode = new StringBuilder();
        StringBuilder tmp = new StringBuilder();
        orderCode.append(CERTIFICATE_CODE + "-" + String.format("%010d", orderId) + "-");
        tmp.append(CERTIFICATE_CODE + String.format("%010d", orderId));
        int res = Integer.parseInt(tmp.substring(0, 1));
        for (int i = 1; i < tmp.length(); i++) {
            res = (Integer.parseInt(tmp.substring(i, i + 1))) ^ res;
        }
        res = res % 10;
        orderCode.append(res);
        return orderCode.toString();
    }

    public String pedSupplierNumber(int supplierId) {
        return String.format("%04d", supplierId);
    }

    public String createSupplierOrderSBID(int supplierId, int supplierOrderId) {

        StringBuilder barcode = new StringBuilder();
        StringBuilder str = new StringBuilder();

        barcode.append(SUPPLIER_CODE + "-" + String.format("%04d", supplierId) + "-" + String.format("%06d", supplierOrderId) + "-");
        str.append(SUPPLIER_CODE + String.format("%04d", supplierId) + String.format("%06d", supplierOrderId));
        int res = Integer.parseInt(str.substring(0, 1));
        //    System.out.println("RES :: " +res);
        for (int i = 1; i < str.length(); i++) {
            res = (Integer.parseInt(str.substring(i, i + 1))) ^ res;
        }
        res = res % 10;
        barcode.append(res);

        return barcode.toString();
    }

    public void export() {
        U.http.stringFromUrl(U.config.get("shookbook.export.url"));
    }

    public float calcTotalSumWithoutShipmentFee(int orderId) {
        float sum = 0;
        for (OrderProduct orderProduct : orderProductDao.findAllByOrderId(orderId))
            sum += orderProduct.getUnitPrice() * orderProduct.getQuantity();

        return sum;
    }

    public float calcTotalOriginalSumWithoutShipmentFee(int orderId) {
        float sum = 0;
        for (OrderProduct orderProduct : orderProductDao.findAllByOrderId(orderId))
            sum += orderProduct.getUnitPrice() * orderProduct.getQuantityNeeded();

        return sum;
    }

    public void sendEmail(String to, String subject, String html) {
        Destination destination = new Destination().withToAddresses(new String[]{to});

        Message message = new Message().
                withSubject(new Content().withData(subject)).
                withBody(new Body().withHtml(new Content().withData(html)));

        SendEmailRequest request = new SendEmailRequest().withSource(U.config.get("shookbook.mail.from")).withDestination(destination).withReplyToAddresses("customer_support@shook-book.com").withMessage(message);

        try {
            AmazonSimpleEmailServiceClient client = new AmazonSimpleEmailServiceClient(new BasicAWSCredentials(U.config.get("shookbook.aws.accessKey"), U.config.get("shookbook.aws.secretKey")));

            Region REGION = Region.getRegion(Regions.EU_WEST_1);
            client.setRegion(REGION);

            client.sendEmail(request);
        } catch (Exception ex) {
            System.out.println("The email was not sent.");
            System.out.println("Error message: " + ex.getMessage());
        }
    }

    public void sendEmailFrom(String from, String to, String subject, String html) {
        String source;
        switch (from) {
            case "orders":
                source = "orders@shook-book.com";
                break;
            case "customer_support":
                source = "customer_support@shook-book.com";
                break;
            default:
                source = "orders@shook-book.com";
                break;
        }

        Destination destination = new Destination().withToAddresses(new String[]{to});


        Message message = new Message().
                withSubject(new Content().withData(subject)).
                withBody(new Body().withHtml(new Content().withData(html)));

        SendEmailRequest request = new SendEmailRequest().withSource(source).withDestination(destination).withReplyToAddresses("customer_support@shook-book.com").withMessage(message);

        try {
            AmazonSimpleEmailServiceClient client = new AmazonSimpleEmailServiceClient(new BasicAWSCredentials(U.config.get("shookbook.aws.accessKey"), U.config.get("shookbook.aws.secretKey")));

            Region REGION = Region.getRegion(Regions.EU_WEST_1);
            client.setRegion(REGION);

            client.sendEmail(request);
        } catch (Exception ex) {
            System.out.println("The email was not sent.");
            System.out.println("Error message: " + ex.getMessage());
        }
    }

    public void sendEmailWithCc(String to, String cc, String subject, String html) {
        String source;
        source = "orders@shook-book.com";

        Destination destination = new Destination().withToAddresses(new String[]{to}).withCcAddresses(cc.split(","));


        Message message = new Message().
                withSubject(new Content().withData(subject)).
                withBody(new Body().withHtml(new Content().withData(html)));

        SendEmailRequest request = new SendEmailRequest().withSource(source).withDestination(destination).withReplyToAddresses("customer_support@shook-book.com").withMessage(message);

        try {
            AmazonSimpleEmailServiceClient client = new AmazonSimpleEmailServiceClient(new BasicAWSCredentials(U.config.get("shookbook.aws.accessKey"), U.config.get("shookbook.aws.secretKey")));

            Region REGION = Region.getRegion(Regions.EU_WEST_1);
            client.setRegion(REGION);

            client.sendEmail(request);
        } catch (Exception ex) {
            System.out.println("The email was not sent.");
            System.out.println("Error message: " + ex.getMessage());
        }
    }

    public void sendEmailWithBcc(String to, String bcc, String subject, String html) {
        String source;
        source = "orders@shook-book.com";

        Destination destination = new Destination().withToAddresses(new String[]{to}).withBccAddresses(bcc.split(","));


        Message message = new Message().
                withSubject(new Content().withData(subject)).
                withBody(new Body().withHtml(new Content().withData(html)));

        SendEmailRequest request = new SendEmailRequest().withSource(source).withDestination(destination).withReplyToAddresses("customer_support@shook-book.com").withMessage(message);

        try {
            AmazonSimpleEmailServiceClient client = new AmazonSimpleEmailServiceClient(new BasicAWSCredentials(U.config.get("shookbook.aws.accessKey"), U.config.get("shookbook.aws.secretKey")));

            Region REGION = Region.getRegion(Regions.EU_WEST_1);
            client.setRegion(REGION);

            client.sendEmail(request);
        } catch (Exception ex) {
            System.out.println("The email was not sent.");
            System.out.println("Error message: " + ex.getMessage());
        }
    }

    public void sendEmailWithAttachment(String from, String to, String subject, String html, byte[] attachment, String filename, String mimeType) {
        try {

            String source;
            switch (from) {
                case "orders":
                    source = "orders@shook-book.com";
                    break;
                case "customer_support":
                    source = "customer_support@shook-book.com";
                    break;
                default:
                    source = "orders@shook-book.com";
                    break;
            }

            Session session = Session.getInstance(new Properties());
            MimeMessage mimeMessage = new MimeMessage(session);
            mimeMessage.setSubject(subject);

            MimeMultipart mimeMultipart = new MimeMultipart();

            BodyPart p = new MimeBodyPart();
            p.setContent(html, "text/html; charset=utf-8");
            mimeMultipart.addBodyPart(p);

            MimeBodyPart mimeBodyPart = new MimeBodyPart();
            mimeBodyPart.setFileName(filename);
            mimeBodyPart.setDescription("Attachment", "UTF-8");
            DataSource ds = new ByteArrayDataSource(attachment, mimeType);
            mimeBodyPart.setDataHandler(new DataHandler(ds));
            mimeMultipart.addBodyPart(mimeBodyPart);

            mimeMessage.setContent(mimeMultipart);

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            mimeMessage.writeTo(outputStream);

            RawMessage rawMessage = new RawMessage(ByteBuffer.wrap(outputStream.toByteArray()));

            SendRawEmailRequest rawEmailRequest = new SendRawEmailRequest(rawMessage);
            rawEmailRequest.setDestinations(Arrays.asList(new String[]{to}));
            rawEmailRequest.setSource(source);

            AmazonSimpleEmailServiceClient client = new AmazonSimpleEmailServiceClient(new BasicAWSCredentials(U.config.get("shookbook.aws.accessKey"), U.config.get("shookbook.aws.secretKey")));

            Region REGION = Region.getRegion(Regions.EU_WEST_1);
            client.setRegion(REGION);

            client.sendRawEmail(rawEmailRequest);
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("The email was not sent2.");
            System.out.println("Error message: " + ex.getMessage());
        }

    }

    public void sendMobileNotification(String deviceToken, String notificationText) {
        try {
            HttpURLConnection httpcon = (HttpURLConnection) ((new URL("https://fcm.googleapis.com/fcm/send").openConnection()));
            httpcon.setDoOutput(true);
            httpcon.setRequestProperty("Content-Type", "application/json");
            httpcon.setRequestProperty("Authorization", "key=AAAAxQRSu8s:APA91bFfYPz8juTyT7tSV_09Yr6GqqIPFrAtGSox-o1ZNvSs-v-pCuojcqn4iU-0eaNnRFvcqlQuBX-ZL_gcqFQFcZcUF2U5AdEyWQa_amGeEKCcYb63CahAnoW1y6G2cALZ9HzDhwTE");
            httpcon.setRequestMethod("POST");
            httpcon.connect();
            System.out.println("Connected!");

            StringBuilder stringBuilder = new StringBuilder("{\"to\":\"").append(deviceToken).append("\",\"notification\":{\"body\":\"").append(notificationText).append("\"}}");
            String connString = stringBuilder.toString();
            System.out.format("   Sending string: %s%n",connString);
            byte[] outputBytes = connString.getBytes("UTF-8");
            httpcon.getOutputStream().write(outputBytes);
            Reader input = new BufferedReader(new InputStreamReader(httpcon.getInputStream(), "UTF-8"));

            StringBuilder response = new StringBuilder();

            for (int c; (c = input.read()) >= 0; ) {
                response.append((char) c);
            }
            System.out.println("   Http POST request sent!");
            System.out.format("    Http response: %s%n", response);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public Double[] getGoogleGeoCode (String address) {
        Double [] coordinates = {0.0D, 0.0D};
        String googleAPI = "AIzaSyD78NqTX9Sr868d_yF8fRfc5-aboD4NilE";

        try {
            URL url = new URL("https://maps.googleapis.com/maps/api/geocode/json?address=\"" + U.var.urlEncode(address) + "\"&key=" + googleAPI);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            String output = "", full = "";
            String latStr = "", lngStr = "", location = "";
            Boolean foundGeometry = false;
            Boolean foundLocation = false;
            Boolean done = false;
            Boolean gotLat = false;

            while ((output = br.readLine()) != null) {
                if (output.contains("\"geometry\" : {")) {
                    foundGeometry = true;
                }
                if (foundGeometry && output.contains("\"location\" : {")) {
                    foundLocation = true;
                    location = output;
                }
                if (!done && foundLocation && output.contains("\"lat\" : ")) {
                    latStr = output;
                }
                if (!done && foundLocation && output.contains("\"lng\" : ")) {
                    lngStr = output;
                    done = true;
                }
                full += output;
            }

            String [] elements = latStr.split(" ");
            Integer index = 0;
            Double lat = 0.0D, lng = 0.0D;
            foundLocation = false;
            for (String element : elements) {
                if (foundLocation) {
                    coordinates[0] = U.var.parseDouble(element.substring(0, element.length() - 1));
                    foundLocation = false;
                }
                if (element.equals(":")) {
                    foundLocation = true;
                }
            }

            elements = lngStr.split(" ");
            foundLocation = false;
            for (String element : elements) {
                if (foundLocation) {
                    coordinates[1] = U.var.parseDouble(element);
                    foundLocation = false;
                }
                if (element.equals(":")) {
                    foundLocation = true;
                }
            }

            conn.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return coordinates;
    }
}
