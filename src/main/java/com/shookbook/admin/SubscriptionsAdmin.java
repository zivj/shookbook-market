package com.shookbook.admin;

import com.shookbook.dao.shookbook.*;
import com.shookbook.dao.wordpress.WpOrderItemmetaDao;
import com.shookbook.dao.wordpress.WpOrderItemsDao;
import com.shookbook.dao.wordpress.WpPostmetaDao;
import com.shookbook.dao.wordpress.WpPostsDao;
import com.shookbook.entities.shookbook.Customer;
import com.shookbook.entities.shookbook.Order;
import com.shookbook.entities.shookbook.Product;
import com.shookbook.entities.shookbook.Subscription;
import com.shookbook.entities.wordpress.WpOrderItem;
import com.shookbook.entities.wordpress.WpOrderItemmeta;
import com.shookbook.entities.wordpress.WpPost;
import com.shookbook.entities.wordpress.WpPostmeta;
import com.shookbook.utils.U;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.SessionAttributes;

import javax.servlet.http.HttpServletRequest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@SessionAttributes("thought")
public class SubscriptionsAdmin {
    @Autowired
    private Common common;
    @Autowired
    private LogisticsAdmin logisticsAdmin;
    @Autowired
    private ProductsDao productsDao;
    @Autowired
    private CustomersDao customersDao;
    @Autowired
    private OrdersDao ordersDao;
    @Autowired
    private OrderProductDao orderProductDao;
    @Autowired
    private ItemsDao itemsDao;
    @Autowired
    private SuppliersDao suppliersDao;
    @Autowired
    private SupplierOrdersDao supplierOrdersDao;
    @Autowired
    private SupplierOrderItemDao supplierOrderItemDao;
    @Autowired
    private WorkersDao workersDao;
    @Autowired
    private SubscriptionsDao subscriptionsDao;
    @Autowired
    private GlobalsDao globalsDao;
    @Autowired
    private CouponsDao couponsDao;
    @Autowired
    private WpPostsDao wpPostsDao;
    @Autowired
    private WpPostmetaDao wpPostmetaDao;
    @Autowired
    private WpOrderItemsDao wpOrderItemsDao;
    @Autowired
    private WpOrderItemmetaDao wpOrderItemmetaDao;

    @RequestMapping(value = "/admin/subscription/test", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public void generateTest(HttpServletRequest request) {
        DateFormat completeDateDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        DateFormat shippingDeliveryDateDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        List<Subscription> allSubscriptions = subscriptionsDao.findAll();
        Date deliveryDate = new Date();

        for ( Subscription subscription : allSubscriptions) {
            Customer customer = customersDao.getOne(subscription.getCustomerId());
            Order lastOrder = ordersDao.findOneByCustomerIdDesc(subscription.getCustomerId());
            boolean creditExpired = false;
            Map<Integer, Integer> orderedProductsMap = new HashMap<>();
            Map<Integer, Integer> missingProductsMap = new HashMap<>();
            if ((subscription.getIsDeleted().equals('0')) && (subscription.getValid().equals('1'))) {
                if ((customer.getCreditCardHide().equals('0')) && !creditExpired) {  // If credit card valid
                    String[] products = subscription.getCart().split(";");

                    deliveryDate = subscriptionGetDeliveryDate(U.var.parseInt(subscription.getDayOfDelivery()), subscription, customer);
                }
            }
        }
    }

    @RequestMapping(value = "/admin/subscription/generateOrdersCron", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public void generateOrdersCron(HttpServletRequest request) {
        DateFormat completeDateDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        DateFormat shippingDeliveryDateDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        List<Subscription> allSubscriptions = subscriptionsDao.findAll();
        Date now = new Date();

        for ( Subscription subscription : allSubscriptions) {
            Customer customer = customersDao.getOne(subscription.getCustomerId());
            Order lastOrder = ordersDao.findOneByCustomerIdDesc(subscription.getCustomerId());
            boolean creditExpired = false;
            Map<Integer, Integer> orderedProductsMap = new HashMap<>();
            Map<Integer, Integer> missingProductsMap = new HashMap<>();
            if ((subscription.getIsDeleted().equals('0')) && (subscription.getValid().equals('1'))) {
                if (((customer.getCreditCardHide().equals('0') && !creditExpired)) || (customer.getInvoiceOnly() == 1)) {  // If credit card valid
                    String[] products = subscription.getCart().split(";");

                    Date deliveryDate = subscriptionGetDeliveryDate(U.var.parseInt(subscription.getDayOfDelivery()), subscription, customer);
                    if (deliveryDate.after(now)) {
                        Integer deliveryPrice = customer.getType() != Customer.CustomerTypes.SILVER ? 18 : 35;
                        Float couponDiscount = 0F;

                        String orderKey = "order_" + Long.toHexString(new Random().nextLong()).substring(3); //TODO change
                        Float orderTotal = 0F;
                        WpPost wpPost = wpPostsDao.save(new WpPost(orderKey, "", products.length / 2));
                        boolean firstProduct = true;
                        for (int index = 0; index < products.length; index = index + 2) {
                            Integer productId = checkProductAvailability(U.var.parseInt(products[index]), U.var.parseInt(products[index + 1]), '0');
                            Integer productQty = U.var.parseInt(products[index + 1]);
                            if (productId >= 0) {
                                Product product = productsDao.findOneByWpId(productId);
                                orderTotal += product.getPrice() * productQty;
                                if (firstProduct) {
                                    List<WpPostmeta> wpPostmetas = wpPostmetaDao.save(Arrays.asList(new WpPostmeta[]{
                                            new WpPostmeta(wpPost.getId(), "_order_key", orderKey),
                                            new WpPostmeta(wpPost.getId(), "_order_currency", "ILS"),
                                            new WpPostmeta(wpPost.getId(), "_prices_include_tax", "no"),
                                            new WpPostmeta(wpPost.getId(), "_customer_ip_address", U.var.isEmptyStr(request.getHeader("X-Forwarded-For")) ? request.getRemoteAddr() : request.getHeader("X-Forwarded-For")),
                                            new WpPostmeta(wpPost.getId(), "_customer_user_agent", request.getHeader("User-Agent")),
                                            new WpPostmeta(wpPost.getId(), "_customer_user", "" + customer.getWpId()),
                                            new WpPostmeta(wpPost.getId(), "_created_via", "checkout"),
                                            new WpPostmeta(wpPost.getId(), "_order_version", "2.5.3"),
                                            new WpPostmeta(wpPost.getId(), "_billing_first_name", customer.getFirstName()),
                                            new WpPostmeta(wpPost.getId(), "_billing_last_name", customer.getLastName()),
                                            new WpPostmeta(wpPost.getId(), "_billing_company", ""),
                                            new WpPostmeta(wpPost.getId(), "_billing_email", customer.getEmail()),
                                            new WpPostmeta(wpPost.getId(), "_billing_phone", customer.getPhoneNo()),
                                            new WpPostmeta(wpPost.getId(), "_billing_country", "IL"),
                                            new WpPostmeta(wpPost.getId(), "_billing_address_1", customer.getCity() + " " + customer.getStreetNo()),
                                            new WpPostmeta(wpPost.getId(), "_billing_address_2", ""),
                                            new WpPostmeta(wpPost.getId(), "_billing_city", customer.getCity()),
                                            new WpPostmeta(wpPost.getId(), "_billing_state", ""),
                                            new WpPostmeta(wpPost.getId(), "_billing_postcode", "0"),
                                            new WpPostmeta(wpPost.getId(), "_billing_street", customer.getStreet()),
                                            new WpPostmeta(wpPost.getId(), "_billing_street_no", customer.getStreetNo()),
                                            new WpPostmeta(wpPost.getId(), "_billing_floor_no", customer.getFloorNo()),
                                            new WpPostmeta(wpPost.getId(), "_billing_apartment", customer.getApartment()),
                                            new WpPostmeta(wpPost.getId(), "_billing_entry_code", customer.getEntryCode()),
                                            new WpPostmeta(wpPost.getId(), "_shipping_first_name", subscription.getFirstName()),
                                            new WpPostmeta(wpPost.getId(), "_shipping_last_name", subscription.getLastName()),
                                            new WpPostmeta(wpPost.getId(), "_shipping_country", "IL"),
                                            new WpPostmeta(wpPost.getId(), "_shipping_address_1", subscription.getStreet() + " " + subscription.getStreetNo()),
                                            new WpPostmeta(wpPost.getId(), "_shipping_address_2", ""),
                                            new WpPostmeta(wpPost.getId(), "_shipping_city", subscription.getCity()),
                                            new WpPostmeta(wpPost.getId(), "_shipping_state", ""),
                                            new WpPostmeta(wpPost.getId(), "_shipping_street", subscription.getStreet()),
                                            new WpPostmeta(wpPost.getId(), "_shipping_street_no", subscription.getStreetNo()),
                                            new WpPostmeta(wpPost.getId(), "_shipping_apartment", subscription.getApartment()),
                                            new WpPostmeta(wpPost.getId(), "_shipping_floor_no", subscription.getFloorNo()),
                                            new WpPostmeta(wpPost.getId(), "_shipping_entry_code", subscription.getEntryCode()),
                                            new WpPostmeta(wpPost.getId(), "_payment_method", "cardcom"),
                                            new WpPostmeta(wpPost.getId(), "_payment_method_title", "תשלום באשראי"),
                                            new WpPostmeta(wpPost.getId(), "_cart_discount", "" + couponDiscount),
                                            new WpPostmeta(wpPost.getId(), "_cart_discount_tax", "0"),
                                            new WpPostmeta(wpPost.getId(), "_order_shipping", "" + deliveryPrice),
                                            new WpPostmeta(wpPost.getId(), "_order_shipping_tax", "0"),
                                            new WpPostmeta(wpPost.getId(), "_order_tax", "0"),
                                            new WpPostmeta(wpPost.getId(), "_download_permissions_granted", "1"),
                                            new WpPostmeta(wpPost.getId(), "_recorded_sales", "yes"),
                                            new WpPostmeta(wpPost.getId(), "_recorded_coupon_usage_counts", "" + 0),
                                            new WpPostmeta(wpPost.getId(), "_completed_date", completeDateDateFormat.format(wpPost.getPostDate())),
                                            new WpPostmeta(wpPost.getId(), "_order_stock_reduced", "1"),
                                            new WpPostmeta(wpPost.getId(), "sb_platform", "subscription"),
                                            new WpPostmeta(wpPost.getId(), "shipping_delivery_window", "" + subscription.getDeliveryWindow()),
                                            new WpPostmeta(wpPost.getId(), "shipping_delivery_date", shippingDeliveryDateDateFormat.format(deliveryDate))
                                    }));
                                    firstProduct = false;
                                }

                                WpOrderItem wpOrderItem = wpOrderItemsDao.save(new WpOrderItem(product.getNameHeb(), "line_item", wpPost.getId()));

                                List<WpOrderItemmeta> wpWoocommerceOrderItemmetas = new LinkedList<>();
                                wpWoocommerceOrderItemmetas.add(new WpOrderItemmeta(wpOrderItem.getOrderItemId(), "_qty", products[index + 1]));
                                wpWoocommerceOrderItemmetas.add(new WpOrderItemmeta(wpOrderItem.getOrderItemId(), "_tax_class", ""));
                                wpWoocommerceOrderItemmetas.add(new WpOrderItemmeta(wpOrderItem.getOrderItemId(), "_product_id", "" + productId));
                                wpWoocommerceOrderItemmetas.add(new WpOrderItemmeta(wpOrderItem.getOrderItemId(), "_variation_id", "0")); // TODO change
                                wpWoocommerceOrderItemmetas.add(new WpOrderItemmeta(wpOrderItem.getOrderItemId(), "_line_subtotal", "" + product.getPrice() * productQty));
                                wpWoocommerceOrderItemmetas.add(new WpOrderItemmeta(wpOrderItem.getOrderItemId(), "_line_total", "" + product.getPrice() * productQty));
                                wpWoocommerceOrderItemmetas.add(new WpOrderItemmeta(wpOrderItem.getOrderItemId(), "_line_subtotal_tax", "0"));
                                wpWoocommerceOrderItemmetas.add(new WpOrderItemmeta(wpOrderItem.getOrderItemId(), "_line_tax", "0"));
                                wpWoocommerceOrderItemmetas.add(new WpOrderItemmeta(wpOrderItem.getOrderItemId(), "_line_tax_data", "a:2:{s:5:\"total\";a:0:{}s:8:\"subtotal\";a:0:{}}"));

                                wpOrderItemmetaDao.save(wpWoocommerceOrderItemmetas);
                                orderedProductsMap.put(productId, productQty);
                            } else {
                                missingProductsMap.put(productId, productQty);

                            }
                        }
                        WpPostmeta wpPostmeta = wpPostmetaDao.save(new WpPostmeta(wpPost.getId(), "_order_total", "" + (orderTotal - couponDiscount)));
                        WpOrderItem wpOrderShippingItem = wpOrderItemsDao.save(new WpOrderItem("עלות משלוח", "shipping", wpPost.getId()));
                        List<WpOrderItemmeta> wpOrderShippingItemmetas = new LinkedList<>();
                        wpOrderShippingItemmetas.add(new WpOrderItemmeta(wpOrderShippingItem.getOrderItemId(), "method_id", "flat_rate"));
                        wpOrderShippingItemmetas.add(new WpOrderItemmeta(wpOrderShippingItem.getOrderItemId(), "cost", "" + deliveryPrice));
                        wpOrderShippingItemmetas.add(new WpOrderItemmeta(wpOrderShippingItem.getOrderItemId(), "taxes", "a:0:{}"));
                        wpOrderItemmetaDao.save(wpOrderShippingItemmetas);

                        subscriptionSendMail(wpPost.getId(), customer.getEmail(), orderedProductsMap, missingProductsMap, Subscription.OrderStatuses.SUCCESS);

                /* if (!couponsJson.keySet().isEmpty()) {
                    for (String key : couponsJson.keySet()) {
                        wpWoocommerceOrderItems.add(new WpOrderItem(key, "coupon", wpPost.getId()));
                    }
                } */
                /*for (int i = products.length/2 + 1; i < wpOrderItems.size(); i++) {
                    WpOrderItem wpOrderItem = wpOrderItems.get(i);
                    wpWoocommerceOrderItemmetas.add(new WpOrderItemmeta(wpOrderItem.getOrderItemId(), "discount_amount", "" + couponsJson.get(wpWoocommerceOrderItem.getOrderItemName())));
                    wpWoocommerceOrderItemmetas.add(new WpOrderItemmeta(wpOrderItem.getOrderItemId(), "discount_amount_tax", "0"));
                }*/


                    } else {
                        subscriptionSendMail(0L, customer.getEmail(), orderedProductsMap, missingProductsMap, Subscription.OrderStatuses.CREDIT_PROBLEM);
                    }
                }
            }
        }
    }


    private int checkProductAvailability(Integer prodctId, Integer productQty, Character allowSimilarProducts) {
        boolean productAvalable = wpPostmetaDao.findOneByIdAndKey((long)prodctId, "_stock_status").getMetaValue().equals("_stock_status");
        Integer inStock = U.var.parseInt(wpPostmetaDao.findOneByIdAndKey((long)prodctId, "_stock").getMetaValue());
//        System.out.format("Stock: %d   Qty: %d%n", inStock,)
        if (inStock > productQty) {  // Product available
            wpPostmetaDao.updateByIdAndKey((long)prodctId, "_stock", "" + (inStock - productQty));
            return prodctId;
        } else {
            if (allowSimilarProducts.equals('0')) {
                return 0;
            } else {  // Search for similar product
                prodctId = findSimilarProduct(prodctId, productQty);
                wpPostmetaDao.updateByIdAndKey((long)prodctId, "_stock", "" + (inStock - productQty));
            }
        }
        return -1;
    }

    private int findSimilarProduct(Integer prodctId, Integer productQty) {
        return prodctId;
    }

    private boolean subscriptionSendMail(Long wpOrderId,
                                         String email,
                                         Map<Integer, Integer> orderedProductsMap,
                                         Map<Integer, Integer> missingProductsMap,
                                         Subscription.OrderStatuses status) {

        switch (status) {
            case SUCCESS: // Order succsess
                common.sendEmailWithCc(email, "orders@shook-book.com", "שוקבוק - הזמנה שבועית חדשה", U.http.stringFromUrl("https://shookbook.co.il/custom_pages/subscription_order_complete_email.php?orderId=" + wpOrderId));
                break;
            case CREDIT_PROBLEM: // Card expired
                common.sendEmailWithCc(email, "orders@shook-book.com", "שוקבוק - כרטיס אשראי לא בתוקף", U.http.stringFromUrl("https://shookbook.co.il/custom_pages/app/order_complete_email.php"));
                break;
        }
        return true;
    }

    private Date subscriptionGetDeliveryDate(Integer dayOfDelivery, Subscription subscription, Customer customer) {
        Date deliveryDate;
        Date lastOrderDate = new Date();
        Calendar dateOfOrder = Calendar.getInstance();
        int saturday = dateOfOrder.get(Calendar.DAY_OF_WEEK );
        dateOfOrder.add(Calendar.DAY_OF_MONTH, -saturday);
        Integer dayOfOrder = U.var.parseInt(subscription.getDayOfDelivery());

        Date tmp = dateOfOrder.getTime();

        SimpleDateFormat deliveryDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        WpPost wpLastOrder = wpPostsDao.findLastSubscriptionOrder((long)customer.getWpId());
        if (wpLastOrder != null) {
            String lastOrderDateStr = wpPostmetaDao.findOneByIdAndKey(wpLastOrder.getId(), "shipping_delivery_date").getMetaValue();
            try {
                lastOrderDate = deliveryDateFormat.parse(lastOrderDateStr);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        switch (subscription.getFrequency()) {
            case DOUWEEK:
                break;
            case WEEKLY:
                dateOfOrder.add(Calendar.DAY_OF_WEEK, dayOfOrder);
                break;
            case BIWEEKLY:  // If last order is more than a week from Saturday
                if ((dateOfOrder.getTime().getTime() - lastOrderDate.getTime()) > 7*24*60*60*1000) {
                    dateOfOrder.add(Calendar.DAY_OF_WEEK, dayOfOrder);
                }
                break;
            case MONTHLY:
                Calendar nextSaturday = dateOfOrder;
                nextSaturday.add(Calendar.DAY_OF_MONTH, 7);
                int dayInMonth = nextSaturday.get(Calendar.DAY_OF_MONTH);
                if (dayInMonth < 7) {
                    dateOfOrder.add(Calendar.DAY_OF_WEEK, dayOfOrder);
                }
                break;
        }

        deliveryDate = dateOfOrder.getTime();
        return deliveryDate;
    }

}
