package com.shookbook.admin;

import com.shookbook.dao.shookbook.*;
import com.shookbook.dao.wordpress.WpOrderItemsDao;
import com.shookbook.dao.wordpress.WpPostmetaDao;
import com.shookbook.entities.shookbook.*;
import com.shookbook.utils.U;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@SessionAttributes("thought")
public class LogisticsAdmin {
    @Autowired
    private Common common;
    @Autowired
    private ProductsDao productsDao;
    @Autowired
    private CustomersDao customersDao;
    @Autowired
    private OrdersDao ordersDao;
    @Autowired
    private OrderProductDao orderProductDao;
    @Autowired
    private ItemsDao itemsDao;
    @Autowired
    private SuppliersDao suppliersDao;
    @Autowired
    private SupplierOrdersDao supplierOrdersDao;
    @Autowired
    private SupplierOrderItemDao supplierOrderItemDao;
    @Autowired
    private WorkersDao workersDao;
    @Autowired
    private GeoPoinsDao geoPoinsDao;
    @Autowired
    private RoutesDao routesDao;
    @Autowired
    private RouteStopDao routeStopDao;
    @Autowired
    private GlobalsDao globalsDao;
    @Autowired
    private CouponsDao couponsDao;
    @Autowired
    private SmsTemplatesDao smsTemplatesDao;
    @Autowired
    private WpPostmetaDao wpPostmetaDao;
    @Autowired
    private WpOrderItemsDao wpOrderItemsDao;


    @RequestMapping(value = "/admin/logistics2", method = RequestMethod.GET)
    public Object logistics2(@RequestParam(value = "deliveryDate", required = false) String deliveryDate, @RequestParam(value = "openRow", required = false) String openRow, Model model, HttpServletRequest request) {
        if (!U.var.parseBoolean(request.getSession().getAttribute("login"))) {
            return "redirect:/admin/login";
        }

        Date ordersDate = new Date();
        Integer gotDate = 0;
        if ((deliveryDate == null) || (deliveryDate.length() < 4)) { // No date
            Long time = new Date().getTime();
            Date today = new Date(time - (time % (24 * 60 * 60 * 1000) + 0 * 60 * 60 * 1000));
            ordersDate = today;
            System.out.format("   ### No date%n");
        } else {
            try {
                System.out.format(" ### Date: %s%n", deliveryDate);
                ordersDate = new SimpleDateFormat("MM/dd/yyyy").parse(deliveryDate);
                ordersDate = new Date(ordersDate.getTime() - 0 * 60 * 60 * 1000);
                gotDate = 1;
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        List<Map<String, Object>> routesColumns = new LinkedList<>();

        List<Routes> routes = routesDao.findAllByDeliveryDate(ordersDate);
        System.out.format("   ### Date: %s, Routes: %d%n", ordersDate, routes.size());
        int i = 1;

        if ((openRow == null) || (openRow.length() != routes.size())) {
            StringBuilder openRowSB = new StringBuilder();
            for (int r = 0; r < routes.size(); r++) {
                openRowSB.append('0');
            }
            openRow = openRowSB.toString();
        }

        for (Routes route : routes) {
            Map<String, Object> column = new HashMap<>();
            column.put("id", route.getId());
            column.put("routeNumber", i);
            if (i <= openRow.length()) {  // Flag to poen route at html
                if (openRow.charAt(i-1) == '1') {
                   column.put("openRow", i);
                   System.out.format(" ### %s    %d%n", openRow.charAt(i-1), i);
                } else {
                    column.put("openRow", 0);
                }
            } else {
                column.put("openRow", 0);
            }
            column.put("direction", route.getDirection());
            column.put("visible", route.getVisible());
            column.put("deliveryWindow", route.getDeliveryWindow());
            column.put("driver", route.getDriverName());
            if (route.getDeliveryWindow() == 0) {
                column.put("prefix", route.getPrefix() + "A");
            } else {
                column.put("prefix", route.getPrefix() + "P");
            }
            column.put("estimatedStart", new SimpleDateFormat("HH:mm").format(route.getEstimatedStart()));
            column.put("actualStart", route.getActualStart() == null ? "" : new SimpleDateFormat("HH:mm").format(route.getActualStart()));
            column.put("start", route.getActualStart() == null ? 1 : 0);
            Integer routePoints = routesDao.getNumberOfStops(route.getId());
            Integer routeOpenPoints = routesDao.getNumberOfOpenStops(route.getId());
            column.put("close", routeOpenPoints > 0 ? 1 : 0);
            column.put("open", route.getActualStart() != null ? 0 : 1);
            column.put("numberOfStops", routePoints);
            column.put("numberOfRemainingPoints", routeOpenPoints);
            List<RouteStop> routeStops = routeStopDao.findAllByRouteId(route.getId());
            List<Map<String, Object>> routeStopColumns = new LinkedList<>();
            for (RouteStop stop : routeStops) {
                Map<String, Object> stopRow = new HashMap<>();
                stopRow.put("orderId", stop.getOrderId());
                stopRow.put("estimatedTime", new SimpleDateFormat("HH:mm").format(stop.getEstimatedDelivery()));
                if (stop.getActualDelivery() != null) {
                    stopRow.put("status", 1);
                    stopRow.put("color", "829a4f");
                    stopRow.put("actualDelivery", new SimpleDateFormat("HH:mm").format(stop.getActualDelivery()));
                } else {
                    stopRow.put("status", 0);
                    stopRow.put("color", "ffffff");
                    stopRow.put("actualDelivery", "");
                }

                Order order = ordersDao.findOne(stop.getOrderId());
                Customer customer = customersDao.getOne(order.getCustomerId());
                stopRow.put("address", order.getStreet() + " " + order.getStreetNo() + ", " + order.getCity());
                stopRow.put("dhlNumber", order.getDhlPackageNumber());
                stopRow.put("customerName", order.getFirstName() + " " + order.getLastName());
                stopRow.put("customerPhoneNumber", customer.getPhone());
                stopRow.put("customerPhoneNumber2", customer.getPhone2());
                stopRow.put("remarks", (customer.getRemarks() == null ? "" : customer.getRemarks()) + (order.getRemarks() == null ? "" : " : " + order.getRemarks()));
                routeStopColumns.add(stopRow);
            }
            column.put("routeStop", routeStopColumns);
            routesColumns.add(column);
            i++;
        }
        List<Order> orders;

        model.addAttribute("routesColumns", routesColumns);
        model.addAttribute("gotDate", gotDate);
        model.addAttribute("deliveryDate", deliveryDate);
        model.addAttribute("openRow", openRow);
        Map<String, Object> globals = new HashMap<>();
        globals.put("modules_enable_coupons", globalsDao.findOne("modules_enable_coupons").getValue());
        globals.put("modules_enable_invoice", globalsDao.findOne("modules_enable_invoice").getValue());
        globals.put("modules_enable_logistics", globalsDao.findOne("modules_enable_logistics").getValue());
        globals.put("modules_enable_packing", globalsDao.findOne("modules_enable_packing").getValue());
        Object logedWorker = request.getSession().getAttribute("userId");
        Worker worker = workersDao.getOne(Integer.parseInt(logedWorker.toString()));
        globals.put("worker_name", worker.getFirstName() + " " + worker.getLastName());
        model.addAttribute("globals", globals);
        model.addAttribute("admin", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.ADMIN.ordinal() ? 1 : 0);

        return "LogisticsHub2";
    }

    @RequestMapping(value = "/admin/logistics", method = RequestMethod.GET)
    public Object logistics(Model model, HttpServletRequest request) {
        if (!U.var.parseBoolean(request.getSession().getAttribute("login"))) {
            return "redirect:/admin/login";
        }

        List<Order> orders = ordersDao.findAllCurrentAssignmentsByHdlPackageNumber();

        List<Map<String, Object>> orderColumns = new LinkedList<>();
        List<Map<String, Object>> routesColumns = new LinkedList<>();
        Map<String, Integer[]> routesData = new HashMap<>();
        Integer[] array = {0, 0};

        for (Order order : orders) {
            Map<String, Object> column = new HashMap<>();
            String data = getBlogicOrder(order.getId());
            String[] elements = data.split("&");
            Map<String, String> fields = new HashMap<>();
            for (int index = 0; index < elements.length; index++) {
                String[] fieldData = new String[2];
                if (elements[index].substring(elements[index].length() - 1).equals("=")) {
                    fieldData[0] = elements[index].substring(0, elements[index].length() - 1);
                    fieldData[1] = "";
                } else {
                    fieldData = elements[index].split("=");
                }
                fields.put(fieldData[0], fieldData[1]);
            }

            if (fields.get("DHLCode") != null) {
                String route = fields.get("DHLCode").substring(0, Math.min(fields.get("DHLCode").length(), 2));
                StringBuilder routeMap = new StringBuilder();
                Customer customer = customersDao.findOne(order.getCustomerId());
                column.put("lineNumber", U.var.parseInt(fields.get("DHLCode").substring(fields.get("DHLCode").length() - 4)));
                column.put("linePrefix", fields.get("DHLCode").length() < 2 ? fields.get("DHLCode") : fields.get("DHLCode").substring(0, 2));
                column.put("dhlNumber", fields.get("DHLCode"));
                column.put("route", route);
                column.put("customerName", fields.get("FullName"));
                column.put("address", fields.get("Address"));
                column.put("id", fields.get("Order"));
                column.put("status", fields.get("UploadStatus"));
                //System.out.format("   ### Order: %d  Boxes: %d", U.var.parseInt(fields.get("Order")), U.var.parseInt(fields.get("Boxes")));
                column.put("boxes", U.var.parseInt(fields.get("Boxes")));
                column.put("estimatedTime", fields.get("EstimatedTimeOfArrival"));
                column.put("deliveryTime", fields.get("TimeOfArrival"));
                column.put("remarks", fields.get("Remarks"));
                column.put("customerPhoneNumber", fields.get("Phone"));
                column.put("customerPhoneNumber2", customer.getPhone2());
                column.put("joinDate", new SimpleDateFormat("dd-MM-yyyy").format(customer.getCreatedDate()));
                column.put("disableRoute", order.getSmsCode() > 1 ? 0 : 1);
                orderColumns.add(column);

                if (routesData.get(route) != null) {
                    array = routesData.get(route);
                    array[0]++;
                    array[1] = array[1] + (order.getStatus().ordinal() == 3 ? 0 : 1);
                    routesData.put(route, new Integer[]{array[0], array[1]});
                } else {
                    array[0] = 1;
                    array[1] = order.getStatus().ordinal() == 3 ? 0 : 1;
                    routesData.put(route, new Integer[]{array[0], array[1]});
                }
            } else {

            }
        }

        Iterator iterator = routesData.entrySet().iterator();
        while (iterator.hasNext()) {
            Map<String, Object> routeColumn = new HashMap<>();
            Map.Entry routeData = (Map.Entry)iterator.next();
            String routeKey = routeData.getKey().toString();
            Integer[] routeValues = routesData.get(routeKey);
            System.out.format("   #  Got %s with values %d,%d%n", routeKey, routeValues[0], routeValues[1]);
            routeColumn.put("dhlNumber", routeKey);
            routeColumn.put("assignments", routeValues[0]);
            routeColumn.put("delivered", routeValues[1]);
            routesColumns.add(routeColumn);
            //iterator.remove(); // avoids a ConcurrentModificationException
        }

        model.addAttribute("orderColumns", orderColumns);
        model.addAttribute("routesColumns", routesColumns);
        Map<String, Object> globals = new HashMap<>();
        globals.put("modules_enable_coupons", globalsDao.findOne("modules_enable_coupons").getValue());
        globals.put("modules_enable_invoice", globalsDao.findOne("modules_enable_invoice").getValue());
        globals.put("modules_enable_logistics", globalsDao.findOne("modules_enable_logistics").getValue());
        globals.put("modules_enable_packing", globalsDao.findOne("modules_enable_packing").getValue());
        Object logedWorker = request.getSession().getAttribute("userId");
        Worker worker = workersDao.getOne(Integer.parseInt(logedWorker.toString()));
        globals.put("worker_name", worker.getFirstName() + " " + worker.getLastName());
        model.addAttribute("globals", globals);
        model.addAttribute("admin", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.ADMIN.ordinal() ? 1 : 0);

        return "LogisticsHub";
    }

    @RequestMapping(value = "/admin/logistics/chnageRouteDirction", method = RequestMethod.GET)
    public Object chnageRouteDirction(@RequestParam(value = "routeId") Integer routeId, @RequestParam(value = "deliveryDate") String deliveryDate, @RequestParam(value = "openRow") String openRow, Model model, HttpServletRequest request) {

        Routes route = routesDao.findOne(routeId);
        if (route.getDirection() == 0) {
            route.setDirection(1);
        } else {
            route.setDirection(0);
        }

        routesDao.save(route);

        Map<String, Object> globals = new HashMap<>();
        globals.put("modules_enable_coupons", globalsDao.findOne("modules_enable_coupons").getValue());
        globals.put("modules_enable_invoice", globalsDao.findOne("modules_enable_invoice").getValue());
        globals.put("modules_enable_logistics", globalsDao.findOne("modules_enable_logistics").getValue());
        globals.put("modules_enable_packing", globalsDao.findOne("modules_enable_packing").getValue());
        Object logedWorker = request.getSession().getAttribute("userId");
        Worker worker = workersDao.getOne(Integer.parseInt(logedWorker.toString()));
        globals.put("worker_name", worker.getFirstName() + " " + worker.getLastName());
        model.addAttribute("globals", globals);
        model.addAttribute("admin", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.ADMIN.ordinal() ? 1 : 0);

        System.out.format(" Going to: %s%n", "redirect:/admin/logistics2?deliveryDate=" + deliveryDate + "&openRow=" + openRow);
        return "redirect:/admin/logistics2?deliveryDate=" + deliveryDate + "&openRow=" + openRow;
    }

    @RequestMapping(value = "/admin/logistics/chnageRouteVisibility", method = RequestMethod.GET)
    public Object chnageRouteVisibility(@RequestParam(value = "routeId") Integer routeId, @RequestParam(value = "deliveryDate") String deliveryDate, @RequestParam(value = "openRow") String openRow, Model model, HttpServletRequest request) {

        Routes route = routesDao.findOne(routeId);
        String delivery = route.getDeliveryWindow() == 1 ? "P" : "A";
        String routePrefix = route.getPrefix().toString() + "" + delivery;
        String todayStr = new SimpleDateFormat("yyyy-MM-dd").format(route.getDeliveryDate());
        String openStatus;
        if (route.getVisible() == 0) {
            route.setVisible(1);
            openStatus = openBlogicRoute(routePrefix, todayStr);
        } else {
            route.setVisible(0);
            openStatus = hideBlogicRoute(routePrefix, todayStr);
        }

        routesDao.save(route);

        Map<String, Object> globals = new HashMap<>();
        globals.put("modules_enable_coupons", globalsDao.findOne("modules_enable_coupons").getValue());
        globals.put("modules_enable_invoice", globalsDao.findOne("modules_enable_invoice").getValue());
        globals.put("modules_enable_logistics", globalsDao.findOne("modules_enable_logistics").getValue());
        globals.put("modules_enable_packing", globalsDao.findOne("modules_enable_packing").getValue());
        Object logedWorker = request.getSession().getAttribute("userId");
        Worker worker = workersDao.getOne(Integer.parseInt(logedWorker.toString()));
        globals.put("worker_name", worker.getFirstName() + " " + worker.getLastName());
        model.addAttribute("globals", globals);
        model.addAttribute("admin", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.ADMIN.ordinal() ? 1 : 0);

        System.out.format(" Going to: %s%n", "redirect:/admin/logistics2?deliveryDate=" + deliveryDate + "&openRow=" + openRow);
        return "redirect:/admin/logistics2?deliveryDate=" + deliveryDate + "&openRow=" + openRow;
    }

    @RequestMapping(value = "/admin/logistics/openRoute", method = RequestMethod.GET)
    public Object openRoute(@RequestParam(value = "orderId", required = false) Integer orderId,
                            @RequestParam(value = "routeId", required = false) Integer routeId,
                            @RequestParam(value = "deliveryDate") String deliveryDate,
                            @RequestParam(value = "openRow") String openRow,
                            Model model, HttpServletRequest request) {

        Routes ordersRoute = new Routes();
        RouteStop stop = new RouteStop();
        Order order = new Order();
        if (routeId != null) {
            ordersRoute = routesDao.findOne(routeId);
            stop = ordersRoute.getDirection() == 0 ? routeStopDao.getFirstInRoute(ordersRoute.getId()) : routeStopDao.getLastInRoute(ordersRoute.getId());
            order = ordersDao.findOne(stop.getOrderId());
        } else if (orderId != null) {
            order = ordersDao.findOne(orderId);
            stop = routeStopDao.findOneByOrderId(orderId);
            ordersRoute = routesDao.findOne(stop.getRouteId());
        }

        //System.out.format("   ### Got route %d  order %d   stop %d%n", ordersRoute.getId(), order.getId(), stop.getId());
        Integer daylightSavingTime = U.var.parseInt(globalsDao.getOne("daylight_saving_time").getValue());
        Date routeOpenTime = new Date(new Date().getTime() + (2 + daylightSavingTime) * 60 * 60 * 1000);
        try {
            routeOpenTime = new SimpleDateFormat("HH:mm").parse(new SimpleDateFormat("HH:mm").format(routeOpenTime));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        ordersRoute.setActualStart(routeOpenTime);
        ordersRoute.setVisible(1);
        routesDao.save(ordersRoute);

        String delivery = ordersRoute.getDeliveryWindow() == 1 ? "P" : "A";
        String route = ordersRoute.getPrefix().toString() + "" + delivery;
        //System.out.format("   ### I'm oppening route %s  with prefix %s%n", route, ordersRoute.getPrefix());
        //String route = order.getDhlPackageNumber().substring(0, 2);
        String time = order.getEstimatedDeliveryTime().toString();
        Date today = new Date();
        String todayStr = new SimpleDateFormat("yyyy-MM-dd").format(today);
        String openStatus = openBlogicRoute(route, todayStr);

        if (openStatus.equals("OK")) {
            //System.out.format("   ### Route: %s   Time: %s, Window: %d%n", route + "%", time, U.var.parseInt(order.getDeliveryWindow()));
            List<Order> nextOrders;
            if (ordersRoute.getDirection() == 1) {
                nextOrders = ordersDao.findNextOrdersToDeliverSub(route + "%", time, "1:30", U.var.parseInt(order.getDeliveryWindow()));
            } else {
                nextOrders = ordersDao.findNextOrdersToDeliver(route + "%", time, "1:30", U.var.parseInt(order.getDeliveryWindow()));
            }
            for (Order nextOrder : nextOrders) {
                if (nextOrder.getSmsCode() < 2) {
                    sendSms(nextOrder.getId(), 2);
                    nextOrder.setSmsCode(2);
                    ordersDao.save(nextOrder);
                }
            }
        }

        return "redirect:/admin/logistics2?deliveryDate=" + deliveryDate + "&openRow=" + openRow;
    }

    @RequestMapping(value = "/admin/logistics/closeRoute", method = RequestMethod.GET)
    public Object closeRoute(@RequestParam(value = "routeId") Integer routeId,
                             @RequestParam(value = "deliveryDate") String deliveryDate,
                             @RequestParam(value = "openRow") String openRow,
                             Model model, HttpServletRequest request) {

        Integer daylightSavingTime = U.var.parseInt(globalsDao.getOne("daylight_saving_time").getValue());
        Date routeCloseTime = new Date(new Date().getTime() + (2 + daylightSavingTime) * 60 * 60 * 1000);
        try {
            routeCloseTime = new SimpleDateFormat("HH:mm").parse(new SimpleDateFormat("HH:mm").format(routeCloseTime));
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        List<RouteStop> stops;
        Order order;
        stops = routeStopDao.findAllByRouteId(routeId);

        for (RouteStop stop : stops) {
            if (stop.getActualDelivery() == null) {
                stop.setActualDelivery(routeCloseTime);
                routeStopDao.save(stop);
                order = ordersDao.findOne(stop.getOrderId());
                sendSms(order.getId(), 3); // Send order delivered SMS
                order.setSmsCode(3);
                ordersDao.save(order);
            }
        }
        return "redirect:/admin/logistics2?deliveryDate=" + deliveryDate + "&openRow=" + openRow;
    }

    @RequestMapping(value = "/admin/logistics/showMap", method = RequestMethod.GET)
    public Object showMap(@RequestParam(value = "prefix") String prefix, @RequestParam(value = "deliveryDate", required = false) String deliveryDate, Model model, HttpServletRequest request) {

        Date date = new Date();
        if (deliveryDate == null) {
            try {
                //System.out.format("   ### No date: date is %s", date.toString());
                date = new SimpleDateFormat("yyyy-MM-dd").parse(new SimpleDateFormat("mm/DD/yyyy").format(date));
            } catch (Exception ex) {
                //System.out.format("   ### No date: date is %s", date.toString());
                ex.printStackTrace();
            }
        } else {
            try {
                date = new SimpleDateFormat("MM/dd/yyyy").parse(deliveryDate);
                //System.out.format("   ### Got date: date is %s", date.toString());
            } catch (Exception ex) {
                //System.out.format("   ### Got date: date is %s", date.toString());
                ex.printStackTrace();
            }
        }

        //System.out.format("   ### Date: %s%n", date.toString());
        List <Order> orders = ordersDao.findAllDeliveredOrderByPrefixDesc(prefix+"%", date);
        StringBuilder route = new StringBuilder();
        String lat = "", lon = "";
        boolean foundSameAddress;
        boolean sameAddress;
        //System.out.format("   Got %d orders for prefix %s%n", orders.size(), prefix);
        for (Order order : orders) {
            foundSameAddress = false;
            List<GeoPoint> geoPoints = geoPoinsDao.findAllByCustomerId(order.getCustomerId());
            //System.out.format("   Got %d GeoPoints%n", geoPoints.size());
            for (GeoPoint geoPoint : geoPoints) {
                sameAddress = true;
                if (!order.getCity().equals(geoPoint.getCity())) {
                    sameAddress = false;
                }
                if (!order.getStreet().equals(geoPoint.getStreet())) {
                    sameAddress = false;
                }
                if (!order.getStreetNo().equals(geoPoint.getStreetNumber())) {
                    sameAddress = false;
                }
                if (sameAddress) {
                    foundSameAddress = true;
                    lat = geoPoint.getLatitude().toString();
                    lon = geoPoint.getLongitude().toString();
                }
            }

            if (foundSameAddress) {
                route.append(lat).append("+").append(lon).append("/");
            } else {
                route.append(order.getStreet().replace(" ", "+")).append("+").append(order.getStreetNo());
                route.append("+").append(order.getCity().replace(" ", "+")).append("/");
            }
        }

        //System.out.format("   ### %s%n", route.toString());

        return "redirect:https://www.google.com/maps/dir/" + route.toString();
    }

    @RequestMapping(value = "/admin/logistics/remoteSetDeliveredOrders", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public void remoteSetDeliveredOrders(@RequestParam(value = "data") String data) {
        System.out.format("   ### Remote server sent: %s to close orders%n", data);
        String[] elements = data.split(";");
        Integer intervalHours =  U.var.parseInt(globalsDao.findOne("logistics_sms_time_before_delivery").getValue()) / 60;
        Integer intervalMinutes = U.var.parseInt(globalsDao.findOne("logistics_sms_time_before_delivery").getValue()) - intervalHours * 60;

        for (int index = 0; index < elements.length; index++) {
            if (U.var.parseInt(elements[index]) > 0) {
                Integer orderId = U.var.parseInt(elements[index]);
                Order order = ordersDao.findOne(orderId);
                order.setStatus(Order.OrderStatuses.DELIVERED);

                sendSms(orderId, 3); // Send order delivered SMS
                order.setSmsCode(3);
                ordersDao.save(order);

                // Get True time of arrival
                RouteStop stop = routeStopDao.findOneByOrderId(orderId);
                Routes route = routesDao.getOne(stop.getRouteId());
                String orderData = getBlogicOrder(orderId);
                String[] orderElements = orderData.split("&");
                Map<String, String> fields = new HashMap<>();
                for (int orderIndex = 0; orderIndex < orderElements.length; orderIndex++) {
                    String[] fieldData = new String[2];
                    if (orderElements[orderIndex].substring(orderElements[orderIndex].length() - 1).equals("=")) {
                        fieldData[0] = orderElements[orderIndex].substring(0, orderElements[orderIndex].length() - 1);
                        fieldData[1] = "";
                    } else {
                        fieldData = orderElements[orderIndex].split("=");
                    }
                    fields.put(fieldData[0], fieldData[1]);
                }

                if (stop.getActualDelivery() == null) {
                    String stopTimeStr = fields.get("TimeOfArrival");
                    Integer daylightSavingTime = U.var.parseInt(globalsDao.getOne("daylight_saving_time").getValue());
                    Date stopTime = new Date();
                    try {
                        stopTime = new SimpleDateFormat("HH:mm").parse(stopTimeStr);
                        stopTime = new Date(stopTime.getTime() + 1 * 60 * 60 * 1000);
                        System.out.format("   # Time: %s   %d%n", new SimpleDateFormat("HH:mm").format(stopTime), daylightSavingTime);
                    } catch (Exception ex) {
                        //System.out.format("   ### Cannot set stop time. Stop id: %d%n", stop.getId());
                        ex.printStackTrace();
                    }
                    System.out.format("   ### Setting stop time to %s. Stop id: %d%n", stopTime.toString(), stop.getId());
                    stop.setActualDelivery(stopTime);
                    routeStopDao.save(stop);
                }

                Date estimatedDeliveryTime = order.getEstimatedDeliveryTime();

                List<Order> nextOrders;

                if (route.getDirection() == 1) {
                    nextOrders = ordersDao.findNextOrdersToDeliverSub(order.getDhlPackageNumber().substring(0, 2) + "%", estimatedDeliveryTime.toString(), intervalHours.toString() + ":" + intervalMinutes, U.var.parseInt(order.getDeliveryWindow()));
                } else {
                    nextOrders = ordersDao.findNextOrdersToDeliver(order.getDhlPackageNumber().substring(0, 2) + "%", estimatedDeliveryTime.toString(), intervalHours.toString() + ":" + intervalMinutes, U.var.parseInt(order.getDeliveryWindow()));
                }
                if (nextOrders.size() > 0) {
                    for (Order nextOrder : nextOrders) {
                        if (nextOrder.getSmsCode() < 2) {
                            sendSms(nextOrder.getId(), 2);
                            nextOrder.setSmsCode(2);
                            ordersDao.save(nextOrder);
                        }
                    }
                }
            }
        }
    }

    @RequestMapping(value = "/admin/logistics/setOrderDelivered", method = RequestMethod.GET)
    public Object setOrderDelivered(@RequestParam(value = "orderId") Integer orderId,
                                    @RequestParam(value = "deliveryDate") String deliveryDate,
                                    @RequestParam(value = "openRow") String openRow,
                                    Model model, HttpServletRequest request) {

        Date now = new Date();
        Integer daylightSavingTime = U.var.parseInt(globalsDao.getOne("daylight_saving_time").getValue());
        Date currentTime = new Date(now.getTime() + (2 + daylightSavingTime) * 60 * 60 * 1000);
        RouteStop stop = routeStopDao.findOneByOrderId(orderId);
        stop.setActualDelivery(currentTime);
        routeStopDao.save(stop);
        DateFormat dateFormat = new SimpleDateFormat("HH:m", Locale.ENGLISH);
        String timeOfArrival = dateFormat.format(currentTime);
        String status = setBlogicOrder(orderId, 1700555704L, "שרות לקוחות", timeOfArrival, 1,1, 0);
        if (status.equals("OK")) {
            //System.out.format("   ### Order complition SMS sent to order number %d%n", orderId);
            Order order = ordersDao.findOne(orderId);
            order.setStatus(Order.OrderStatuses.DELIVERED);
            ordersDao.save(order);
        }
        return "redirect:/admin/logistics2?deliveryDate=" + deliveryDate + "&openRow=" + openRow;
    }

    @RequestMapping(value = "/admin/logistics/getBlogicOrder", method = RequestMethod.GET)
    public Object getWebBlogicOrder(@RequestParam(value = "orderId") Integer orderId, Model model, HttpServletRequest request) {

        StringBuilder result = new StringBuilder();

        try {
            StringBuilder parameters = new StringBuilder();
            parameters.append("orderId").append('=').append(orderId);
            byte[] parametersBytes = parameters.toString().getBytes();
            URL url = new URL("http://logistics.shookbook.co.il/api/getBlogicOrder.php?orderId=" + orderId);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            // conn.setInstanceFollowRedirects( false );
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestProperty("charset", "utf-8");
            conn.setRequestProperty("Content-Length", "" + parametersBytes.length);
            conn.setUseCaches(false);
            conn.getOutputStream().write(parametersBytes);
            Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

            for (int c; (c = in.read()) >= 0; ) {
                result.append((char) c);
            }
        } catch (Exception ex) {

        }

        //System.out.format("   ### %s%n",result);
        String[] elements = result.toString().split("&");
        Map<String, String> fields = new HashMap<>();
        for (int index = 0; index < elements.length; index++) {
            String[] data = new String[2];
            if (elements[index].substring(elements[index].length() - 1).equals("=")) {
                data[0] = elements[index].substring(0, elements[index].length() - 1);
                data[1] = "";
            } else {
                data = elements[index].split("=");
            }
            fields.put(data[0], data[1]);
        }
        //System.out.format("   ### Address: %s%n", fields.get("Address"));
        return result;
    }

    public String getBlogicOrder(Integer orderId) {

        StringBuilder result = new StringBuilder();

        try {
            StringBuilder parameters = new StringBuilder();
            parameters.append("orderId").append('=').append(orderId);
            byte[] parametersBytes = parameters.toString().getBytes();
            URL url = new URL("http://logistics.shookbook.co.il/api/getBlogicOrder.php?orderId=" + orderId);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            // conn.setInstanceFollowRedirects( false );
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestProperty("charset", "utf-8");
            conn.setRequestProperty("Content-Length", "" + parametersBytes.length);
            conn.setUseCaches(false);
            conn.getOutputStream().write(parametersBytes);
            Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

            for (int c; (c = in.read()) >= 0; ) {
                result.append((char) c);
            }
        } catch (Exception ex) {

        }

        String resultStr = result.toString();
        resultStr = resultStr.substring(1, resultStr.length());
        return resultStr;
    }

    public String openBlogicRoute(String route, String deliveryDate) {

        //System.out.format("   ### Opening route : %s  date: %s%n", route, deliveryDate);
        StringBuilder result = new StringBuilder();
        StringBuilder parameters = new StringBuilder();
        parameters.append("?route=").append(route);
        parameters.append("&deliveryDate=").append(deliveryDate);

        try {
            byte[] parametersBytes = parameters.toString().getBytes();
            URL url = new URL("http://logistics.shookbook.co.il/api/openRoute.php" + parameters.toString());
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            System.out.format("   ### Site: %s%n", url.toString());
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestProperty("charset", "utf-8");
            conn.setRequestProperty("Content-Length", "" + parametersBytes.length);
            conn.setUseCaches(false);
            conn.getOutputStream().write(parametersBytes);
            Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

            for (int c; (c = in.read()) >= 0; ) {
                result.append((char) c);
            }
        } catch (Exception ex) {
            //System.out.format("   ### Cannot read Site%n");
            ex.printStackTrace();
        }

        //System.out.format("   ### %s%n", result);
        String[] elements = result.toString().split("&");
        Map<String, String> fields = new HashMap<>();
        for (int index = 0; index < elements.length; index++) {
            String[] data = new String[2];
            if (elements[index].substring(elements[index].length() - 1).equals("=")) {
                data[0] = elements[index].substring(0, elements[index].length() - 1);
                data[1] = "";
            } else {
                data = elements[index].split("=");
            }
            fields.put(data[0], data[1]);
        }
        return fields.get("Status");
    }

    public String hideBlogicRoute(String route, String deliveryDate) {

        //System.out.format("   ### Opening route : %s  date: %s%n", route, deliveryDate);
        StringBuilder result = new StringBuilder();
        StringBuilder parameters = new StringBuilder();
        parameters.append("?route=").append(route);
        parameters.append("&deliveryDate=").append(deliveryDate);

        try {
            byte[] parametersBytes = parameters.toString().getBytes();
            URL url = new URL("http://logistics.shookbook.co.il/api/hideRoute.php" + parameters.toString());
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            System.out.format("   ### Site: %s%n", url.toString());
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestProperty("charset", "utf-8");
            conn.setRequestProperty("Content-Length", "" + parametersBytes.length);
            conn.setUseCaches(false);
            conn.getOutputStream().write(parametersBytes);
            Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

            for (int c; (c = in.read()) >= 0; ) {
                result.append((char) c);
            }
        } catch (Exception ex) {
            //System.out.format("   ### Cannot read Site%n");
            ex.printStackTrace();
        }

        //System.out.format("   ### %s%n", result);
        String[] elements = result.toString().split("&");
        Map<String, String> fields = new HashMap<>();
        for (int index = 0; index < elements.length; index++) {
            String[] data = new String[2];
            if (elements[index].substring(elements[index].length() - 1).equals("=")) {
                data[0] = elements[index].substring(0, elements[index].length() - 1);
                data[1] = "";
            } else {
                data = elements[index].split("=");
            }
            fields.put(data[0], data[1]);
        }
        return fields.get("Status");
    }

    public String setBlogicOrder(Integer orderId, Long imie, String driverName, String timeOfArrival, Integer setDelivered, Integer updateOnly, Integer debug) {

        //System.out.format("   ### Order: %d   IMIE: %s   Time: %s%n", orderId, imie.toString(), timeOfArrival);
        Order order = ordersDao.findOne(orderId);
        Customer customer = customersDao.findOne(order.getCustomerId());
        StringBuilder result = new StringBuilder();
        StringBuilder parameters = new StringBuilder();
        parameters.append("?orderId=").append(orderId);
        parameters.append("&updateOnly=").append(updateOnly);
        if (setDelivered > 0) {
            parameters.append("&status=").append(setDelivered.toString());
        }

        if (imie >= 0) {
            parameters.append("&imie=").append(imie.toString());
        }

        parameters.append("&driverName=").append(U.var.urlEncode(driverName));

        String customerName = order.getFirstName() + " " + order.getLastName();
        if (customerName.length() > 38) {
            customerName = customerName.substring(0, 38);
        }
        parameters.append("&customerName=").append(U.var.urlEncode(customerName));
        parameters.append("&dhlCode=").append(U.var.urlEncode(order.getDhlPackageNumber()));
        parameters.append("&orderWpId=").append(order.getWpId());
        parameters.append("&address=").append(U.var.urlEncode(order.getStreet() + " " + order.getStreetNo() + " " + order.getCity()));
        parameters.append("&boxes=").append(order.getBoxes());
        StringBuilder floor = new StringBuilder();
        if (!order.getFloorNo().equals("")) {
            floor.append("קומה: " + order.getFloorNo() + " ");
        }

        if (!order.getApartment().equals("")) {
            floor.append("דירה: " + order.getApartment());
        }

        String appartment = floor.toString();
        if (appartment.length() > 18) {
            appartment = appartment.substring(0, 18);
        }
        if (appartment.length() > 0) {
            parameters.append("&appartment=").append(U.var.urlEncode(appartment));
        } else {
            parameters.append("&appartment=");
        }

        if (!timeOfArrival.equals("")) {
            parameters.append("&timeOfArrival=").append(U.var.urlEncode(timeOfArrival));
        }
        parameters.append("&phone=").append(U.var.urlEncode(customer.getPhoneExt() + "-" + customer.getPhoneNo()));
        Date deliveryDate = order.getDeliveryWindow() == 1 ? new DateTime(order.getDeliveryDate()).minusDays(1).toDate() : order.getDeliveryDate();
        String deliveryDateStr = new SimpleDateFormat("yyyy-MM-dd").format(deliveryDate);
        parameters.append("&deliveryDate=").append(U.var.urlEncode(deliveryDateStr));
        parameters.append("&deliveryWindow=").append(order.getDeliveryWindow());
        StringBuilder remarks = new StringBuilder();
        String customerRemarks = customer.getRemarks();
        if (customerRemarks != null) {
            remarks.append(customerRemarks).append("   ");
        }

        if (order.getRemarks() != null) {
            remarks.append(order.getRemarks());
        }

        if (remarks.length() > 0) {
            parameters.append("&remarks=").append(U.var.urlEncode(remarks.toString()));
        }

        parameters.append("&debug=").append(debug);

        try {
            byte[] parametersBytes = parameters.toString().getBytes();
            //URL url = new URL("http://logistics.shookbook.co.il/api/setBlogicOrder.php" + U.var.urlEncode(parameters.toString()));
            URL url = new URL("http://logistics.shookbook.co.il/api/setBlogicOrder.php" + parameters.toString());
            //URL url = new URL("http://logistics.shookbook.co.il/api/setBlogicOrder.php?orderId=101010&debug=1");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            System.out.format("   ### Site: %s%n", url.toString());
            // conn.setInstanceFollowRedirects( false );
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestProperty("charset", "utf-8");
            conn.setRequestProperty("Content-Length", "" + parametersBytes.length);
            conn.setUseCaches(false);
            //System.out.format("   ### Site: %s%n", url.toString());
            conn.getOutputStream().write(parametersBytes);
            Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

            //System.out.format("   ### Site: %s%n", url.toString());
            for (int c; (c = in.read()) >= 0; ) {
                result.append((char) c);
            }
        } catch (Exception ex) {
            System.out.format("   ### Cannot read Site%n");
            ex.printStackTrace();
        }

        System.out.format("   ### %s%n", result);
        String[] elements = result.toString().split("&");
        Map<String, String> fields = new HashMap<>();
        for (int index = 0; index < elements.length; index++) {
            String[] data = new String[2];
            if (elements[index].substring(elements[index].length() - 1).equals("=")) {
                data[0] = elements[index].substring(0, elements[index].length() - 1);
                data[1] = "";
            } else {
                data = elements[index].split("=");
            }
            fields.put(data[0], data[1]);
        }
        return fields.get("Status");
    }

    public String sendSms(Integer orderId, Integer type) {

        Order order = ordersDao.findOne(orderId);
        Customer customer = customersDao.findOne(order.getCustomerId());
        StringBuilder parameters = new StringBuilder();
        parameters.append("?orderId=").append(orderId);
        parameters.append("&type=").append(type);
        parameters.append("&phone=").append(customer.getPhoneExt() + "-" + customer.getPhoneNo());

        SmsTemplate messageTemplate;
        if ((customer.getCouponGroup() == null) || (customer.getCouponGroup() == 0)) {
            messageTemplate = smsTemplatesDao.findOneByCityAndType(order.getCity(), type);
        } else {
            messageTemplate = smsTemplatesDao.findOneByCityAndTypeAndCouponGroup(order.getCity(), type, customer.getCouponGroup());
            if (messageTemplate == null) {
                messageTemplate = smsTemplatesDao.findOneByCityAndType(order.getCity(), type);
            }
        }

        if (messageTemplate == null) {
            messageTemplate = smsTemplatesDao.findOneByType(type);
            System.out.format(" No Specific message got: %s Type: %d%n", messageTemplate.getMessage(), type);
        }

        String message = "";

        switch (type) {
            case 1 : // First message - estimated time of arrival
                Date deliveryDate = order.getDeliveryWindow() == 1 ? new DateTime(order.getDeliveryDate()).minusDays(1).toDate() : order.getDeliveryDate();
                Calendar cal = Calendar.getInstance();
                cal.setTime(deliveryDate);
                int day = cal.get(Calendar.DAY_OF_WEEK);
                int month = cal.get(Calendar.MONTH) + 1;
                String dayOfWeek;
                String dateStr = cal.get(Calendar.DAY_OF_MONTH) + "/" + month;

                switch (day) {
                    case 1:
                        dayOfWeek = "ראשון";
                        break;
                    case 2:
                        dayOfWeek = "שני";
                        break;
                    case 3:
                        dayOfWeek = "שלישי";
                        break;
                    case 4:
                        dayOfWeek = "רביעי";
                        break;
                    case 5:
                        dayOfWeek = "חמישי";
                        break;
                    case 6:
                        dayOfWeek = "שישי";
                        break;
                    case 7:
                        dayOfWeek = "שבת";
                        break;
                    default:
                        dayOfWeek = "לא ידוע";
                        break;
                }

                DateFormat dateFormat = new SimpleDateFormat("HH:m", Locale.ENGLISH);
                String[] deliveryTime;
                try {
                    deliveryTime = dateFormat.format(order.getEstimatedDeliveryTime()).split(":");
                } catch (Exception ex) {
                    deliveryTime = "00:00".split(":");
                    ex.printStackTrace();
                }

                Integer deliveryHour = U.var.parseInt(deliveryTime[0]);
                System.out.format("   ### Time : %s %d%n", order.getEstimatedDeliveryTime(), deliveryHour);
                Integer minDeliveryHour = deliveryHour < 9 ? 8 : deliveryHour - 1;
                Integer maxDeliveryHour = deliveryHour > 20 ? 23 : deliveryHour + 3;
                if (order.getDeliveryWindow() == 0) {  // Day deliveries limit to 18:00
                    maxDeliveryHour = deliveryHour > 15 ? 18 : deliveryHour + 3;
                }
                String  deliveryTimeStr = minDeliveryHour.toString() + ":00-" + maxDeliveryHour.toString() + ":00";
                message = messageTemplate.getMessage().replace("%1", dayOfWeek);
                message = message.replace("%2", dateStr);
                message = message.replace("%3", deliveryTimeStr);
                System.out.format("   ### Sending message %s%n", message);
                break;
            case 2 : message = messageTemplate.getMessage(); break;
            case 3 : message = messageTemplate.getMessage().replace("%d", "" + order.getWpId()); break;
            default: break;
        }


        StringBuilder result = new StringBuilder();

        parameters.append("&msg=").append(U.var.urlEncode(message));

        System.out.format("   ### SMS Parameters: %s%n", parameters.toString());
        if (messageTemplate.getEnable() == 1) {
            try {
                byte[] parametersBytes = parameters.toString().getBytes();
                //URL url = new URL("http://logistics.shookbook.co.il/api/setBlogicOrder.php" + U.var.urlEncode(parameters.toString()));
                URL url = new URL("http://logistics.shookbook.co.il/api/setSmsMessage.php" + parameters.toString());
                //URL url = new URL("http://logistics.shookbook.co.il/api/setBlogicOrder.php?orderId=101010&debug=1");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                System.out.format("   ### Site: %s%n", url.toString());
                // conn.setInstanceFollowRedirects( false );
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                conn.setRequestProperty("charset", "utf-8");
                conn.setRequestProperty("Content-Length", "" + parametersBytes.length);
                conn.setUseCaches(false);
                //System.out.format("   ### Site: %s%n", url.toString());
                conn.getOutputStream().write(parametersBytes);
                Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

                //System.out.format("   ### Site: %s%n", url.toString());
                for (int c; (c = in.read()) >= 0; ) {
                    result.append((char) c);
                }
            } catch (Exception ex) {
                System.out.format("   ### Cannot read Site%n");
                ex.printStackTrace();
            }
        } else {
            System.out.format("   ## Not sending sms message disabled: %s%n", message);
        }

        return "OK";
    }
}
