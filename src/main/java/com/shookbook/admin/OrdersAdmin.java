package com.shookbook.admin;

import com.shookbook.dao.shookbook.*;
import com.shookbook.dao.wordpress.WpOrderItemsDao;
import com.shookbook.dao.wordpress.WpPostmetaDao;
import com.shookbook.entities.shookbook.*;
import com.shookbook.entities.wordpress.WpOrderItem;
import com.shookbook.types.HashSetIgnoresNulls;
import com.shookbook.utils.U;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@SessionAttributes("thought")
public class OrdersAdmin {
    @Autowired
    private Common common;
    @Autowired
    private LogisticsAdmin logisticsAdmin;
    @Autowired
    private CouponsAdmin couponsAdmin;
    @Autowired
    private ProductsDao productsDao;
    @Autowired
    private CustomersDao customersDao;
    @Autowired
    private OrdersDao ordersDao;
    @Autowired
    private OrderProductDao orderProductDao;
    @Autowired
    private ItemsDao itemsDao;
    @Autowired
    private SuppliersDao suppliersDao;
    @Autowired
    private SupplierOrdersDao supplierOrdersDao;
    @Autowired
    private SupplierOrderItemDao supplierOrderItemDao;
    @Autowired
    private WorkersDao workersDao;
    @Autowired
    private RefundsDao refundsDao;
    @Autowired
    private GeoPoinsDao geoPoinsDao;
    @Autowired
    private GlobalsDao globalsDao;
    @Autowired
    private CouponsDao couponsDao;
    @Autowired
    private WpPostmetaDao wpPostmetaDao;
    @Autowired
    private WpOrderItemsDao wpOrderItemsDao;

    @RequestMapping(value = "/admin/orders", method = RequestMethod.GET)
    public Object select(Model model, HttpServletRequest request) {
        List<Order> orders = ordersDao.findAllOrderByIdDesc();
        Set<Integer> customerIds = new HashSetIgnoresNulls<>();
        for (Order order : orders) {
            customerIds.add(order.getCustomerId());
        }
        Map<Integer, Customer> customerMap = new HashMap<>();
        for (Customer customer : customersDao.findAll(customerIds)) {
            customerMap.put(customer.getId(), customer);
        }

        List<Map<String, Object>> columns = new LinkedList<>();
        for (Order order : orders) {
            Map<String, Object> column = new HashMap<>();
            column.put("id", order.getId());
            column.put("barcode", order.getBarcode());
            column.put("customerBarcode", customerMap.get(order.getCustomerId()).getBarcode());
            column.put("name", customerMap.get(order.getCustomerId()).getFirstName() + " " + customerMap.get(order.getCustomerId()).getLastName());
            column.put("email", customerMap.get(order.getCustomerId()).getEmail());
            column.put("phone", customerMap.get(order.getCustomerId()).getPhone());
            column.put("city", customerMap.get(order.getCustomerId()).getCity());
            columns.add(column);
        }
        model.addAttribute("columns", columns);
        model.addAttribute("admin", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.ADMIN.ordinal() ? 1 : 0);

        return "Orders";
    }


    @RequestMapping(value = "/admin/orders/add", method = RequestMethod.GET)
    public Object add(Model model, HttpServletRequest request) {
        List<Product> products = productsDao.findAll();
        Map<Integer, Object> productMap = new HashMap<>();
        for (Product product : products) {
            productMap.put(product.getId(), product);
        }
        List<Map<String, Object>> columns = new LinkedList<>();
        for (Item item : itemsDao.findAll()) {
            Map<String, Object> column = new HashMap<>();
            column.put("item", item);
            column.put("product", productMap.get(item.getProductId()));
            columns.add(column);
        }
        model.addAttribute("columns", columns);
        model.addAttribute("admin", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.ADMIN.ordinal() ? 1 : 0);

        return "OrdersAdd";
    }

    @RequestMapping(value = "/admin/orders/addHeb", method = RequestMethod.GET)
    public Object addHeb(Model model, HttpServletRequest request) {
        //        @RequestParam(value = "id") Integer id,
        //Order order = ordersDao.getOne(id);
        Map<String, Object> tmp = new HashMap<>();
        tmp.put("orderNumber", common.createOrderCode(0));
        //will be relevent in the future...
        // tmp.put("clientNumber", customersDao.findOne(1).getBarcode());
        List<Product> products = productsDao.findAll();
        Map<Integer, Object> productMap = new HashMap<>();
        for (Product product : products) {
            productMap.put(product.getId(), product);
        }
        List<Map<String, Object>> columns = new LinkedList<>();
        for (Item item : itemsDao.findAll()) {
            Map<String, Object> column = new HashMap<>();
            column.put("item", item);
            column.put("product", productMap.get(item.getProductId()));
            columns.add(column);
        }
        model.addAttribute("columns", columns);
        model.addAttribute("admin", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.ADMIN.ordinal() ? 1 : 0);

        return "OrdersAddHeb";
    }


    @RequestMapping(value = "/admin/orders/get", method = RequestMethod.GET)
    public Object get(@RequestParam(value = "id") Integer id, Model model, HttpServletRequest request) {
        String barcode = ordersDao.findOne(id).getBarcode();
        model.addAttribute("qr", barcode);
        model.addAttribute("admin", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.ADMIN.ordinal() ? 1 : 0);

        return "OrdersGet";
    }

    private class _Order {
        Order order;
        String orderId;
        String customerId;
        String firstName;
        String lastName;
        String phone;
        String email;
        String no;
        String street;
        String city;
        String zip;
        Short creditCardToken;
        List<Object[]> products;


        public String createBarcode(Customer customer) {
            StringBuilder barcode = new StringBuilder();
            barcode.append('<').append(orderId).append(">");
            return barcode.toString();
        }

        public void insert() {
            Customer customer = customersDao.findOneByBarcode(customerId);
            if (customer == null) {
                customer = new Customer();
                customer.setBarcode(customerId);
            }

            if (firstName != null) {
                customer.setFirstName(firstName);
            }
            if (lastName != null) {
                customer.setLastName(lastName);
            }
            if (phone != null) {
                customer.setPhone(phone);
            }
            if (email != null) {
                customer.setEmail(email);
            }
            if (no != null) {
                customer.setStreetNo(no);
            }
            if (street != null) {
                customer.setStreet(street);
            }
            if (city != null) {
                customer.setCity(city);
            }
            if (creditCardToken != null) {
                customer.setCreditPoints(Short.decode("0"));
            }
            customersDao.save(customer);

            order = new Order();
            order.setPointer((short) 0);
            order.setBarcode(createBarcode(customer));
            order.setCustomerId(customer.getId());
            order.setShippingDate(new Date(System.currentTimeMillis()));
            ordersDao.save(order);
            if (orderId == null) {
                orderId = "" + order.getId();
                order.setBarcode(createBarcode(customer));
                ordersDao.save(order);
            }

            List<OrderProduct> orderProducts = new LinkedList<>();

            for (Object[] product : products) {
                OrderProduct orderProduct = new OrderProduct();
                orderProduct.setOrderId(order.getId());
                orderProduct.setItemId(U.var.parseInt(product[0]));
                orderProduct.setQuantityNeeded(U.var.parseInt(product[2]));
                orderProduct.setQuantity(0f);
                orderProducts.add(orderProduct);
            }
            orderProductDao.save(orderProducts);
        }
    }

    @RequestMapping(value = "/admin/orders/add/submit", method = RequestMethod.POST)
    public Object addSubmit(HttpServletRequest request) {
        _Order _order = new _Order();
        _order.customerId = request.getParameter("customerId");
        _order.firstName = request.getParameter("firstName");
        _order.lastName = request.getParameter("lastName");
        _order.phone = request.getParameter("phone");
        _order.phone = request.getParameter("phone2");
        _order.email = request.getParameter("email");
        _order.no = request.getParameter("no");
        _order.street = request.getParameter("street");
        _order.city = request.getParameter("city");
        _order.zip = request.getParameter("zip");
        _order.products = new LinkedList<>();
        for (Item item : itemsDao.findAll()) {
            int c = U.var.parseInt(request.getParameter("item" + item.getId()));
            if (c != 0) {
                _order.products.add(new Object[]{item.getId(), item.getBarcode(), c});
            }
        }
        _order.insert();
        return "redirect:/admin/orders/get?id=" + _order.order.getId();
    }


    @RequestMapping(value = "/admin/orders/import", method = RequestMethod.POST)
    public Object ordersImport(@RequestParam(value = "importFile") MultipartFile importFile, Model model, HttpServletRequest request) {
        Map<String, Item> itemMap = new HashMap<>();
        for (Item item : itemsDao.findAll()) {
            itemMap.put(item.getBarcode(), item);
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(importFile.getInputStream()));
            String line = reader.readLine();
            String[] t = line.split(",");
            _Order[] _orders = new _Order[t.length - 2];
            for (int i = 2; i < t.length; i++) {
                _orders[i - 2] = new _Order();
                _orders[i - 2].customerId = t[i];
                _orders[i - 2].products = new LinkedList<>();
            }

            for (; (line = reader.readLine()) != null; ) {
                t = line.split(",");
                for (int i = 2; i < t.length; i++) {
                    int c = U.var.parseInt(t[i]);
                    if (c > 0) {
                        Item item = itemMap.get(t[0]);
                        _orders[i - 2].products.add(new Object[]{item.getId(), item.getBarcode(), c});
                    }
                }
            }
            for (_Order _order : _orders) {
                _order.insert();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return "redirect:/admin/orders";
    }

    @RequestMapping(value = "/admin/orders/print", method = RequestMethod.POST)
    public Object print(@RequestParam(value = "fromOrder") Integer fromOrder, @RequestParam(value = "toOrder") Integer toOrder, Model model, HttpServletRequest request) {
        List<Map<String, Object>> orders = new LinkedList<>();

        int i = 0;
        for (Order order : ordersDao.findAllBetweenIds(fromOrder, toOrder)) {
            Map<String, Object> o = new HashMap<>();
            o.put("index", i++);
            o.put("barcode", order.getBarcode());
            orders.add(o);
        }
        model.addAttribute("orders", orders);
        model.addAttribute("admin", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.ADMIN.ordinal() ? 1 : 0);

        return "OrdersPrint";
    }

    @RequestMapping(value = "/admin/customers/orders/order", method = RequestMethod.GET)
    public Object order(@RequestParam(value = "id") Integer id, Model model, HttpServletRequest request) {
        int shippingFee;
        if (!U.var.parseBoolean(request.getSession().getAttribute("login"))) {
            return "redirect:/admin/login";
        }
        Order order = ordersDao.findOne(id);
        int customerId = order.getCustomerId();
        Customer customer = customersDao.findOne(customerId);

        Map<Integer, Worker> workersMap = new HashMap<>();
        workersMap.put(0, new Worker());
        for (Worker worker : workersDao.findAll()) {
            workersMap.put(worker.getId(), worker);
        }

        Map<String, Object> column = new HashMap<>();
        column.put("id", order.getId());
        column.put("orderNumber", common.createOrderCode(order.getId()));
        column.put("wpId", order.getWpId());
        column.put("dhlPackageNumber", order.getDhlPackageNumber());
        column.put("orderShippingDate", order.getLastPackingTimestamp() == null ? "" : new SimpleDateFormat("dd-MM-yyyy").format(order.getLastPackingTimestamp()));
        column.put("customerId", customer.getId());
        column.put("customerEmail", customer.getEmail());
        column.put("customerEmailto", "mailto:" + U.var.urlEncode(customer.getEmail()));
        column.put("customerPhone", customer.getPhone());
        column.put("customerPhone2", customer.getPhone2());
        column.put("cardHolder", order.getClearingCardHolder());
        column.put("clearingStatus", order.getClearingStatus().heb);
        column.put("card4Digits", order.getClearingFourDigits());
        column.put("cardExpiration", order.getClearingMonth() + "/" + order.getClearingYear());
        column.put("creditCardType", order.getCardTypeString() + " / " + order.getCardMutagString());
        column.put("shipmentStreet", order.getStreet() + " " + order.getStreetNo());
        column.put("shipmentCity", order.getCity());
        column.put("shipmentApartment", order.getApartment());
        column.put("shipmentFloor", order.getFloorNo());
        column.put("shipmentEntryCode", order.getEntryCode());
        column.put("orderCreatedDate", new SimpleDateFormat("dd-MM-yyyy").format(order.getCreatedDate()));
        column.put("clientName", order.getFirstName() + " " + order.getLastName());
        column.put("memberId", customer.getWpId());
        column.put("boxes", order.getBoxes());
        column.put("remarks", order.getRemarks());
        column.put("deliveryWindow", order.getDeliveryWindow());
        Date deliveryDate = order.getDeliveryDate();
        if (order.getDeliveryWindow() == 1) {
            deliveryDate = new Date(deliveryDate.getTime() - 24*60*60*1000);
        }
        column.put("deliveryDate", deliveryDate == null ? "" : new SimpleDateFormat("dd-MM-yyyy").format(deliveryDate));
        column.put("workerId", workersMap.get(order.getPackingWorkerId()).getSbId());
        column.put("googleMap", "https://maps.google.com/?q=" + U.var.urlEncode(order.getStreet() + " " + order.getStreetNo() + " " + order.getCity() + " ישראל"));

        List<Order> orders = ordersDao.findAllByCustomerIdDesc(order.getCustomerId());

        column.put("numberOfOrders", orders.size());

        model.addAttribute("column", column);
        model.addAttribute("admin", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.ADMIN.ordinal() ? 1 : 0);


        Set<Integer> productIds = new HashSetIgnoresNulls<>();
        Set<Integer> itemIds = new HashSetIgnoresNulls<>();
        for (OrderProduct orderProduct : orderProductDao.findAllByOrderId(id)) {
            productIds.add(orderProduct.getProductId());
            itemIds.add(orderProduct.getItemId());
        }

        Map<Integer, Product> productsMap = new HashMap<>();
        if (!productIds.isEmpty()) {
            for (Product product : productsDao.findAll(productIds)) {
                productsMap.put(product.getId(), product);

            }
        }

        Map<Integer, Item> itemsMap = new HashMap<>();
        if (!itemIds.isEmpty()) {
            for (Item item : itemsDao.findAll(itemIds)) {
                itemsMap.put(item.getId(), item);
            }
        }

        List<Map<String, Object>> elements = new LinkedList<>();
        //---------
        //Calculate VAT::
        float totalVat = 0;
        DecimalFormat df = new DecimalFormat("#.##");
        List<OrderProduct> orderProducts = orderProductDao.findAllByOrderId(order.getId());
        float sum = 0;
        float originalSum = 0;
        float vatRate = U.var.parseFloat(globalsDao.findOne("vat_rate").getValue());
        for (OrderProduct orderProduct : orderProducts) {
            originalSum += orderProduct.getUnitPrice() * orderProduct.getQuantityNeeded();
            sum += orderProduct.getUnitPrice() * orderProduct.getQuantity();
            if (productsMap.get(orderProduct.getProductId()).hasVat()) {
                totalVat += (orderProduct.getUnitPrice() * orderProduct.getQuantity()) * (1 - 1 / vatRate);
            }
            Map<String, Object> element = new HashMap<>();
            Item item = itemsMap.get(orderProduct.getItemId());
            element.put("itemBarcode", item == null ? "N/A" : item.getBarcode());
            element.put("orderId", orderProduct.getOrderId());
            element.put("productId", orderProduct.getProductId());
            element.put("orderProductId", orderProduct.getId());
            element.put("productName", productsMap.get(orderProduct.getProductId()).getNameHeb() + " - " + productsMap.get(orderProduct.getProductId()).getNameWeight());
            element.put("quantityNeeded", (int) (orderProduct.getQuantityNeeded()));
            element.put("quantity", (int) (orderProduct.getQuantity()));
            element.put("unitPrice", orderProduct.getUnitPrice());
            element.put("sumToPay", df.format(orderProduct.getUnitPrice() * orderProduct.getQuantity()));
            // element.put("vat", productsMap.get(orderProduct.getProductId()).getVat());
            elements.add(element);
        }

        if (customer.getType().equals(Customer.CustomerTypes.GOLD) || customer.getType().equals(Customer.CustomerTypes.PLATINUM) || originalSum >= Integer.parseInt(globalsDao.findOne("delivery_price_min_for_reduced").getValue())) {
            shippingFee = Integer.parseInt(globalsDao.findOne("delivery_price_reduced").getValue());
        } else {
            shippingFee = Integer.parseInt(globalsDao.findOne("delivery_price_standard").getValue());
        }
        totalVat += shippingFee * (1 - 1 / vatRate);

        List<Map<String, Object>> orderCoupons = new LinkedList<>();

        if (order.getType().getOrderTypeCode() < 80) { // No coupons for external orders
            List<WpOrderItem> orderItems = wpOrderItemsDao.findAllByOrderIdAndKey(U.var.parseLong(order.getWpId()), "coupon");
            for (WpOrderItem orderItem : orderItems) {
                Map<String, Object> orderCopoun = new HashMap<>();
                orderCopoun.put("couponTitle", orderItem.getOrderItemName());
                orderCoupons.add(orderCopoun);
            }
        }

        String lat = "", lon = "";
        boolean foundSameAddress = false;
        boolean sameAddress;
        List<GeoPoint> geoPoints = geoPoinsDao.findAllByCustomerId(customerId);
        for (GeoPoint geoPoint : geoPoints) {
            sameAddress = true;
            if (!order.getCity().equals(geoPoint.getCity())) {
                sameAddress = false;
            }
            if (!order.getStreet().equals(geoPoint.getStreet())) {
                sameAddress = false;
            }
            if (!order.getStreetNo().equals(geoPoint.getStreetNumber())) {
                sameAddress = false;
            }
            if (sameAddress) {
                foundSameAddress = true;
                lat = geoPoint.getLatitude().toString();
                lon = geoPoint.getLongitude().toString();
            }
        }

        model.addAttribute("location", lat + "    " + lon);
        model.addAttribute("gotLocation", foundSameAddress ? 1 : 0);
        model.addAttribute("geoGroup", (customer.getForceGeotag() > 0) ? 1 : 0);
        model.addAttribute("orderType", order.getType().getOrderTypeCode());
        model.addAttribute("orderType", order.getType().getOrderTypeCode());
        model.addAttribute("originalSum", df.format(originalSum));
        model.addAttribute("shippingFee", df.format(shippingFee));
        model.addAttribute("discountSum", df.format(U.var.parseFloat(order.getDiscountSum())));
        model.addAttribute("vat", df.format(totalVat));
        model.addAttribute("totalSumWithoutShipmentFee", df.format(sum));
        model.addAttribute("totalSumToPay", df.format(sum - order.getDiscountSum() + shippingFee));
        model.addAttribute("orderId", order.getId());
        model.addAttribute("statusId", order.getStatus().ordinal());
        int canChange = ((order.getStatus() == Order.OrderStatuses.OPEN) ||
                        (order.getStatus() == Order.OrderStatuses.HARVEST) ||
                        (order.getStatus() == Order.OrderStatuses.IN_ORDER)) ? 1 : 0;
        model.addAttribute("canChange", canChange);
        int canRefund = ((order.getClearingStatus() == Order.ClearStatus.UNCLEAR) ||
                                (order.getClearingStatus() == Order.ClearStatus.PENDING)) &&
                        ((order.getStatus() == Order.OrderStatuses.SHIPMENT) ||
                                (order.getStatus() == Order.OrderStatuses.DELIVERED))
                         ? 1 : 0;
        int couponOnly = ((order.getClearingStatus() == Order.ClearStatus.SUCCEED) ||
                         (order.getClearingStatus() == Order.ClearStatus.INVOICE_ONLY) ||
                         (order.getClearingStatus() == Order.ClearStatus.MANUAL)) ? 1 : 0;
        model.addAttribute("canRefund", couponOnly == 1 ? 0 : canRefund);
        model.addAttribute("couponOnly", couponOnly);
        model.addAttribute("statusHeb", order.getStatus().heb);
        model.addAttribute("elements", elements);
        model.addAttribute("products", productsDao.findAllValid());
        model.addAttribute("coupons", couponsDao.findAllValidForCustomerId(order.getCustomerId()));
        model.addAttribute("orderCoupons", orderCoupons);
        Map<String, Object> globals = new HashMap<>();
        globals.put("modules_enable_coupons", globalsDao.findOne("modules_enable_coupons").getValue());
        globals.put("modules_enable_invoice", globalsDao.findOne("modules_enable_invoice").getValue());
        globals.put("modules_enable_logistics", globalsDao.findOne("modules_enable_logistics").getValue());
        globals.put("modules_enable_packing", globalsDao.findOne("modules_enable_packing").getValue());
        Object logedWorker = request.getSession().getAttribute("userId");
        Worker worker = workersDao.getOne(Integer.parseInt(logedWorker.toString()));
        Worker.Types workerRole = worker.getType();
        globals.put("worker_name", worker.getFirstName() + " " + worker.getLastName());
        model.addAttribute("globals", globals);
        model.addAttribute("previlige", ((workerRole == Worker.Types.ADMIN) || (workerRole == Worker.Types.CUSTOMER_SUPPORT)) ? 1 : 0);
        model.addAttribute("admin", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.ADMIN.ordinal() ? 1 : 0);

        return "OrderDetails";
    }

    @RequestMapping(value = "/admin/customers/orders/certificates", method = RequestMethod.GET)
    public Object orderCertificates(HttpServletRequest request) {
        List<Order> orders = ordersDao.findAllNotCertificated();
        if (orders.isEmpty()) {
            return "Back";
        }

        StringBuilder orderIds = new StringBuilder();
        Set<Integer> customerIds = new HashSetIgnoresNulls<>();
        for (Order order : orders) {
            orderIds.append(order.getId()).append(',');
            customerIds.add(order.getCustomerId());
        }
        orderIds.setLength(orderIds.length() - 1);
        Map<Integer, Customer> customersMap = new HashMap<>();
        for (Customer customer : customersDao.findAll(customerIds)) {
            customersMap.put(customer.getId(), customer);
        }

        for (Order order : orders) {
            order.setCertificated('1');
            common.sendEmail(customersMap.get(order.getCustomerId()).getEmail(), "שוקבוק - תעודת משלוח מספר " + common.createCertificateCode(order.getId()), U.http.stringFromUrl("http://" + request.getLocalName() + ":" + request.getLocalPort() + "/admin/customers/orders/certificate?id=" + order.getId()));
        }
        ordersDao.save(orders);
        return "redirect:/admin/customers/orders/certificate?ids=" + orderIds.toString();
    }

    @RequestMapping(value = "/admin/customers/orders/certificatesCron", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public void orderCertificatesCron(HttpServletRequest request) {
        List<Order> orders = ordersDao.findAllNotCertificated();
        if (!orders.isEmpty()) {

            StringBuilder orderIds = new StringBuilder();
            Set<Integer> customerIds = new HashSetIgnoresNulls<>();
            for (Order order : orders) {
                orderIds.append(order.getId()).append(',');
                customerIds.add(order.getCustomerId());
            }
            orderIds.setLength(orderIds.length() - 1);
            Map<Integer, Customer> customersMap = new HashMap<>();
            for (Customer customer : customersDao.findAll(customerIds)) {
                customersMap.put(customer.getId(), customer);
            }

            for (Order order : orders) {
                order.setCertificated('1');
                common.sendEmail(customersMap.get(order.getCustomerId()).getEmail(), "שוקבוק - תעודת משלוח מספר " + common.createCertificateCode(order.getId()), U.http.stringFromUrl("http://" + request.getLocalName() + ":" + request.getLocalPort() + "/admin/customers/orders/certificate?id=" + order.getId()));
            }
            ordersDao.save(orders);
        }
        // return "redirect:/admin/customers/orders/certificate?ids=" + orderIds.toString();
    }

    @RequestMapping(value = "/admin/customers/orders/certificate", method = RequestMethod.GET)
    public Object orderCertificate(@RequestParam(value = "id", required = false) Integer id, @RequestParam(value = "ids", required = false) String ids, Model model, HttpServletRequest request) {
        if (id == null) {
            List<String> urls = new LinkedList<>();
            for (String id0 : ids.split(",")) {
                urls.add("http://" + request.getLocalName() + ":" + request.getLocalPort() + "/admin/customers/orders/certificate?id=" + id0);
            }
            model.addAttribute("back", "/admin/customers/orders");
            model.addAttribute("urls", urls);
            return "Pages";
        } else {
            float vatRate = U.var.parseFloat(globalsDao.findOne("vat_rate").getValue());
            Order order = ordersDao.findOne(id);
            Customer customer = customersDao.findOne(order.getCustomerId());
            List<OrderProduct> orderProducts = orderProductDao.findAllByOrderId(id);
            Set<Integer> productIds = new HashSetIgnoresNulls<>();
            for (OrderProduct orderProduct : orderProducts) {
                productIds.add(orderProduct.getProductId());
                if (order.getCertificated() == '0') {
                    orderProduct.setQuantityDelivered(orderProduct.getQuantity());
                    orderProductDao.save(orderProduct);
                }
            }
            HashMap<Integer, Product> productsMap = new HashMap<>();
            for (Product product : productsDao.findAll(productIds)) {
                productsMap.put(product.getId(), product);
            }

            List<Map<String, Object>> products = new LinkedList<>();
            float total = 0;
            float originalTotal = 0;
            float totalVat = 0;
            float discount = U.var.parseFloat(order.getDiscountSum());

            for (OrderProduct orderProduct : orderProducts) {
                if (orderProduct.getQuantityDelivered() > 0) {
                    Map<String, Object> map = new HashMap<>();
                    Product product = productsMap.get(orderProduct.getProductId());
                    if ("".equals(product.getNameWeight())) {
                        map.put("name", product.getNameHeb());
                    } else {
                        map.put("name", product.getNameHeb() + " - " + product.getNameWeight());
                    }
                    map.put("packageType", product.getPackageType().heb);
                    map.put("unitPrice", orderProduct.getUnitPrice());
                    map.put("vat", String.format("%.2f", orderProduct.getUnitPrice() * (1 - 1 / vatRate) * U.var.parseInt(product.getVat())));
                    map.put("quantity", orderProduct.getQuantityDelivered());
                    map.put("totalPrice", String.format("%.2f", orderProduct.getQuantityDelivered() * orderProduct.getUnitPrice()));
                    totalVat += orderProduct.getUnitPrice() * (1 - 1 / vatRate) * U.var.parseInt(product.getVat());
                    total += orderProduct.getQuantityDelivered() * orderProduct.getUnitPrice();
                    products.add(map);
                }
                originalTotal += orderProduct.getQuantityNeeded() * orderProduct.getUnitPrice();
            }

            List<String> missingProducts = new LinkedList<>();
            for (OrderProduct orderProduct : orderProducts) {
                if (orderProduct.getQuantityDelivered() < orderProduct.getQuantityNeeded()) {
                    Product product = productsMap.get(orderProduct.getProductId());
                    String line = product.getNameHeb() + " " + (orderProduct.getQuantityNeeded() - orderProduct.getQuantityDelivered()) + " יחידות";
                    missingProducts.add(line);
                }
            }

            float shippingFee;
            if (customer.getType().ordinal() > Customer.CustomerTypes.SILVER.ordinal() || originalTotal >= Integer.parseInt(globalsDao.findOne("delivery_price_min_for_reduced").getValue())) {
                shippingFee = U.var.parseFloat(globalsDao.findOne("delivery_price_reduced").getValue());
            } else {
                shippingFee = U.var.parseFloat(globalsDao.findOne("delivery_price_standard").getValue());
            }

            totalVat += shippingFee * (1 - 1 / vatRate);

            model.addAttribute("orderId", order.getId());
            model.addAttribute("orderCode", common.createOrderCode(order.getId()));
            model.addAttribute("certificateCode", common.createCertificateCode(order.getId()));
            model.addAttribute("fullName", order.getFirstName() + " " + order.getLastName());
            model.addAttribute("address", order.getStreet() + " " + U.var.ignoreNull(order.getStreetNo()) + ", " + order.getCity());
            model.addAttribute("products", products);
            model.addAttribute("missingProducts", missingProducts.size() == 0 ? null : missingProducts);
            model.addAttribute("discount", String.format("%.2f", discount));
            model.addAttribute("shippingFee", String.format("%.2f", shippingFee));
            model.addAttribute("total", String.format("%.2f", total));
            model.addAttribute("totalVat", String.format("%.2f", totalVat));
            model.addAttribute("grandTotal", String.format("%.2f", total - discount + shippingFee));
            order.setSum(total - discount + shippingFee);
            ordersDao.save(order);

            return "OrderCertificate";
        }
    }

    @RequestMapping(value = "/admin/customers/orders/order/submit/quantity", method = RequestMethod.POST)
    public Object submitItemQuantity(@RequestParam(value = "orderId") Integer orderId, Model model, HttpServletRequest request) {
        int orderProductId;
        float quantity;
        Order order = ordersDao.findOne(orderId);
        for (String param : request.getParameterMap().keySet()) {
            if (param != null && param.startsWith("newQuantity")) {
                orderProductId = Integer.parseInt(param.substring(11));
                quantity = Float.valueOf(request.getParameter(param));
                OrderProduct orderProduct = orderProductDao.findOne(orderProductId);
                if (quantity == 0) {
                   orderProduct.setStatus(OrderProduct.OrderProductStatuses.UNSCANNED);
                } else if (quantity == orderProduct.getQuantityNeeded()) {
                   orderProduct.setStatus(OrderProduct.OrderProductStatuses.SCANNED);
                } else {
                   orderProduct.setStatus(OrderProduct.OrderProductStatuses.MISSING);
                }
                if (orderProduct.getQuantity() > quantity) {
                    Refund refund = new Refund();
                    refund.setCustomerId(order.getCustomerId());
                    refund.setOrderId(orderId);
                    refund.setCouponId(0);
                    refund.setRootCause(Refund.RootCause.BAD_PRODUCT);
                    refund.setCause(Refund.Cause.QUALITY);
                    refund.setType(Refund.Types.CHARGE);
                    Object logedWorker = request.getSession().getAttribute("userId");
                    refund.setWorkerId(Integer.parseInt(logedWorker.toString()));
                    refund.setQuantity(orderProduct.getQuantity() - quantity);
                    refund.setAmount((orderProduct.getQuantity() - quantity) * orderProduct.getUnitPrice());
                    refund.setDetails(productsDao.findOne(orderProduct.getProductId()).getNameHeb());
                    refundsDao.save(refund);
                }
                orderProduct.setQuantity(quantity);
                orderProductDao.save(orderProduct);
            }
        }
        return "redirect:/admin/customers/orders/order?id=" + orderId;
    }

    @RequestMapping(value = "/admin/customers/orders/printEnvelope", method = RequestMethod.GET)
    public Object printEnvelope(@RequestParam(value = "id") Integer id, Model model, HttpServletRequest request) {
        Order order = ordersDao.findOne(id);
        int customerId = order.getCustomerId();
        Customer customer = customersDao.findOne(customerId);


        Map<String, Object> details = new HashMap<>();
        details.put("customerName", customer.getFirstName() + " " + customer.getLastName());
        details.put("customerAddress", customer.getStreet() + " " + customer.getStreetNo());
        details.put("customerCity", customer.getCity());

        model.addAttribute("details", details);
        model.addAttribute("admin", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.ADMIN.ordinal() ? 1 : 0);

        return "PrintEnvelope";
    }

    @RequestMapping(value = "/admin/customers/orders/order/submit/boxes", method = RequestMethod.POST)
    public Object ordersDetailsSubmitBoxes(@RequestParam(value = "orderId") Integer orderId, @RequestParam(value = "boxesCount") Integer boxesCount, Model model, HttpServletRequest request) {
        Order order = ordersDao.findOne(orderId);
        order.setBoxes(boxesCount.shortValue());
        logisticsAdmin.setBlogicOrder(orderId, -1L, "", "", -1, 1, 0);
        ordersDao.save(order);
        return "redirect:/admin/customers/orders/order?id=" + orderId;
    }

    @RequestMapping(value = "/admin/customers/orders/order/submit/deliveryWindowChange", method = RequestMethod.GET)
    public Object ordersDetailsSubmitdeliveryWindowChange(@RequestParam(value = "orderId") Integer orderId, Model model, HttpServletRequest request) {
        Order order = ordersDao.findOne(orderId);
        if (order.getDeliveryWindow() == 0) {
            order.setDeliveryWindow((short) 1);
        } else {
            order.setDeliveryWindow((short) 0);
        }
        //logisticsAdmin.setBlogicOrder(orderId, -1L, "", "", -1, 1, 0);
        ordersDao.save(order);
        return "redirect:/admin/customers/orders/order?id=" + orderId;
    }

    @RequestMapping(value = "/admin/customers/orders/order/submit/productQuantity", method = RequestMethod.POST)
    public Object updateProductQuantity(@RequestParam(value = "orderProductId") Integer orderProductId, @RequestParam(value = "newQuantity") Float newQuantity, HttpServletRequest request) {
        OrderProduct orderProduct = orderProductDao.findOne(orderProductId);
        Order order = ordersDao.findOne(orderProduct.getOrderId());
        Product product = productsDao.findOne(orderProduct.getProductId());

        int deltaQuantity = U.var.parseInt(newQuantity - orderProduct.getQuantityNeeded());
        int stock = U.var.parseInt(wpPostmetaDao.findOneByIdAndKey(U.var.parseLong(product.getWpId()), "_stock").getMetaValue());
        if ((deltaQuantity < 0) || (stock > deltaQuantity)) {
            // Order status change
            // if (order.getStatus() == Order.OrderStatuses.IN_ORDER) {
            if ((order.getStatus() == Order.OrderStatuses.IN_ORDER) || (order.getStatus() == Order.OrderStatuses.HARVEST)) {
                Item item = itemsDao.findOne(orderProduct.getItemId());
                if ((deltaQuantity > 0) && (item.getInventory() < deltaQuantity)) {
                    return "redirect:/admin/customers/orders/order?id=" + orderProduct.getOrderId();
                }
                orderProduct.setItemId(item.getId());
                item.setInventory((short) (item.getInventory() - deltaQuantity));
                itemsDao.save(item);
            }
            wpPostmetaDao.updateByIdAndKey(U.var.parseLong(product.getWpId()), "_stock", "" + (stock - deltaQuantity));
            orderProduct = orderProductDao.save(orderProduct);

            if (newQuantity == 0) {
                orderProductDao.delete(orderProductId);
            } else {
                orderProduct.setQuantityNeeded(newQuantity);
                orderProductDao.save(orderProduct);
            }
        }

        return "redirect:/admin/customers/orders/order?id=" + orderProduct.getOrderId();
    }

    @RequestMapping(value = "/admin/customers/orders/order/submit/refundQuantity", method = RequestMethod.POST)
    public Object refundProductQuantity(@RequestParam(value = "refundOrderProductId") Integer refundOrderProductId,
                                        @RequestParam(value = "newQuantity") Float newQuantity,
                                        @RequestParam(value = "refundCause")  Integer refundCause,
                                        @RequestParam(value = "refundRemark") String refundRemark,
                                        HttpServletRequest request) {

        OrderProduct orderProduct = orderProductDao.findOne(refundOrderProductId);
        Order order = ordersDao.findOne(orderProduct.getOrderId());
        Product product = productsDao.findOne(orderProduct.getProductId());

        int deltaQuantity = U.var.parseInt(orderProduct.getQuantityNeeded() - newQuantity);
        Refund refund = new Refund();
        refund.setQuantity(newQuantity);
        String details = product.getNameHeb();
        if (refundRemark.length() > 1) {
            details = details + " - " + refundRemark;
        }
        refund.setDetails(details);
        refund.setAmount(newQuantity * orderProduct.getUnitPrice());
        refund.setType(Refund.Types.CHARGE);
        refund.setRootCause(Refund.RootCause.values()[refundCause]);
        switch (Refund.RootCause.values()[refundCause]) {
            case BAD_PRODUCT: refund.setCause(Refund.Cause.QUALITY); break;
            case WEIGHT: refund.setCause(Refund.Cause.QUALITY); break;
            case MISSING: refund.setCause(Refund.Cause.HANDLING); break;
            case DISORDERED: refund.setCause(Refund.Cause.HANDLING); break;
            case WRONG_PRODUCT: refund.setCause(Refund.Cause.HANDLING); break;
            default: refund.setCause(Refund.Cause.MISC); break;
        }
        refund.setCouponId(0);
        refund.setOrderId(order.getId());
        refund.setCustomerId(order.getCustomerId());
        Object logedWorker = request.getSession().getAttribute("userId");
        Worker worker = workersDao.getOne(Integer.parseInt(logedWorker.toString()));
        refund.setWorkerId(worker.getId());
        refundsDao.save(refund);

        orderProduct.setQuantity(deltaQuantity);
        orderProductDao.save(orderProduct);

        return "redirect:/admin/customers/orders/order?id=" + orderProduct.getOrderId();
    }

    @RequestMapping(value = "/admin/customers/orders/order/submit/addOrderProduct", method = RequestMethod.POST)
    public Object addOrderProduct(@RequestParam(value = "addProductOrderId") Integer orderId, @RequestParam(value = "productId") Integer productId, @RequestParam(value = "quantity") Float quantity, HttpServletRequest request) {
        Order order = ordersDao.findOne(orderId);
        Product product = productsDao.findOne(productId);
        String stockStr = wpPostmetaDao.findOneByIdAndKey(U.var.parseLong(product.getWpId()), "_stock").getMetaValue();
        Integer stockInt = Math.round(Float.parseFloat(stockStr));
        int stock = U.var.parseInt(wpPostmetaDao.findOneByIdAndKey(U.var.parseLong(product.getWpId()), "_stock").getMetaValue());
        if (stockInt > quantity) {
            OrderProduct orderProduct = new OrderProduct(orderId, productId, quantity, product.getPrice());
            // if (order.getStatus() == Order./*OrderStatuses.IN_ORDER) {
            if ((order.getStatus() == Order.OrderStatuses.IN_ORDER) || (order.getStatus() == Order.OrderStatuses.HARVEST)) {
                List<Item> items = itemsDao.findAllByProductIdWithQuantity(productId, quantity);
                if (items.isEmpty()) {
                    return "redirect:/admin/customers/orders/order?id=" + orderId;
                }
                Item item = items.get(0);
                orderProduct.setItemId(item.getId());
                item.setInventory((short) (item.getInventory() - quantity));
                itemsDao.save(item);
            }
            wpPostmetaDao.updateByIdAndKey(U.var.parseLong(product.getWpId()), "_stock", "" + (stock - quantity));
            orderProductDao.save(orderProduct);
        }
        return "redirect:/admin/customers/orders/order?id=" + orderId;
    }

    @RequestMapping(value = "/admin/customers/orders/order/submit/remarks", method = RequestMethod.POST)
    public Object ordersDetailsSubmitRemarks(@RequestParam(value = "orderId") Integer orderId, @RequestParam(value = "remarks") String remarks, Model model, HttpServletRequest request) {
        Order order = ordersDao.findOne(orderId);
        order.setRemarks(remarks);
        ordersDao.save(order);
        return "redirect:/admin/customers/orders/order?id=" + orderId;
    }

    @RequestMapping(value = "/admin/customers/orders/order/edit", method = RequestMethod.POST)
    public Object ordersDetailsEdit(@RequestParam(value = "orderId") Integer orderId, Model model, HttpServletRequest request) {
        Order order = ordersDao.findOne(orderId);
        Map<String, Object> column = new HashMap<>();
        column.put("id", order.getId());
        column.put("orderNumber", common.createOrderCode(order.getId()));
        column.put("wpId", order.getWpId());
        column.put("cardHolder", order.getClearingCardHolder());
        column.put("clearingStatus", order.getClearingStatus().heb);
        column.put("clearingStatusId", order.getClearingStatus().ordinal());
        column.put("card4Digits", order.getClearingFourDigits());
        column.put("cardExpiration", order.getClearingMonth() + "/" + order.getClearingYear());
        column.put("creditCardType", order.getCardTypeString() + " / " + order.getCardMutagString());
        column.put("shipmentStreet", order.getStreet());
        column.put("shipmentStreetNo", order.getStreetNo());
        column.put("shipmentCity", order.getCity());
        column.put("shipmentApartment", order.getApartment());
        column.put("shipmentFloor", order.getFloorNo());
        column.put("shipmentEntryCode", order.getEntryCode());
        column.put("clientName", order.getFirstName() + " " + order.getLastName());
        column.put("remarks", order.getRemarks());
        column.put("deliveryDate", order.getDeliveryDate() == null ? "" : new SimpleDateFormat("dd-MM-yyyy").format(order.getDeliveryDate()));
        model.addAttribute("column", column);

        Map<String, Object> globals = new HashMap<>();
        globals.put("modules_enable_coupons", globalsDao.findOne("modules_enable_coupons").getValue());
        globals.put("modules_enable_invoice", globalsDao.findOne("modules_enable_invoice").getValue());
        globals.put("modules_enable_logistics", globalsDao.findOne("modules_enable_logistics").getValue());
        globals.put("modules_enable_packing", globalsDao.findOne("modules_enable_packing").getValue());
        Object logedWorker = request.getSession().getAttribute("userId");
        Worker worker = workersDao.getOne(Integer.parseInt(logedWorker.toString()));
        globals.put("worker_name", worker.getFirstName() + " " + worker.getLastName());
        model.addAttribute("globals", globals);
        model.addAttribute("customer_support", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.CUSTOMER_SUPPORT.ordinal() ? 1 : 0);
        model.addAttribute("admin", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.ADMIN.ordinal() ? 1 : 0);

        return "OrderDetailsEdit";
    }

    @RequestMapping(value = "/admin/customers/orders/order/submitEdit", method = RequestMethod.POST)
    public Object ordersDetailsSubmitEdit(@RequestParam(value = "orderId") Integer orderId,
                                          @RequestParam(value = "street") String street,
                                          @RequestParam(value = "streetNo") String streetNo,
                                          @RequestParam(value = "city") String city,
                                          @RequestParam(value = "apartment") String apartment,
                                          @RequestParam(value = "floorNo") String floorNo,
                                          @RequestParam(value = "entryCode") String entryCode,
                                          @RequestParam(value = "clearingStatus") Integer clearingStatusOrdinal,
                                          @RequestParam(value = "cardHolder") String cardHolder,
                                          @RequestParam(value = "creditProfile") String creditProfile,
                                          @RequestParam(value = "remarks") String remarks) {
        Order order = ordersDao.findOne(orderId);
        String tokenCode = "";
        Date tokenValidThrough = null;
        String profileDescription = "";
        Integer tokenValid = 1;
        Integer tokenRespond = 99;
        String validYear = "";
        String validMonth = "";
        String cardOwner = "";
        String card4Digits = "";
        Short cardType = -1;
        Short cardMutag = -1;

        if (creditProfile.equals("noContent")) {
            // Do nothing
        } else {
            StringBuilder parameters = new StringBuilder();
            parameters.append("terminalnumber").append('=').append(U.var.urlEncode(globalsDao.findOne("cardcom_cvv_terminal_number").getValue())).append('&');
            parameters.append("username").append('=').append(U.var.urlEncode(globalsDao.findOne("cardcom_username").getValue())).append('&');
            parameters.append("lowprofilecode").append('=').append(U.var.urlEncode(creditProfile));

            StringBuilder result = new StringBuilder();

//            ResponseCode=0&Description=Low Profile Code Found&terminalnumber=34515&lowprofilecode=7f71070b-1fbb-4b6a-b4b7-a6b170574cf8&Operation=3&ProssesEndOK=0&DealRespone=41&DealResponse=41&InternalDealNumber=22906448&TokenResponse=0&Token=e9b59231-7181-4099-b1d8-005a03d88ff3&TokenExDate=20200201&CardValidityYear=2020&CardValidityMonth=1 &CardOwnerID=&NumOfPayments=1&ExtShvaParams.CardNumber5=9014&ExtShvaParams.Status1=700&ExtShvaParams.Sulac25=2&ExtShvaParams.JParameter29=2&ExtShvaParams.Tokef30=0120&ExtShvaParams.Sum36=10&ExtShvaParams.SumStars52=00000000&ExtShvaParams.ApprovalNumber71=0000000&ExtShvaParams.FirstPaymentSum78=00000000&ExtShvaParams.ConstPayment86=00000000&ExtShvaParams.NumberOfPayments94=00&ExtShvaParams.AbroadCard119=0&ExtShvaParams.CardTypeCode60=1&ExtShvaParams.Mutag24=1&ExtShvaParams.CardOwnerName=%d7%96%d7%99%d7%95+%d7%99%d7%a2%d7%a7%d7%91%d7%99&ExtShvaParams.CreditType63=1&ExtShvaParams.DealType61=01&ExtShvaParams.ChargType66=50&ExtShvaParams.TerminalNumber=34515&ExtShvaParams.BinId=0&ExtShvaParams.InternalDealNumber=22906448&ExtShvaParams.CouponNumber=28005602&TokenApprovalNumber=0000000&CardOwnerName=%d7%96%d7%99%d7%95+%d7%99%d7%a2%d7%a7%d7%91%d7%99&ReturnValue=1234&CoinId=1&OperationResponse=0&OperationResponseText=OKStringBuilder result = new StringBuilder();


            try {
                byte[] parametersBytes = parameters.toString().getBytes();
                URL url = new URL("https://secure.cardcom.co.il/Interface/BillGoldGetLowProfileIndicator.aspx");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                // conn.setInstanceFollowRedirects( false );
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                conn.setRequestProperty("charset", "utf-8");
                conn.setRequestProperty("Content-Length", "" + parametersBytes.length);
                conn.setUseCaches(false);
                conn.getOutputStream().write(parametersBytes);
                Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

                for (int c; (c = in.read()) >= 0; ) {
                    result.append((char) c);
                }

            } catch (Exception ex) {
                System.out.format("   Invoice failed!!%n");
            }
            String[] profileFields = result.toString().split("&");
            String decodedField = "";

            for (int index = 0; index < profileFields.length; index++) {
//                System.out.format("   %s%n", invoiceFields[index]);
                try {
                    decodedField = java.net.URLDecoder.decode(profileFields[index], "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                String[] values = decodedField.split("=");
//                if (values[0].equals("ExtReadInvoiceHead.CustomerNumber")) {
                if (values[0].equals("ResponseCode")) {
                    tokenValid = U.var.parseInt(values[1]);
                }

                if (values[0].equals("Description")) {
                    profileDescription = values[1];
                }

                if (values[0].equals("Token")) {
                    tokenCode = values[1];
                }

                if (values[0].equals("TokenExDate")) {
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");

                    try {
                        tokenValidThrough = dateFormat.parse(values[1]);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }

                if (values[0].equals("CardOwnerName")) {
                    cardOwner = values[1];
                }

                if (values[0].equals("CardValidityYear")) {
                    validYear = values[1];
                }

                if (values[0].equals("CardValidityMonth")) {
                    validMonth = values[1];
                }

                if (values[0].equals("ExtShvaParams.CardNumber5")) {
                    card4Digits = values[1];
                }

                if (values[0].equals("ExtShvaParams.CardTypeCode60")) {
                    cardType = Short.parseShort(values[1]);
                }

                if (values[0].equals("ExtShvaParams.Mutag24")) {
                    cardMutag = Short.parseShort(values[1]);
                }
            }
//            System.out.format(" %s%n", result);
        }

//        System.out.format("  Profile: %s   Description: %s   Token valid: %d   Token: %s   Owner: %s   Valid thruogh: %s-%s   Type: %d   Mutag: %d%n", creditProfile, profileDescription, tokenValid, tokenCode, cardOwner, validMonth, validYear, cardType, cardMutag);

        if (tokenValid == 0) {
            order.setClearingToken(tokenCode);
            order.setClearingTokenValidThrough(tokenValidThrough);
            order.setClearingProfile(creditProfile);
            order.setClearingCardHolder(cardOwner);
            order.setClearingMonth(validMonth);
            order.setClearingYear(validYear);
            order.setClearingFourDigits(card4Digits);
            order.setClearingCardType(cardType);
            order.setClearingCardMutag(cardMutag);
        }

        order.setStreet(street);
        order.setStreetNo(streetNo);
        order.setCity(city);
        order.setApartment(apartment);
        order.setFloorNo(floorNo);
        order.setEntryCode(entryCode);
        order.setRemarks(remarks);
        Order.ClearStatus clearingStatus = Order.ClearStatus.values()[clearingStatusOrdinal];
        order.setClearingStatus(clearingStatus);
        ordersDao.save(order);
        return "redirect:/admin/customers/orders/order?id=" + orderId;
    }

    @RequestMapping(value = "/admin/orders/coupon", method = RequestMethod.GET)
    public Object orderCoupon(@RequestParam(value = "orderId", required = false) Integer orderId, @RequestParam(value = "customerId", required = false) Integer customerId, Model model, HttpServletRequest request) {
        if (!U.var.parseBoolean(request.getSession().getAttribute("login"))) {
            return "redirect:/admin/login";
        }
        Order order = ordersDao.findOne(orderId);
        Customer customer = customersDao.findOne(customerId);
        StringBuilder orderProdcutsData = new StringBuilder();

        Map<String, Object> column = new HashMap<>();
        column.put("orderId", order.getId());
        column.put("customerId", customer.getId());
        column.put("barcode", customer.getBarcode());
        column.put("fullName", customer.getFirstName() + " " + customer.getLastName());
        column.put("firstName", customer.getFirstName());
        column.put("lastName", customer.getLastName());
        column.put("nekoshook", customer.getCreditPoints());
        column.put("email", customer.getEmail());
        column.put("city", customer.getCity());
        column.put("house_number", customer.getStreetNo());
        column.put("street", customer.getStreet());
        column.put("phone", customer.getPhone());
        column.put("phone2", customer.getPhone2());
        column.put("creditPoints", customer.getCreditPoints());

        List<Map<String, Object>> productList = new LinkedList<>();
        for (Product product : productsDao.findAllByOrderId(orderId)) {
            Map<String, Object> productColumn = new HashMap<>();
            productColumn.put("name", product.getNameHeb());
            OrderProduct orderProduct = orderProductDao.findOneByOrderIdAndProductId(orderId, product.getId());
            productColumn.put("unit_price", orderProduct.getUnitPrice());
            productColumn.put("quantity", orderProduct.getQuantity());
            productColumn.put("id", product.getId());
            productList.add(productColumn);
            orderProdcutsData.append(product.getNameHeb()).append(';');
            orderProdcutsData.append(orderProduct.getUnitPrice()).append(';');
            orderProdcutsData.append(orderProduct.getQuantity()).append(';');
        }
        model.addAttribute("productList", productList);
        model.addAttribute("orderProductsData", orderProdcutsData.toString());

        model.addAttribute("column", column);
        Map<String, Object> globals = new HashMap<>();
        globals.put("modules_enable_coupons", globalsDao.findOne("modules_enable_coupons").getValue());
        globals.put("modules_enable_invoice", globalsDao.findOne("modules_enable_invoice").getValue());
        globals.put("modules_enable_logistics", globalsDao.findOne("modules_enable_logistics").getValue());
        globals.put("modules_enable_packing", globalsDao.findOne("modules_enable_packing").getValue());
        Object logedWorker = request.getSession().getAttribute("userId");
        Worker worker = workersDao.getOne(Integer.parseInt(logedWorker.toString()));
        globals.put("worker_name", worker.getFirstName() + " " + worker.getLastName());
        model.addAttribute("globals", globals);
        model.addAttribute("admin", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.ADMIN.ordinal() ? 1 : 0);

        return "OrderCoupon3";
    }

    @RequestMapping(value = "/admin/orders/coupon/submit", method = RequestMethod.POST)
    public Object orderCouponSubmit(@RequestParam(value = "orderId", required = false) Integer orderId,
                                    @RequestParam(value = "refunds_data", required = false) String refundsData,
                                    @RequestParam(value = "couponDetails", required = false) String couponRemarks,
                                    @RequestParam(value = "singleCouponAmount", required = false) Float singleCouponAmount,
                                    Model model, HttpServletRequest request) {

        Coupon.CouponTypes type = Coupon.CouponTypes.COMBINED;
        Coupon.CouponCause cause = Coupon.CouponCause.COMPANSATION;
        String remarks;
        remarks = "קופון מרובה";
        if (couponRemarks.length() > 1) {
            remarks = remarks + " - " + couponRemarks;
        }
        Object logedWorker = request.getSession().getAttribute("userId");
        Worker worker = workersDao.getOne(Integer.parseInt(logedWorker.toString()));
        Order order = ordersDao.findOne(orderId);
        Customer customer = customersDao.findOne(order.getCustomerId());
        String[] elements = refundsData.split(";");
        Integer couponId = couponsAdmin.createCoupon(type, cause, singleCouponAmount, remarks, order.getCustomerId(), orderId);
        for (int index = 0; index < elements.length/4; index++) {
            if(U.var.parseFloat(elements[4*index + 2]) > 0) {
                Integer rootCause = U.var.parseInt(elements[4*index]);
                System.out.format("   ## Refund %d%n", U.var.parseInt(elements[4*index]));
                Refund refund = new Refund();
                refund.setOrderId(orderId);
                refund.setCustomerId(order.getCustomerId());
                refund.setWorkerId(worker.getId());
                refund.setRootCause(Refund.RootCause.values()[rootCause]);
                switch (Refund.RootCause.values()[rootCause]) {
                    case BAD_PRODUCT: refund.setCause(Refund.Cause.QUALITY); break;
                    case WEIGHT: refund.setCause(Refund.Cause.QUALITY); break;
                    case MISSING: refund.setCause(Refund.Cause.HANDLING); break;
                    case DISORDERED: refund.setCause(Refund.Cause.HANDLING); break;
                    case WRONG_PRODUCT: refund.setCause(Refund.Cause.HANDLING); break;
                    case NO_DELIVERY: refund.setCause(Refund.Cause.DELIVERY); break;
                    case DELIVERY_BOY: refund.setCause(Refund.Cause.DELIVERY); break;
                    case BAD_BOX: refund.setCause(Refund.Cause.DELIVERY); break;
                    case COMPANSATION: refund.setCause(Refund.Cause.COMPANSATION); break;
                    case GENERAL: refund.setCause(Refund.Cause.MISC); break;
                    default: refund.setCause(Refund.Cause.MISC); break;
                }
                refund.setType(Refund.Types.COUPON);
                refund.setCouponId(couponId);
                refund.setQuantity(U.var.parseInt(elements[4*index+1]));
                refund.setAmount(U.var.parseFloat(elements[4*index+2]));
                refund.setDetails(elements[4*index+3]);
                refundsDao.save(refund);
            }
        }

        if (couponId > 0) {
            String mailSubject = "קופון זיכוי";
            common.sendEmailFrom("customer_support", customer.getEmail(), "שוקבוק - " + mailSubject, U.http.stringFromUrl("http://" + request.getLocalName() + ":" + request.getLocalPort() + "/admin/customers/coupon/email?customerId=" + customer.getId() + "&couponId=" + couponId));
        }
        Map<String, Object> globals = new HashMap<>();
        globals.put("modules_enable_coupons", globalsDao.findOne("modules_enable_coupons").getValue());
        globals.put("modules_enable_invoice", globalsDao.findOne("modules_enable_invoice").getValue());
        globals.put("modules_enable_logistics", globalsDao.findOne("modules_enable_logistics").getValue());
        globals.put("modules_enable_packing", globalsDao.findOne("modules_enable_packing").getValue());
        globals.put("worker_name", worker.getFirstName() + " " + worker.getLastName());
        model.addAttribute("globals", globals);
        model.addAttribute("admin", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.ADMIN.ordinal() ? 1 : 0);

        return "redirect:/admin/customers/orders/order?id=" + orderId;
    }

    @RequestMapping(value = "/admin/orders/setGeoCode", method = RequestMethod.GET)
    public Object getGeoCode(@RequestParam(value = "orderId", required = false) Integer orderId, @RequestParam(value = "customerId", required = false) Integer customerId, Model model, HttpServletRequest request) {

        // String googleAPI = "AIzaSyD78NqTX9Sr868d_yF8fRfc5-aboD4NilE";
        Order order = ordersDao.findOne(orderId);
        String address = order.getStreet() + " " + order.getStreetNo() + " " + order.getCity();
        Double[] coordinates = common.getGoogleGeoCode(address);
        System.out.format("%n%n%n   # %s %s %n", coordinates[0].toString(), coordinates[1].toString());

        GeoPoint geoPoint = new GeoPoint();
        geoPoint.setCustomerId(order.getCustomerId());
        geoPoint.setCity(order.getCity());
        geoPoint.setStreet(order.getStreet());
        geoPoint.setStreetNumber(order.getStreetNo());
        geoPoint.setLatitude(coordinates[0]);
        geoPoint.setLongitude(coordinates[1]);
        geoPoint.setTimeConstraints("NA");
        geoPoinsDao.save(geoPoint);

        return "redirect:https://maps.google.com/?q=" + coordinates[0].toString() + "," + coordinates[1].toString();
    }
}
