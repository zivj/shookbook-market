package com.shookbook.admin;

import com.shookbook.dao.shookbook.*;
import com.shookbook.dao.wordpress.WpOrderItemsDao;
import com.shookbook.dao.wordpress.WpPostmetaDao;
import com.shookbook.dao.wordpress.WpPostsDao;
import com.shookbook.dao.wordpress.WpUsermetaDao;
import com.shookbook.entities.shookbook.*;
import com.shookbook.entities.wordpress.WpOrderItem;
import com.shookbook.entities.wordpress.WpPost;
import com.shookbook.entities.wordpress.WpPostmeta;
import com.shookbook.utils.U;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@SessionAttributes("thought")
public class CouponsAdmin {

    private static final Logger logger = Logger.getLogger(CouponsAdmin.class);

    @Autowired
    private Common common;
    @Autowired
    private CustomersDao customersDao;
    @Autowired
    private CouponsDao couponsDao;
    @Autowired
    private PointsDao pointsDao;
    @Autowired
    private OrdersDao ordersDao;
    @Autowired
    private RefundsDao refundsDao;
    @Autowired
    private WpPostsDao wpPostsDao;
    @Autowired
    private WpPostmetaDao wpPostmetaDao;
    @Autowired
    private WpUsermetaDao wpUsermetaDao;
    @Autowired
    private WpOrderItemsDao wpOrderItemsDao;
    @Autowired
    private WorkersDao workersDao;
    @Autowired
    private GlobalsDao globalsDao;

    public final static String[] couponFilters = new String[]{"הכל", "שנה", "3 חודשים", "הנחה קבוצתית", "קבוצות", "מוצר פגום", "אריזה", "משלוח", "פיצוי", "כללי", "מותג"};

    @RequestMapping(value = "/admin/coupons", method = RequestMethod.GET)
    public Object coupons(@RequestParam(value = "filter", defaultValue = "2") Integer filter, Model model, HttpServletRequest request) {
        List<Coupon> coupons;
        switch (filter) {
            case 0:
                coupons = couponsDao.findAll();
                break;
            case 1:
                coupons = couponsDao.findAllInYear();
                break;
            case 2:
                coupons = couponsDao.findAllIn3Months();
                break;
            case 3:
                coupons = couponsDao.findAllByCause(Coupon.CouponCause.GROUP_DISCOUNT.ordinal());
                break;
            case 4:
                coupons = couponsDao.findAllByCause(Coupon.CouponCause.GROUP.ordinal());
                break;
            case 5:
                coupons = couponsDao.findAllByCause(Coupon.CouponCause.BAD_PRODUCT.ordinal());
                break;
            case 6:
                coupons = couponsDao.findAllByCause(Coupon.CouponCause.DELIVERY.ordinal());
                break;
            case 7:
                coupons = couponsDao.findAllByCause(Coupon.CouponCause.COMPANSATION.ordinal());
                break;
            case 8:
                coupons = couponsDao.findAllByCause(Coupon.CouponCause.GENERAL.ordinal());
                break;
            case 9:
                coupons = couponsDao.findAllByCause(Coupon.CouponCause.PACKING.ordinal());
                break;
            case 10:
                coupons = couponsDao.findAllByType(Coupon.CouponTypes.BRAND.ordinal());
                break;
            default:
                coupons = couponsDao.findAll();
                break;
        }

        Customer customer;


        List<Map<String, Object>> columns = new LinkedList<>();
        for (Coupon coupon : coupons) {
            Map<String, Object> column = new HashMap<>();

            if ((coupon.getCustomerId() != 0) || ((coupon.getCause() == Coupon.CouponCause.GROUP) && (coupon.getLeadId() != 0))) {
                if (coupon.getCause() == Coupon.CouponCause.GROUP) {
                    customer = customersDao.getOne(coupon.getLeadId());
                } else {
                    customer = customersDao.getOne(coupon.getCustomerId());
                }
                column.put("customerId", customer.getId());
                column.put("customerName", customer.getFirstName() + " " + customer.getLastName());
                column.put("customerType", customer.getType().heb);
                column.put("typeId", customer.getType().ordinal());
                column.put("customerPhoneNumber", customer.getPhone());
                column.put("customerMailAddress", customer.getEmail());
                column.put("joinDate", new SimpleDateFormat("dd-MM-yyyy").format(customer.getCreatedDate()));
            } else {
                column.put("customerName", " ");
                column.put("customerType", " ");
                column.put("typeId", " ");
                column.put("customerPhoneNumber", " ");
                column.put("customerMailAddress", " ");
                column.put("joinDate", " ");
            }
            column.put("couponId", coupon.getId());
            column.put("couponTitle", coupon.getTitle());
            column.put("couponAmount", coupon.getAmount());
            column.put("couponType", coupon.getType());
            column.put("couponTypeHeb", coupon.getType().heb);
            column.put("couponCause", coupon.getCause());
            column.put("couponCauseHeb", coupon.getCause().heb);
            column.put("couponDetails", coupon.getDetails());
            WpPost wpPost = wpPostsDao.findPostIdByTitle(coupon.getTitle());
            Integer couponUsage = 0;
            if (wpPost != null) {
                couponUsage = wpPostmetaDao.getPostUsage(wpPost.getId().intValue());
            }
            coupon.setCouponUsage(couponUsage);
            couponsDao.save(coupon);
            column.put("couponUsage", coupon.getCouponUsage());
            column.put("couponGroup", coupon.getCouponGroup());
            column.put("couponDate", new SimpleDateFormat("dd-MM-yyyy").format(coupon.getCreatedDate()));
            String orderNumber;
            if (coupon.getOrderId() == 0) {
                orderNumber = " ";
            } else {
                orderNumber = Integer.toString(coupon.getOrderId());
            }
            column.put("couponOrder", orderNumber);
            columns.add(column);
        }

        model.addAttribute("filter", couponFilters[filter]);
        model.addAttribute("columns", columns);
        Map<String, Object> globals = new HashMap<>();
        globals.put("modules_enable_coupons", globalsDao.findOne("modules_enable_coupons").getValue());
        globals.put("modules_enable_invoice", globalsDao.findOne("modules_enable_invoice").getValue());
        globals.put("modules_enable_logistics", globalsDao.findOne("modules_enable_logistics").getValue());
        globals.put("modules_enable_packing", globalsDao.findOne("modules_enable_packing").getValue());
        Object logedWorker = request.getSession().getAttribute("userId");
        Worker worker = workersDao.getOne(Integer.parseInt(logedWorker.toString()));
        globals.put("worker_name", worker.getFirstName() + " " + worker.getLastName());
        model.addAttribute("globals", globals);
        model.addAttribute("admin", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.ADMIN.ordinal() ? 1 : 0);
        model.addAttribute("customer_support", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.CUSTOMER_SUPPORT.ordinal() ? 1 : 0);

        return "Coupons";
    }

    @RequestMapping(value = "/admin/coupons/create", method = RequestMethod.GET)
    public Object createCoupon(Model model, HttpServletRequest request) {
        if (!U.var.parseBoolean(request.getSession().getAttribute("login"))) {
            return "redirect:/admin/login";
        }

        Map<String, Object> column = new HashMap<>();
        column.put("customerId", 0);

        model.addAttribute("column", column);
        Map<String, Object> globals = new HashMap<>();
        globals.put("modules_enable_coupons", globalsDao.findOne("modules_enable_coupons").getValue());
        globals.put("modules_enable_invoice", globalsDao.findOne("modules_enable_invoice").getValue());
        globals.put("modules_enable_logistics", globalsDao.findOne("modules_enable_logistics").getValue());
        globals.put("modules_enable_packing", globalsDao.findOne("modules_enable_packing").getValue());
        Object logedWorker = request.getSession().getAttribute("userId");
        Worker worker = workersDao.getOne(Integer.parseInt(logedWorker.toString()));
        globals.put("worker_name", worker.getFirstName() + " " + worker.getLastName());
        model.addAttribute("globals", globals);
        model.addAttribute("admin", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.ADMIN.ordinal() ? 1 : 0);

        return "CouponsCreate";
    }

    @RequestMapping(value = "/admin/coupons/brand/create", method = RequestMethod.GET)
    public Object createBrandCoupon(Model model, HttpServletRequest request) {
        if (!U.var.parseBoolean(request.getSession().getAttribute("login"))) {
            return "redirect:/admin/login";
        }

        Map<String, Object> column = new HashMap<>();
        column.put("customerId", 0);

        model.addAttribute("column", column);
        Map<String, Object> globals = new HashMap<>();
        globals.put("modules_enable_coupons", globalsDao.findOne("modules_enable_coupons").getValue());
        globals.put("modules_enable_invoice", globalsDao.findOne("modules_enable_invoice").getValue());
        globals.put("modules_enable_logistics", globalsDao.findOne("modules_enable_logistics").getValue());
        globals.put("modules_enable_packing", globalsDao.findOne("modules_enable_packing").getValue());
        Object logedWorker = request.getSession().getAttribute("userId");
        Worker worker = workersDao.getOne(Integer.parseInt(logedWorker.toString()));
        globals.put("worker_name", worker.getFirstName() + " " + worker.getLastName());
        model.addAttribute("globals", globals);
        model.addAttribute("admin", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.ADMIN.ordinal() ? 1 : 0);

        return "CouponBrandCreate";
    }

    @RequestMapping(value = "/admin/customers/coupon/submit", method = RequestMethod.POST)
    public Object customersCouponSubmit(@RequestParam(value = "customerId") Integer customerId, @RequestParam(value = "orderId") Integer orderId, @RequestParam(value = "couponType") Integer couponType, @RequestParam(value = "couponCause") Integer couponCause, @RequestParam(value = "couponAmount") Float couponAmount, @RequestParam(value = "couponDetails") String couponDetails, @RequestParam(value = "couponLeadCustomerEmail") String couponLeadCustomerEmail, Model model, HttpServletRequest request) {
        System.out.format("Generate coupon for customer: %d with the following params. OrderId: %d   Type: %d   Cause: %d   Amoun: %f   Details: %s%n", customerId, orderId, couponType, couponCause, couponAmount, couponDetails);
        Coupon coupon = new Coupon();
        coupon.setCustomerId(customerId);
        coupon.setOrderId(orderId);
        coupon.setAmount(couponAmount);
        coupon.setDetails(couponDetails);
        coupon.setCouponUsage(0);
        coupon.setIsDeleted('0');
        coupon.setCreatedDate(new Date(System.currentTimeMillis()));

        switch (couponType) {
            case 5:
                coupon.setType(Coupon.CouponTypes.DISCOUNT);
                break;
            case 7:
                coupon.setType(Coupon.CouponTypes.PERCENT);
                break;
            case 9:
                coupon.setType(Coupon.CouponTypes.POINTS);
                break;
            case 1:
                coupon.setType(Coupon.CouponTypes.PRODUCT);
                break;
            case 8:
                coupon.setType(Coupon.CouponTypes.COMMERCIAL);
                break;
            case 3:
                coupon.setType(Coupon.CouponTypes.GIFT);
                break;
            case 2:
                coupon.setType(Coupon.CouponTypes.BRAND);
                break;
            case 0:
                coupon.setType(Coupon.CouponTypes.CANCELLED);
                break;
        }

        switch (couponCause) {
            case 0:
                coupon.setCause(Coupon.CouponCause.BAD_PRODUCT);
                break;
            case 1:
                coupon.setCause(Coupon.CouponCause.DELIVERY);
                break;
            case 2:
                coupon.setCause(Coupon.CouponCause.COMPANSATION);
                break;
            case 3:
                coupon.setCause(Coupon.CouponCause.GENERAL);
                break;
            case 4:
                coupon.setCause(Coupon.CouponCause.GROUP_DISCOUNT);
                break;
            case 5:
                coupon.setCause(Coupon.CouponCause.GROUP);
                break;
            case 6:
                coupon.setCause(Coupon.CouponCause.PACKING);
                break;
        }

        coupon.setTitle("tmp");
        coupon.setLeadId(0);

        Random random = new Random();
        Integer randomNumber = random.nextInt(85000) + 11999;
        coupon.setRandom(randomNumber);

        if (coupon.getCause() == Coupon.CouponCause.GROUP) {
            System.out.format("   Email: %s%n", couponLeadCustomerEmail);
            Customer leadCustomer = customersDao.findOneByEmail(couponLeadCustomerEmail);
            if (leadCustomer.getId() != null) {
                coupon.setLeadId(leadCustomer.getId());
            }
            System.out.format("   Email: %s   %d%n", couponLeadCustomerEmail, leadCustomer.getId());
        }

        couponsDao.save(coupon);

        Calendar postDate = Calendar.getInstance();
        int dayOfWeek = postDate.get(Calendar.DAY_OF_WEEK);
        int weekInYear = postDate.get(Calendar.WEEK_OF_YEAR);
        int currentYear = postDate.get(Calendar.YEAR);
        String currentYearStr = Integer.toString(currentYear);
        String dateField = Integer.toString(dayOfWeek) + String.format("%02d", weekInYear) + currentYearStr.substring(currentYearStr.length() - 2);

        int couponAmountInt = Math.round(couponAmount * 10);
        String amountField = Integer.toString(randomNumber ^ couponAmountInt);

        Integer typeFieldInt = (coupon.getId() * 10 + couponType) ^ randomNumber;
        String typeField = String.format("%05d", typeFieldInt);

        String couponTitle = dateField + '-' + amountField + '-' + typeField + '-' + String.format("%04d", coupon.getId()) + '-' + Integer.toString(couponType);

        coupon.setTitle(couponTitle);

        WpPost wpCoupon = wpPostsDao.save(new WpPost(coupon));
        String wpLink = "https://www.shookbook.co.il/?post_type=shop_coupon&#038;p=" + wpCoupon.getId();
        wpCoupon.setGuid(wpLink);
        coupon.setWpId(wpCoupon.getId().intValue());
        wpPostsDao.save(wpCoupon);
        couponsDao.save(coupon);

        // End setting the coupon

        // Setting coupon variables

        Customer customer;
        if (customerId != 0) {
            customer = customersDao.getOne(coupon.getCustomerId());
        } else if (coupon.getCause() == Coupon.CouponCause.GROUP) {
            customer = customersDao.getOne(coupon.getLeadId());
            customerId = customer.getId();
        } else {
            customer = customersDao.getOne(1);
        }


        switch (coupon.getType()) {
            case DISCOUNT:
                wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "discount_type", "fixed_cart"));
                if (coupon.getCause() == Coupon.CouponCause.GROUP_DISCOUNT || coupon.getCause() == Coupon.CouponCause.GROUP) {
                    wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "usage_limit", ""));
                } else {
                    wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "usage_limit", "1"));
                }
                break;

            case PERCENT:
                wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "discount_type", "percent"));
                wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "usage_limit", "1"));
                break;

            case COMMERCIAL:
                wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "discount_type", "fixed_cart"));
                wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "usage_limit", ""));
                break;

            default:
                wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "discount_type", "fixed_cart"));
                wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "usage_limit", "1"));
                break;
        }

        switch (coupon.getCause()) {
            case GROUP:
            case GROUP_DISCOUNT:
                break;

            default:
                break;
        }

        wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "coupon_amount", "" + coupon.getAmount()));
        wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "individual_use", "no"));
        wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "usage_limit_per_user", "1"));
        wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "free_shipping", "no"));
        wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "exclude_sale_items", "no"));
        wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "slide_template", "default"));
        wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "exclude_product_categories", "a:0:{}"));
        wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "product_categories", "a:0:{}"));
        wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "maximum_amount", ""));
        wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "minimum_amount", ""));
        wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "expiry_date", ""));
        wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "limit_usage_to_x_items", ""));
        wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "exclude_product_ids", ""));
        wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "product_ids", ""));
        wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "_edit_last", "1"));

        String mailSubject;

        switch (coupon.getType()) {
            case DISCOUNT:
            case COMBINED:
                mailSubject = "קופון זיכוי";
                break;

            case POINTS:
                mailSubject = "מימוש נקודות";
                Integer wpCustomerId = customer.getWpId();
                short deductedPoints = (short) (U.var.parseInt(-coupon.getAmount() * 2));
                short customerPoints = (short) (U.var.parseInt(pointsDao.sum(customerId)));
                short updatedPoints = (short) (U.var.parseInt(customerPoints + deductedPoints));
                pointsDao.save(new Point(customerId, deductedPoints, 'M', 0, new Date(System.currentTimeMillis())));
                customer.setCreditPoints(updatedPoints);
                wpUsermetaDao.updateByIdAndKey((long) customer.getWpId(), "shookbook_points", Integer.toString(updatedPoints));
                customersDao.save(customer);
                break;

            default:
                mailSubject = "קופון זיכוי";
                break;
        }

        switch (coupon.getCause()) {
            case DELIVERY:
                mailSubject = "זיכוי משלוח";
                break;

            case GROUP:
                mailSubject = "ברוך הבא לקבוצה";
                wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "_used_by", "" + customer.getWpId()));
                wpOrderItemsDao.save(new WpOrderItem(coupon.getTitle(), "coupon", 0));
                break;

            default:
                break;
        }


        // Refund creation

        if (coupon.getCause() == Coupon.CouponCause.BAD_PRODUCT && coupon.getOrderId() > 0 && coupon.getCustomerId() > 0) {
            Refund refund = new Refund();
            refund.setCustomerId(coupon.getCustomerId());
            refund.setOrderId(coupon.getOrderId());
            refund.setCouponId(0);
            refund.setRootCause(Refund.RootCause.BAD_PRODUCT);
            refund.setType(Refund.Types.COUPON);
            Object logedWorker = request.getSession().getAttribute("userId");
            refund.setWorkerId(Integer.parseInt(logedWorker.toString()));
            refund.setAmount(coupon.getAmount());
            refund.setDetails("Coupon " + coupon.getTitle());
            refund.setQuantity(0);
            refundsDao.save(refund);
        }

        logger.info("SB: " + request.getSession().getAttribute("userName") + ": Coupon created: " + coupon.getTitle());

        if (customerId != 0) {
            String customerEmail = customersDao.getOne(customerId).getEmail();
            int emailLength = customerEmail.length();
            if (coupon.getCause() == Coupon.CouponCause.GROUP) {
                wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "customer_email", "a:0:{}"));
                common.sendEmailFrom("customer_support", customerEmail, "שוקבוק - " + mailSubject, U.http.stringFromUrl("http://" + request.getLocalName() + ":" + request.getLocalPort() + "/admin/coupons/groupEmail?customerId=" + customerId + "&couponId=" + coupon.getId()));
                return "redirect:/admin/coupons";
            } else {
                wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "customer_email", "a:1:{i:0;s:" + emailLength + ":\"" + customerEmail + "\";}"));
                common.sendEmailFrom("customer_support", customerEmail, "שוקבוק - " + mailSubject, U.http.stringFromUrl("http://" + request.getLocalName() + ":" + request.getLocalPort() + "/admin/customers/coupon/email?customerId=" + customerId + "&couponId=" + coupon.getId()));

//                return "redirect:/admin/customers/customer?id=" + customerId;
                return "redirect:/admin/customers/customer?id=" + customerId + "&couponId=" + coupon.getId();

            }
        } else {
            wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "customer_email", "a:0:{}"));
            return "redirect:/admin/coupons";
        }
    }

    @RequestMapping(value = "/admin/customers/coupon/brand/submit", method = RequestMethod.POST)
    public Object customersCouponBrandSubmit(@RequestParam(value = "couponType") Integer couponType,
                                             @RequestParam(value = "couponCause") Integer couponCause,
                                             @RequestParam(value = "couponAmount") Float couponAmount,
                                             @RequestParam(value = "couponDetails") String couponDetails,
                                             @RequestParam(value = "numberOfCoupons") Integer numberOfCoupons,
                                             Model model, HttpServletRequest request) {

        for (Integer couponNumber = 0; couponNumber < numberOfCoupons; couponNumber++) {
            Coupon coupon = new Coupon();
            coupon.setCustomerId(0);
            coupon.setOrderId(0);
            coupon.setAmount(couponAmount);
            coupon.setDetails(couponDetails);
            coupon.setCouponUsage(0);
            coupon.setIsDeleted('0');
            coupon.setCreatedDate(new Date(System.currentTimeMillis()));

            switch (couponType) {
                case 5:
                    coupon.setType(Coupon.CouponTypes.DISCOUNT);
                    break;
                case 7:
                    coupon.setType(Coupon.CouponTypes.PERCENT);
                    break;
                case 1:
                    coupon.setType(Coupon.CouponTypes.PRODUCT);
                    break;
                case 8:
                    coupon.setType(Coupon.CouponTypes.COMMERCIAL);
                    break;
                case 3:
                    coupon.setType(Coupon.CouponTypes.GIFT);
                    break;
                case 2:
                    coupon.setType(Coupon.CouponTypes.BRAND);
                    break;
                case 0:
                    coupon.setType(Coupon.CouponTypes.CANCELLED);
                    break;
            }

            switch (couponCause) {
                case 1:
                    coupon.setCause(Coupon.CouponCause.DELIVERY);
                    break;
                case 2:
                    coupon.setCause(Coupon.CouponCause.COMPANSATION);
                    break;
                case 3:
                    coupon.setCause(Coupon.CouponCause.GENERAL);
                    break;
                case 4:
                    coupon.setCause(Coupon.CouponCause.GROUP_DISCOUNT);
                    break;
                case 5:
                    coupon.setCause(Coupon.CouponCause.GROUP);
                    break;
            }

            coupon.setTitle("tmp");

            Random random = new Random();
            Integer randomNumber = random.nextInt(85000) + 11999;
            coupon.setRandom(randomNumber);

            couponsDao.save(coupon);

            Calendar postDate = Calendar.getInstance();
            int dayOfWeek = postDate.get(Calendar.DAY_OF_WEEK);
            int weekInYear = postDate.get(Calendar.WEEK_OF_YEAR);
            int currentYear = postDate.get(Calendar.YEAR);
            String currentYearStr = Integer.toString(currentYear);
            String dateField = Integer.toString(dayOfWeek) + Integer.toString(weekInYear) + currentYearStr.substring(currentYearStr.length() - 2);

            int couponAmountInt = Math.round(couponAmount * 10);
            String amountField = Integer.toString(randomNumber ^ couponAmountInt);

            Integer typeFieldInt = (coupon.getId() * 10 + couponType) ^ randomNumber;
            String typeField = String.format("%05d", typeFieldInt);

            String couponTitle = dateField + '-' + amountField + '-' + typeField + '-' + String.format("%04d", coupon.getId()) + '-' + Integer.toString(couponType);

            coupon.setTitle(couponTitle);

            WpPost wpCoupon = wpPostsDao.save(new WpPost(coupon));
            String wpLink = "https://www.shookbook.co.il/?post_type=shop_coupon&#038;p=" + wpCoupon.getId();
            wpCoupon.setGuid(wpLink);
            coupon.setWpId(wpCoupon.getId().intValue());
            wpPostsDao.save(wpCoupon);
            couponsDao.save(coupon);

            // End setting the coupon


            switch (coupon.getType()) {
                case DISCOUNT:
                case COMBINED:
                    wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "discount_type", "fixed_cart"));
                    if (coupon.getCause() == Coupon.CouponCause.GROUP_DISCOUNT || coupon.getCause() == Coupon.CouponCause.GROUP) {
                        wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "usage_limit", ""));
                    } else {
                        wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "usage_limit", "1"));
                    }
                    break;

                case PERCENT:
                    wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "discount_type", "percent"));
                    wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "usage_limit", "1"));
                    break;

                case COMMERCIAL:
                    wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "discount_type", "fixed_cart"));
                    wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "usage_limit", ""));
                    break;

                default:
                    wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "discount_type", "fixed_cart"));
                    wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "usage_limit", "1"));
                    break;
            }

            wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "coupon_amount", "" + coupon.getAmount()));
            wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "individual_use", "no"));
            wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "usage_limit_per_user", "1"));
            wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "free_shipping", "no"));
            wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "exclude_sale_items", "no"));
            wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "slide_template", "default"));
            wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "exclude_product_categories", "a:0:{}"));
            wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "product_categories", "a:0:{}"));
            wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "maximum_amount", ""));
            wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "minimum_amount", ""));
            wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "expiry_date", ""));
            wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "limit_usage_to_x_items", ""));
            wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "exclude_product_ids", ""));
            wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "product_ids", ""));
            wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "_edit_last", "1"));

        }
        return "redirect:/admin/coupons";
    }


    @RequestMapping(value = "/admin/customers/coupon/email", method = RequestMethod.GET)
    public Object couponEmail(@RequestParam(value = "customerId", required = false) Integer customerId, @RequestParam(value = "couponId", required = false) Integer couponId, Model model, HttpServletRequest request) {

        Customer customer = customersDao.findOne(customerId);
        Coupon coupon = couponsDao.findOne(couponId);

        Map<String, Object> column = new HashMap<>();
        column.put("customerId", customer.getId());
        column.put("fullName", customer.getFirstName() + " " + customer.getLastName());
        column.put("firstName", customer.getFirstName());
        column.put("lastName", customer.getLastName());
        column.put("nekoshook", customer.getCreditPoints());
        column.put("couponTitle", coupon.getTitle());
        column.put("couponAmount", coupon.getAmount());
        column.put("couponType", coupon.getType().toString());
        column.put("DisplayQRcode", globalsDao.findOne("coupon_display_qr_code").getValue());

        model.addAttribute("column", column);

        return "CouponEmail";
    }

    @RequestMapping(value = "/admin/coupons/groupEmail", method = RequestMethod.GET)
    public Object couponsGroupEmail(@RequestParam(value = "customerId", required = false) Integer customerId, @RequestParam(value = "couponId", required = false) Integer couponId, Model model, HttpServletRequest request) {

        Customer customer = customersDao.findOne(customerId);
        Coupon coupon = couponsDao.findOne(couponId);


        Map<String, Object> column = new HashMap<>();
        column.put("customerId", customer.getId());
        column.put("fullName", customer.getFirstName() + " " + customer.getLastName());
        column.put("firstName", customer.getFirstName());
        column.put("lastName", customer.getLastName());
        column.put("nekoshook", customer.getCreditPoints());
        column.put("couponTitle", coupon.getTitle());
        column.put("couponDetails", coupon.getDetails());
        column.put("couponAmount", coupon.getAmount());
        column.put("couponType", coupon.getType().toString());
        column.put("DisplayQRcode", globalsDao.findOne("coupon_display_qr_code").getValue());

        model.addAttribute("column", column);

        return "CouponGroupEmail";
    }

    @RequestMapping(value = "/admin/coupons/coupon", method = RequestMethod.GET)
    public Object couponDetails(@RequestParam(value = "id") Integer id, Model model, HttpServletRequest request) {
        if (!U.var.parseBoolean(request.getSession().getAttribute("login"))) {
            return "redirect:/admin/login";
        }

        Coupon coupon = couponsDao.findOne(id);

        Map<String, Object> column = new HashMap<>();
        column.put("id", coupon.getId());
        column.put("title", coupon.getTitle());
        column.put("createdDate", new SimpleDateFormat("dd-MM-yyyy").format(coupon.getCreatedDate()));
        column.put("type", coupon.getType());
        column.put("cause", coupon.getCause());
        column.put("usage", coupon.getCouponUsage());

        WpPost wpPost = wpPostsDao.findPostIdByTitle(coupon.getTitle());
        List<String> wpCustomers = wpPostmetaDao.findAllUsage(wpPost.getId().intValue());
        List<WpOrderItem> wpOrderItems = wpOrderItemsDao.findAllByTitle(coupon.getTitle());

        StringBuilder stringBuilder = new StringBuilder("\n");
        List<Map<String, Object>> orders = new LinkedList<>();
        Customer customer;

        for (WpOrderItem wpOrderItem : wpOrderItems) {

            Map<String, Object> orderData = new HashMap<>();
            Order order = ordersDao.findOneByWpID(wpOrderItem.getOrderId().intValue());
            if (order != null) {
                if (wpOrderItem.getOrderId().intValue() != 0) {
                    customer = customersDao.findOne(order.getCustomerId());
                    orderData.put("OrderId", order.getId());
                    orderData.put("OrderBarcode", order.getBarcode());
                    System.out.format("   %d   %d   %d   %d%n", wpOrderItem.getOrderItemId(), wpOrderItem.getOrderId().intValue(), order.getId(), customer.getId());
                } else {
                    System.out.format("   aaa - %d%n", wpOrderItem.getOrderItemId());
                    customer = customersDao.findOne(coupon.getLeadId());
                    orderData.put("OrderId", "");
                    orderData.put("OrderBarcode", "");
                }

                orderData.put("customerId", customer.getId());
                orderData.put("email", customer.getEmail());
                stringBuilder.append(customer.getEmail()).append(',');
                orderData.put("fullName", customer.getFirstName() + " " + customer.getLastName());
                orders.add(orderData);
            }
        }

        column.put("bccList", stringBuilder);

        List<Map<String, Object>> customers = new LinkedList<>();
        for (String wpCustomer : wpCustomers) {

            Map<String, Object> customerData = new HashMap<>();
            customer = customersDao.findOneByWpID(Integer.valueOf(wpCustomer));
            customerData.put("customerId", customer.getId());
            customerData.put("fullName", customer.getFirstName() + " " + customer.getLastName());
            customers.add(customerData);
        }

        model.addAttribute("orders", orders);
        model.addAttribute("customers", customers);
        model.addAttribute("column", column);
        Map<String, Object> globals = new HashMap<>();
        globals.put("modules_enable_coupons", globalsDao.findOne("modules_enable_coupons").getValue());
        globals.put("modules_enable_invoice", globalsDao.findOne("modules_enable_invoice").getValue());
        globals.put("modules_enable_logistics", globalsDao.findOne("modules_enable_logistics").getValue());
        globals.put("modules_enable_packing", globalsDao.findOne("modules_enable_packing").getValue());
        Object logedWorker = request.getSession().getAttribute("userId");
        Worker worker = workersDao.getOne(Integer.parseInt(logedWorker.toString()));
        globals.put("worker_name", worker.getFirstName() + " " + worker.getLastName());
        model.addAttribute("globals", globals);


        return "CouponDetails";
    }

    @RequestMapping(value = "/admin/coupons/orders/submit/applyCoupon", method = RequestMethod.POST)
    public Object applyCoupon(@RequestParam(value = "addCouponOrderId") Integer orderId, @RequestParam(value = "couponId") Integer couponId, HttpServletRequest request) {

        System.out.format("   Apply coupon for order: %d with coupon: %d%n", orderId, couponId);
        Order order = ordersDao.findOne(orderId);
        Coupon coupon = couponsDao.findOne(couponId);
        Customer customer = customersDao.findOne(order.getCustomerId());
        WpPost wpCoupon = wpPostsDao.findOne(U.var.parseLong(coupon.getWpId()));
        Integer couponUsage = wpPostmetaDao.getPostUsage(wpCoupon.getId().intValue());
        WpPostmeta couponAllowedUsageMeta = wpPostmetaDao.findOneByIdAndKey(wpCoupon.getId(), "usage_limit_per_user");
        WpPostmeta couponAllowedUsageAllMeta = wpPostmetaDao.findOneByIdAndKey(wpCoupon.getId(), "usage_limit");
        Integer couponAllowedUsage = U.var.parseInt(couponAllowedUsageMeta.getMetaValue());
        Integer couponAllowedUsageAll = U.var.parseInt(couponAllowedUsageAllMeta.getMetaValue());
        Integer couponUsedByCustomer = wpPostmetaDao.getPostUsageByUser(wpCoupon.getId().intValue(), Integer.toString(customer.getWpId()));
        if ((coupon.getCustomerId() == 0) || (coupon.getCustomerId() == order.getCustomerId())) {
            // If customerId is OK
            if (((couponUsage < couponAllowedUsageAll) || (couponAllowedUsageAll == 0)) && ((couponUsedByCustomer < couponAllowedUsage) || (couponAllowedUsage == 0))) {
                // If coupon usage is OK
                wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "_used_by", "" + customer.getWpId()));
                wpOrderItemsDao.save(new WpOrderItem(coupon.getTitle(), "coupon", order.getWpId()));
                Float discount = coupon.getAmount();
                if (coupon.getType() == Coupon.CouponTypes.PERCENT) {
                    discount = (order.getSum() - U.var.parseInt(globalsDao.findOne("delivery_price_reduced").getValue())) * coupon.getAmount() / 100;
                }
                order.setDiscountSum(discount + order.getDiscountSum());
                order.setDiscountCode(coupon.getTitle());
                coupon.setCouponUsage(couponUsage + 1);
                ordersDao.save(order);
            }
        }

        logger.info("SB: " + request.getSession().getAttribute("userName") + ": Apply coupon " + coupon.getTitle() + " -> Order:" + order.getId() + "   Customer: " + customer.getFullName());
        return "redirect:/admin/customers/orders/order?id=" + orderId;
    }

    @ResponseBody
    @RequestMapping(value = "/admin/coupons/csv/usage", method = RequestMethod.GET)
    public Object csvCouponUsage(@RequestParam(value = "couponId") Integer couponId, Model model, HttpServletResponse response) {
        Coupon coupon = couponsDao.findOne(couponId);
        StringBuilder stringBuilder = new StringBuilder("\n");
        stringBuilder.append("Coupon:").append(',').append('"').append(coupon.getTitle()).append('"').append("\n");
        List<WpOrderItem> wpOrderItems = wpOrderItemsDao.findAllByTitle(coupon.getTitle());

        String fullName = "";
        Customer customer;

        for (WpOrderItem wpOrderItem : wpOrderItems) {
            if (wpOrderItem.getOrderId().intValue() != 0) {
                Order order = ordersDao.findOneByWpID(wpOrderItem.getOrderId().intValue());
                customer = customersDao.findOne(order.getCustomerId());
            } else {
                customer = customersDao.findOne(coupon.getLeadId());
            }

            fullName = customer.getFirstName() + " " + customer.getLastName();
            stringBuilder.append('"').append(fullName).append('"').append(',').append('"').append(customer.getEmail()).append('"').append("\n");
        }

        response.setHeader("Content-Disposition", "attachment; filename=\"coupon_details.csv\"");
        response.setContentLength(stringBuilder.length());

        response.setContentType("text/csv; charset=");
        return stringBuilder.toString();
    }

    @ResponseBody
    @RequestMapping(value = "/admin/coupons/csv/exportGroups", method = RequestMethod.GET)
    public Object csvCouponExportGroups(Model model, HttpServletResponse response) {
        List<Coupon> coupons = couponsDao.findAllByCause(Coupon.CouponCause.GROUP.ordinal());

        StringBuilder stringBuilder = new StringBuilder("\n");
        stringBuilder.append("Title").append(',').append("Name").append(',').append("Leader").append(',').append("Email").append(',').append("Creation Date").append(',').append("Users").append("\n");

        Customer customer;
        for (Coupon coupon : coupons) {

            List<String> users = wpPostmetaDao.findAllUsage(coupon.getWpId());

            for (String userStr : users) {
                Integer user = U.var.parseInt(userStr);
                customer = customersDao.findOneByWpID(user);
                stringBuilder.append('"').append(coupon.getTitle()).append('"').append(',').append('"').append(coupon.getDetails()).append('"').append(',');
                if (coupon.getLeadId() != 0) {
                    stringBuilder.append('"').append(customer.getFirstName()).append(' ').append(customer.getLastName()).append('"').append(',');
                    stringBuilder.append('"').append(customer.getEmail()).append('"').append(',');
                } else {
                    stringBuilder.append(' ').append(',').append(' ').append(',');
                }
                stringBuilder.append('"').append(coupon.getCreatedDate()).append('"').append(',').append('"').append(coupon.getCouponUsage()).append('"').append("\n");
            }
        }

        response.setHeader("Content-Disposition", "attachment; filename=\"groups_details.csv\"");
        response.setContentLength(stringBuilder.length());

        response.setContentType("text/csv; charset=");
        return stringBuilder.toString();
    }

    public Integer createCoupon(Coupon.CouponTypes type, Coupon.CouponCause cause, Float amount, String details, Integer customerId, Integer orderId) {

        System.out.format("Generate coupon for customer: %d with the following params. OrderId: %d   Type: %d   Cause: %d   Amoun: %f   Details: %s%n", customerId, orderId, type.ordinal(), cause.ordinal(), amount, details);
        Coupon coupon = new Coupon();
        coupon.setCustomerId(customerId);
        coupon.setOrderId(orderId);
        coupon.setAmount(amount);
        coupon.setDetails(details);
        coupon.setCouponUsage(0);
        coupon.setIsDeleted('0');
        coupon.setCreatedDate(new Date(System.currentTimeMillis()));
        coupon.setType(type);
        coupon.setCause(cause);
        coupon.setTitle("tmp");
        coupon.setLeadId(0);

        Random random = new Random();
        Integer randomNumber = random.nextInt(85000) + 11999;
        coupon.setRandom(randomNumber);

        couponsDao.save(coupon);

        Calendar postDate = Calendar.getInstance();
        int dayOfWeek = postDate.get(Calendar.DAY_OF_WEEK);
        int weekInYear = postDate.get(Calendar.WEEK_OF_YEAR);
        int currentYear = postDate.get(Calendar.YEAR);
        String currentYearStr = Integer.toString(currentYear);
        String dateField = Integer.toString(dayOfWeek) + String.format("%02d", weekInYear) + currentYearStr.substring(currentYearStr.length() - 2);

        int couponAmountInt = Math.round(amount * 10);
        String amountField = Integer.toString(randomNumber ^ couponAmountInt);

        Integer typeFieldInt = (coupon.getId() * 10 + type.ordinal()) ^ randomNumber;
        String typeField = String.format("%05d", typeFieldInt);

        String couponTitle = dateField + '-' + amountField + '-' + typeField + '-' + String.format("%04d", coupon.getId()) + '-' + Integer.toString(type.ordinal());

        coupon.setTitle(couponTitle);

        WpPost wpCoupon = wpPostsDao.save(new WpPost(coupon));
        String wpLink = "https://www.shookbook.co.il/?post_type=shop_coupon&#038;p=" + wpCoupon.getId();
        wpCoupon.setGuid(wpLink);
        coupon.setWpId(wpCoupon.getId().intValue());
        wpPostsDao.save(wpCoupon);
        couponsDao.save(coupon);

        // End setting the coupon

        // Setting coupon variables

        Customer customer;
        if (customerId != 0) {
            customer = customersDao.getOne(customerId);
        } else {
            customer = customersDao.getOne(1);
        }


        switch (coupon.getType()) {
            case DISCOUNT:
            case COMBINED:
                wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "discount_type", "fixed_cart"));
                if (coupon.getCause() == Coupon.CouponCause.GROUP_DISCOUNT || coupon.getCause() == Coupon.CouponCause.GROUP) {
                    wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "usage_limit", ""));
                } else {
                    wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "usage_limit", "1"));
                }
                break;

            case PERCENT:
                wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "discount_type", "percent"));
                wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "usage_limit", "1"));
                break;

            case COMMERCIAL:
                wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "discount_type", "fixed_cart"));
                wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "usage_limit", ""));
                break;

            default:
                wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "discount_type", "fixed_cart"));
                wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "usage_limit", "1"));
                break;
        }

        switch (coupon.getCause()) {
            case GROUP:
            case GROUP_DISCOUNT:
                break;

            default:
                break;
        }

        wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "coupon_amount", "" + coupon.getAmount()));
        wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "individual_use", "no"));
        wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "usage_limit_per_user", "1"));
        wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "free_shipping", "no"));
        wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "exclude_sale_items", "no"));
        wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "slide_template", "default"));
        wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "exclude_product_categories", "a:0:{}"));
        wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "product_categories", "a:0:{}"));
        wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "maximum_amount", ""));
        wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "minimum_amount", ""));
        wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "expiry_date", ""));
        wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "limit_usage_to_x_items", ""));
        wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "exclude_product_ids", ""));
        wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "product_ids", ""));
        wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "_edit_last", "1"));

        switch (coupon.getType()) {
            case POINTS:
                Integer wpCustomerId = customer.getWpId();
                short deductedPoints = (short) (U.var.parseInt(-coupon.getAmount() * 2));
                short customerPoints = (short) (U.var.parseInt(pointsDao.sum(customerId)));
                short updatedPoints = (short) (U.var.parseInt(customerPoints + deductedPoints));
                pointsDao.save(new Point(customerId, deductedPoints, 'M', 0, new Date(System.currentTimeMillis())));
                customer.setCreditPoints(updatedPoints);
                wpUsermetaDao.updateByIdAndKey((long) customer.getWpId(), "shookbook_points", Integer.toString(updatedPoints));
                customersDao.save(customer);
                break;

            default:
                break;
        }

        switch (coupon.getCause()) {
            case DELIVERY:
                break;

            case GROUP:
                wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "_used_by", "" + customer.getWpId()));
                wpOrderItemsDao.save(new WpOrderItem(coupon.getTitle(), "coupon", 0));
                break;

            default:
                break;
        }


        if (customerId != 0) {
            String customerEmail = customersDao.getOne(customerId).getEmail();
            int emailLength = customerEmail.length();
            if (coupon.getCause() == Coupon.CouponCause.GROUP) {
                wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "customer_email", "a:0:{}"));
            } else {
                wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "customer_email", "a:1:{i:0;s:" + emailLength + ":\"" + customerEmail + "\";}"));
            }
        } else {
            wpPostmetaDao.save(new WpPostmeta(wpCoupon.getId(), "customer_email", "a:0:{}"));
        }

        return coupon.getId();
    }
}
