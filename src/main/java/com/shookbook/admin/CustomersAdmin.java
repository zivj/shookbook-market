package com.shookbook.admin;

import com.mashape.unirest.http.Unirest;
import com.shookbook.dao.shookbook.*;
import com.shookbook.dao.wordpress.WpPostmetaDao;
import com.shookbook.dao.wordpress.WpPostsDao;
import com.shookbook.dao.wordpress.WpUsermetaDao;
import com.shookbook.entities.shookbook.*;
import com.shookbook.types.HashSetIgnoresNulls;
import com.shookbook.utils.U;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by MorM on 4/20/16.
 */
@Controller
@SessionAttributes("thought")
public class CustomersAdmin {
    private static final Logger logger = Logger.getLogger(CustomersAdmin.class);

    @Autowired
    private Common common;
    @Autowired
    private ProductsDao productsDao;
    @Autowired
    private CustomersDao customersDao;
    @Autowired
    private OrdersDao ordersDao;
    @Autowired
    private OrderProductDao orderProductDao;
    @Autowired
    private ItemsDao itemsDao;
    @Autowired
    private SuppliersDao suppliersDao;
    @Autowired
    private SupplierOrdersDao supplierOrdersDao;
    @Autowired
    private SupplierOrderItemDao supplierOrderItemDao;
    @Autowired
    private GlobalsDao globalsDao;
    @Autowired
    private PointsDao pointsDao;
    @Autowired
    private WorkersDao workersDao;
    @Autowired
    private CitiesDao citiesDao;
    @Autowired
    private CityGroupsDoa cityGroupsDoa;
    @Autowired
    private CouponsDao couponsDao;
    @Autowired
    private GeoPoinsDao geoPoinsDao;
    @Autowired
    private DeliveryDatesDao deliveryDatesDao;
    @Autowired
    private ClubOrderDao clubOrderDao;
    @Autowired
    private WpUsermetaDao wpUsermetaDao;
    @Autowired
    private WpPostsDao wpPostsDao;
    @Autowired
    private WpPostmetaDao wpPostmetaDao;

    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public Object admin(HttpServletRequest request) {
        return "redirect:/admin/login";
    }

    @RequestMapping(value = "/admin/login", method = RequestMethod.GET)
    public Object login(HttpServletRequest request, Model model) {

        String version = "Version: 3.6.2-shookbook";
        model.addAttribute("version", version);
        return "Login";
    }

    @RequestMapping(value = "/admin/logout", method = RequestMethod.GET)
    public Object logout(HttpServletRequest request, Model model) {
        request.getSession().removeAttribute("login");
        String version = "Version: 3.6.2-shookbook";
        model.addAttribute("version", version);
        return "Login";
    }

    @RequestMapping(value = "/admin/login/submit", method = RequestMethod.POST)
    public Object loginSubmit(HttpServletRequest request) {

        request.getSession().setMaxInactiveInterval(60 * 60);  // Set session timeout one hour

        Worker worker = workersDao.login(request.getParameter("username"), request.getParameter("password"));
        if (worker != null && worker.getType().ordinal() > 0) {
            request.getSession().setAttribute("login", true);
            request.getSession().setAttribute("admin", worker.getType().ordinal());
            request.getSession().setAttribute("userId", worker.getId());
            request.getSession().setAttribute("userName", worker.getUsername());
            /*common.userName = worker.getFirstName() + " " + worker.getLastName();
            common.customerSupportPrivilege = (worker.getType() == Worker.Types.CUSTOMER_SUPPORT) ? 1 : 0;
            common.adminPrivilege = (worker.getType() == Worker.Types.ADMIN) ? 1 : 0;
            common.enableCouponModule = U.var.parseInt(globalsDao.findOne("modules_enable_coupons").getValue());
            common.enableInvoiceModule = U.var.parseInt(globalsDao.findOne("modules_enable_invoice").getValue());
            common.enableLogisticsModule = U.var.parseInt(globalsDao.findOne("modules_enable_logistics").getValue());
            common.enablePackingModule = U.var.parseInt(globalsDao.findOne("modules_enable_packing").getValue());
             switch (common.interfaceLanguage) {
                case "IL" : common.languageDir = "rtl"; break;
                case "EN" : common.languageDir = "ltr"; break;
                default : common.languageDir = "rtl"; break;
            }
            Object logedWorker = request.getSession().getAttribute("userId");
*/
            return "redirect:/admin/customers/orders";
        }
        return "redirect:/admin/login";
    }

    @RequestMapping(value = "/admin/customers/changeBarcode", method = RequestMethod.GET)
    public Object changeBarcode(Model model, HttpServletRequest request) {
        List<String> groupACities = Arrays.asList(
                "בנימינה גבעת עדה",
                "בנימינה",
                "גבעת עדה",
                "זכרון יעקב",
                "מעגן מיכאל",
                "מעיין צבי",
                "אור עקיבא",
                "שדות ים",
                "פרדס חנה כרכור",
                "פרדס חנה",
                "חנה כרכור",
                "קיסריה",
                "גבעת אולגה",
                "בית חנניה",
                "חיפה",
                "טירת כרמל",
                "כפר גלים",
                "מעלה שביט",
                "בית אורן",
                "גבעת וולפסון",
                "עספיא",
                "בית צבי",
                "בת שלמה",
                "גבע כרמל",
                "דור",
                "כרם מהר''ל",
                "מאיר שפיה",
                "מרכז מיר''ב",
                "נווה ים",
                "עין איילה",
                "עין חוד",
                "עין כרמל",
                "עתלית",
                "חוף כרמל",
                "פוריידיס",
                "נשר",
                "חריש",
                "קרית חיים חיפה",
                "קרית ים",
                "קרית חיים",
                "קרית מוצקין",
                "קרית ביאליק",
                "קרית אתא",
                "קרית טבעון",
                "קריית ים",
                "קריית חיים",
                "קריית מוצקין",
                "קריית ביאליק",
                "קריית אתא",
                "קריית טבעון",
                "כפר חסידים",
                "רמת ישי",
                "רמת יוחנן",
                "בית שערים",
                "כפר ביאליק",
                "אלונים",
                "יוקנעם",
                "יוקנעם עילית",
                "כפר יהושע",
                "בית אורן",
                "סוללים",
                "קיבוץ סוללים",
                "יגור"
        );

        List<String> groupBCities = Arrays.asList(
                "רביבים",
                "משאבי שדה",
                "רתמים",
                "מגדים",
                "טללים",
                "אשלים",
                "מדרשת בן גוריון",
                "שדה בוקר"
        );

        List<String> groupCCities = Arrays.asList(
                "טייבה",
                "טיבה"
        );

        List<String> groupDCities = Arrays.asList(
                "ברקן",
                "אריאל"
        );

        List<String> groupZCities = Arrays.asList(
                "באר מילכה",
                "קדש ברנע",
                "ניצני סני",
                "כפר ורדים",
                "שוקדה",
                "בארי"
        );

        List<Customer> customers = customersDao.findAll();
        for (Customer customer : customers) {
            if (customer.getId() < 10000) {
                int zone = 0;
                if (groupACities.contains(customer.getCity())) {
                    zone = 1;
                } else if (groupBCities.contains(customer.getCity())) {
                    zone = 2;
                } else if (groupCCities.contains(customer.getCity())) {
                    zone = 3;
                } else if (groupDCities.contains(customer.getCity())) {
                    zone = 4;
                } else if (groupZCities.contains(customer.getCity())) {
                    zone = 9;
                }

                Random random = new Random();
                Integer randomNumber = random.nextInt(979999) + 19264;
                String randomField = String.format("%06d", randomNumber);

                Calendar cal = Calendar.getInstance();
                cal.setTime(customer.getCreatedDate());
                int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
                int weekInYear = cal.get(Calendar.WEEK_OF_YEAR);
                String weekInYearStr = String.format("%02d", weekInYear);

                Integer xor = (Character.getNumericValue(randomField.charAt(0)) ^ Character.getNumericValue(randomField.charAt(1)) ^ Character.getNumericValue(randomField.charAt(2)) ^
                        Character.getNumericValue(randomField.charAt(3)) ^ Character.getNumericValue(randomField.charAt(4)) ^ Character.getNumericValue(randomField.charAt(5)) ^
                        zone ^ dayOfWeek ^ Character.getNumericValue(weekInYearStr.charAt(0)) ^ Character.getNumericValue(weekInYearStr.charAt(1))) % 10;

                String customerId = String.valueOf(dayOfWeek) + weekInYearStr + zone + randomField + '-' + String.valueOf(xor);

                customer.setBarcode(customerId);
                customersDao.save(customer);

            }
        }
        return "redirect:/admin/customers";
    }

    @RequestMapping(value = "/admin/customers/add/submit", method = RequestMethod.POST)
    public Object customerAddSubmit(@RequestParam(value = "firstName") String firstName, @RequestParam(value = "lastName") String lastName, @RequestParam(value = "phone") String phone,
                                    @RequestParam(value = "phone2") String phone2, @RequestParam(value = "email") String email, @RequestParam(value = "street") String street,
                                    @RequestParam(value = "no") String houseNo, @RequestParam(value = "city") String city, Model model, HttpServletRequest request) {
        Customer customer = new Customer();
        customer.setBarcode("12233");
        customer.setFirstName(firstName);
        customer.setLastName(lastName);
        customer.setPhone(phone);
        customer.setPhone2(phone2);
        customer.setEmail(email);
        customer.setStreet(street);
        customer.setStreetNo(houseNo);
        customer.setCity(city);
        customer.setIsDeleted('0');
        customer.setType(Customer.CustomerTypes.SILVER);
        customersDao.save(customer);
        return "redirect:/admin/customers";
    }

    @RequestMapping(value = "/admin/customers/add", method = RequestMethod.GET)
    public Object customerAdd(HttpServletRequest request) {

        return "CustomerAdd";
    }

    public final static String[] orderFilters = new String[]{"הכל", "שנה", "3 חודשים", "חודש", "פתוחות", "בקטיף", "בטיפול", "נשלחו", "בוטלו", "בגבייה", "בעיית גביה", "הזמנות במעקב", "בדיקה לפני גבייה"};

    @RequestMapping(value = "/admin/customers/orders", method = RequestMethod.GET)
    public Object customersOrders(Model model, @RequestParam(value = "filter", defaultValue = "3") Integer filter, @RequestParam(value = "sortBy", defaultValue = "id") String sortBy, @RequestParam(value = "sortOrder", defaultValue = "asc") String sortOrder, HttpServletRequest request) {

        /* if (true) {
            String password = "1234567890123456";
            Boolean[] passwordBits = new Boolean[password.length() * 8];
            Long passwordInHex = 0L;
            StringBuilder input = new StringBuilder();
            StringBuilder output = new StringBuilder();

            for (int i = 0; i < password.length(); i++) {
                String str = password.substring(i, i + 1);
                Integer asciiInt = (int) str.charAt(0);
                input.append(Integer.toHexString(asciiInt));
            }

            input.replace(0,input.length(), "a0d51b9d462c32ee73d6bd6d40cdd962");
            //input.replace(0,input.length(), "00000000000000000000000000000000");

            for (int i = 0; i < input.length() / 2; i++) {
                String str1 = input.substring(i * 2 + 1, i * 2 + 2);
                String str2 = input.substring(i * 2, i * 2 + 1);
                for (int j = 0; j < 4; j++) {
                    int lowNum = Integer.valueOf(str1, 16);
                    int highNum = Integer.valueOf(str2, 16);
                    passwordBits[i * 8 + j] = (lowNum & (1 << j)) > 0 ? true : false;
                    passwordBits[i * 8 + 4 + j] = (highNum & (1 << j)) > 0 ? true : false;
                }
            }

            Boolean[] passwordArray = new Boolean[128];

            if (passwordBits.length <= 128) {
                for (int i = 0; i < 128; i++) {
                    if (i < passwordBits.length) {
                        passwordArray[i] = passwordBits[i];
                    } else {
                        passwordArray[i] = false;
                    }
                }
            }

            Boolean[] topLfsr = new Boolean[32];
            Boolean[] bottomLfsr = new Boolean[32];

            Boolean[] topSpin = new Boolean[128];
            Boolean[] bottomSpin = new Boolean[128];

            Boolean[] topResult = new Boolean[128];
            Boolean[] bottomResult = new Boolean[128];

            Boolean[] permutationResult = new Boolean[128];

            Integer[] permutationTable = {0, 2, 8, 10, 4, 6, 12, 14, 1, 3, 9, 11, 5, 7, 13, 15};    // Decript
            Boolean result = false;

            Integer topSeed = 0x55dc6201;
            Integer bottomSeed = 0x9af3c025;

            for (int i = 0; i < 32; i++) {
                topLfsr[i] = (((1 << i) & topSeed) >>> i) == 1;
            }

            for (int i = 0; i < 32; i++) {
                bottomLfsr[i] = (((1 << i) & bottomSeed) >>> i) == 1;
            }

            for (int i = 0; i < 128; i++) {
                result = topLfsr[31] ^ topLfsr[30] ^ topLfsr[28] ^ topLfsr[5];
                for (int j = 1; j < 32; j++) {
                    topLfsr[32 - j] = topLfsr[31 - j];
                }
                topLfsr[0] = result;
                topSpin[127 - i] = result;

                result = bottomLfsr[31] ^ bottomLfsr[30] ^ bottomLfsr[12];
                for (int j = 1; j < 32; j++) {
                    bottomLfsr[32 - j] = bottomLfsr[31 - j];
                }
                bottomLfsr[0] = result;
                bottomSpin[127 - i] = result;
            }

            for (int i = 0; i < 128; i++) {
                topResult[i] = passwordArray[i] ^ bottomSpin[i];
            }

            for (int i = 0; i < 32; i++) {
                Integer tmp = 0;
                for (int j = 0; j < 4; j++) {
                    tmp += topResult[i * 4 + j] ? 1 << j : 0;
                }
                Integer permutaion = permutationTable[tmp];
                for (int j = 0; j < 4; j++) {
                    permutationResult[i * 4 + j] = (permutaion & (1 << j)) > 0 ? true : false;
                }
            }

            Boolean[] passwordResult = new Boolean[128];

            for (int i = 0; i < 128; i++) {
                passwordResult[i] = permutationResult[i] ^ topSpin[i];
            }

        } else {
            String password = "1ab";
            Boolean[] passwordBits = new Boolean[password.length() * 8];
            Long passwordInHex = 0L;
            StringBuilder input = new StringBuilder();
            StringBuilder output = new StringBuilder();

            for (int i = 0; i < password.length(); i++) {
                String str = password.substring(i, i + 1);
                Integer asciiInt = (int) str.charAt(0);
                input.append(Integer.toHexString(asciiInt));
            }

            for (int i = 0; i < input.length() / 2; i++) {
                String str1 = input.substring(i * 2 + 1, i * 2 + 2);
                String str2 = input.substring(i * 2, i * 2 + 1);
                for (int j = 0; j < 4; j++) {
                    int lowNum = Integer.valueOf(str1, 16);
                    int highNum = Integer.valueOf(str2, 16);
                    passwordBits[i * 8 + j] = (lowNum & (1 << j)) > 0 ? true : false;
                    passwordBits[i * 8 + 4 + j] = (highNum & (1 << j)) > 0 ? true : false;
                }
            }

            Boolean[] passwordArray = new Boolean[128];

            if (passwordBits.length <= 128) {
                for (int i = 0; i < 128; i++) {
                    if (i < passwordBits.length) {
                        passwordArray[i] = passwordBits[i];
                    } else {
                        passwordArray[i] = false;
                    }
                }
            }

            Boolean[] topLfsr = new Boolean[32];
            Boolean[] bottomLfsr = new Boolean[32];

            Boolean[] topSpin = new Boolean[128];
            Boolean[] bottomSpin = new Boolean[128];

            Boolean[] topResult = new Boolean[128];
            Boolean[] bottomResult = new Boolean[128];

            Boolean[] permutationResult = new Boolean[128];

            Integer[] permutationTable = {0, 8, 1, 9, 4, 12, 5, 13, 2, 10, 3, 11, 6, 14, 7, 15};    // Encrypt
            Boolean result = false;

            Integer topSeed = 0x55dc6201;
            Integer bottomSeed = 0x9af3c025;

            for (int i = 0; i < 32; i++) {
                topLfsr[i] = (((1 << i) & topSeed) >>> i) == 1;
            }

            for (int i = 0; i < 32; i++) {
                bottomLfsr[i] = (((1 << i) & bottomSeed) >>> i) == 1;
            }

            for (int i = 0; i < 128; i++) {
                result = topLfsr[31] ^ topLfsr[30] ^ topLfsr[28] ^ topLfsr[5];
                for (int j = 1; j < 32; j++) {
                    topLfsr[32 - j] = topLfsr[31 - j];
                }
                topLfsr[0] = result;
                topSpin[127 - i] = result;

                result = bottomLfsr[31] ^ bottomLfsr[30] ^ bottomLfsr[12];
                for (int j = 1; j < 32; j++) {
                    bottomLfsr[32 - j] = bottomLfsr[31 - j];
                }
                bottomLfsr[0] = result;
                bottomSpin[127 - i] = result;
            }

            for (int i = 0; i < 128; i++) {
                topResult[i] = passwordArray[i] ^ topSpin[i];
            }

            for (int i = 0; i < 32; i++) {
                Integer tmp = 0;
                for (int j = 0; j < 4; j++) {
                    tmp += topResult[i * 4 + j] ? 1 << j : 0;
                }
                Integer permutaion = permutationTable[tmp];
                for (int j = 0; j < 4; j++) {
                    permutationResult[i * 4 + j] = (permutaion & (1 << j)) > 0 ? true : false;
                }
            }

            Boolean[] passwordResult = new Boolean[128];

            for (int i = 0; i < 128; i++) {
                passwordResult[i] = permutationResult[i] ^ bottomSpin[i];
            }
        }
        // Ziv LFSR
        */

        if (!U.var.parseBoolean(request.getSession().getAttribute("login"))) {
            return "redirect:/admin/login";
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        Map<Integer, Integer> statusCounts = new HashMap<>();
        for (int i = 0; i < 6; i++) {
            statusCounts.put(i, 0);
        }
        for (Object[] r : ordersDao.findAllStatusCounts()) {
            statusCounts.put(U.var.parseInt(r[0]), U.var.parseInt(r[1]));
        }

        Map<String, Integer> dateCountsMorning = new HashMap<>();
        for (Object[] r : ordersDao.findAllOpenDeliveryDateMorningCounts()) {
            if (r[1].equals(null)) {
                dateCountsMorning.put(dateFormat.format(r[0]), 0);
            } else {
                dateCountsMorning.put(dateFormat.format(r[0]), U.var.parseInt(r[1]));
            }
        }

        Map<String, Integer> dateCountsEvening = new HashMap<>();
        for (Object[] r : ordersDao.findAllOpenDeliveryDateEveningCounts()) {
            if (r[1].equals(null)) {
                dateCountsEvening.put(dateFormat.format(r[0]), 0);
            } else {
                dateCountsEvening.put(dateFormat.format(r[0]), U.var.parseInt(r[1]));
            }
        }

        List<Order> orders;
        switch (filter) {
            case 0:
                orders = ordersDao.findAllOrderByIdDesc();
                break;
            case 1:
                orders = ordersDao.findAllOrderLastYearByIdDesc();
                break;
            case 2:
                orders = ordersDao.findAllOrderLast3MonthsByIdDesc();
                break;
            default:
            case 3:
                orders = ordersDao.findAllOrderLastMonthByIdDesc();
                break;
            case 4:
                orders = ordersDao.findAllOrderByStatusByIdDesc(1);
                break;
            case 5:
                orders = ordersDao.findAllOrderByStatusByIdDesc(5);
                break;
            case 6:
                orders = ordersDao.findAllOrderByStatusByIdDesc(2);
                break;
            case 7:
                orders = ordersDao.findAllOrderByStatusByIdDesc(3);
                break;
            case 8:
                orders = ordersDao.findAllOrderByStatusByIdDesc(4);
                break;
            case 9:
                orders = ordersDao.findAllClearableToDate();
                break;
            case 10:
                orders = ordersDao.findAllNotCleared();
                break;
            case 11:
                orders = ordersDao.findAllNeedAttention();
                break;
            case 12:
                orders = ordersDao.findAllCreditCheck();
                break;
        }

//        orders.sort(Comparator.comparing(Order::getDeliveryDate));

        switch (sortBy) {
            case "deliveryDate" :
                if (sortOrder.equals("asc")) {
                    System.out.format("   ### Asc%n");
                    model.addAttribute("sortOrder", "desc");
                    Collections.sort(orders, new Comparator<Order>() {
                        public int compare(Order o1, Order o2) {
                            return Long.valueOf(o1.getDeliveryDate().getTime()).compareTo(o2.getDeliveryDate().getTime());
                        }
                    });
                } else {
                    System.out.format("   ### Desc%n");
                    model.addAttribute("sortOrder", "asc");
                    Collections.sort(orders, new Comparator<Order>() {
                        public int compare(Order o1, Order o2) {
                            return Long.valueOf(o2.getDeliveryDate().getTime()).compareTo(o1.getDeliveryDate().getTime());
                        }
                    });
                }
                break;
            default : orders.sort(Comparator.comparing(Order::getId).reversed());
        }

        Set<Integer> customerIds = new HashSetIgnoresNulls<>();
        for (Order order : orders) {
            customerIds.add(order.getCustomerId());
        }
        Map<Integer, Customer> customerMap = new HashMap<>();

        for (Customer customer : customersDao.findAll(customerIds)) {
            customerMap.put(customer.getId(), customer);
        }

        Map<Integer, Worker> workersMap = new HashMap<>();
        workersMap.put(0, new Worker());
        for (Worker worker : workersDao.findAll()) {
            workersMap.put(worker.getId(), worker);
        }

        Float ordersSum = 0F;
        List<Map<String, Object>> columns = new LinkedList<>();
        for (Order order : orders) {
            Map<String, Object> column = new HashMap<>();
            ordersSum += order.getSum();
            ordersSum -= order.getDiscountSum();
            column.put("id", order.getId());
            column.put("orderNumber", common.createOrderCode(order.getId()));
            column.put("orderNumberWP", order.getWpId());
            column.put("customerNumber", customerMap.get(order.getCustomerId()).getBarcode());
            column.put("customerName", customerMap.get(order.getCustomerId()).getFirstName() + " " + customerMap.get(order.getCustomerId()).getLastName());
            column.put("customerId", customerMap.get(order.getCustomerId()).getId());
            column.put("orderCreateDate", new SimpleDateFormat("dd-MM-yyyy").format(order.getCreatedDate()));
            Date harvestDate = order.getDeliveryDate();
            harvestDate = new Date(harvestDate.getTime() - 24*60*60*1000);
            column.put("orderHarvestDate", (harvestDate == null ? null : new SimpleDateFormat("dd-MM-yyyy").format(harvestDate)));
            Date deliveryDate = order.getDeliveryDate();
            if (order.getDeliveryWindow() == 1) {
              deliveryDate = new Date(deliveryDate.getTime() - 24*60*60*1000);
            }
            column.put("orderDeliveryDate", (deliveryDate == null ? null : new SimpleDateFormat("dd-MM-yyyy").format(deliveryDate)));
            column.put("orderShipmentDate", (order.getLastPackingTimestamp() == null ? null : new SimpleDateFormat("dd-MM-yyyy").format(order.getLastPackingTimestamp())));
            column.put("refrigerated", (order.getRefrigerated() == 0 ? "לא" : "כן"));
            column.put("customerId", order.getCustomerId());
            column.put("deliveryWindow", order.getDeliveryWindow());

            column.put("customerType", customerMap.get(order.getCustomerId()).getType().heb);
            column.put("typeId", customerMap.get(order.getCustomerId()).getType().ordinal());
            column.put("orderAmount", order.getSum());

            //column.put("creditCardNumber", "N/A");
            column.put("dhl_shipping_number", order.getDhlPackageNumber());
            column.put("worker_id", workersMap.get(order.getPackingWorkerId()).getSbId());
            column.put("status", order.getStatus().heb);
            column.put("statusId", order.getStatus().ordinal());
            column.put("enableClearing", order.getClearingStatus() == Order.ClearStatus.UNCLEAR &&
                                         ((order.getStatus() == Order.OrderStatuses.SHIPMENT) || (order.getStatus() == Order.OrderStatuses.DELIVERED)));
            column.put("attention", customerMap.get(order.getCustomerId()).getAttentionFlag().ordinal());
            String fontColor = "#000000";
            if (customerMap.get(order.getCustomerId()).getAttentionFlag().ordinal() > 0) {
                fontColor = "#a10000";
            } else if (U.var.parseInt(customerMap.get(order.getCustomerId()).getEnvironmentFriendly()) == 1) {
                fontColor = "#6a9b34";
            }
            column.put("fontColor", fontColor);
            column.put("green", customerMap.get(order.getCustomerId()).getEnvironmentFriendly());

            boolean sameAddress;
            boolean foundSameAddress = false;
            List<GeoPoint> geoPoints = geoPoinsDao.findAllByCustomerId(order.getCustomerId());
            for (GeoPoint geoPoint : geoPoints) {
                sameAddress = true;
                if (!order.getCity().equals(geoPoint.getCity())) {
                    sameAddress = false;
                }
                if (!order.getStreet().equals(geoPoint.getStreet())) {
                    sameAddress = false;
                }
                if (!order.getStreetNo().equals(geoPoint.getStreetNumber())) {
                    sameAddress = false;
                }

                if (sameAddress) {
                    foundSameAddress = true;
                }
            }
            column.put("gotLocation", foundSameAddress ? 1 : 0);
            column.put("geoGroup", (customerMap.get(order.getCustomerId()).getForceGeotag() > 0) ? 1 : 0);

            columns.add(column);
        }

        StringBuilder title1 = new StringBuilder();
        title1.append("בוקר: ");
        StringBuilder title2 = new StringBuilder();
        StringBuilder title4 = new StringBuilder();
        title4.append("ערב: ");
        StringBuilder title5 = new StringBuilder();
        List<Date> deliveryDates;

        Date lastDate = ordersDao.findLastDeliveryDateByHarvesting();
        if (lastDate == null) {
            deliveryDates = deliveryDatesDao.findNextThree(new Date());
        } else {
            deliveryDates = deliveryDatesDao.findNextThree(lastDate);
        }

        Date deliveryDate;
        Calendar cal = Calendar.getInstance();
        int day;
        String dayOfWeekMorning;
        String dayOfWeekEvening;

        for (int index = 0; index < deliveryDates.size(); index++) {
            deliveryDate = deliveryDates.get(index);
            if (dateCountsMorning.get(dateFormat.format(deliveryDate)) == null) {
                dateCountsMorning.put(dateFormat.format(deliveryDate), 0);
            }
            cal.setTime(deliveryDate);
            day = cal.get(Calendar.DAY_OF_WEEK);
            switch (day) {
                case 1:
                    dayOfWeekMorning = "ראשון";
                    dayOfWeekEvening = "שבת";
                    break;
                case 2:
                    dayOfWeekMorning = "שני";
                    dayOfWeekEvening = "ראשון";
                    break;
                case 3:
                    dayOfWeekMorning = "שלישי";
                    dayOfWeekEvening = "שני";
                    break;
                case 4:
                    dayOfWeekMorning = "רביעי";
                    dayOfWeekEvening = "שלישי";
                    break;
                case 5:
                    dayOfWeekMorning = "חמישי";
                    dayOfWeekEvening = "רביעי";
                    break;
                case 6:
                    dayOfWeekMorning = "שישי";
                    dayOfWeekEvening = "חמישי";
                    break;
                case 7:
                    dayOfWeekMorning = "שבת";
                    dayOfWeekEvening = "שישי";
                    break;
                default:
                    dayOfWeekMorning = "לא ידוע";
                    dayOfWeekEvening = "לא ידוע";
                    break;
            }

            String morningCount = dateCountsMorning.get(dateFormat.format(deliveryDate)) != null ? dateCountsMorning.get(dateFormat.format(deliveryDate)).toString() : "" + 0;
            String eveningCount = dateCountsEvening.get(dateFormat.format(deliveryDate)) != null ? dateCountsEvening.get(dateFormat.format(deliveryDate)).toString() : "" + 0;
            title1.append(dayOfWeekMorning).append(" - ").append(morningCount).append(" ");
            title4.append(dayOfWeekEvening).append(" - ").append(eveningCount).append(" ");
        }


        title2.append("פתוחות: ").append(statusCounts.get(1)).append(' ');
        title2.append("בקטיף: ").append(statusCounts.get(5)).append(' ');
        title2.append("בטיפול: ").append(statusCounts.get(2)).append(' ');
        title2.append("נשלחו: ").append(statusCounts.get(3)).append(' ');
        // title.append("סגורות: ").append(statusCounts.get(0)).append(' ');
        title2.append("בוטלו: ").append(statusCounts.get(4));

        Integer attention = ordersDao.findAllNeedAttention().size() > 0 ? 1 : 0;

        // Check duplicate open and pending orders

        List<Order> duplicateOrders = ordersDao.findAllOpenWithSameCustomerId();
        Integer dupMatch = 0;
        int dupMatchPartial;
        int index = 0;
        int info;

        while (dupMatch == 0 && index < duplicateOrders.size() - 1) {
            dupMatchPartial = 1;
            info = 0;
            while (dupMatchPartial == 1 && info < 6) {
                switch (info) {
                    case 0:
                        if (!Objects.equals(duplicateOrders.get(index).getCity(), duplicateOrders.get(index + 1).getCity())) {
                            dupMatchPartial = 0;
                        }
                        break;
                    case 1:
                        if (!Objects.equals(duplicateOrders.get(index).getStreet(), duplicateOrders.get(index + 1).getStreet())) {
                            dupMatchPartial = 0;
                        }
                        break;
                    case 2:
                        if (!Objects.equals(duplicateOrders.get(index).getStreetNo(), duplicateOrders.get(index + 1).getStreetNo())) {
                            dupMatchPartial = 0;
                        }
                        break;
                    case 3:
                        if (!Objects.equals(duplicateOrders.get(index).getApartment(), duplicateOrders.get(index + 1).getApartment())) {
                            dupMatchPartial = 0;
                        }
                        break;
                    case 4:
                        if (!Objects.equals(duplicateOrders.get(index).getFloorNo(), duplicateOrders.get(index + 1).getFloorNo())) {
                            dupMatchPartial = 0;
                        }
                        break;
                    case 5:
                        dupMatch = 1;
                }
                info++;
            }
            index++;
        }

        if (dupMatch == 0) {
            index = 0;
            duplicateOrders = ordersDao.findAllPendingWithSameCustomerId();
            while (dupMatch == 0 && index < duplicateOrders.size() - 1) {
                dupMatchPartial = 1;
                info = 0;
                while (dupMatchPartial == 1 && info < 6) {
                    switch (info) {
                        case 0:
                            if (!Objects.equals(duplicateOrders.get(index).getCity(), duplicateOrders.get(index + 1).getCity())) {
                                dupMatchPartial = 0;
                            }
                            break;
                        case 1:
                            if (!Objects.equals(duplicateOrders.get(index).getStreet(), duplicateOrders.get(index + 1).getStreet())) {
                                dupMatchPartial = 0;
                            }
                            break;
                        case 2:
                            if (!Objects.equals(duplicateOrders.get(index).getStreetNo(), duplicateOrders.get(index + 1).getStreetNo())) {
                                dupMatchPartial = 0;
                            }
                            break;
                        case 3:
                            if (!Objects.equals(duplicateOrders.get(index).getApartment(), duplicateOrders.get(index + 1).getApartment())) {
                                dupMatchPartial = 0;
                            }
                            break;
                        case 4:
                            if (!Objects.equals(duplicateOrders.get(index).getFloorNo(), duplicateOrders.get(index + 1).getFloorNo())) {
                                dupMatchPartial = 0;
                            }
                            break;
                        case 5:
                            dupMatch = 1;
                    }
                    info++;
                }
                index++;
            }
        }

        model.addAttribute("duplicateOrders", dupMatch);
        model.addAttribute("attention", attention);
        model.addAttribute("title1", title1.toString());
        model.addAttribute("title2", title2.toString());
        model.addAttribute("title3", orders.size());
        model.addAttribute("title4", title4.toString());
        model.addAttribute("title5", title5.toString());
        model.addAttribute("ordersSum", String.format("%,.2f", ordersSum));
        model.addAttribute("columns", columns);
        model.addAttribute("filter", orderFilters[filter]);
        model.addAttribute("filterValue", Arrays.asList(orderFilters).indexOf(orderFilters[filter]));
        model.addAttribute("sortOrder", sortOrder);
        Map<String, Object> globals = new HashMap<>();
        globals.put("modules_enable_coupons", globalsDao.findOne("modules_enable_coupons").getValue());
        globals.put("modules_enable_invoice", globalsDao.findOne("modules_enable_invoice").getValue());
        globals.put("modules_enable_logistics", globalsDao.findOne("modules_enable_logistics").getValue());
        globals.put("modules_enable_packing", globalsDao.findOne("modules_enable_packing").getValue());
        Object logedWorker = request.getSession().getAttribute("userId");
        Worker worker = workersDao.getOne(Integer.parseInt(logedWorker.toString()));
        globals.put("worker_name", worker.getFirstName() + " " + worker.getLastName());
        /*
        globals.put("modules_enable_coupons", common.enableCouponModule);
        globals.put("modules_enable_invoice", common.enableInvoiceModule);
        globals.put("modules_enable_logistics", common.enableLogisticsModule);
        globals.put("modules_enable_packing", common.enablePackingModule);
        globals.put("worker_name", common.userName);
        model.addAttribute("customer_support", common.customerSupportPrivilege);
        model.addAttribute("admin", common.adminPrivilege);
        */
        model.addAttribute("globals", globals);
        model.addAttribute("customer_support", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.CUSTOMER_SUPPORT.ordinal() ? 1 : 0);
        model.addAttribute("admin", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.ADMIN.ordinal() ? 1 : 0);


        return "CustomersOrders";
    }

    public final static String[] customerFilters = new String[]{"הכל", "שנה", "3 חודשים", "הזמינו ב 3 חודשים האחרונים", "הזמינו בחודש האחרון", "הזמינו יותר מפעם אחת", "הזמינו יותר 5 פעמים", "הזמינו יותר 10 פעמים", "אריזה ירוקה"};

    @RequestMapping(value = "/admin/customers", method = RequestMethod.GET)
    public Object customers(Model model, @RequestParam(value = "filter", defaultValue = "2") Integer filter, HttpServletRequest request) {
        if (!U.var.parseBoolean(request.getSession().getAttribute("login"))) {
            return "redirect:/admin/login";
        }

        Map<Integer, Integer> orderCounts = new HashMap<>();
        for (Object[] o : customersDao.findAllOrderCount()) {
            orderCounts.put(U.var.parseInt(o[0]), U.var.parseInt(o[1]));
        }

        Map<Integer, String> orderDates = new HashMap<>();
        for (Object[] o : ordersDao.findLastDeliveredWithSameCustomerId()) {
            if (o[1] == null) {
                orderDates.put(U.var.parseInt(o[0]), " ");
            } else {
                orderDates.put(U.var.parseInt(o[0]), new SimpleDateFormat("dd-MM-yyyy").format(o[1]));
            }
        }

        List<Customer> customers;
        switch (filter) {
            case 0:
                customers = customersDao.findAll();
                break;
            case 1:
                customers = customersDao.findAllInYear();
                break;
            case 2:
                customers = customersDao.findAllIn3Months();
                break;
            case 3:
                customers = customersDao.findAllHadOrderIn3Months();
                break;
            default:
            case 4:
                customers = customersDao.findAllHadOrderInLastMonth();
                break;
            case 5:
                customers = customersDao.findAllOrderMoreThan1Time();
                break;
            case 6:
                customers = customersDao.findAllOrderMoreThan5Times();
                break;
            case 7:
                customers = customersDao.findAllOrderMoreThan10Times();
                break;
            case 8:
                customers = customersDao.findAllEnvFriendly();
                break;
        }

        List<Map<String, Object>> columns = new LinkedList<>();
        for (Customer customer : customers) {
            Map<String, Object> column = new HashMap<>();
            column.put("customerName", customer.getFirstName() + " " + customer.getLastName());
            column.put("id", customer.getId());
            column.put("customerNumber", customer.getBarcode());
            column.put("customerId", customer.getId());
            column.put("customerType", customer.getType().heb);
            column.put("typeId", customer.getType().ordinal());
            column.put("customerPhoneNumber", customer.getPhone());
            column.put("customerMailAddress", customer.getEmail());
            column.put("joinDate", new SimpleDateFormat("dd-MM-yyyy").format(customer.getCreatedDate()));
            column.put("ordersNumber", orderCounts.get(customer.getId()));
            column.put("lastOrderDate", orderDates.get(customer.getId()));
            boolean attentionFlag = U.var.parseInt(customer.getAttentionFlag().ordinal()) > 0 ? true : false;
            column.put("attentionFlag", attentionFlag);
            column.put("green", U.var.parseInt(customer.getEnvironmentFriendly()));
            columns.add(column);
        }

        model.addAttribute("filter", customerFilters[filter]);
        model.addAttribute("columns", columns);
        Map<String, Object> globals = new HashMap<>();
        globals.put("modules_enable_coupons", globalsDao.findOne("modules_enable_coupons").getValue());
        globals.put("modules_enable_invoice", globalsDao.findOne("modules_enable_invoice").getValue());
        globals.put("modules_enable_logistics", globalsDao.findOne("modules_enable_logistics").getValue());
        globals.put("modules_enable_packing", globalsDao.findOne("modules_enable_packing").getValue());
        Object logedWorker = request.getSession().getAttribute("userId");
        Worker worker = workersDao.getOne(Integer.parseInt(logedWorker.toString()));
        globals.put("worker_name", worker.getFirstName() + " " + worker.getLastName());
        model.addAttribute("globals", globals);
        model.addAttribute("admin", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.ADMIN.ordinal() ? 1 : 0);

        return "Customers";
    }

    @RequestMapping(value = "/admin/customers/orders/printQRcodeCustomers", method = RequestMethod.GET)
    public Object printQRCodeCustomers(@RequestParam(value = "id") Integer id, Model model, HttpServletRequest request) {
        String barcode = ordersDao.findOne(id).getBarcode();
        Order order = ordersDao.findOne(id);
        int customerId = order.getCustomerId();
        Customer customer = customersDao.findOne(customerId);

        Map<String, Object> column = new HashMap<>();

        column.put("orderNumber", common.createOrderCode(order.getId()));
        String barcodeStr = String.format("%011d", order.getWpId());
        column.put("barcode", order.getBarcode());
        column.put("barcodeStr", barcodeStr);
        column.put("dhl_number", order.getDhlPackageNumber());
        column.put("customerName", order.getFirstName() + " " + order.getLastName());
        column.put("green", U.var.parseInt(customer.getEnvironmentFriendly()));
        column.put("street", order.getStreet() + " " + order.getStreetNo());
        column.put("apartment", order.getApartment());
        column.put("floor", order.getFloorNo());
        column.put("city", order.getCity());
        column.put("house_number", order.getStreetNo());
        column.put("phones", customer.getPhone() + (customer.getPhone2() != null ? "   " + customer.getPhone2() : ""));
        column.put("boxes", "1/" + order.getBoxes());
        column.put("customerNumber", customer.getBarcode());
        column.put("QRcodeStyle", globalsDao.findOne("app_barcode_style").getValue());
        model.addAttribute("qr", barcode);
        model.addAttribute("column", column);
        model.addAttribute("admin", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.ADMIN.ordinal() ? 1 : 0);

        return "PrintQRcodeCustomers";
    }

    @RequestMapping(value = "/admin/customers/orders/submit/status", method = RequestMethod.POST)
    public Object submitStatus(@RequestParam(value = "orderId") Integer orderId,
                               @RequestParam(value = "orderStatus") Integer statusOrdinal,
                               @RequestParam(value = "returnToOrders", required = false) Boolean returnToOrders,
                               HttpServletRequest request) {
        Order order = ordersDao.findOne(orderId);

        Order.OrderStatuses status = Order.OrderStatuses.values()[statusOrdinal];
        if (status != order.getStatus()) {
            order.setStatus(status);
            if (order.getType().getOrderTypeCode() < 80) {  // No external orders
                wpPostmetaDao.updateByIdAndKey((long) order.getWpId(), "_sb_status", "" + status);
            }
            if (status == Order.OrderStatuses.SHIPMENT) {
                order.setPackingWorkerId(U.var.parseInt(request.getSession().getAttribute("userId")));
                order.setLastPackingTimestamp(new Date(System.currentTimeMillis()));
            }
        }
        order = ordersDao.save(order);
        logger.info("SB: " + request.getSession().getAttribute("userName") + ": update order status " + order.getId() + " -> " + status);
        return "redirect:/admin/customers/orders" + (U.var.parseBoolean(returnToOrders) ? "" : "/order?id=" + orderId);
    }

    @RequestMapping(value = "/admin/customers/orders/submit/clearingStatus", method = RequestMethod.POST)
    public Object submitCleringStatus(@RequestParam(value = "orderId") Integer orderId,
                                      @RequestParam(value = "orderClearingStatus") Integer clearingStatusOrdinal,
                                      HttpServletRequest request) {
        Order order = ordersDao.findOne(orderId);

        Order.ClearStatus clearingStatus = Order.ClearStatus.values()[clearingStatusOrdinal];
        if (clearingStatus != order.getClearingStatus()) {
            order.setClearingStatus(clearingStatus);
        }
        order = ordersDao.save(order);
        logger.info("SB: " + request.getSession().getAttribute("userName") + ": update order clearing status " + order.getId() + " -> " + clearingStatus);
        return "redirect:/admin/customers/customer?id=" + customersDao.findOne(order.getCustomerId()).getId();
    }

    @RequestMapping(value = "/admin/customers/submit/customeType", method = RequestMethod.POST)
    public Object submitCustomerType(@RequestParam(value = "customerId") Integer customerId, @RequestParam(value = "customerType") Integer type, Model model, HttpServletRequest request) {
        Customer customer = customersDao.findOne(customerId);
        customer.setType(Customer.CustomerTypes.values()[type]);
        customer.setTypeDate(new Date(System.currentTimeMillis()));
        customersDao.save(customer);
        logger.info("SB: " + request.getSession().getAttribute("userName") + ": set customer type " + customer.getId() + " -> " + customer.getType());
        return "redirect:/admin/customers";
    }

    @RequestMapping(value = "/admin/customers/customer", method = RequestMethod.GET)
    public Object customer(@RequestParam(value = "id") Integer id, Model model, HttpServletRequest request) {
        if (!U.var.parseBoolean(request.getSession().getAttribute("login"))) {
            return "redirect:/admin/login";
        }
        Customer customer = customersDao.findOne(id);

        Map<String, Object> column = new HashMap<>();

        List <Object []> groupsUsages = wpPostsDao.findUsageByTitleAndType("%-8", "shop_coupon");
        Integer wpId = customer.getWpId();
        String groups = "";

        for (Object [] groupsUsage : groupsUsages) {
            if (U.var.parseInt(groupsUsage[0]) == wpId) {
                System.out.format("   %s%n", groupsUsage[1].toString());
                Coupon groupCoupon = couponsDao.findOneByTitle(groupsUsage[1].toString());
                if (groupCoupon != null) {
                    groups = groups.concat(groupCoupon.getDetails() + " ");
                }
            }
        }

        column.put("id", customer.getId());
        column.put("barcode", customer.getBarcode());
        column.put("createdDate", customer.getCreatedDate());
        column.put("fullName", customer.getFirstName() + " " + customer.getLastName());
        column.put("entryCode", customer.getEntryCode());
        column.put("orderCreatedDate", new SimpleDateFormat("dd-MM-yyyy").format(customer.getCreatedDate()));
        column.put("firstName", customer.getFirstName());
        column.put("lastName", customer.getLastName());
        column.put("groups", groups);
        column.put("nekoshook", customer.getCreditPoints());
        column.put("email", customer.getEmail());
        column.put("emailto", "mailto:" + U.var.urlEncode(customer.getEmail()));
        column.put("shipmentAddress", customer.getCity() + " " + customer.getStreet() + " ");
        column.put("city", customer.getCity());
        CityGroup cityGroup = cityGroupsDoa.findOneByCity(customer.getCity());
        if (cityGroup != null) {
            column.put("cityGroup", cityGroupsDoa.findOneByCity(customer.getCity()).getDescription());
        } else {
            column.put("cityGroup", "NA");
        }
        column.put("apartment", customer.getApartment());
        column.put("streetNo", customer.getStreetNo());
        column.put("floor", customer.getFloorNo());
        column.put("street", customer.getStreet());
        column.put("phone", customer.getPhone());
        column.put("phone2", customer.getPhone2());
        column.put("remarks", customer.getRemarks());
        column.put("creditPoints", customer.getCreditPoints());
        column.put("creditCardHide", customer.getCreditCardHide() == '0' ? "הסתר כרטיס" : "החזר כרטיס");
        column.put("makeGreen", customer.getEnvironmentFriendly() == '0' ? "אריזה ירוקה" : "אריזה רגילה");
        String genderStr;
        Character gender;
        if (customer.getGender() != null) {
            gender = customer.getGender();
            switch (customer.getGender()) {
                case 'F':
                    genderStr = "נקבה";
                    break;
                case 'M':
                    genderStr = "זכר";
                    break;
                default:
                    genderStr = "";
                    break;
            }
        } else {
            gender = ' ';
            genderStr = "";

        }
        column.put("gender", gender);
        column.put("genderStr", genderStr);
        boolean attentionFlagBol = U.var.parseInt(customer.getAttentionFlag().ordinal()) > 0 ? true : false;
        column.put("attentionFlag", attentionFlagBol);
        column.put("attentionCause", customer.getAttentionCause());
        column.put("googleMap", "https://maps.google.com/?q=" + U.var.urlEncode(customer.getStreet() + " " + customer.getStreetNo() + " " + customer.getCity() + " ישראל"));


        Order order = ordersDao.findOneByCustomerIdDesc(customer.getId());
        if (order != null) {
            column.put("creditCardOwner", (order.getClearingCardHolder() == null ? "CardOwner" : order.getClearingCardHolder()));
            column.put("creditCardNo", order.getClearingFourDigits());
            column.put("creditCardValidity", order.getClearingMonth() + "/" + order.getClearingYear());
            column.put("creditCardType", order.getCardTypeString() + " / " + order.getCardMutagString());

            column.put("shipping_street", order.getStreet() + " " + order.getStreetNo());
            column.put("shipping_house_number", order.getApartment());
            column.put("shipping_city", order.getCity());
        }

        List<Map<String, Object>> orders = new LinkedList<>();
        List<Order> customerOrders = ordersDao.findAllByCustomerIdDesc(id);
        for (Order customerOrder : customerOrders) {
            Map<String, Object> orderColumn = new HashMap<>();
            orderColumn.put("id", customerOrder.getId());
            orderColumn.put("wpId", customerOrder.getWpId());
            orderColumn.put("dhlPackageNumber", customerOrder.getDhlPackageNumber());
            orderColumn.put("createdDate", new SimpleDateFormat("dd/MM/yyyy").format(customerOrder.getCreatedDate()));
            String lastPackingTimeStamp = " ";
            if (customerOrder.getLastPackingTimestamp() != null) {
                lastPackingTimeStamp = new SimpleDateFormat("dd/MM/yyyy").format(customerOrder.getLastPackingTimestamp());
            }
            orderColumn.put("lastPackingTimestamp", lastPackingTimeStamp);
            Date deliveryDate = customerOrder.getDeliveryDate();
            if (deliveryDate == null) {
                orderColumn.put("deliveryDate", " ");
            } else {
                if (order.getDeliveryWindow() == 1) {
                    deliveryDate = new Date(deliveryDate.getTime() - 24*60*60*1000);
                }
                System.out.format("   ### Order: %d   %s%n", customerOrder.getId(), new SimpleDateFormat("dd/MM/yyyy").format(deliveryDate));
                orderColumn.put("deliveryDate", new SimpleDateFormat("dd/MM/yyyy").format(deliveryDate));
            }
            orderColumn.put("deliveryWindow", customerOrder.getDeliveryWindow());
            orderColumn.put("boxes", customerOrder.getBoxes());
            orderColumn.put("sum", customerOrder.getSum());
            orderColumn.put("status", customerOrder.getStatus().heb);
            orderColumn.put("clearingStatus", customerOrder.getClearingStatus().heb);
            orders.add(orderColumn);
        }

        model.addAttribute("orders", orders);

        List<Coupon> coupons = couponsDao.findAllByCustomerIdDesc(id);
        model.addAttribute("coupons", coupons);

//        column.put("creditCardOwner", (lastOrder.getClearingCardHolder() != null ? lastOrder.getClearingCardHolder() : "CardOwner"));


//
//        column.put("billing_street", customer.getBilling_street());
//        column.put("billing_city", customer.getBilling_city());
//        column.put("billing_zip", customer.getBilling_zip());


        model.addAttribute("column", column);
        Map<String, Object> globals = new HashMap<>();
        globals.put("modules_enable_coupons", globalsDao.findOne("modules_enable_coupons").getValue());
        globals.put("modules_enable_invoice", globalsDao.findOne("modules_enable_invoice").getValue());
        globals.put("modules_enable_logistics", globalsDao.findOne("modules_enable_logistics").getValue());
        globals.put("modules_enable_packing", globalsDao.findOne("modules_enable_packing").getValue());
        Object logedWorker = request.getSession().getAttribute("userId");
        Worker worker = workersDao.getOne(Integer.parseInt(logedWorker.toString()));
        globals.put("worker_name", worker.getFirstName() + " " + worker.getLastName());
        model.addAttribute("globals", globals);
        model.addAttribute("customer_support", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.CUSTOMER_SUPPORT.ordinal() ? 1 : 0);
        model.addAttribute("admin", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.ADMIN.ordinal() ? 1 : 0);


        return "Customer";
    }

    @RequestMapping(value = "/admin/customers/customer/edit", method = RequestMethod.GET)
    public Object customerEdit(@RequestParam(value = "id") Integer id, Model model, HttpServletRequest request) {
        if (!U.var.parseBoolean(request.getSession().getAttribute("login"))) {
            return "redirect:/admin/login";
        }
        Customer customer = customersDao.findOne(id);

        Map<String, Object> column = new HashMap<>();
        column.put("id", customer.getId());
        column.put("barcode", customer.getBarcode());
        column.put("createdDate", customer.getCreatedDate());
        column.put("entryCode", customer.getEntryCode());
        column.put("orderCreatedDate", new SimpleDateFormat("dd-MM-yyyy").format(customer.getCreatedDate()));
        column.put("firstName", customer.getFirstName());
        column.put("lastName", customer.getLastName());
        column.put("nekoshook", customer.getCreditPoints());
        column.put("email", customer.getEmail());
        column.put("shipmentAddress", customer.getCity() + " " + customer.getStreet() + " ");
        column.put("city", customer.getCity());
        column.put("apartment", customer.getApartment());
        column.put("streetNo", customer.getStreetNo());
        column.put("floorNo", customer.getFloorNo());
        column.put("street", customer.getStreet());
        column.put("phone", customer.getPhone());
        column.put("phone2", customer.getPhone2());
        column.put("remarks", customer.getRemarks());
        column.put("creditPoints", customer.getCreditPoints());
        column.put("attentionFlag", customer.getAttentionFlag().ordinal());
        column.put("attentionCause", customer.getAttentionCause());

        model.addAttribute("column", column);
        Map<String, Object> globals = new HashMap<>();
        globals.put("modules_enable_coupons", globalsDao.findOne("modules_enable_coupons").getValue());
        globals.put("modules_enable_invoice", globalsDao.findOne("modules_enable_invoice").getValue());
        globals.put("modules_enable_logistics", globalsDao.findOne("modules_enable_logistics").getValue());
        globals.put("modules_enable_packing", globalsDao.findOne("modules_enable_packing").getValue());
        Object logedWorker = request.getSession().getAttribute("userId");
        Worker worker = workersDao.getOne(Integer.parseInt(logedWorker.toString()));
        globals.put("worker_name", worker.getFirstName() + " " + worker.getLastName());
        model.addAttribute("globals", globals);
        model.addAttribute("admin", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.ADMIN.ordinal() ? 1 : 0);


        return "CustomerEdit";
    }

    @RequestMapping(value = "/admin/customers/customer/submitEdit", method = RequestMethod.POST)
    public Object customerEditSubmit(@RequestParam(value = "id") Integer id,
                                            @RequestParam(value = "firstName") String firstName,
                                            @RequestParam(value = "lastName") String lastName,
                                            @RequestParam(value = "email") String email,
                                            @RequestParam(value = "phone") String phone,
                                            @RequestParam(value = "phone2") String phone2,
                                            @RequestParam(value = "street") String street,
                                            @RequestParam(value = "city") String city,
                                            @RequestParam(value = "streetNo") String streetNo,
                                            @RequestParam(value = "floorNo") String floorNo,
                                            @RequestParam(value = "apartment") String apartment,
                                            @RequestParam(value = "entryCode") String entryCode,
                                            @RequestParam(value = "remarks") String remarks,
                                            @RequestParam(value = "nekoshook") String nekoshook,
                                            @RequestParam(value = "attentionCause") String attentionCause,
                                            @RequestParam(value = "attentionFlag", required = false) Integer attentionFlag,
                                            HttpServletRequest request) {
        if (!U.var.parseBoolean(request.getSession().getAttribute("login"))) {
            return "redirect:/admin/login";
        }

        Customer customer = customersDao.findOne(id);

        Customer.AlertTypes attentionType = Customer.AlertTypes.values()[attentionFlag];

        if (U.var.parseInt(nekoshook) - U.var.parseInt(pointsDao.sum(id)) != 0) {
            pointsDao.save(new Point(id, (short) (U.var.parseInt(nekoshook) - U.var.parseInt(pointsDao.sum(id))), 'M', 0, new Date(System.currentTimeMillis())));
            customer.setCreditPoints((short) U.var.parseInt(pointsDao.sum(id)));
        }
        //        Customer customer = new Customer();
        customer.setFirstName(firstName);
        customer.setLastName(lastName);
        customer.setEmail(email);
        customer.setCity(city);
        customer.setStreet(street);
        customer.setRemarks(remarks);
        customer.setPhone(phone);
        customer.setPhone2(phone2);

        if (phone.length() > 7) {
            customer.setPhoneNo(phone.substring(phone.length() - 7));
            Character charAt3 = phone.charAt(2);
            if (charAt3.equals('-')) {
                customer.setPhoneExt(phone.substring(0,2));
            } else {
                customer.setPhoneExt(phone.substring(0,3));
            }
        }
        if (phone2.length() > 7) {
            customer.setPhone2No(phone2.substring(phone2.length() - 7));
            Character charAt3 = phone2.charAt(2);
            if (charAt3.equals('-')) {
                customer.setPhoneExt(phone2.substring(0,2));
            } else {
                customer.setPhoneExt(phone2.substring(0,3));
            }
        }

        customer.setApartment(apartment);
        customer.setFloorNo(floorNo);
        customer.setStreetNo(streetNo);
        customer.setEntryCode(entryCode);
        customer.setAttentionFlag(attentionType);
        customer.setAttentionCause(attentionCause);
        customersDao.save(customer);

        wpUsermetaDao.updateByIdAndKey((long) customer.getWpId(), "billing_first_name", firstName);
        wpUsermetaDao.updateByIdAndKey((long) customer.getWpId(), "billing_last_name", lastName);
        wpUsermetaDao.updateByIdAndKey((long) customer.getWpId(), "billing_email", email);
        wpUsermetaDao.updateByIdAndKey((long) customer.getWpId(), "billing_city", city);
        wpUsermetaDao.updateByIdAndKey((long) customer.getWpId(), "billing_street", street);
        wpUsermetaDao.updateByIdAndKey((long) customer.getWpId(), "billing_phone", phone);
        wpUsermetaDao.updateByIdAndKey((long) customer.getWpId(), "billing_phone2", phone2);
        wpUsermetaDao.updateByIdAndKey((long) customer.getWpId(), "billing_entry_code", entryCode);
        wpUsermetaDao.updateByIdAndKey((long) customer.getWpId(), "billing_apartment", apartment);
        wpUsermetaDao.updateByIdAndKey((long) customer.getWpId(), "billing_apartment", apartment);
        wpUsermetaDao.updateByIdAndKey((long) customer.getWpId(), "billing_floor_no", floorNo);
        wpUsermetaDao.updateByIdAndKey((long) customer.getWpId(), "billing_street_no", streetNo);

        logger.info("SB: " + request.getSession().getAttribute("userName") + ": update customer details " + customer.getId());

        return "redirect:/admin/customers/customer?id=" + id;
    }

    @RequestMapping(value = "/admin/customers/submit/hideCard", method = RequestMethod.POST)
    public Object customersSubmitHideCard(@RequestParam(value = "customerId") Integer customerId, HttpServletRequest request) {
        if (!U.var.parseBoolean(request.getSession().getAttribute("login"))) {
            return "redirect:/admin/login";
        }
        Customer customer = customersDao.findOne(customerId);
        customer.setCreditCardHide(customer.getCreditCardHide() == '0' ? '1' : '0');
        customersDao.save(customer);
        return "redirect:/admin/customers/customer?id=" + customerId;
    }

    @RequestMapping(value = "/admin/customers/customer/makeGreen", method = RequestMethod.GET)
    public Object customersMakeGreen(@RequestParam(value = "id") Integer id, HttpServletRequest request) {
        if (!U.var.parseBoolean(request.getSession().getAttribute("login"))) {
            return "redirect:/admin/login";
        }
        Customer customer = customersDao.findOne(id);
        customer.setEnvironmentFriendly(customer.getEnvironmentFriendly() == '0' ? '1' : '0');
        customersDao.save(customer);
        return "redirect:/admin/customers/customer?id=" + id;
    }

    @RequestMapping(value = "/admin/customers/removeCustomer", method = RequestMethod.GET)
    public Object customerRemove(@RequestParam(value = "id") Integer id, Model model, HttpServletRequest request) {

        Customer customer = customersDao.findOne(id);

        customer.setIsDeleted('1');
        customersDao.save(customer);
        logger.info("SB: " + request.getSession().getAttribute("userName") + ": remove customer " + customer.getId());

        return "redirect:/admin/customers";
    }


    @RequestMapping(value = "/admin/printPendingOrdersQRCodes", method = RequestMethod.GET)
    public Object printPendingOrdersQRCodes(Model model, HttpServletRequest request) {
        if (!U.var.parseBoolean(request.getSession().getAttribute("login"))) {
            return "redirect:/admin/login";
        }
        List<Map<String, Object>> orders = new LinkedList<>();
        int i = 0;

        if (U.var.parseInt(globalsDao.findOne("packing_print_pending_order").getValue()) == 0) {
            for (Order order : ordersDao.findAllEveningPendingOrderReadyToDhlOrdered()) {
                for (int j = 0; j < 2; j++) {
                    Map<String, Object> orderDetails = new HashMap<>();
                    Customer customer = customersDao.findOne(order.getCustomerId());
                    Integer rule = U.var.parseInt(customer.getEnvironmentFriendly());
                    ClubOrder clubOrder = clubOrderDao.findOneByWpOrderId(order.getWpId());
                    if (clubOrder != null) {
                        rule = 3;
                    }
                    orderDetails.put("orderNumber", common.createOrderCode(order.getId()));
                    orderDetails.put("index", i++);
                    String barcodeStr = String.format("%011d", order.getWpId());
                    orderDetails.put("barcode", order.getBarcode());
                    orderDetails.put("barcodeStr", barcodeStr);
                    orderDetails.put("dhl_number", order.getDhlPackageNumber());
                    orderDetails.put("customerName", order.getFirstName() + " " + order.getLastName());
                    orderDetails.put("street", order.getStreet() + " " + order.getStreetNo());
                    orderDetails.put("floor", order.getFloorNo());
                    orderDetails.put("city", order.getCity());
                    orderDetails.put("apartment", order.getApartment());
                    orderDetails.put("floor", order.getFloorNo());
                    orderDetails.put("house_number", order.getStreetNo());
                    orderDetails.put("phones", customer.getPhone() + (customer.getPhone2() != null ? "   " + customer.getPhone2() : ""));
                    orderDetails.put("boxes", "" + (j + 1) + "/" + order.getBoxes());
                    orderDetails.put("customerNumber", customer.getBarcode());
                    orderDetails.put("green", rule);

                    orders.add(orderDetails);
                }
            }

            for (Order order : ordersDao.findAllMorningPendingOrderReadyToDhlOrderedDesc()) {
                for (int j = 0; j < 2; j++) {
                    Map<String, Object> orderDetails = new HashMap<>();
                    Customer customer = customersDao.findOne(order.getCustomerId());
                    orderDetails.put("orderNumber", common.createOrderCode(order.getId()));
                    orderDetails.put("index", i++);
                    String barcodeStr = String.format("%011d", order.getWpId());
                    orderDetails.put("barcode", order.getBarcode());
                    orderDetails.put("barcodeStr", barcodeStr);
                    orderDetails.put("dhl_number", order.getDhlPackageNumber());
                    orderDetails.put("customerName", order.getFirstName() + " " + order.getLastName());
                    orderDetails.put("street", order.getStreet() + " " + order.getStreetNo());
                    orderDetails.put("floor", order.getFloorNo());
                    orderDetails.put("city", order.getCity());
                    orderDetails.put("apartment", order.getApartment());
                    orderDetails.put("floor", order.getFloorNo());
                    orderDetails.put("house_number", order.getStreetNo());
                    orderDetails.put("phones", customer.getPhone() + (customer.getPhone2() != null ? "   " + customer.getPhone2() : ""));
                    orderDetails.put("boxes", "" + (j + 1) + "/" + order.getBoxes());
                    orderDetails.put("customerNumber", customer.getBarcode());
                    orderDetails.put("green", U.var.parseInt(customer.getEnvironmentFriendly()));

                    orders.add(orderDetails);
                }
            }
        } else {
            System.out.format("   # Shuffling prefix %n");
            List<String> prefixes = ordersDao.findAllPendingOrderPrifix(1); // Get Evening first
            Collections.shuffle(prefixes);
            for (String prefix : prefixes) {
                System.out.format("   # prefix: %s %n", prefix);
                for (Order order : ordersDao.findAllPendingOrderByPrefix(prefix + '%')) {
                    for (int j = 0; j < 2; j++) {
                        Map<String, Object> orderDetails = new HashMap<>();
                        Customer customer = customersDao.findOne(order.getCustomerId());
                        orderDetails.put("orderNumber", common.createOrderCode(order.getId()));
                        orderDetails.put("index", i++);
                        String barcodeStr = String.format("%011d", order.getWpId());
                        orderDetails.put("barcode", order.getBarcode());
                        orderDetails.put("barcodeStr", barcodeStr);
                        orderDetails.put("dhl_number", order.getDhlPackageNumber());
                        orderDetails.put("customerName", order.getFirstName() + " " + order.getLastName());
                        orderDetails.put("street", order.getStreet() + " " + order.getStreetNo());
                        orderDetails.put("floor", order.getFloorNo());
                        orderDetails.put("city", order.getCity());
                        orderDetails.put("apartment", order.getApartment());
                        orderDetails.put("floor", order.getFloorNo());
                        orderDetails.put("house_number", order.getStreetNo());
                        orderDetails.put("phones", customer.getPhone() + (customer.getPhone2() != null ? "   " + customer.getPhone2() : ""));
                        orderDetails.put("boxes", "" + (j + 1) + "/" + order.getBoxes());
                        orderDetails.put("customerNumber", customer.getBarcode());
                        orderDetails.put("green", U.var.parseInt(customer.getEnvironmentFriendly()));

                        orders.add(orderDetails);
                    }
                }
            }

            prefixes = ordersDao.findAllPendingOrderPrifix(0); // Get Evening first
            Collections.shuffle(prefixes);
            for (String prefix : prefixes) {
                System.out.format("   # prefix: %s %n", prefix);
                for (Order order : ordersDao.findAllPendingOrderByPrefix(prefix + '%')) {
                    for (int j = 0; j < 2; j++) {
                        Map<String, Object> orderDetails = new HashMap<>();
                        Customer customer = customersDao.findOne(order.getCustomerId());
                        orderDetails.put("orderNumber", common.createOrderCode(order.getId()));
                        orderDetails.put("index", i++);
                        String barcodeStr = String.format("%011d", order.getWpId());
                        orderDetails.put("barcode", order.getBarcode());
                        orderDetails.put("barcodeStr", barcodeStr);
                        orderDetails.put("dhl_number", order.getDhlPackageNumber());
                        orderDetails.put("customerName", order.getFirstName() + " " + order.getLastName());
                        orderDetails.put("street", order.getStreet() + " " + order.getStreetNo());
                        orderDetails.put("floor", order.getFloorNo());
                        orderDetails.put("city", order.getCity());
                        orderDetails.put("apartment", order.getApartment());
                        orderDetails.put("floor", order.getFloorNo());
                        orderDetails.put("house_number", order.getStreetNo());
                        orderDetails.put("phones", customer.getPhone() + (customer.getPhone2() != null ? "   " + customer.getPhone2() : ""));
                        orderDetails.put("boxes", "" + (j + 1) + "/" + order.getBoxes());
                        orderDetails.put("customerNumber", customer.getBarcode());
                        orderDetails.put("green", U.var.parseInt(customer.getEnvironmentFriendly()));

                        orders.add(orderDetails);
                    }
                }
            }
        }
/*        if (U.var.parseInt(globalsDao.findOne("packing_print_pending_order").getValue()) == 0) {
            for (Order order : ordersDao.findAllPendingOrderReadyToDhlOrderedDesc()) {
                for (int j = 0; j < 2; j++) {
                    Map<String, Object> orderDetails = new HashMap<>();
                    Customer customer = customersDao.findOne(order.getCustomerId());
                    orderDetails.put("orderNumber", common.createOrderCode(order.getId()));
                    orderDetails.put("index", i++);
                    String barcodeStr = String.format("%011d", order.getWpId());
                    orderDetails.put("barcode", order.getBarcode());
                    orderDetails.put("barcodeStr", barcodeStr);
                    orderDetails.put("dhl_number", order.getDhlPackageNumber());
                    orderDetails.put("customerName", order.getFirstName() + " " + order.getLastName());
                    orderDetails.put("street", order.getStreet() + " " + order.getStreetNo());
                    orderDetails.put("floor", order.getFloorNo());
                    orderDetails.put("city", order.getCity());
                    orderDetails.put("apartment", order.getApartment());
                    orderDetails.put("floor", order.getFloorNo());
                    orderDetails.put("house_number", order.getStreetNo());
                    orderDetails.put("phones", customer.getPhone() + (customer.getPhone2() != null ? "   " + customer.getPhone2() : ""));
                    orderDetails.put("boxes", "" + (j + 1) + "/" + order.getBoxes());
                    orderDetails.put("customerNumber", customer.getBarcode());

                    orders.add(orderDetails);
                }
            }
        } else {
            System.out.format("   # Shuffling prefix %n");
            List<String> prefixes = ordersDao.findAllPendingOrderPrifix(0); // Get Morning first
            Collections.shuffle(prefixes);
            for (String prefix : prefixes) {
                System.out.format("   # prefix: %s %n", prefix);
                for (Order order : ordersDao.findAllPendingOrderByPrifix(prefix + '%')) {
                    for (int j = 0; j < 2; j++) {
                        Map<String, Object> orderDetails = new HashMap<>();
                        Customer customer = customersDao.findOne(order.getCustomerId());
                        orderDetails.put("orderNumber", common.createOrderCode(order.getId()));
                        orderDetails.put("index", i++);
                        String barcodeStr = String.format("%011d", order.getWpId());
                        orderDetails.put("barcode", order.getBarcode());
                        orderDetails.put("barcodeStr", barcodeStr);
                        orderDetails.put("dhl_number", order.getDhlPackageNumber());
                        orderDetails.put("customerName", order.getFirstName() + " " + order.getLastName());
                        orderDetails.put("street", order.getStreet() + " " + order.getStreetNo());
                        orderDetails.put("floor", order.getFloorNo());
                        orderDetails.put("city", order.getCity());
                        orderDetails.put("apartment", order.getApartment());
                        orderDetails.put("floor", order.getFloorNo());
                        orderDetails.put("house_number", order.getStreetNo());
                        orderDetails.put("phones", customer.getPhone() + (customer.getPhone2() != null ? "   " + customer.getPhone2() : ""));
                        orderDetails.put("boxes", "" + (j + 1) + "/" + order.getBoxes());
                        orderDetails.put("customerNumber", customer.getBarcode());

                        orders.add(orderDetails);
                    }
                }
            }
        } */

        model.addAttribute("orders", orders);
        model.addAttribute("QRcodeStyle", globalsDao.findOne("app_barcode_style").getValue());
        model.addAttribute("admin", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.ADMIN.ordinal() ? 1 : 0);

        return "PrintPendingOrdersQRCodes";
    }

    //Sending CSV for spoecific email
    @RequestMapping(value = "/admin/sendCsv", method = RequestMethod.GET)
    public Object sendCsv(Model model, HttpServletRequest request) {
        //        FileWriter fileWriter = null;
        if (!U.var.parseBoolean(request.getSession().getAttribute("login"))) {
            return "redirect:/admin/login";
        }

        String NEW_LINE_SEPARATOR = "\n";
        String COMMA_DELIMITER = ",";
        Writer fileWriter = null;
        try {
            File fileDir = new File("dhl.csv");
            fileWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileDir), "UTF-8"));
            //            fileWriter = new FileWriter("dhl.csv");
            fileWriter.append("מספר דרישה,תאריך אספקה,סוג דרישה,סוג הזמנה,עדיפות,מסלול,ס. תכנון,ס. הובלה,מרכז,נקוד,קוד חברה,מספר לקוח,סוג לקוח,שם לקוח,קבוצת לקוח,קוד אתר,שם אתר,שם איש קשר 2,רחוב,מספר בית,הערות,עיר,טלפון 1,טלפון 2,שמן שירות,מספריחידות,משטחים,משקל,נפח,ערך,הערות,הערות לקוח,הערות הזמנה");
            fileWriter.append(NEW_LINE_SEPARATOR);
            List<Map<String, Object>> orders = new LinkedList<>();
            int i = 0;

            Map<Integer, Customer> customerMap = new HashMap<>();
            for (Customer customer : customersDao.findAll()) {
                customerMap.put(customer.getId(), customer);
            }

            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

            for (Order order : ordersDao.findAllHarvestingOrder()) {
                Customer customer = customerMap.get(order.getCustomerId());
                fileWriter.append(order.getId().toString());
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append(dateFormat.format(new Date(System.currentTimeMillis() + 1000L * 60L * 60L * 48L)));
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append("D");
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append("CUST");
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append("1");
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append("");
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append("");
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append("");
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append("DGF");
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append("");
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append("DGF");
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append(customer.getBarcode());
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append("Customer");
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append(customer.getLastName() + " " + customer.getFirstName());
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append("");
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append(customer.getBarcode());
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append(customer.getLastName() + " " + customer.getFirstName());
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append("");
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append(order.getStreet() + " " + order.getStreetNo());
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append(order.getApartment());
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append(order.getEntryCode());
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append(order.getCity());
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append(customer.getPhone());
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append(customer.getPhone2());
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append("10");
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append("1");
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append("1");
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append("1");
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append("1");
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append("1");
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append("");
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append(customer.getRemarks());
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append(order.getRemarks());
                fileWriter.append(NEW_LINE_SEPARATOR);

            }
            System.out.println("CSV file was created successfully !!!");

        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            e.printStackTrace();
        } finally {
            try {
                fileWriter.flush();
                fileWriter.close();
            } catch (IOException e) {
                System.out.println("Error while flushing/closing fileWriter !!!");
                e.printStackTrace();
            }
        }
        try {
 /*           EmailAttachment attachment = new EmailAttachment();
            attachment.setPath("dhl.csv");
            attachment.setDisposition(EmailAttachment.ATTACHMENT);
            attachment.setDescription("DHL csv");
            attachment.setName("DHL csv");
            // Create the email message
            MultiPartEmail email = new MultiPartEmail();
//            email.setHostName("ip-172-31-24-236.eu-central-1.compute.internal");
            email.addTo(globalsDao.findEmail().getValue(), "Shookbook");
//            email.setFrom(globalsDao.findEmailFrom().getValue(), "Shookbook Server");
            email.setSubject("DHL CSV");
            email.setMsg("DHL csv attached below");

            // add the attachment
            email.attach(attachment);

            // send the email
            email.send();
*/

            //Default email address if not exist
            String to = globalsDao.findEmail().getValue();
            //            String from = "root@localhost";
            Properties properties = System.getProperties();


            Session session = Session.getDefaultInstance(properties);

            // Create a default MimeMessage object.
            MimeMessage message = new MimeMessage(session);

            // Set From: header field of the header.
            //            message.setFrom(new InternetAddress(from));

            // Set To: header field of the header.
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

            // Set Subject: header field
            message.setSubject("CSV file to DHL");

            // Create the message part
            BodyPart messageBodyPart = new MimeBodyPart();

            // Fill the message
            messageBodyPart.setText("CSV attached below");

            // Create a multipar message
            Multipart multipart = new MimeMultipart();

            // Set text message part
            multipart.addBodyPart(messageBodyPart);

            // Part two is attachment
            messageBodyPart = new MimeBodyPart();
            String filename = "dhl.csv";
            DataSource source = new FileDataSource(filename);
            messageBodyPart.setDataHandler(new DataHandler(source));
            messageBodyPart.setFileName(filename);
            multipart.addBodyPart(messageBodyPart);

            // Send the complete message parts
            message.setContent(multipart);

            // Send message
            Transport.send(message);

            System.out.println("Sent message successfully....");
        } catch (MessagingException mex) {
            mex.printStackTrace();
        }

        return "redirect:/admin/customers/orders";
    }


    //Sending CSV for spoecific email
    @RequestMapping(value = "/admin/olsSendCsv", method = RequestMethod.GET)
    public Object oldSendCsv(Model model, HttpServletRequest request) {
        //        FileWriter fileWriter = null;
        if (!U.var.parseBoolean(request.getSession().getAttribute("login"))) {
            return "redirect:/admin/login";
        }

        String NEW_LINE_SEPARATOR = "\n";
        String COMMA_DELIMITER = ",";
        Writer fileWriter = null;
        try {
            File fileDir = new File("dhl.csv");
            fileWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileDir), "UTF-8"));
            //            fileWriter = new FileWriter("dhl.csv");
            fileWriter.append("מספר מזהה, מספר חבר,טלפון נוסף, טלפון,קוד שער,דירה,ישוב,רחוב,שם משפחה,שם פרטי,מספר הזמנה");
            fileWriter.append(NEW_LINE_SEPARATOR);
            List<Map<String, Object>> orders = new LinkedList<>();
            int i = 0;
            for (Order order : ordersDao.findAllHarvestingOrder()) {
                Customer customer = customersDao.findOne(order.getCustomerId());
                fileWriter.append(order.getId().toString());
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append(customer.getBarcode());
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append(customer.getPhone2Ext() + customer.getPhone2No());
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append(customer.getPhoneExt() + customer.getPhoneNo());
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append(order.getEntryCode());
                fileWriter.append(COMMA_DELIMITER);
                if (!U.var.isEmptyStr(order.getApartment())) {
                    fileWriter.append("ד. " + order.getApartment() + " ");
                }
                if (!U.var.isEmptyStr(order.getFloorNo())) {
                    fileWriter.append("ק. " + order.getFloorNo());
                }
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append(order.getCity());
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append(order.getStreet() + " " + order.getStreetNo());
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append(order.getLastName());
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append(order.getFirstName());
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append(common.createOrderCode(order.getId()));
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append(NEW_LINE_SEPARATOR);


            }
            System.out.println("CSV file was created successfully !!!");

        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            e.printStackTrace();
        } finally {
            try {
                fileWriter.flush();
                fileWriter.close();
            } catch (IOException e) {
                System.out.println("Error while flushing/closing fileWriter !!!");
                e.printStackTrace();
            }
        }
        try {
 /*           EmailAttachment attachment = new EmailAttachment();
            attachment.setPath("dhl.csv");
            attachment.setDisposition(EmailAttachment.ATTACHMENT);
            attachment.setDescription("DHL csv");
            attachment.setName("DHL csv");
            // Create the email message
            MultiPartEmail email = new MultiPartEmail();
//            email.setHostName("ip-172-31-24-236.eu-central-1.compute.internal");
            email.addTo(globalsDao.findEmail().getValue(), "Shookbook");
//            email.setFrom(globalsDao.findEmailFrom().getValue(), "Shookbook Server");
            email.setSubject("DHL CSV");
            email.setMsg("DHL csv attached below");

            // add the attachment
            email.attach(attachment);

            // send the email
            email.send();
*/

            //Default email address if not exist
            String to = globalsDao.findEmail().getValue();
            //            String from = "root@localhost";
            Properties properties = System.getProperties();


            Session session = Session.getDefaultInstance(properties);

            // Create a default MimeMessage object.
            MimeMessage message = new MimeMessage(session);

            // Set From: header field of the header.
            //            message.setFrom(new InternetAddress(from));

            // Set To: header field of the header.
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

            // Set Subject: header field
            message.setSubject("CSV file to DHL");

            // Create the message part
            BodyPart messageBodyPart = new MimeBodyPart();

            // Fill the message
            messageBodyPart.setText("CSV attached below");

            // Create a multipar message
            Multipart multipart = new MimeMultipart();

            // Set text message part
            multipart.addBodyPart(messageBodyPart);

            // Part two is attachment
            messageBodyPart = new MimeBodyPart();
            String filename = "dhl.csv";
            DataSource source = new FileDataSource(filename);
            messageBodyPart.setDataHandler(new DataHandler(source));
            messageBodyPart.setFileName(filename);
            multipart.addBodyPart(messageBodyPart);

            // Send the complete message parts
            message.setContent(multipart);

            // Send message
            Transport.send(message);

            System.out.println("Sent message successfully....");
        } catch (MessagingException mex) {
            mex.printStackTrace();
        }
        //        catch (EmailException e)
        //        {
        //            e.printStackTrace();
        //        }


        return "redirect:/admin/customers/orders";
    }

    private List<Order> clearOrders = null;
    private List<Order> clearOrdersDone = null;
    private List<Order> clearOrdersFailed = null;

    @RequestMapping(value = "/admin/customers/orders/clearOrders", method = RequestMethod.POST)
    public Object clearOrders(@RequestParam(value = "date") String dateString, Model model, HttpServletRequest request) {
        Date date = null;
        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        try {
            date = dateFormat.parse(dateString);
        } catch (ParseException e) {
            try {
                date = dateFormat.parse("01/01/1970");
            } catch (ParseException error) {
                error.printStackTrace();
            }
        }

        List<Order> orders = ordersDao.findAllClearable(date, new Date(date.getTime() + 1000 * 60 * 60 * 24 - 1));
        if (orders.size() > 0) {
            Map<String, Object> column = new HashMap<>();
            column.put("deliveryDate", dateString);
            column.put("numberOfOrders", orders.size());
            model.addAttribute("column", column);

            Map<String, Object> globals = new HashMap<>();
            globals.put("modules_enable_coupons", globalsDao.findOne("modules_enable_coupons").getValue());
            globals.put("modules_enable_invoice", globalsDao.findOne("modules_enable_invoice").getValue());
            globals.put("modules_enable_logistics", globalsDao.findOne("modules_enable_logistics").getValue());
            globals.put("modules_enable_packing", globalsDao.findOne("modules_enable_packing").getValue());
            Object logedWorker = request.getSession().getAttribute("userId");
            Worker worker = workersDao.getOne(Integer.parseInt(logedWorker.toString()));
            globals.put("worker_name", worker.getFirstName() + " " + worker.getLastName());
            model.addAttribute("globals", globals);
            return "ClearOrders";
        } else {
            return "redirect:/admin/customers/orders";
        }

    }

    @RequestMapping(value = "/admin/customers/orders/sendInvoice", method = RequestMethod.POST)
    public Object sendInvoice(@RequestParam(value = "orderId") Integer id, Model model, HttpServletRequest request) {

        sendInvoice(id, request.getSession().getAttribute("userName"));

        Map<String, Object> globals = new HashMap<>();
        globals.put("modules_enable_coupons", globalsDao.findOne("modules_enable_coupons").getValue());
        globals.put("modules_enable_invoice", globalsDao.findOne("modules_enable_invoice").getValue());
        globals.put("modules_enable_logistics", globalsDao.findOne("modules_enable_logistics").getValue());
        globals.put("modules_enable_packing", globalsDao.findOne("modules_enable_packing").getValue());
        Object logedWorker = request.getSession().getAttribute("userId");
        Worker worker = workersDao.getOne(Integer.parseInt(logedWorker.toString()));
        globals.put("worker_name", worker.getFirstName() + " " + worker.getLastName());
        model.addAttribute("globals", globals);
        return "redirect:/admin/customers/orders/order?id=" + id;
    }

    @RequestMapping(value = "/admin/customers/orders/clearAll", method = RequestMethod.GET)
    public Object clearAll(@RequestParam(value = "date") String dateString, Model model, HttpServletRequest request) throws Exception {
        Date date = new SimpleDateFormat("MM/dd/yyyy").parse(dateString);
        synchronized (this) {
            if (clearOrders == null) {
                clearOrders = new LinkedList<>(ordersDao.findAllClearable(date, new Date(date.getTime() + 1000 * 60 * 60 * 24 - 1)));
                clearOrdersDone = new LinkedList<>();
                clearOrdersFailed = new LinkedList<>();
            }

            if (clearOrders.isEmpty()) {
                clearOrders = null;
                clearOrdersDone = null;
                clearOrdersFailed = null;
                model.addAttribute("done", true);
            } else {
                Order order = clearOrders.remove(0);
                Integer invoiceOnly = U.var.parseInt(customersDao.findOne(order.getCustomerId()).getInvoiceOnly());
                if (invoiceOnly == 0) {
                   if ("BusinessConfirmationPage".equals(clear(order.getId(), request.getSession().getAttribute("userName")))) {
                       Thread.sleep(1000);
                       order.setExtra("בוצע");
                       clearOrdersDone.add(order);
                   } else {
                       order.setExtra("נכשל");
                       clearOrdersFailed.add(order);
                   }
                } else {
                    if ("true".equals(sendInvoice(order.getId(), request.getSession().getAttribute("userName")))) {
                        Thread.sleep(1000);
                        order.setExtra("בוצע");
                        clearOrdersDone.add(order);
                    } else {
                        order.setExtra("נכשל");
                        clearOrdersFailed.add(order);
                    }
                }

                List<Order> ordersDone = new LinkedList<>(clearOrders);
                List<Order> ordersFailed = new LinkedList<>(clearOrders);
                ordersDone.addAll(clearOrdersDone);
                ordersFailed.addAll(clearOrdersFailed);
                model.addAttribute("ordersDone", ordersDone);
                model.addAttribute("ordersFailed", ordersFailed);
                if (clearOrders.isEmpty()) {
                    clearOrders = null;
                    clearOrdersDone = null;
                    model.addAttribute("done", true);
                } else {
                    model.addAttribute("done", false);
                }
            }
            return "ClearAll";
        }
    }

    @RequestMapping(value = "/admin/customers/creditProblem", method = RequestMethod.GET)
    public Object creditProblem(@RequestParam(value = "orderId") Integer orderId, Model model, HttpServletRequest request) {
        Order order = ordersDao.findOne(orderId);
        Date deliveryDate = order.getDeliveryDate();
        if (order.getDeliveryWindow() == 1) {
            deliveryDate = new Date(deliveryDate.getTime() - 24*60*60*1000);
        }
        Map<String, Object> column = new HashMap<>();
        column.put("firstName", customersDao.findOne(order.getCustomerId()).getFirstName());
        column.put("orderId", order.getWpId());
        column.put("deliveryDate", new SimpleDateFormat("dd/MM").format(deliveryDate));
        model.addAttribute("column", column);
        return "CreditProblem";
    }

    private final DateFormat deliveryDateFormat = new SimpleDateFormat("dd/MM/yyyy");

    private String clear(int id, Object admin) {
        logger.info("SB: " + admin + ": clear order " + id);
        Order order = ordersDao.findOne(id);
        int customerId = order.getCustomerId();
        Customer customer = customersDao.findOne(customerId);
        StringBuilder parameters = new StringBuilder();
        if ((order.getStatus() != Order.OrderStatuses.SHIPMENT) && (order.getStatus() != Order.OrderStatuses.DELIVERED) && (order.getStatus() != Order.OrderStatuses.CLOSED)) {
            return "OrderStatusMessage";
        }
        if (order.getClearingStatus() != Order.ClearStatus.UNCLEAR) {
            return "ClearanceMessage";
        }

        float originalSum = common.calcTotalOriginalSumWithoutShipmentFee(order.getId());
        float sum = common.calcTotalSumWithoutShipmentFee(order.getId());

        parameters.append("codepage").append('=').append(U.var.urlEncode(globalsDao.findOne("cardcom_code_page").getValue())).append('&');
        parameters.append("terminalnumber").append('=').append(U.var.urlEncode(globalsDao.findOne("cardcom_terminal_number").getValue())).append('&');
        parameters.append("username").append('=').append(U.var.urlEncode(globalsDao.findOne("cardcom_username").getValue())).append('&');
        parameters.append("TokenToCharge.Token").append('=').append(U.var.urlEncode(order.getClearingToken())).append('&');
        parameters.append("TokenToCharge.CardValidityMonth").append('=').append(U.var.urlEncode(order.getClearingMonth())).append('&');
        //  parameters.append("TokenToCharge.UserPassword").append('=').append(U.var.urlEncode(globalsDao.findOne("cardcom_password").getValue() != null ? globalsDao.findOne("cardcom_password").getValue() : "aSD34SDF-1asd")).append('&');
        parameters.append("TokenToCharge.CardValidityYear").append('=').append(U.var.urlEncode(order.getClearingYear())).append('&');


        if (customer.getType().equals(Customer.CustomerTypes.GOLD) || customer.getType().equals(Customer.CustomerTypes.PLATINUM) || originalSum >= Integer.parseInt(globalsDao.findOne("delivery_price_min_for_reduced").getValue())) {
            sum = sum - order.getDiscountSum() + Integer.parseInt(globalsDao.findOne("delivery_price_reduced").getValue());
        } else {
            sum = sum - order.getDiscountSum() + Integer.parseInt(globalsDao.findOne("delivery_price_standard").getValue());
        }
        parameters.append("TokenToCharge.SumToBill").append('=').append(U.var.urlEncode("" + (Math.round(sum * 100.0) / 100.0))).append('&');


        //        parameters.append("TokenToCharge.SumToBill").append('=').append(U.var.urlEncode(order.getSum()+"")).append('&');
        parameters.append("TokenToCharge.ApprovalNumber").append('=').append(U.var.urlEncode("0")).append('&');
        parameters.append("TokenToCharge.IdentityNumber").append('=').append(U.var.urlEncode(order.getClearingCustomerId() + "")).append('&');

        parameters.append("InvoiceHead.CustName").append('=').append(U.var.urlEncode((customer.getFirstName() + " " + customer.getLastName() != null ? customer.getFirstName() + " " + customer.getLastName() : "N/A"))).append('&');
        parameters.append("InvoiceHead.ExtIsVatFree").append('=').append(false).append('&');
        parameters.append("InvoiceHead.SendByEmail").append('=').append(U.var.urlEncode("true")).append('&');
        parameters.append("InvoiceHead.Language").append('=').append(U.var.urlEncode("he")).append('&');
        parameters.append("InvoiceHead.CustAddresLine1").append('=').append(U.var.urlEncode(customer.getStreet() + " " + customer.getStreetNo())).append('&');
//        parameters.append("InvoiceHead.CustAddresLine2").append('=').append(U.var.urlEncode(customer.getZip())).append('&');
        parameters.append("InvoiceHead.Email").append('=').append(U.var.urlEncode(customer.getEmail())).append('&');
        //parameters.append("InvoiceHead.Email").append('=').append(U.var.urlEncode("Morchuka@gmail.com")).append('&');
        parameters.append("InvoiceHead.CustCity").append('=').append(U.var.urlEncode(customer.getCity())).append('&');
        parameters.append("InvoiceHead.CustLinePH").append('=').append(U.var.urlEncode(customer.getPhone2() != null ? customer.getPhone2() : "---")).append('&');
        parameters.append("InvoiceHead.CustMobilePH").append('=').append(U.var.urlEncode(customer.getPhone() != null ? customer.getPhone() : "---")).append('&');
        parameters.append("InvoiceHead.Comments").append('=').append(U.var.urlEncode(String.format("חשבונית עבור הזמנה מספר %s שהתקבלה בתאריך %s", "" + order.getWpId(), deliveryDateFormat.format(order.getDeliveryDate())))).append('&');


        //Product List:
        Set<Integer> productIds = new HashSetIgnoresNulls<>();
        Map<Integer, Product> productsMap = new HashMap<>();
        for (OrderProduct orderProduct : orderProductDao.findAllByOrderId(id)) {
            productIds.add(orderProduct.getProductId());
        }
        if (!productIds.isEmpty()) {
            for (Product product : productsDao.findAll(productIds)) {
                productsMap.put(product.getId(), product);

            }
        }
        int i = 0;
        for (OrderProduct orderProduct : orderProductDao.findAllByOrderId(id)) {
            parameters.append("InvoiceLines" + (i != 0 ? i : "") + ".Description").append('=').append(U.var.urlEncode(productsMap.get(orderProduct.getProductId()).getNameHeb() + "")).append('&');
            parameters.append("InvoiceLines" + (i != 0 ? i : "") + ".Price").append('=').append(U.var.urlEncode(orderProduct.getUnitPrice() + "")).append('&');
            parameters.append("InvoiceLines" + (i != 0 ? i : "") + ".Quantity").append('=').append(U.var.urlEncode((int) orderProduct.getQuantity() + "")).append('&');
            parameters.append("InvoiceLines" + (i != 0 ? i : "") + ".IsPriceIncludeVAT").append('=').append(true).append('&');
            parameters.append("InvoiceLines" + (i != 0 ? i : "") + ".IsVatFree").append('=').append(U.var.urlEncode(!productsMap.get(orderProduct.getProductId()).hasVat() + "")).append('&');
            //  System.out.println("need vat :  " + !productsMap.get(orderProduct.getProductId()).hasVat()+"");
            i++;
        }
        if (customer.getType().equals(Customer.CustomerTypes.GOLD) || customer.getType().equals(Customer.CustomerTypes.PLATINUM) || originalSum >= Integer.parseInt(globalsDao.findOne("delivery_price_min_for_reduced").getValue())) {
            parameters.append("InvoiceLines" + (i != 0 ? i : "") + ".Description").append('=').append(U.var.urlEncode("משלוח מוזל")).append('&');
            parameters.append("InvoiceLines" + (i != 0 ? i : "") + ".Price").append('=').append(U.var.urlEncode(globalsDao.findOne("delivery_price_reduced").getValue() + "")).append('&');
        } else {
            parameters.append("InvoiceLines" + (i != 0 ? i : "") + ".Description").append('=').append(U.var.urlEncode("משלוח")).append('&');
            parameters.append("InvoiceLines" + (i != 0 ? i : "") + ".Price").append('=').append(U.var.urlEncode(globalsDao.findOne("delivery_price_standard").getValue() + "")).append('&');
        }

        parameters.append("InvoiceLines" + (i != 0 ? i : "") + ".Quantity").append('=').append(U.var.urlEncode("1")).append('&');
        parameters.append("InvoiceLines" + (i != 0 ? i : "") + ".IsPriceIncludeVAT").append('=').append(true).append('&');
        parameters.append("InvoiceLines" + (i != 0 ? i : "") + ".IsVatFree").append('=').append(false).append('&');

        if (order.getDiscountSum() > 0) {
            i++;
            parameters.append("InvoiceLines" + (i != 0 ? i : "") + ".Description").append('=').append(U.var.urlEncode("הנחה ללא מעמ")).append('&');
            parameters.append("InvoiceLines" + (i != 0 ? i : "") + ".Price").append('=').append(U.var.urlEncode(-order.getDiscountSum() + "")).append('&');
            parameters.append("InvoiceLines" + (i != 0 ? i : "") + ".Quantity").append('=').append(U.var.urlEncode("1")).append('&');
            parameters.append("InvoiceLines" + (i != 0 ? i : "") + ".IsPriceIncludeVAT").append('=').append(false).append('&');
            parameters.append("InvoiceLines" + (i != 0 ? i : "") + ".IsVatFree").append('=').append(true).append('&');
        }

        //        System.out.println(parameters.toString());

        StringBuilder result = new StringBuilder();
        StringBuilder response = new StringBuilder();

        try {
            byte[] parametersBytes = parameters.toString().getBytes();
            URL url = new URL("https://secure.cardcom.co.il/interface/ChargeToken.aspx");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            // conn.setInstanceFollowRedirects( false );
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestProperty("charset", "utf-8");
            conn.setRequestProperty("Content-Length", "" + parametersBytes.length);
            conn.setUseCaches(false);
            conn.getOutputStream().write(parametersBytes);
            Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

            //Checking the result
            boolean flag = true;
            //        for (int c; (c= in.read()) >= 0;) {
            //
            //                str.append((char) c);
            //        }

            for (int c; (c = in.read()) >= 0; ) {
                if (flag && c != '&') {
                    response.append((char) c);
                } else {
                    flag = false;
                }
                result.append((char) c);
            }

        } catch (Exception ex) {
            return "BusinessFailedPage";
        }

        int start = response.indexOf("=") + 1;
        int end = response.length();
        if (response.substring(start, end).equals("0")) {
            order.setClearingStatus(Order.ClearStatus.SUCCEED);
            order.setStatus(Order.OrderStatuses.CLOSED);
            System.out.format("   clearingStutus: %s%n", Order.ClearStatus.SUCCEED.toString());
            order.setSum(sum);
            System.out.format("   Sum: %f%n", sum);

            String[] fields = result.toString().split("&");

            for (int index = 0; index < fields.length; index++) {
                String[] values = fields[index].split("=");
                if (values[0].equals("InternalDealNumber")) {
                    System.out.format("   InternalDealNumber: %d%n", U.var.parseInt(values[1]));
                    order.setClearingDealNumber(U.var.parseInt(values[1]));
                }

                if (values[0].equals("InvoiceResponse.InvoiceNumber")) {
                    System.out.format("   InvoiceNumber: %d%n", U.var.parseInt(values[1]));
                    order.setClearingInvoiceNumber(U.var.parseInt(values[1]));
                }
            }

            float sumWithShipmentWithoutDiscount = sum - order.getDiscountSum();
            float silverPoints = U.var.parseFloat(globalsDao.findOne("silver_points").getValue());
            float goldPoints = U.var.parseFloat(globalsDao.findOne("gold_points").getValue());
            float platinumPoints = U.var.parseFloat(globalsDao.findOne("platinum_points").getValue());
            switch (customer.getType()) {
                case SILVER:
                    pointsDao.save(new Point(customerId, (short) (sumWithShipmentWithoutDiscount / silverPoints), 'B', sumWithShipmentWithoutDiscount, new Date(System.currentTimeMillis())));
                    break;
                case GOLD:
                    pointsDao.save(new Point(customerId, (short) (sumWithShipmentWithoutDiscount / goldPoints), 'B', sumWithShipmentWithoutDiscount, new Date(System.currentTimeMillis())));
                    break;
                case PLATINUM:
                    pointsDao.save(new Point(customerId, (short) (sumWithShipmentWithoutDiscount / platinumPoints), 'B', sumWithShipmentWithoutDiscount, new Date(System.currentTimeMillis())));
                    break;
            }
            customer.setCreditPoints(pointsDao.sum(customerId).shortValue());
            customersDao.save(customer);
            ordersDao.save(order);

            return "BusinessConfirmationPage";
        } else {
            common.sendEmailWithBcc(customer.getEmail(), globalsDao.findOne("credit_problem_email_cc").getValue(), "שוקבוק - בעיית גביה", U.http.stringFromUrl("http://admin.shookbook.co.il/admin/customers/creditProblem?orderId=" + order.getId()));
            order.setClearingStatus(Order.ClearStatus.FAILED);
            ordersDao.save(order);
            return "BusinessFailedPage";
        }
    }

    @RequestMapping(value = "/admin/customers/sendSmsToCustomer", method = RequestMethod.POST)
    public Object sendSmsMsg(@RequestParam(value = "smsCustomerId") Integer id, @RequestParam(value = "msg") String msg, HttpServletRequest request) {
        Object result = sendSms(id, msg, request.getSession().getAttribute("userName"));

        if (U.var.parseInt(result) == 0) {
            return "redirect:/admin/customers/customer?id=" + id;
        } else {
            return "redirect:/admin/customers/customer?id=" + id;
        }

    }

    private Object sendSms(int id, String msg, Object admin) {

        StringBuilder smsUrl = new StringBuilder();

        String smsMsg = "";
        String customerName = customersDao.getOne(id).getFirstName() + " " + customersDao.getOne(id).getLastName();


        try {
            smsMsg = URLEncoder.encode(msg, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        }

        smsUrl.append("http://www.smsender.co.il/sendsms.aspx");
        smsUrl.append("?msg=").append(smsMsg);
        smsUrl.append("&name=").append(globalsDao.findOne("cardcom_sms_username").getValue());
        smsUrl.append("&pass=").append(globalsDao.findOne("cardcom_sms_password").getValue());
        smsUrl.append("&from=").append(globalsDao.findOne("cardcom_sms_from").getValue());
        smsUrl.append("&to=").append(customersDao.getOne(id).getPhone());
        smsUrl.append("&date=").append(deliveryDateFormat.format(new Date()));
        smsUrl.append("&codepage=65001");

        String url = smsUrl.toString();

        System.out.format("   SMS sent to %s%n", customerName);
        Integer result = 400;
        try {
            result = Unirest.get(url).asBinary().getStatus();
        } catch (Exception e) {
            System.out.format("   SMS sender Caught exception: %s%n", customerName);
            e.printStackTrace();
        }

        return result;
    }

    @RequestMapping(value = "/admin/customers/orders/customerClearanceDetails", method = RequestMethod.GET)
    public Object clearanceDetails(@RequestParam(value = "id") Integer id, HttpServletRequest request) {
        return clear(id, request.getSession().getAttribute("userName"));
    }

    @RequestMapping(value = "/admin/customers/coupon", method = RequestMethod.GET)
    public Object coupon(@RequestParam(value = "customerId", required = false) Integer customerId, Model model, HttpServletRequest request) {
        if (!U.var.parseBoolean(request.getSession().getAttribute("login"))) {
            return "redirect:/admin/login";
        }
        Customer customer = customersDao.findOne(customerId);

        Map<String, Object> column = new HashMap<>();
        column.put("customerId", customer.getId());
        column.put("barcode", customer.getBarcode());
        column.put("fullName", customer.getFirstName() + " " + customer.getLastName());
        column.put("firstName", customer.getFirstName());
        column.put("lastName", customer.getLastName());
        column.put("nekoshook", customer.getCreditPoints());
        column.put("email", customer.getEmail());
        column.put("city", customer.getCity());
        column.put("house_number", customer.getStreetNo());
        column.put("street", customer.getStreet());
        column.put("phone", customer.getPhone());
        column.put("phone2", customer.getPhone2());
        column.put("creditPoints", customer.getCreditPoints());

        model.addAttribute("column", column);
        Map<String, Object> globals = new HashMap<>();
        globals.put("modules_enable_coupons", globalsDao.findOne("modules_enable_coupons").getValue());
        globals.put("modules_enable_invoice", globalsDao.findOne("modules_enable_invoice").getValue());
        globals.put("modules_enable_logistics", globalsDao.findOne("modules_enable_logistics").getValue());
        globals.put("modules_enable_packing", globalsDao.findOne("modules_enable_packing").getValue());
        Object logedWorker = request.getSession().getAttribute("userId");
        Worker worker = workersDao.getOne(Integer.parseInt(logedWorker.toString()));
        globals.put("worker_name", worker.getFirstName() + " " + worker.getLastName());
        model.addAttribute("globals", globals);
        model.addAttribute("admin", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.ADMIN.ordinal() ? 1 : 0);

        return "Coupon";
    }

    @RequestMapping(value = "/admin/customers/orders/confirmationPage", method = RequestMethod.GET)
    public Object confirmClearance(@RequestParam(value = "id") Integer id, Model model, HttpServletRequest request) throws Exception {
        return "BusinessConfirmationPage";
    }

    @RequestMapping(value = "/admin/customers/orders/rejectionPage", method = RequestMethod.GET)
    public Object rejectClearance(@RequestParam(value = "id") Integer id, Model model, HttpServletRequest request) throws Exception {
        return "BusinessFailedPage";
    }

    @RequestMapping(value = "/admin/customers/orders/orderStatusMsgPage", method = RequestMethod.GET)
    public Object orderStatusMsg(@RequestParam(value = "id") Integer id, Model model, HttpServletRequest request) throws Exception {
        return "OrderStatusMessage";
    }

    private String sendInvoice(int id, Object admin)
    {
        logger.info("SB: " + admin + ": sending invoice for order " + id);
        Order order = ordersDao.findOne(id);
        int customerId = order.getCustomerId();
        Customer customer = customersDao.findOne(customerId);
        StringBuilder parameters = new StringBuilder();
        if ((order.getStatus() != Order.OrderStatuses.SHIPMENT) && (order.getStatus() != Order.OrderStatuses.DELIVERED)) {
            return "OrderStatusMessage";
        }
        if (order.getClearingStatus() != Order.ClearStatus.UNCLEAR) {
            return "ClearanceMessage";
        }

        float originalSum = common.calcTotalOriginalSumWithoutShipmentFee(order.getId());
        float sum = common.calcTotalSumWithoutShipmentFee(order.getId());

        //parameters.append("codepage").append('=').append(U.var.urlEncode(globalsDao.findOne("cardcom_code_page").getValue())).append('&');
        parameters.append("terminalnumber").append('=').append(U.var.urlEncode(globalsDao.findOne("cardcom_terminal_number").getValue())).append('&');
        //parameters.append("terminalnumber").append('=').append(U.var.urlEncode("1000")).append('&');
        parameters.append("username").append('=').append(U.var.urlEncode(globalsDao.findOne("cardcom_username").getValue())).append('&');
        //parameters.append("username").append('=').append(U.var.urlEncode("barak9611")).append('&');
        parameters.append("InvoiceType").append('=').append(U.var.urlEncode("305")).append('&');
        parameters.append("CustomPay.TransactionID").append('=').append(U.var.urlEncode("31")).append('&');

        if (customer.getType().equals(Customer.CustomerTypes.GOLD) || customer.getType().equals(Customer.CustomerTypes.PLATINUM) || originalSum >= Integer.parseInt(globalsDao.findOne("delivery_price_min_for_reduced").getValue()))
        {
            sum = sum - order.getDiscountSum() + Integer.parseInt(globalsDao.findOne("delivery_price_reduced").getValue());
        }
        else
        {
            sum = sum - order.getDiscountSum() + Integer.parseInt(globalsDao.findOne("delivery_price_standard").getValue());
        }

        parameters.append("InvoiceHead.CustName").append('=').append(U.var.urlEncode((customer.getFirstName() + " " + customer.getLastName() != null ? customer.getFirstName() + " " + customer.getLastName() : "N/A"))).append('&');
        parameters.append("InvoiceHead.CompID").append('=').append(U.var.urlEncode(customer.getCompanyId())).append('&');
        parameters.append("InvoiceHead.IsAutoCreateUpdateAccount").append('=').append("true").append('&');
        parameters.append("InvoiceHead.ExtIsVatFree").append('=').append("false").append('&');
        parameters.append("InvoiceHead.SendByEmail").append('=').append(U.var.urlEncode("true")).append('&');
        parameters.append("InvoiceHead.Language").append('=').append(U.var.urlEncode("he")).append('&');
        parameters.append("InvoiceHead.CustAddresLine1").append('=').append(U.var.urlEncode(customer.getStreet() + " " + customer.getStreetNo())).append('&');
//        parameters.append("InvoiceHead.CustAddresLine2").append('=').append(U.var.urlEncode(customer.getZip())).append('&');
        parameters.append("InvoiceHead.Email").append('=').append(U.var.urlEncode(customer.getEmail())).append('&');
        //parameters.append("InvoiceHead.Email").append('=').append(U.var.urlEncode("Morchuka@gmail.com")).append('&');
        parameters.append("InvoiceHead.CustCity").append('=').append(U.var.urlEncode(customer.getCity())).append('&');
        parameters.append("InvoiceHead.CustLinePH").append('=').append(U.var.urlEncode(customer.getPhone2() != null ? customer.getPhone2() : "---")).append('&');
        parameters.append("InvoiceHead.CustMobilePH").append('=').append(U.var.urlEncode(customer.getPhone() != null ? customer.getPhone() : "---")).append('&');
        parameters.append("InvoiceHead.Comments").append('=').append(U.var.urlEncode(String.format("חשבונית עבור הזמנה מספר %s שהתקבלה בתאריך %s", "" + order.getWpId(), deliveryDateFormat.format(order.getDeliveryDate())))).append('&');

        //Product List:
        Set<Integer> productIds = new HashSetIgnoresNulls<>();
        Map<Integer, Product> productsMap = new HashMap<>();
        for (OrderProduct orderProduct : orderProductDao.findAllByOrderId(id))
        {
            productIds.add(orderProduct.getProductId());
        }
        if (!productIds.isEmpty())
        {
            for (Product product : productsDao.findAll(productIds))
            {
                productsMap.put(product.getId(), product);

            }
        }
        int i = 0;
        for (OrderProduct orderProduct : orderProductDao.findAllByOrderId(id))
        {
            parameters.append("InvoiceLines" + (i != 0 ? i : "") + ".Description").append('=').append(U.var.urlEncode(productsMap.get(orderProduct.getProductId()).getNameHeb() + "")).append('&');
            parameters.append("InvoiceLines" + (i != 0 ? i : "") + ".Price").append('=').append(U.var.urlEncode(orderProduct.getUnitPrice() + "")).append('&');
            parameters.append("InvoiceLines" + (i != 0 ? i : "") + ".Quantity").append('=').append(U.var.urlEncode((int) orderProduct.getQuantity() + "")).append('&');
            parameters.append("InvoiceLines" + (i != 0 ? i : "") + ".IsPriceIncludeVAT").append('=').append(true).append('&');
            parameters.append("InvoiceLines" + (i != 0 ? i : "") + ".IsVatFree").append('=').append(U.var.urlEncode(!productsMap.get(orderProduct.getProductId()).hasVat() + "")).append('&');
            //  System.out.println("need vat :  " + !productsMap.get(orderProduct.getProductId()).hasVat()+"");
            i++;
        }
        if (customer.getType().equals(Customer.CustomerTypes.GOLD) || customer.getType().equals(Customer.CustomerTypes.PLATINUM) || originalSum >= Integer.parseInt(globalsDao.findOne("delivery_price_min_for_reduced").getValue()))
        {
            parameters.append("InvoiceLines" + (i != 0 ? i : "") + ".Description").append('=').append(U.var.urlEncode("משלוח מוזל")).append('&');
            parameters.append("InvoiceLines" + (i != 0 ? i : "") + ".Price").append('=').append(U.var.urlEncode(globalsDao.findOne("delivery_price_reduced").getValue() + "")).append('&');
        }
        else
        {
            parameters.append("InvoiceLines" + (i != 0 ? i : "") + ".Description").append('=').append(U.var.urlEncode("משלוח")).append('&');
            parameters.append("InvoiceLines" + (i != 0 ? i : "") + ".Price").append('=').append(U.var.urlEncode(globalsDao.findOne("delivery_price_standard").getValue() + "")).append('&');
        }

        parameters.append("InvoiceLines" + (i != 0 ? i : "") + ".Quantity").append('=').append(U.var.urlEncode("1")).append('&');
        parameters.append("InvoiceLines" + (i != 0 ? i : "") + ".IsPriceIncludeVAT").append('=').append(true).append('&');
        parameters.append("InvoiceLines" + (i != 0 ? i : "") + ".IsVatFree").append('=').append(false).append('&');

        if (order.getDiscountSum() > 0)
        {
            i++;
            parameters.append("InvoiceLines" + (i != 0 ? i : "") + ".Description").append('=').append(U.var.urlEncode("הנחה ללא מעמ")).append('&');
            parameters.append("InvoiceLines" + (i != 0 ? i : "") + ".Price").append('=').append(U.var.urlEncode(-order.getDiscountSum() + "")).append('&');
            parameters.append("InvoiceLines" + (i != 0 ? i : "") + ".Quantity").append('=').append(U.var.urlEncode("1")).append('&');
            parameters.append("InvoiceLines" + (i != 0 ? i : "") + ".IsPriceIncludeVAT").append('=').append(false).append('&');
            parameters.append("InvoiceLines" + (i != 0 ? i : "") + ".IsVatFree").append('=').append(true).append('&');
        }

        //        System.out.println(parameters.toString());

        StringBuilder result = new StringBuilder();
        StringBuilder response = new StringBuilder();

        try
        {
            byte[] parametersBytes = parameters.toString().getBytes();
            URL url = new URL("https://secure.cardcom.co.il/Interface/CreateInvoice.aspx");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            // conn.setInstanceFollowRedirects( false );
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestProperty("charset", "utf-8");
            conn.setRequestProperty("Content-Length", "" + parametersBytes.length);
            conn.setUseCaches(false);
            conn.getOutputStream().write(parametersBytes);
            Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

            //Checking the result
            boolean flag = true;
            //        for (int c; (c= in.read()) >= 0;) {
            //
            //                str.append((char) c);
            //        }

            for (int c; (c = in.read()) >= 0; )
            {
                if (flag && c != '&')
                {
                    response.append((char) c);
                }
                else
                {
                    flag = false;
                }
                result.append((char) c);
            }

        }
        catch (Exception ex)
        {
            return "BusinessFailedPage";
        }

        int start = response.indexOf("=") + 1;
        int end = response.length();
        if (response.substring(start, end).equals("0"))
        {
            order.setClearingStatus(Order.ClearStatus.INVOICE_ONLY);
            order.setSum(sum);
            order.setStatus(Order.OrderStatuses.CLOSED);

            Date yearAgo = new Date(System.currentTimeMillis() - 1000 * 60 * 60 * 24 * 365);

            String [] fields = result.toString().split("&");

            for (int index = 0; index < fields.length; index++) {
                String [] values = fields[index].split("=");
                if ( values[0].equals("InternalDealNumber")) {
                    order.setClearingDealNumber(U.var.parseInt(values[1]));
                }

                if (values[0].equals("InvoiceResponse.InvoiceNumber")) {
                    order.setClearingInvoiceNumber(U.var.parseInt(values[1]));
                }
            }

            float sumWithShipmentWithoutDiscount = sum - order.getDiscountSum();
            float silverPoints = U.var.parseFloat(globalsDao.findOne("silver_points").getValue());
            float goldPoints = U.var.parseFloat(globalsDao.findOne("gold_points").getValue());
            float platinumPoints = U.var.parseFloat(globalsDao.findOne("platinum_points").getValue());
            switch (customer.getType())
            {
                case SILVER:
                    pointsDao.save(new Point(customerId, (short) (sumWithShipmentWithoutDiscount / silverPoints), 'B', sumWithShipmentWithoutDiscount, new Date(System.currentTimeMillis())));
                    break;
                case GOLD:
                    pointsDao.save(new Point(customerId, (short) (sumWithShipmentWithoutDiscount / goldPoints), 'B', sumWithShipmentWithoutDiscount, new Date(System.currentTimeMillis())));
                    break;
                case PLATINUM:
                    pointsDao.save(new Point(customerId, (short) (sumWithShipmentWithoutDiscount / platinumPoints), 'B', sumWithShipmentWithoutDiscount, new Date(System.currentTimeMillis())));
                    break;
            }
            customer.setCreditPoints(pointsDao.sum(customerId).shortValue());
            customersDao.save(customer);
            ordersDao.save(order);
            return "true";
        }
        else
        {
            order.setClearingStatus(Order.ClearStatus.FAILED);
            ordersDao.save(order);
            return "false";
        }
    }

    @RequestMapping(value = "/admin/customers/status", method = RequestMethod.GET)
    public Object customerStatus(HttpServletRequest request) {

        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        Integer year = cal.get(Calendar.YEAR);
        Integer lastYear = year - 1;
        List<Object[]> data = customersDao.findAllWithStatusBetweenYears(lastYear.toString(), year.toString());
        customersDao.clearAllType();

        if (data.isEmpty()) {
            return "Back";
        }

        Integer index = 0;
        Float platinumSum = U.var.parseFloat(globalsDao.findOne("platinum_sum").getValue());
        Float goldSum = U.var.parseFloat(globalsDao.findOne("gold_sum").getValue());
        for (Object [] line : data) {
            Customer customer = customersDao.findOne(U.var.parseInt(line[0]));
            if (U.var.parseFloat(line[5]) > platinumSum) { // Check acumulated sum to gold threshold
                customer.setType(Customer.CustomerTypes.PLATINUM);
                common.sendEmail(line[4].toString(), "שוקבוק - חברות במועדון הלקוחות", U.http.stringFromUrl("http://" + request.getLocalName() + ":" + request.getLocalPort() + "/admin/customers/status/email?customerId=" + line[0] + "&sum=" + line[5]));
                customersDao.save(customer);
            } else if (U.var.parseFloat(line[5]) > goldSum) { // Check acumulated sum to gold threshold
                customer.setType(Customer.CustomerTypes.GOLD);
                common.sendEmail(line[4].toString(), "שוקבוק - חברות במועדון הלקוחות", U.http.stringFromUrl("http://" + request.getLocalName() + ":" + request.getLocalPort() + "/admin/customers/status/email?customerId=" + line[0] + "&sum=" + line[5]));
                customersDao.save(customer);
            }

        }

        return "redirect:/admin/customerAdministration";
    }

    @RequestMapping(value = "/admin/customers/status/email", method = RequestMethod.GET)
    public Object couponEmail(@RequestParam(value = "customerId") Integer customerId, @RequestParam(value = "sum") Float sum, Model model, HttpServletRequest request) {

        Customer customer = customersDao.findOne(customerId);
        Map<String, Object> column = new HashMap<>();
        column.put("fullName", customer.getFirstName() + " " + customer.getLastName());
        column.put("firstName", customer.getFirstName());
        column.put("lastName", customer.getLastName());
        if (sum >= U.var.parseFloat(globalsDao.findOne("platinum_sum").getValue())) {
            column.put("status", "פלטינה");
        } else if (sum >= U.var.parseFloat(globalsDao.findOne("gold_sum").getValue())) {
            column.put("status", "זהב");
        } else {
            column.put("status", "כסף");
        }

        model.addAttribute("column", column);

        return "ClubMember";
    }

    @RequestMapping(value = "/admin/customers/submit/gender", method = RequestMethod.POST)
    public Object submitStatus(@RequestParam(value = "customerId") Integer customerId,
                               @RequestParam(value = "customerGender") Character gender,
                               HttpServletRequest request) {
        Customer customer = customersDao.findOne(customerId);

        switch (gender) {
            case '0' : customer.setGender('F'); break;
            case '1' : customer.setGender('M'); break;
            default: break;
        }
        customersDao.save(customer);
        return "redirect:/admin/customers/customer?id=" + customerId;
    }
}
