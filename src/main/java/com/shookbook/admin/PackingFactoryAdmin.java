package com.shookbook.admin;

import com.shookbook.dao.shookbook.*;
import com.shookbook.entities.shookbook.*;
import com.shookbook.types.HashSetIgnoresNulls;
import com.shookbook.utils.U;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

@Controller
@SessionAttributes("thought")
public class PackingFactoryAdmin {
    @Autowired
    private Common common;
    @Autowired
    private ProductsDao productsDao;
    @Autowired
    private ItemsDao itemsDao;
    @Autowired
    private OrdersDao ordersDao;
    @Autowired
    private SuppliersDao suppliersDao;
    @Autowired
    private WorkersDao workersDao;
    @Autowired
    private GlobalsDao globalsDao;
    @Autowired
    private OrderProductDao orderProductDao;

    private void calcPackingTimes(Date packingStartTime, Date packingEndTime) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        int packingStartHour = U.var.parseInt(globalsDao.findOne("packing_start_hour").getValue());
        if (calendar.get(Calendar.HOUR_OF_DAY) < packingStartHour) {
            calendar.setTimeInMillis(calendar.getTimeInMillis() - 1000 * 60 * 60 * 24);
        }
        calendar.set(Calendar.HOUR_OF_DAY, packingStartHour);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        packingStartTime.setTime(calendar.getTimeInMillis());
        packingEndTime.setTime(packingStartTime.getTime() + U.var.parseLong(globalsDao.findOne("packing_total_hours").getValue()) * 1000 * 60 * 60);
    }

    @RequestMapping(value = "/admin/packingFactory", method = RequestMethod.GET)
    public Object packingFactory(Model model, HttpServletRequest request) {
        if (!U.var.parseBoolean(request.getSession().getAttribute("login"))) {
            return "redirect:/admin/login";
        }

        Date packingStartTime = new Date();
        Date packingEndTime = new Date();
        calcPackingTimes(packingStartTime, packingEndTime);

        Map<Integer, Object[]> productCountersMap = new HashMap<>();
        for (Object[] i : productsDao.findAllCountAndSumInPackingFactory(packingStartTime, packingEndTime)) {
            productCountersMap.put(U.var.parseInt(i[0]), i);
        }

        Map<Integer, Product> productsMap = new HashMap<>();
        for (Product product : productsDao.findAllMissing()) {
            productsMap.put(product.getId(), product);
        }
        Product product = null;
        List<Map<String, Object>> missingProducts = new LinkedList<>();
        if (!productsMap.isEmpty()) {
            List<Item> items = itemsDao.findAllByProductIds(productsMap.keySet());

            Set<Integer> supplierIds = new HashSetIgnoresNulls<>();

            for (Item item : items) {
                supplierIds.add(item.getSupplierId());
            }

            Map<Integer, Supplier> suppliersMap = new HashMap<>();
            for (Supplier supplier : suppliersDao.findAll(supplierIds)) {
                suppliersMap.put(supplier.getId(), supplier);
            }

            for (Item item : items) {
                Map<String, Object> map = new HashMap<>();
                if (product == null || item.getProductId() != product.getId()) {
                    product = productsMap.get(item.getProductId());
                    map.put("id", product.getId());
                    map.put("name", product.getNameHeb());
                    Object[] productCounter = productCountersMap.get(product.getId());
                    if (productCounter == null) {
                        map.put("ordersCount", 0);
                        map.put("itemsCount", 0);
                    } else {
                        map.put("ordersCount", productCounter[1]);
                        map.put("itemsCount", productCounter[2]);
                    }
                }
                map.put("barcode", item.getBarcode());
                map.put("supplier", suppliersMap.get(item.getSupplierId()).getName());
                missingProducts.add(map);
            }
        }

        // Ziv - Add Pending items table

        productsMap = new HashMap<>();
        for (Product packing_product : productsDao.findAllnotPackedOrderByWeight()) {
            productsMap.put(packing_product.getId(), packing_product);
        }
        product = null;
        List<Map<String, Object>> packingProducts = new LinkedList<>();
        if (!productsMap.isEmpty()) {
            List<Item> items = itemsDao.findAllByProductIds(productsMap.keySet());

            Set<Integer> supplierIds = new HashSetIgnoresNulls<>();

            for (Item item : items) {
                supplierIds.add(item.getSupplierId());
            }

            Map<Integer, Supplier> suppliersMap = new HashMap<>();
            for (Supplier supplier : suppliersDao.findAll(supplierIds)) {
                suppliersMap.put(supplier.getId(), supplier);
            }

            for (Item item : items) {
                Map<String, Object> map = new HashMap<>();
                if (product == null || item.getProductId() != product.getId()) {
                    product = productsMap.get(item.getProductId());
                    map.put("id", product.getId());
                    map.put("name", product.getNameHeb());
                    Object[] productCounter = productCountersMap.get(product.getId());
                    if (productCounter == null) {
                        map.put("ordersCount", 0);
                        map.put("itemsCount", 0);
                    } else {
                        map.put("ordersCount", productCounter[1]);
                        map.put("itemsCount", productCounter[2]);
                    }
                }
                map.put("barcode", item.getBarcode());
                map.put("supplier", suppliersMap.get(item.getSupplierId()).getName());
                packingProducts.add(map);
            }
        }

        // Ziv end

        List<Map<String, Object>> palettes = new LinkedList<>();
        for (Object[] p : productsDao.findAllPalletsInPackingFactory(packingStartTime, packingEndTime)) {
            Map<String, Object> palette = new HashMap<>();
            palette.put("dhl", p[0]);
            palette.put("boxes", p[1]);
            palettes.add(palette);
        }

        model.addAttribute("allProductsInCurrentPacking", productsDao.findAllInCurrentPackingOrderByWeight());
        model.addAttribute("missingProducts", missingProducts);
        model.addAttribute("packingProducts", packingProducts);
        model.addAttribute("packingNow", ordersDao.findAllPackingNow(packingStartTime, packingEndTime));
        List<Order> packingDone = ordersDao.findAllPackingDone(packingStartTime, packingEndTime);
        model.addAttribute("packingDoneCount", packingDone.size());
        List<Order> packingPending = ordersDao.findAllPendingOrderReadyToDhlOrdered();
        model.addAttribute("packingPending", packingPending.size());
        int packingDoneBoxesSum = 0;
        for (Order order : packingDone) {
            packingDoneBoxesSum += order.getBoxes();
        }
        model.addAttribute("packingDoneBoxesSum", packingDoneBoxesSum);
        model.addAttribute("packingDone", packingDone);
        model.addAttribute("palettes", palettes);

        List<Order> dhlErrorOrders = ordersDao.findAllDhlError();
        Integer dhlError = 0;
        if (dhlErrorOrders.size() > 0) {
            dhlError = 1;
        }

        model.addAttribute("dhlError", dhlError);

        Map<String, Object> globals = new HashMap<>();
        globals.put("modules_enable_coupons", globalsDao.findOne("modules_enable_coupons").getValue());
        globals.put("modules_enable_invoice", globalsDao.findOne("modules_enable_invoice").getValue());
        globals.put("modules_enable_logistics", globalsDao.findOne("modules_enable_logistics").getValue());
        globals.put("modules_enable_packing", globalsDao.findOne("modules_enable_packing").getValue());
        Object logedWorker = request.getSession().getAttribute("userId");
        Worker worker = workersDao.getOne(Integer.parseInt(logedWorker.toString()));
        globals.put("worker_name", worker.getFirstName() + " " + worker.getLastName());
        model.addAttribute("globals", globals);
        model.addAttribute("admin", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.ADMIN.ordinal() ? 1 : 0);

        return "PackingFactory";
    }

    @RequestMapping(value = "/admin/packingFactory/paletteDetails", method = RequestMethod.GET)
    public Object paletteDetails(@RequestParam(value = "paletteName") String paletteName,
                                 Model model, HttpServletRequest request) {
        if (!U.var.parseBoolean(request.getSession().getAttribute("login"))) {
            return "redirect:/admin/login";
        }

        Date packingStartTime = new Date();
        Date packingEndTime = new Date();
        calcPackingTimes(packingStartTime, packingEndTime);
        List<Order> packingDone = ordersDao.findAllPalettePackingDone(paletteName + '%', packingStartTime, packingEndTime);

        Set<Integer> orderIds = new HashSetIgnoresNulls<>();
        for (Order order : packingDone) {
            orderIds.add(order.getId());
        }

        List<OrderProduct> orderProducts = orderProductDao.findAllWithMissingItemsByOrders(orderIds, paletteName + '%');
        Set<Integer> productIds = new HashSetIgnoresNulls<>();
        for (OrderProduct orderProduct : orderProducts) {
            productIds.add(orderProduct.getProductId());
        }

        Map<Integer, Product> productMap = new HashMap<>();
        for (Product product : productsDao.findAll(productIds)) {
            productMap.put(product.getId(), product);
        }

        Map<Integer, Order> orderMap = new HashMap<>();
        for (Order order : ordersDao.findAll(orderIds)) {
            orderMap.put(order.getId(), order);
        }

        List<Map<String, Object>> missingProducts = new LinkedList<>();
        for (OrderProduct orderProduct : orderProducts) {
            Map<String, Object> missingProduct = new HashMap<>();
            missingProduct.put("orderId", orderProduct.getOrderId());
            missingProduct.put("dhlPackageNumber", orderMap.get(orderProduct.getOrderId()).getDhlPackageNumber());
            missingProduct.put("workerId", orderMap.get(orderProduct.getOrderId()).getPackingWorkerId());
            missingProduct.put("productName", productMap.get(orderProduct.getProductId()).getNameHeb());
            missingProduct.put("quantity", orderProduct.getQuantity());
            missingProduct.put("quantityNeeded", orderProduct.getQuantityNeeded());
            missingProducts.add(missingProduct);
        }

        Map<String, Object> globals = new HashMap<>();
        globals.put("modules_enable_coupons", globalsDao.findOne("modules_enable_coupons").getValue());
        globals.put("modules_enable_invoice", globalsDao.findOne("modules_enable_invoice").getValue());
        globals.put("modules_enable_logistics", globalsDao.findOne("modules_enable_logistics").getValue());
        globals.put("modules_enable_packing", globalsDao.findOne("modules_enable_packing").getValue());
        Object logedWorker = request.getSession().getAttribute("userId");
        Worker worker = workersDao.getOne(Integer.parseInt(logedWorker.toString()));
        globals.put("worker_name", worker.getFirstName() + " " + worker.getLastName());
        model.addAttribute("globals", globals);
        model.addAttribute("productMap", productMap);
        model.addAttribute("missingProducts", missingProducts);
        model.addAttribute("packingDone", packingDone);
        model.addAttribute("paletteName", paletteName);

        return "PaletteDetails";
    }

    @RequestMapping(value = "/admin/packingFactory/submit/removeAllMissing", method = RequestMethod.POST)
    public Object removeAllMissing(HttpServletRequest request) {
        productsDao.clearAllMissing();
        return "redirect:/admin/packingFactory";
    }

    @RequestMapping(value = "/admin/packingFactory/submit/removeMissing", method = RequestMethod.POST)
    public Object removeMissing(@RequestParam(value = "productId") Integer productId, HttpServletRequest request) {
        Product product = productsDao.findOne(productId);
        product.setIsMissing('0');
        productsDao.save(product);
        return "redirect:/admin/packingFactory";
    }

    @RequestMapping(value = "/admin/packingFactory/submit/addMissing", method = RequestMethod.POST)
    public Object addMissing(@RequestParam(value = "productId") Integer productId, HttpServletRequest request) {
        Product product = productsDao.findOne(productId);
        product.setIsMissing('1');
        productsDao.save(product);
        return "redirect:/admin/packingFactory";
    }

    @ResponseBody
    @RequestMapping(value = "/admin/packingFactory/csv/weight", method = RequestMethod.GET)
    public Object csvWeight(HttpServletResponse response) {
        StringBuilder stringBuilder = new StringBuilder("ID,Name,Weight code,Quantity\n");
        List<Object[]> productLines;
        System.out.format("   Sting is -%s-", globalsDao.findOne("packing_weight_code_gen").getValue());
        if ("0".equals(globalsDao.findOne("packing_weight_code_gen").getValue())) {
            productLines = productsDao.weightReportPendingOnly();
        } else {
            productLines = productsDao.weightReportPendingOrHarvesting();
        }
        for (Object[] line : productLines) {
            stringBuilder.append('"').append(line[0]).append('"').append(',').append('"').append(line[1]).append('"').append(',').append('"').append(line[2]).append('"').append(',').append('"').append(line[3]).append('"').append('\n');
        }
        response.setHeader("Content-Disposition", "attachment; filename=\"weight.csv\"");
        response.setContentLength(stringBuilder.length());
        response.setContentType("text/csv");
        return stringBuilder.toString();
    }

    @ResponseBody
    @RequestMapping(value = "/admin/packingFactory/csv/missing", method = RequestMethod.GET)
    public Object csvMissing(HttpServletResponse response) {
        StringBuilder stringBuilder = new StringBuilder("Product Id,Order Id,Quantity needed,Quantity, DHL number, Weight\n");
        Date packingStartTime = new Date();
        Date packingEndTime = new Date();
        calcPackingTimes(packingStartTime, packingEndTime);
        for (Object[] line : orderProductDao.missingReport(packingStartTime, packingEndTime)) {
            stringBuilder.append(line[0]).append(',').append(line[1]).append(',').append(line[2]).append(',').append(line[3]).append(',').append(line[4]).append(',').append(line[5]).append('\n');
        }
        response.setHeader("Content-Disposition", "attachment; filename=\"missing.csv\"");
        response.setContentLength(stringBuilder.length());
        response.setContentType("text/csv");
        return stringBuilder.toString();
    }

    @ResponseBody
    @RequestMapping(value = "/admin/packingFactory/csv/export", method = RequestMethod.GET)
    public Object csvExport(Model model, HttpServletRequest request, HttpServletResponse response) {
        StringBuilder stringBuilder = new StringBuilder("Missing:\nid,name,barcode,supplier,orders count,items count\n");
        packingFactory(model, request);

        for (Map<String, Object> missingProduct : (List<Map<String, Object>>) model.asMap().get("missingProducts")) {
            stringBuilder.append('"').append(missingProduct.get("id")).append('"').append(',').append('"').append(missingProduct.get("name")).append('"').append(',').append('"').append(missingProduct.get("barcode")).append('"').
                    append('"').append(missingProduct.get("supplier")).append('"').append(',').append('"').append(missingProduct.get("ordersCount")).append('"').append(',').append('"').append(missingProduct.get("itemsCount")).append('"').
                    append('\n');
        }

        stringBuilder.append("\n\n\nPacking now:\nbarcode,dhl,packingWorkerId\n");
        for (Order order : (List<Order>) model.asMap().get("packingNow")) {
            stringBuilder.append('"').append(order.getBarcode()).append('"').append(',').append('"').append(order.getDhlPackageNumber()).append('"').append(',').append('"').append(order.getPackingWorkerId()).append('"').append('\n');
        }

        stringBuilder.append("\n\n\nPalettes:\ndhl,boxes\n");
        for (Map<String, Object> palette : (List<Map<String, Object>>) model.asMap().get("palettes")) {
            stringBuilder.append('"').append(palette.get("dhl")).append('"').append(',').append('"').append(palette.get("boxes")).append('"').append('\n');
        }

        stringBuilder.append("\n\n\nPacking done:\nbarcode,dhl,packingWorkerId\n");
        for (Order order : (List<Order>) model.asMap().get("packingDone")) {
            stringBuilder.append('"').append(order.getBarcode()).append('"').append(',').append('"').append(order.getDhlPackageNumber()).append('"').append(',').append('"').append(order.getPackingWorkerId()).append('"').append('\n');
        }

        response.setHeader("Content-Disposition", "attachment; filename=\"packingFactory.csv\"");
        response.setContentLength(stringBuilder.length());
        response.setContentType("text/csv");
        return stringBuilder.toString();
    }
}
