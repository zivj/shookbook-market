package com.shookbook.admin;

import com.shookbook.Application;
import com.shookbook.dao.shookbook.*;
import com.shookbook.dao.wordpress.WpPostmetaDao;
import com.shookbook.dao.wordpress.WpPostsDao;
import com.shookbook.dao.wordpress.WpTermRelationshipsDao;
import com.shookbook.entities.shookbook.*;
import com.shookbook.entities.wordpress.WpPost;
import com.shookbook.entities.wordpress.WpPostmeta;
import com.shookbook.entities.wordpress.WpTermRelationships;
import com.shookbook.utils.U;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Controller
@SessionAttributes("thought")
public class ProductsAdmin {
    private static final Logger logger = Logger.getLogger(ProductsAdmin.class);
    @Autowired
    private Common common;
    @Autowired
    private ProductsDao productsDao;
    @Autowired
    private SuppliersDao suppliersDao;
    @Autowired
    private ItemsDao itemsDao;
    @Autowired
    private OrderProductDao orderProductDao;
    @Autowired
    private CategoriesDao categoriesDao;
    @Autowired
    private GlobalsDao globalsDao;
    @Autowired
    private WorkersDao workersDao;
    @Autowired
    private WpPostsDao wpPostsDao;
    @Autowired
    private WpPostmetaDao wpPostmetaDao;
    @Autowired
    private WpTermRelationshipsDao wpTermRelationshipsDao;

    @RequestMapping(value = "/admin/printBarcode", method = RequestMethod.GET)
    public Object printBarcode(@RequestParam(value = "product_name") String product_name,
                               @RequestParam(value = "packing_weight_code") String packing_weight_code,
                               @RequestParam(value = "rule") Integer rule,
                               @RequestParam(value = "barcode") String barcode
            , Model model, HttpServletRequest request) {

        model.addAttribute("product_name", product_name);
        model.addAttribute("barcode", barcode);
        model.addAttribute("rule", rule);
        model.addAttribute("packing_weight_code", Short.parseShort(packing_weight_code));
        model.addAttribute("admin", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.ADMIN.ordinal() ? 1 : 0);

        return "printBarcode";
    }


    @RequestMapping(value = "/admin/products", method = RequestMethod.GET)
    public Object products(Model model, HttpServletRequest request) {
        if (!U.var.parseBoolean(request.getSession().getAttribute("login"))) {
            return "redirect:/admin/login";
        }

        List<Product> products = productsDao.findAll();

        Object logedWorker = request.getSession().getAttribute("userId");
        Worker worker = workersDao.getOne(Integer.parseInt(logedWorker.toString()));
        Integer workerSbId = U.var.parseInt(worker.getSbId());
        Integer privilege = worker.getType().ordinal();


        List<Item> items = itemsDao.findAllByProductIdDesc();
        Map<Integer, Supplier> supplierMap = new HashMap<>();
        for (Supplier supplier : suppliersDao.findAll()) {
            supplierMap.put(supplier.getId(), supplier);
        }

        int i = 0;
        List<Map<String, Object>> columns = new LinkedList<>();
        if (privilege == Worker.Types.SUPPLIER.ordinal()) {
            products = productsDao.findAllDesc();
        } else {
            products = productsDao.findAllDesc();
        }

        for (Product product : products) {
            Map<String, Object> column = new HashMap<>();
            List<Map<String, Object>> suppliers = new LinkedList<>();

            if (privilege == Worker.Types.SUPPLIER.ordinal()) {
                if (i != items.size()) {
                    //System.out.format("   Product %d %d%n", items.get(i).getProductId(), product.getId());
                    while (items.get(i).getProductId() == product.getId()) {
                        if (items.get(i).getSupplierId() == workerSbId) {
                            column.put("id", product.getId());
                            column.put("product", product.getNameHeb());
                            column.put("image", product.getImageUrl());
                            Map<String, Object> supplier = new HashMap<>();
                            supplier.put("supplier", supplierMap.get(items.get(i).getSupplierId()).getName());
                            supplier.put("barcode", items.get(i).getBarcode());
                            String barcodeClean = items.get(i).getBarcode().replaceAll("-", "");
                            if (barcodeClean.length() < 11) {
                                barcodeClean = "00" + barcodeClean;
                            } else {
                                barcodeClean = "0" + barcodeClean;
                            }
                            supplier.put("barcode_clean", barcodeClean);
                            supplier.put("item_name", items.get(i).getName());
                            supplier.put("supplier_id", items.get(i).getSupplierId());
                            supplier.put("item_id", items.get(i).getId());
                            supplier.put("inventory", items.get(i).getInventory());
                            supplier.put("rule", product.getRefrigerated().ordinal() > 0 ? 2 : product.getPackingRule().ordinal()); // If refrigirated change symbol
                            supplier.put("packing_weight_code", product.getPackingWeightCode());

                            suppliers.add(supplier);
                        }

                        i++;
                        if (i == items.size() - 1) {
                            break;
                        }
                    }
                }
                if (suppliers.size() > 0) {
                    column.put("suppliers", suppliers);
                }
            } else {
                column.put("id", product.getId());
                column.put("product", product.getNameHeb());
                column.put("image", product.getImageUrl());
                column.put("freeze", product.getRefrigerated().ordinal() > 0 ? 1 : 0);
                //System.out.format("   ## Product: %s%n", product.getNameHeb());
                //System.out.format("   ## Size of items: %d%n", items.size());
                if (i != items.size()) {
                    //System.out.format("   ## Product id: %d  Item product id: %d%n", product.getId(), items.get(i).getProductId());
                    while (items.get(i).getProductId() == product.getId()) {
                        // System.out.format("   ## Item: %s  i: %d%n", product.getNameHeb(), i);
                        Map<String, Object> supplier = new HashMap<>();
                        supplier.put("supplier", supplierMap.get(items.get(i).getSupplierId()).getName());
                        supplier.put("barcode", items.get(i).getBarcode());
                        String barcodeClean = items.get(i).getBarcode().replaceAll("-", "");
                        if (barcodeClean.length() < 10) {
                            barcodeClean = "00" + barcodeClean;
                        } else {
                            barcodeClean = "0" + barcodeClean;
                        }
                        supplier.put("barcode_clean", barcodeClean);
                        supplier.put("item_name", items.get(i).getName());
                        supplier.put("supplier_id", items.get(i).getSupplierId());
                        supplier.put("item_id", items.get(i).getId());
                        supplier.put("inventory", items.get(i).getInventory());
                        supplier.put("rule", product.getRefrigerated().ordinal() > 0 ? 2 : product.getPackingRule().ordinal()); // If refrigirated change symbol
                        supplier.put("packing_weight_code", product.getPackingWeightCode());

                        suppliers.add(supplier);

                        i++;
                        if (i == items.size() - 1) {
                            break;
                        }
                    }
                }
                column.put("suppliers", suppliers);
            }

            columns.add(column);
        }

        Map<String, Object> globals = new HashMap<>();
        globals.put("modules_enable_coupons", globalsDao.findOne("modules_enable_coupons").getValue());
        globals.put("modules_enable_invoice", globalsDao.findOne("modules_enable_invoice").getValue());
        globals.put("modules_enable_logistics", globalsDao.findOne("modules_enable_logistics").getValue());
        globals.put("modules_enable_packing", globalsDao.findOne("modules_enable_packing").getValue());
        globals.put("worker_name", worker.getFirstName() + " " + worker.getLastName());
        model.addAttribute("globals", globals);
        model.addAttribute("columns", columns);
        model.addAttribute("admin", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.ADMIN.ordinal() ? 1 : 0);
        return "Products";
    }


    //Before update
    @RequestMapping(value = "/admin/items/update", method = RequestMethod.GET)
    public Object update(@RequestParam(value = "id") Integer id, Model model, HttpServletRequest request) {
        Item item = itemsDao.findOne(id);
        Product product = productsDao.findOne(item.getProductId());
        Supplier currentSupplier = suppliersDao.findOne(item.getSupplierId());
        List<Map<String, Object>> suppliers = new LinkedList<>();
        for (Supplier supplierItr : suppliersDao.findAll()) {

            Map<String, Object> supplier = new HashMap<>();
            supplier.put("id", supplierItr.getId());
            supplier.put("name", supplierItr.getName());
            suppliers.add(supplier);
        }

        Category category = categoriesDao.findOne(product.getCategoryId());
        model.addAttribute("barcode", getUpcABarcode(item.getBarcode()));
        model.addAttribute("barcode_standard", item.getBarcode());
        model.addAttribute("product_name", product.getNameHeb());
        model.addAttribute("image_url", product.getImageUrl());
        model.addAttribute("category", category.getName());
        model.addAttribute("name", item.getName());
        model.addAttribute("buying_cost", item.getBuyingCost());
        model.addAttribute("supplier", currentSupplier.getName());
        model.addAttribute("supplierid", currentSupplier.getId());
        model.addAttribute("weight", item.getUnitWeight());
        model.addAttribute("gross_profit", item.getGrossProfit());
        model.addAttribute("inventory", item.getInventory());
        model.addAttribute("order_unit", item.getOrderUnit().ordinal());
        model.addAttribute("itemsPurchased", orderProductDao.quantity(id));
        model.addAttribute("id", id);

        model.addAttribute("suppliers", suppliers);
        Map<String, Object> globals = new HashMap<>();
        globals.put("modules_enable_coupons", globalsDao.findOne("modules_enable_coupons").getValue());
        globals.put("modules_enable_invoice", globalsDao.findOne("modules_enable_invoice").getValue());
        globals.put("modules_enable_logistics", globalsDao.findOne("modules_enable_logistics").getValue());
        globals.put("modules_enable_packing", globalsDao.findOne("modules_enable_packing").getValue());
        Object logedWorker = request.getSession().getAttribute("userId");
        Worker worker = workersDao.getOne(Integer.parseInt(logedWorker.toString()));
        globals.put("worker_name", worker.getFirstName() + " " + worker.getLastName());
        model.addAttribute("globals", globals);
        model.addAttribute("admin", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.ADMIN.ordinal() ? 1 : 0);
        return "ItemEdit";
    }

    //After update
    @RequestMapping(value = "/admin/items/toupdate", method = RequestMethod.POST)
    public String toUpdate(@RequestParam(value = "id") Integer id,
                           @RequestParam(value = "item_name") String itemName,
                           @RequestParam(value = "inventory") String inventory,
                           @RequestParam(value = "gross_profit") String gross_profit,
                           @RequestParam(value = "buying_cost") String buying_cost,
                           @RequestParam(value = "order_unit") String orderUnit,
                           @RequestParam(value = "weight") String weight,

                           Model model, HttpServletRequest request) {
        Item item = itemsDao.findOne(id);
        item.setName(itemName);
        int delta = item.getInventory() - U.var.parseInt(inventory);
        item.setInventory(Short.parseShort(inventory));
        item.setGrossProfit(Float.parseFloat(gross_profit));
        item.setBuyingCost(Float.parseFloat(buying_cost));
        item.setOrderUnit(Item.OrderUnits.values()[orderUnit.charAt(0) - '0']);
        item.setUnitWeight(Float.parseFloat(weight));
        item.setIsDeleted('0');
        item = itemsDao.save(item);
        Product product = productsDao.findOne(item.getProductId());
        WpPostmeta postmeta = wpPostmetaDao.findOneByIdAndKey(U.var.parseLong(product.getWpId()), "_stock");
        postmeta.setMetaValue("" + (U.var.parseInt(postmeta.getMetaValue()) - delta));
        postmeta = wpPostmetaDao.save(postmeta);
        wpPostmetaDao.updateByIdAndKey(U.var.parseLong(product.getWpId()), "_stock_status", U.var.parseInt(postmeta.getMetaValue()) > 0 ? "instock" : "outofstock");
        logger.info("SB: " + request.getSession().getAttribute("userName") + ": update item " + item.getId());

        return "redirect:" + "/admin/products";


    }

    @RequestMapping(value = "/admin/items/add", method = RequestMethod.GET)
    public Object add(@RequestParam(value = "id") Integer id, Model model, HttpServletRequest request) {

        Product product = productsDao.findOne(id);
        List<Supplier> suppliers = suppliersDao.findAll();
        if (product != null) {
            Category category = categoriesDao.findOne(product.getCategoryId());
            model.addAttribute("suppliers", suppliers);
            model.addAttribute("category", category.getName());
            model.addAttribute("image_url", product.getImageUrl());
            model.addAttribute("id", id);
        }

        Map<String, Object> globals = new HashMap<>();
        globals.put("modules_enable_coupons", globalsDao.findOne("modules_enable_coupons").getValue());
        globals.put("modules_enable_invoice", globalsDao.findOne("modules_enable_invoice").getValue());
        globals.put("modules_enable_logistics", globalsDao.findOne("modules_enable_logistics").getValue());
        globals.put("modules_enable_packing", globalsDao.findOne("modules_enable_packing").getValue());
        Object logedWorker = request.getSession().getAttribute("userId");
        Worker worker = workersDao.getOne(Integer.parseInt(logedWorker.toString()));
        globals.put("worker_name", worker.getFirstName() + " " + worker.getLastName());
        model.addAttribute("globals", globals);
        model.addAttribute("admin", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.ADMIN.ordinal() ? 1 : 0);
        return "ItemAdd";
    }

    @RequestMapping(value = "/admin/items/toadd", method = RequestMethod.POST)
    public Object itemsAdd(@RequestParam(value = "id") Integer id,
                           @RequestParam(value = "item_name") String itemName,
                           @RequestParam(value = "buying_cost") String buying_cost,
                           @RequestParam(value = "weight") String weight,
                           @RequestParam(value = "supplier") String supplierId,
                           @RequestParam(value = "gross_profit") String gross_profit,
                           @RequestParam(value = "order_unit") String orderUnit,
                           @RequestParam(value = "inventory") String inventory,
                           HttpServletRequest request) {

        Item item = new Item();
        Supplier supplier = suppliersDao.findOne(Integer.parseInt(supplierId));
        Product product = productsDao.findOne(id);
        item.setName(itemName);
        item.setSupplierId(supplier.getId());
        item.setUnitWeight(U.var.parseFloat(weight));
        item.setOrderUnit(Item.OrderUnits.values()[orderUnit.charAt(0) - '0']);

        String zz = String.format("%02d", product.getCategoryId());
        String yyyy = String.format("%04d", supplier.getId());

        Integer check = itemsDao.count(supplier.getId());
        String xx;
        if (check == null) {
            xx = String.format("%02d", 1);

        } else if (itemsDao.count(supplier.getId()) >= 99) {
            xx = String.format("%03d", (itemsDao.count(supplier.getId()) + 1) % 1000);

        } else {
            xx = String.format("%02d", (itemsDao.count(supplier.getId()) + 1) % 100);
        }
        char[] xor = (zz + yyyy + xx).toCharArray();
        int[] arr = new int[xor.length];
        int i = 0;
        //Moving char array to integer array
        for (char a : xor) {
            arr[i] = Integer.parseInt(String.valueOf(xor[i]));
            i++;
        }
        //Xoring the current and next integer and storing in the next element
        for (int k = 0; k < arr.length - 1; k++) {
            arr[k + 1] = arr[k] ^ arr[k + 1];
        }
        //the last xoring score is stored on the last arr element
        Integer N = arr[arr.length - 1] % 10;
        String barcode = zz + "-" + yyyy + "-" + xx + "-" + N.toString();
        item.setBarcode(barcode);
        item.setProductId(id);
        item.setBuyingCost(Float.parseFloat(buying_cost));
        //TODO buying weight
//        item.weigh(Float.parseFloat(weight));
        item.setGrossProfit(Float.parseFloat(gross_profit));
        item.setInventory(Short.parseShort(inventory));
        item.setIsDeleted('0');
        item = itemsDao.save(item);
        WpPostmeta postmeta = wpPostmetaDao.findOneByIdAndKey(U.var.parseLong(product.getWpId()), "_stock");
        postmeta.setMetaValue("" + (U.var.parseInt(postmeta.getMetaValue()) + item.getInventory()));
        postmeta = wpPostmetaDao.save(postmeta);
        wpPostmetaDao.updateByIdAndKey(U.var.parseLong(product.getWpId()), "_stock_status", U.var.parseInt(postmeta.getMetaValue()) > 0 ? "instock" : "outofstock");
        logger.info("SB: " + request.getSession().getAttribute("userName") + ": add item " + item.getId());


        return "redirect:" + "/admin/products";

    }

    public String getUpcABarcode(String barcode) {
        String upca = barcode.replaceAll("\\D+", "");
        if (upca.length() > 11) {
            upca = upca.substring(0, 11);
        } else {
            while (upca.length() < 11) {
                upca = "0" + upca;
            }
        }
        int oddSum = 0;
        int evenSum = 0;
        for (int i = 0; i < 11; i += 2) {
            oddSum += upca.charAt(i) - '0';
        }
        for (int i = 1; i < 10; i += 2) {
            evenSum += upca.charAt(i) - '0';
        }
        int checksum = (10 - ((oddSum * 3 + evenSum) % 10)) % 10;

        upca += checksum;

        return upca;
    }

    @RequestMapping(value = "/admin/products/add", method = RequestMethod.GET)
    public Object productAdd(Model model, HttpServletRequest request) {
        List<Map<String, Object>> categories = new LinkedList<>();
        for (Category category : categoriesDao.getAllCategories()) {
            Map<String, Object> categoriesMap = new HashMap<>();
            categoriesMap.put("name", category.getName());
            categoriesMap.put("id", category.getId());
            categories.add(categoriesMap);
        }

        model.addAttribute("files", listOfImages(request));
        model.addAttribute("categories", categories);
        Map<String, Object> globals = new HashMap<>();
        globals.put("modules_enable_coupons", globalsDao.findOne("modules_enable_coupons").getValue());
        globals.put("modules_enable_invoice", globalsDao.findOne("modules_enable_invoice").getValue());
        globals.put("modules_enable_logistics", globalsDao.findOne("modules_enable_logistics").getValue());
        globals.put("modules_enable_packing", globalsDao.findOne("modules_enable_packing").getValue());
        Object logedWorker = request.getSession().getAttribute("userId");
        Worker worker = workersDao.getOne(Integer.parseInt(logedWorker.toString()));
        globals.put("worker_name", worker.getFirstName() + " " + worker.getLastName());
        model.addAttribute("globals", globals);
        model.addAttribute("admin", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.ADMIN.ordinal() ? 1 : 0);
        return "ProductAddNew";
    }


    public List listOfImages(HttpServletRequest request) {
        File folder = new File(Application.imageRoot);

        File[] listOfFiles = folder.listFiles();
        List<Map<String, Object>> files = new LinkedList<>();
        for (File file : listOfFiles) {
            Map<String, Object> filesMap = new HashMap<>();
            filesMap.put("name", file.getName());
            filesMap.put("path", "https://shookbook.co.il/wp-content/uploads/images/products/" + file.getName());
            files.add(filesMap);
        }
        return files;
    }

    @RequestMapping(value = "/images/", method = RequestMethod.GET)
    public void images(@RequestParam(value = "filename") String filename, HttpServletResponse response) throws IOException {
        response.setContentType("image/jpeg");
        try {
            response.getOutputStream().write(U.var.readFully(new FileInputStream("images/" + filename), 1024));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/admin/products/toadd", method = RequestMethod.POST)
    public Object productsToAdd(@RequestParam(value = "name_heb") String nameHeb,
                                @RequestParam(value = "name") String name,
                                @RequestParam(value = "name_weight") String nameWeight,
                                @RequestParam(value = "unit_type") String unit_type,
                                @RequestParam(value = "weight") String weight,
                                @RequestParam(value = "image_url") String imageUrl,
                                @RequestParam(value = "price") String price,
                                @RequestParam(value = "packing_weight_code") String packing_weight_code,
                                @RequestParam(value = "vat") String vat,
                                @RequestParam(value = "package_type") String package_type,
                                @RequestParam(value = "packing_action") String packing_action,
                                @RequestParam(value = "handling") String handling,
                                @RequestParam(value = "stamp") String stamp,
                                @RequestParam(value = "category_id") String categoryId,
                                @RequestParam(value = "status") String status,
                                @RequestParam(value = "suppliers_names") String suppliers_names,
                                @RequestParam(value = "key_words") String key_words,
                                @RequestParam(value = "info") String info,
                                HttpServletRequest request) {

        Product product = new Product();
        product.setName(name);
        product.setNameHeb(nameHeb);
        product.setNameWeight(nameWeight);
        product.setNameThai("");
        product.setUnitType(Short.parseShort(unit_type));
        product.setUnitWeight(Float.parseFloat(weight));
        product.setImageUrl(imageUrl);
        product.setPrice(Float.parseFloat(price));
        product.setVat(vat.charAt(0));
        product.setPackingWeightCode(Short.parseShort(packing_weight_code));
        product.setPackageType(Product.PackageTypes.valueOf(package_type));
        product.setPackingAction(Product.PackingAction.valueOf(packing_action));
        System.out.format(" $$$ %s%n", handling);
        product.setRefrigerated(Product.Handling.values()[handling.charAt(0) - '0']);
        product.setPackingRule(Product.PackingRules.NONE);
        product.setCategoryId(Integer.parseInt(categoryId));
        product.setStatus(Product.ProductStatuses.values()[status.charAt(0) - '0']);
        product.setStamp(Product.Stamp.values()[stamp.charAt(0) - '0']);
        product.setKeyWords(key_words);
        product.setIsDeleted('0');
        product.setIsMissing('0');
        product.setSupplierNames(suppliers_names);
        if ((info != "") && (info.length() > 0)) {
            product.setInfo(info);
            product.setMoreInfo('1');
        } else {
            product.setInfo("");
            product.setMoreInfo('0');
        }
        product = productsDao.save(product);

        WpPost wpProduct = wpPostsDao.save(new WpPost(product, false));

        product.setWpId(wpProduct.getId().intValue());
        product = productsDao.save(product);

        WpPost wpAttachment = wpPostsDao.save(new WpPost(product, true));

        wpPostmetaDao.save(new WpPostmeta(wpProduct.getId(), "_thumbnail_id", "" + wpAttachment.getId()));
        wpPostmetaDao.save(new WpPostmeta(wpProduct.getId(), "_visibility", "hidden"));
        wpPostmetaDao.save(new WpPostmeta(wpProduct.getId(), "total_sales", "0"));
        wpPostmetaDao.save(new WpPostmeta(wpProduct.getId(), "_regular_price", "" + product.getPrice()));
        wpPostmetaDao.save(new WpPostmeta(wpProduct.getId(), "_price", "" + product.getPrice()));
        wpPostmetaDao.save(new WpPostmeta(wpProduct.getId(), "_manage_stock", "yes"));
        wpPostmetaDao.save(new WpPostmeta(wpProduct.getId(), "_stock", "0"));
        wpPostmetaDao.save(new WpPostmeta(wpProduct.getId(), "_stock_status", "outofstock"));
        if (product.getPackageType() == Product.PackageTypes.UNIT) {
            wpPostmetaDao.save(new WpPostmeta(wpProduct.getId(), "display_price", "₪" + String.format("%.2f", product.getPrice()) + " ליח'"));
        } else if (product.getPackageType() == Product.PackageTypes.PACK) {
            wpPostmetaDao.save(new WpPostmeta(wpProduct.getId(), "display_price", "₪" + String.format("%.2f", product.getPrice()) + " למארז"));
        } else {
            wpPostmetaDao.save(new WpPostmeta(wpProduct.getId(), "display_price", "₪" + String.format("%.2f", product.getPrice()) + " לק\"ג"));
        }
        wpPostmetaDao.save(new WpPostmeta(wpProduct.getId(), "supplier_names", product.getSupplierNames()));
        wpPostmetaDao.save(new WpPostmeta(wpProduct.getId(), "unit_weight", "1"));
        wpPostmetaDao.save(new WpPostmeta(wpProduct.getId(), "sb_name_weight", nameWeight));


        wpPostmetaDao.save(new WpPostmeta(wpAttachment.getId(), "_wp_attached_file", product.getImageUrl()));

        //System.out.format("   %d   %d  %n", product.getWpId(), categoriesDao.findOne(product.getCategoryId()).getWpId());
        wpTermRelationshipsDao.save(new WpTermRelationships(product.getWpId(), categoriesDao.findOne(product.getCategoryId()).getWpId(), 0));

        logger.info("SB: " + request.getSession().getAttribute("userName") + ": add product " + product.getId() + "(" + product.getName() + ")");

        return "redirect:" + "/admin/products";
    }

    @RequestMapping(value = "/admin/products/update", method = RequestMethod.GET)
    public Object productUpdate(@RequestParam(value = "id") Integer id, Model model, HttpServletRequest request) {
        Product product = productsDao.findOne(id);
        List<Map<String, Object>> categories = new LinkedList<>();
        for (Category category : categoriesDao.getAllCategories()) {
            Map<String, Object> categoriesMap = new HashMap<>();
            categoriesMap.put("name", category.getName());
            categoriesMap.put("id", category.getId());
            categories.add(categoriesMap);
        }

        List<Map<String, Object>> columns = new LinkedList<>();
        Map<String, Object> column = new HashMap<>();
        for (int i = 0; i < 4; i++) {
            column.put(Integer.toString(i), i);
        }
        columns.add(column);


        Category category = categoriesDao.findOne(product.getCategoryId());
        model.addAttribute("id", product.getId());
        model.addAttribute("name", product.getName());
        model.addAttribute("name_heb", product.getNameHeb());
        model.addAttribute("name_weight", product.getNameWeight());
        model.addAttribute("name_thai", product.getNameThai());
        model.addAttribute("unit_type", product.getUnitType());
        model.addAttribute("weight", product.getUnitWeight());
        model.addAttribute("image_url", product.getImageUrl());
        model.addAttribute("price", product.getPrice());
        model.addAttribute("units_sold", product.getUnitsSold());
        model.addAttribute("vat", product.getVat());
        model.addAttribute("files", listOfImages(request));
        model.addAttribute("packing_weight_code", product.getPackingWeightCode());
        model.addAttribute("package_type", product.getPackageType().ordinal());
        model.addAttribute("packing_action", product.getPackingAction().ordinal());
        model.addAttribute("categories", categoriesDao.getAllCategories());
        model.addAttribute("category_name", category.getName());
        model.addAttribute("category_id", category.getId());
        model.addAttribute("status", product.getStatus().ordinal());
        model.addAttribute("stamp", product.getStamp().ordinal());
        model.addAttribute("handling", product.getRefrigerated().ordinal());
        model.addAttribute("refregirated", product.getRefrigerated());
        model.addAttribute("suppliers_names", product.getSupplierNames());
        model.addAttribute("key_words", product.getKeyWords());
        model.addAttribute("info", product.getInfo());
        Map<String, Object> globals = new HashMap<>();
        globals.put("modules_enable_coupons", globalsDao.findOne("modules_enable_coupons").getValue());
        globals.put("modules_enable_invoice", globalsDao.findOne("modules_enable_invoice").getValue());
        globals.put("modules_enable_logistics", globalsDao.findOne("modules_enable_logistics").getValue());
        globals.put("modules_enable_packing", globalsDao.findOne("modules_enable_packing").getValue());
        Object logedWorker = request.getSession().getAttribute("userId");
        Worker worker = workersDao.getOne(Integer.parseInt(logedWorker.toString()));
        globals.put("worker_name", worker.getFirstName() + " " + worker.getLastName());
        model.addAttribute("globals", globals);
        model.addAttribute("columns", columns);
        model.addAttribute("admin", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.ADMIN.ordinal() ? 1 : 0);

        return "ProductEdit";
    }

    @RequestMapping(value = "/admin/products/toupdate", method = RequestMethod.POST)
    public Object productToUpdate(@RequestParam(value = "id") Integer id,
                                  @RequestParam(value = "name") String name,
                                  @RequestParam(value = "name_heb") String nameHeb,
                                  @RequestParam(value = "name_weight") String nameWeight,
                                  @RequestParam(value = "unit_type") String unit_type,
                                  @RequestParam(value = "weight") String weight,
                                  @RequestParam(value = "image_url") String imageUrl,
                                  @RequestParam(value = "image_url_selector") String image_url_selector,
                                  @RequestParam(value = "price") String price,
                                  @RequestParam(value = "vat") String vat,
                                  @RequestParam(value = "packing_weight_code") String packing_weight_code,
                                  @RequestParam(value = "package_type") String package_type,
                                  @RequestParam(value = "packing_action") String packing_action,
                                  @RequestParam(value = "status") String status,
                                  @RequestParam(value = "stamp") String stamp,
                                  @RequestParam(value = "handling") String handling,
                                  @RequestParam(value = "suppliers_names") String suppliers_names,
                                  @RequestParam(value = "key_words") String key_words,
                                  @RequestParam(value = "info") String info,
                                  Model model, HttpServletRequest request) {
        //System.out.format("   Name: %s", name);
        Product product = productsDao.findOne(id);
        WpPost wpProduct = wpPostsDao.findOne(U.var.parseLong(product.getWpId()));
        WpPost wpAttachment = wpPostsDao.findOne(U.var.parseLong(wpPostmetaDao.findOneByIdAndKey(wpProduct.getId(), "_thumbnail_id").getMetaValue()));
        product.setNameHeb(nameHeb);
        product.setNameWeight(nameWeight);
        wpProduct.setPostTitle(nameHeb);
        product.setUnitType(Float.parseFloat(unit_type));
        product.setUnitWeight(Float.parseFloat(weight));
        product.setName(name);
        wpProduct.setPostName(name.replace(' ', '_'));
        if (image_url_selector.equals("")) {
            product.setImageUrl(imageUrl);

        } else {
            product.setImageUrl(image_url_selector);
        }
        wpAttachment.setGuid(product.getImageUrl());
        wpPostmetaDao.updateByIdAndKey(wpAttachment.getId(), "_wp_attached_file",product.getImageUrl());
        product.setPrice(Float.parseFloat(price));
        wpPostmetaDao.updateByIdAndKey(wpProduct.getId(), "_regular_price", "" + product.getPrice());
        wpPostmetaDao.updateByIdAndKey(wpProduct.getId(), "_price", "" + product.getPrice());
        product.setVat(vat.charAt(0));
        product.setPackageType(Product.PackageTypes.valueOf(package_type));
        product.setPackingAction(Product.PackingAction.valueOf(packing_action));
        product.setPackingWeightCode(Short.parseShort(packing_weight_code));
        int stock = 0;
        if (product.getStatus() == Product.ProductStatuses.OUT_OF_STOCK && status.charAt(0) - '0' > 0) {
            for (Item item : itemsDao.findAllByProductIdWithParam(product.getId())) {
                stock += item.getInventory();
            }
            wpPostmetaDao.updateByIdAndKey(U.var.parseLong(product.getWpId()), "_stock", "" + stock);
        }
        product.setStatus(Product.ProductStatuses.values()[status.charAt(0) - '0']);
        wpPostmetaDao.updateByIdAndKey(wpProduct.getId(), "_visibility", product.getStatus() == Product.ProductStatuses.OUT_OF_STOCK ? "hidden" : "visible");
        wpPostmetaDao.updateByIdAndKey(U.var.parseLong(product.getWpId()), "_stock_status", stock > 0 ? "instock" : "outofstock");

        product.setStamp(Product.Stamp.values()[stamp.charAt(0) - '0']);
        product.setRefrigerated(Product.Handling.values()[handling.charAt(0) - '0']);
        product.setKeyWords(key_words);
        wpProduct.setPostExcerpt(key_words);
        product.setSupplierNames(suppliers_names);
        wpPostmetaDao.updateByIdAndKey(wpProduct.getId(), "supplier_names", suppliers_names);

        if (product.getPackageType() == Product.PackageTypes.UNIT) {
            wpPostmetaDao.updateByIdAndKey(wpProduct.getId(), "display_price", "₪" + String.format("%.2f", product.getPrice()) + " ליח'");
        } else if (product.getPackageType() == Product.PackageTypes.PACK) {
            wpPostmetaDao.updateByIdAndKey(wpProduct.getId(), "display_price", "₪" + String.format("%.2f", product.getPrice()) + " למארז");
        } else {
            wpPostmetaDao.updateByIdAndKey(wpProduct.getId(), "display_price", "₪" + String.format("%.2f", product.getPrice()) + " לק\"ג");
        }

        wpPostmetaDao.updateByIdAndKey(wpProduct.getId(), "sb_name_weight", nameWeight);
        //System.out.format("   %d   %s%n",wpProduct.getId(), nameWeight);
        if ((info != "") && (info.length() > 0)) {
            product.setInfo(info);
            product.setMoreInfo('1');
        } else {
            product.setMoreInfo('0');
        }

        product = productsDao.save(product);
        wpProduct = wpPostsDao.save(wpProduct);
        wpAttachment = wpPostsDao.save(wpAttachment);
        logger.info("SB: " + request.getSession().getAttribute("userName") + ": update product " + product.getId() + "(" + product.getName() + ")");

        return "redirect:" + "/admin/products";
    }


    @RequestMapping(value = "/admin/products/remove", method = RequestMethod.POST)
    public String productRemove(@RequestParam(value = "id") Integer id,
                                Model model, HttpServletRequest request) {
        Product product = productsDao.findOne(id);
        product.setIsDeleted('1');
        List<Item> items = itemsDao.findAllByProductIdWithParam(id);
        for (Item item : items) {
            item.setIsDeleted('1');
            itemsDao.save(item);
        }
        productsDao.save(product);
        wpPostmetaDao.updateByIdAndKey(U.var.parseLong(product.getWpId()), "_visibility", "hidden");
        wpPostmetaDao.updateByIdAndKey(U.var.parseLong(product.getWpId()), "_stock", "0");
        wpPostmetaDao.updateByIdAndKey(U.var.parseLong(product.getWpId()), "_stock_status", "outofstock");

        logger.info("SB: " + request.getSession().getAttribute("userName") + ": remove product " + product.getId() + "(" + product.getName() + ")");

        return "redirect:" + "/admin/products";
    }

    @RequestMapping(value = "/admin/items/remove", method = RequestMethod.POST)
    public String remove(@RequestParam(value = "id") Integer id,
                         Model model, HttpServletRequest request) {
        Item item = itemsDao.findOne(id);
        item.setIsDeleted('1');
        item = itemsDao.save(item);

        Product product = productsDao.findOne(item.getProductId());
        WpPostmeta postmeta = wpPostmetaDao.findOneByIdAndKey(U.var.parseLong(product.getWpId()), "_stock");
        postmeta.setMetaValue("" + (U.var.parseInt(postmeta.getMetaValue()) - item.getInventory()));
        postmeta = wpPostmetaDao.save(postmeta);
        wpPostmetaDao.updateByIdAndKey(U.var.parseLong(product.getWpId()), "_stock_status", U.var.parseInt(postmeta.getMetaValue()) > 0 ? "instock" : "outofstock");

        logger.info("SB: " + request.getSession().getAttribute("userName") + ": remove item " + item.getId());


        return "redirect:" + "/admin/products";
    }

    public static void resize(String inputImagePath,
                              String outputImagePath, int scaledWidth, int scaledHeight)
            throws IOException {
        // reads input image
        File inputFile = new File(inputImagePath);
        BufferedImage inputImage = ImageIO.read(inputFile);

        // creates output image
        BufferedImage outputImage = new BufferedImage(scaledWidth,
                scaledHeight, inputImage.getType());

        // scales the input image to the output image
        Graphics2D g2d = outputImage.createGraphics();
        g2d.drawImage(inputImage, 0, 0, scaledWidth, scaledHeight, null);
        g2d.dispose();

        // extracts extension of output file
        String formatName = outputImagePath.substring(outputImagePath
                .lastIndexOf(".") + 1);

        // writes to output file
        ImageIO.write(outputImage, formatName, new File(outputImagePath));
    }

    public static void resize(String inputImagePath,
                              String outputImagePath, double percent) throws IOException {
        File inputFile = new File(inputImagePath);
        BufferedImage inputImage = ImageIO.read(inputFile);
        int scaledWidth = (int) (inputImage.getWidth() * percent);
        int scaledHeight = (int) (inputImage.getHeight() * percent);
        resize(inputImagePath, outputImagePath, scaledWidth, scaledHeight);
    }

    @ResponseBody
    @RequestMapping(value = "/admin/products/csv/export", method = RequestMethod.GET)
    public Object csvCouponExportGroups(Model model, HttpServletResponse response) {

        List<Product> products = productsDao.findAll();

        StringBuilder stringBuilder = new StringBuilder("\n");
        stringBuilder.append("Id").append(',').append("Name").append(',').append("Status").append(',').append("Weight code").append(',').append("Category").append(',').append("Price").append(',').append("VAT").append("\n");

        for (Product product : products) {
            stringBuilder.append('"').append(product.getId()).append('"').append(',').append('"').append(product.getNameHeb()).append('"').append(',');
            stringBuilder.append('"').append(product.getStatus()).append('"').append(',').append('"').append(product.getPackingWeightCode()).append('"').append(',');
            stringBuilder.append('"').append(product.getCategoryId()).append('"').append(',').append('"').append(product.getPrice()).append('"').append(',').append('"').append(product.getVat()).append('"').append("\n");
            List<Item> items = itemsDao.findAllByProductIdWithParam(product.getId());
            for (Item item : items) {
                stringBuilder.append(',').append('"').append(item.getBarcode()).append('"').append(',');
                stringBuilder.append(',').append('"').append(item.getInventory()).append('"').append(',');
                stringBuilder.append(',').append('"').append(item.getBuyingCost()).append('"').append('\n');

            }
        }

        response.setHeader("Content-Disposition", "attachment; filename=\"product.csv\"");
        response.setContentLength(stringBuilder.length());

        response.setContentType("text/csv; charset=");
        return stringBuilder.toString();
    }

    @RequestMapping(method = RequestMethod.POST, value = "/admin/products/csv/import")
    public String csvProductsImport(@RequestParam("file") MultipartFile[] fileUpload, RedirectAttributes redirectAttributes) {
        BufferedReader br = null;
        if (fileUpload != null && fileUpload.length > 0) {
            for (MultipartFile file : fileUpload) {

                if (file.getOriginalFilename().contains("/")) {
                    redirectAttributes.addFlashAttribute("message", "Folder separators not allowed");
                    return "redirect:upload";
                }
                if (file.getOriginalFilename().contains("/")) {
                    redirectAttributes.addFlashAttribute("message", "Relative pathnames not allowed");
                    return "redirect:upload";
                }

                if (!file.isEmpty()) {
                    List<DeliveryDate> deliveryDates = new LinkedList<>();
                    try {
                        BufferedOutputStream stream = new BufferedOutputStream(
                                new FileOutputStream(new File(Application.csvRoot + "/" + file.getOriginalFilename())));
                        FileCopyUtils.copy(file.getInputStream(), stream);
                        stream.close();
                        redirectAttributes.addFlashAttribute("message",
                                "You successfully uploaded " + file.getOriginalFilename() + "!");

                        String csvFile = Application.csvRoot + "/" + file.getOriginalFilename();
                        String line = "";
                        String cvsSplitBy = ",";

                        br = new BufferedReader(new InputStreamReader(new FileInputStream(csvFile), "UTF-8"));

                        boolean ignoreFirstLine = true;
                        Item item;

                        while ((line = br.readLine()) != null) {
                            //Ignore csv header line
                            if (!ignoreFirstLine) {

                                // use comma as separator
                                String[] itemData = line.split(cvsSplitBy);
                                //put in map the order id + dhl number

                                if (itemData[0] != "") {
                                    item = itemsDao.findOneByBarcode(itemData[0]);
                                    item.setInventory(Short.parseShort(itemData[1]));
                                    item.setBuyingCost(Float.parseFloat(itemData[2]));
                                    //System.out.format("   code: %s    qty: %d   price: %f%n", item.getBarcode(), item.getInventory(), item.getBuyingCost());
                                    itemsDao.save(item);
                                }

                            }
                            ignoreFirstLine = false;
                        }
                    } catch (Exception ex) {
                        redirectAttributes.addFlashAttribute("message",
                                "You failed to upload " + file.getOriginalFilename() + " => " + ex.getMessage());
                    }
                } else {
                    redirectAttributes.addFlashAttribute("message",
                            "You failed to upload " + file.getOriginalFilename() + " because the file was empty");
                }
            }
        }
        return "redirect:" + "/admin/products";
    }

}
