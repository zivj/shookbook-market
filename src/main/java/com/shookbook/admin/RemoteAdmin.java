package com.shookbook.admin;

import com.shookbook.dao.shookbook.*;
import com.shookbook.dao.wordpress.WpOrderItemsDao;
import com.shookbook.dao.wordpress.WpPostmetaDao;
import com.shookbook.dao.wordpress.WpPostsDao;
import com.shookbook.dao.wordpress.WpUsermetaDao;
import com.shookbook.entities.shookbook.Customer;
import com.shookbook.entities.shookbook.Order;
import com.shookbook.utils.U;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@SessionAttributes("thought")
public class RemoteAdmin {

    private static final Logger logger = Logger.getLogger(CouponsAdmin.class);

    @Autowired
    private Common common;
    @Autowired
    private CustomersDao customersDao;
    @Autowired
    private CouponsDao couponsDao;
    @Autowired
    private PointsDao pointsDao;
    @Autowired
    private OrdersDao ordersDao;
    @Autowired
    private RefundsDao refundsDao;
    @Autowired
    private WpPostsDao wpPostsDao;
    @Autowired
    private WpPostmetaDao wpPostmetaDao;
    @Autowired
    private WpUsermetaDao wpUsermetaDao;
    @Autowired
    private WpOrderItemsDao wpOrderItemsDao;
    @Autowired
    private WorkersDao workersDao;
    @Autowired
    private GlobalsDao globalsDao;

    @RequestMapping(value = "/admin/remote/remoteGetKey", method = RequestMethod.GET)
    public Object remoteGetKey(@RequestParam(value = "wpOrderId") Integer wpOrderId,
                          @RequestParam(value = "wpCustomerId") Integer wpCustomerId,
                          Model model, HttpServletRequest request) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

        Calendar today = Calendar.getInstance();
        today.set(Calendar.HOUR_OF_DAY, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);
        today.set(Calendar.MILLISECOND, 0);

        Integer time = U.var.parseInt(today.getTime().getTime()/1000);
        System.out.format("   ### Date: %d%n", time);
        Integer key = ((time * wpCustomerId - (wpOrderId % wpCustomerId)) + (wpOrderId * time / wpCustomerId)) % 175392;
        System.out.format("   ### Remote key: %d%n", key);

        List<Map<String, Object>> respondKeys = new LinkedList<>();
        Map<String, Object> respondKey = new HashMap<>();
        respondKey.put("key", "key");
        respondKey.put("value", key);
        respondKeys.add(respondKey);

        model.addAttribute("keys", respondKeys);
        return "RemoteRespond";

    }

    @RequestMapping(value = "/admin/remote/orders/createRefund", method = RequestMethod.GET)
    public Object createRefund(@RequestParam(value = "key") Integer key,
                               @RequestParam(value = "wpOrderId") Integer wpOrderId,
                               @RequestParam(value = "wpCustomerId") Integer wpCustomerId,
                               @RequestParam(value = "wpProductId") Integer wpProductId,
                               @RequestParam(value = "qty") Integer qty,
                               Model model, HttpServletRequest request) {

        Integer internalKey = getKey(wpOrderId, wpCustomerId);
        System.out.format("   Key: %d%n", internalKey);
        Integer status = -99;
        List<Map<String, Object>> respondKeys = new LinkedList<>();
        if (internalKey == U.var.parseInt(key)) {
            Order order = ordersDao.findOneByWpID(wpOrderId);
            Customer customer = customersDao.findOne(order.getCustomerId());
            System.out.format("   Order: %d   Customer: %d%n", wpOrderId, wpCustomerId);
            if (customer.getWpId() == wpCustomerId) {
                status = 0;
            } else {
                status = -2;
            }
        } else {
            status = -1;
        }

        Map<String, Object> respondKey = new HashMap<>();
        respondKey.put("key", "status");
        respondKey.put("value", status);
        respondKeys.add(respondKey);
        model.addAttribute("keys", respondKeys);
        return "RemoteRespond";

    }

    public Integer getKey(Integer wpOrderId, Integer wpCustomerId) {

        Calendar today = Calendar.getInstance();
        today.set(Calendar.HOUR_OF_DAY, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);
        today.set(Calendar.MILLISECOND, 0);

        Integer time = U.var.parseInt(today.getTime().getTime()/1000);
        Integer key = ((time * wpCustomerId - (wpOrderId % wpCustomerId)) + (wpOrderId * time / wpCustomerId)) % 175392;

        return key;

    }
}
