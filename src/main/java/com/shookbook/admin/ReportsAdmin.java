package com.shookbook.admin;

import com.shookbook.dao.shookbook.*;
import com.shookbook.entities.shookbook.*;
import com.shookbook.types.HashSetIgnoresNulls;
import com.shookbook.utils.U;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.*;
import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@SessionAttributes("thought")
public class ReportsAdmin {

    private static final Logger logger = Logger.getLogger(ReportsAdmin.class);
    @Autowired
    private Common common;
    @Autowired
    private ProductsDao productsDao;
    @Autowired
    private CustomersDao customersDao;
    @Autowired
    private GeoPoinsDao geoPoinsDao;
    @Autowired
    private OrdersDao ordersDao;
    @Autowired
    private OrderProductDao orderProductDao;
    @Autowired
    private ItemsDao itemsDao;
    @Autowired
    private RefundsDao refundsDao;
    @Autowired
    private CouponsDao couponsDao;
    @Autowired
    private SuppliersDao suppliersDao;
    @Autowired
    private SupplierOrdersDao supplierOrdersDao;
    @Autowired
    private SupplierOrderItemDao supplierOrderItemDao;
    @Autowired
    private WorkersDao workersDao;
    @Autowired
    private GlobalsDao globalsDao;
    @Autowired
    private CategoriesDao categoriesDao;
    @Autowired
    private CityGroupsDoa cityGroupsDoa;
    @Autowired
    private RouteStopDao routeStopDao;
    @Autowired
    private RoutesDao routesDao;

    @RequestMapping(value = "/admin/reports", method = RequestMethod.GET)
    public Object reportMenu(Model model, HttpServletRequest request) {

        if (!U.var.parseBoolean(request.getSession().getAttribute("login"))) {
            return "redirect:/admin/login";
        }
        return "ReportMenu";
    }


    @RequestMapping(value = "/admin/ReportsHub", method = RequestMethod.GET)
    public Object reportHub(@RequestParam(value = "primary") Integer reportType,
                            @RequestParam(value = "secondary") Integer reportSubype,
                            Model model, HttpServletRequest request) {
        if (!U.var.parseBoolean(request.getSession().getAttribute("login"))) {
            return "redirect:/admin/login";
        }

        List<Map<String, Object>> customerList = new LinkedList<>();
        for (Customer customer : customersDao.findAll()) {
            Map<String, Object> column = new HashMap<>();
            column.put("name", customer.getFirstName() + " " + customer.getLastName());
            column.put("barcode", customer.getBarcode());
            column.put("id", customer.getId());
            customerList.add(column);
        }
        model.addAttribute("customerList", customerList);

        List<Map<String, Object>> supplierList = new LinkedList<>();
        for (Supplier supplier : suppliersDao.findAll()) {
            Map<String, Object> supplierColumn = new HashMap<>();
            supplierColumn.put("name", supplier.getName());
            supplierColumn.put("id", supplier.getId());
            supplierList.add(supplierColumn);
        }
        model.addAttribute("supplierList", supplierList);

        List<Map<String, Object>> productList = new LinkedList<>();
        for (Product product : productsDao.findAllValid()) {
            Map<String, Object> productColumn = new HashMap<>();
            productColumn.put("name", product.getNameHeb());
            productColumn.put("id", product.getId());
            productList.add(productColumn);
        }
        model.addAttribute("productList", productList);

        List<Map<String, Object>> categoryList = new LinkedList<>();
        for (Category category : categoriesDao.findAll()) {
            Map<String, Object> categoryColumn = new HashMap<>();
            categoryColumn.put("name", category.getName());
            categoryColumn.put("id", category.getId());
            categoryList.add(categoryColumn);
        }
        model.addAttribute("categoryList", categoryList);

        model.addAttribute("primary", reportType);
        model.addAttribute("secondary", reportSubype);
        Map<String, Object> globals = new HashMap<>();
        globals.put("modules_enable_coupons", globalsDao.findOne("modules_enable_coupons").getValue());
        globals.put("modules_enable_invoice", globalsDao.findOne("modules_enable_invoice").getValue());
        globals.put("modules_enable_logistics", globalsDao.findOne("modules_enable_logistics").getValue());
        globals.put("modules_enable_packing", globalsDao.findOne("modules_enable_packing").getValue());
        Object logedWorker = request.getSession().getAttribute("userId");
        Worker worker = workersDao.getOne(Integer.parseInt(logedWorker.toString()));
        globals.put("worker_name", worker.getFirstName() + " " + worker.getLastName());
        model.addAttribute("globals", globals);
        model.addAttribute("admin", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.ADMIN.ordinal() ? 1 : 0);

        return "ReportsHub";
    }

    @ResponseBody
    @RequestMapping(value = "/admin/reports/generateReport", method = RequestMethod.POST)
    public Object generateReport(@RequestParam(value = "customer_start_date", required = false) String customerStartDate,
                                 @RequestParam(value = "packing_start_date", required = false) String packingStartDate,
                                 @RequestParam(value = "customer_orders_start_date", required = false) String customerOrdersStartDate,
                                 @RequestParam(value = "supplier_orders_start_date", required = false) String supplierOrdersStartDate,
                                 @RequestParam(value = "logistics_start_date", required = false) String logisticsStartDate,
                                 @RequestParam(value = "predictions_start_date", required = false) String predictionsStartDate,
                                 @RequestParam(value = "customer_end_date", required = false) String customerEndDate,
                                 @RequestParam(value = "customer_orders_end_date", required = false) String customerOrdersEndDate,
                                 @RequestParam(value = "supplier_orders_end_date", required = false) String supplierOrdersEndDate,
                                 @RequestParam(value = "supplier_orders_supplier_id", required = false) Integer soSupplierId,
                                 @RequestParam(value = "products_start_date", required = false) String productsStartDate,
                                 @RequestParam(value = "products_end_date", required = false) String productsEndDate,
                                 @RequestParam(value = "packing_end_date", required = false) String packingEndDate,
                                 @RequestParam(value = "logistics_end_date", required = false) String logisticsEndDate,
                                 @RequestParam(value = "logistics_delivery_window", required = false) Integer deliveryWindow,
                                 @RequestParam(value = "predictions_end_date", required = false) String predictionsEndDate,
                                 @RequestParam(value = "predictions_supplier_id", required = false) Integer supplierId,
                                 @RequestParam(value = "predictions_category_id", required = false) Integer categoryId,
                                 @RequestParam(value = "packing_product_id", required = false) Integer packingProductId,
                                 @RequestParam(value = "products_product_id", required = false) Integer productsProductId,
                                 @RequestParam(value = "primary") Integer reportType,
                                 @RequestParam(value = "customers_secondary") Integer customersReportSubtype,
                                 @RequestParam(value = "customer_orders_secondary") Integer customerOrdersReportSubtype,
                                 @RequestParam(value = "supplier_orders_secondary") Integer supplierOrdersReportSubtype,
                                 @RequestParam(value = "products_secondary") Integer productsReportSubtype,
                                 @RequestParam(value = "packing_secondary") Integer packingReportSubtype,
                                 @RequestParam(value = "logistics_secondary") Integer logisticsReportSubtype,
                                 @RequestParam(value = "predictions_secondary") Integer predictionsReportSubtype,
                                 @RequestParam(value = "report_method") Integer reportMethod,
                                 @RequestParam(value = "email", required = false) String email, HttpServletRequest request, HttpServletResponse response) {

        Integer fileType = 0; // Default to Excel
        String startDate;
        String endDate;
        Integer reportSubtype;
        int excelLayout = 0;

        switch (reportType) {
            case 0: // Customer orders
                startDate = customerOrdersStartDate;
                endDate = customerOrdersEndDate;
                reportSubtype = customerOrdersReportSubtype;
                break;
            case 1: // Supplier orders
                startDate = supplierOrdersStartDate;
                endDate = supplierOrdersEndDate;
                reportSubtype = supplierOrdersReportSubtype;
                break;
            case 2: // Customers
                startDate = customerStartDate;
                endDate = customerEndDate;
                reportSubtype = customersReportSubtype;
                break;
            case 5: // Products
                startDate = productsStartDate;
                endDate = productsEndDate;
                reportSubtype = productsReportSubtype;
                break;
            case 6: // Packing
                startDate = packingStartDate;
                endDate = packingEndDate;
                reportSubtype = packingReportSubtype;
                break;
            case 7: // Logistics
                startDate = logisticsStartDate;
                endDate = logisticsEndDate;
                reportSubtype = logisticsReportSubtype;
                break;
            case 8: // Predictions
                startDate = predictionsStartDate;
                endDate = predictionsEndDate;
                reportSubtype = predictionsReportSubtype;
                break;
            default:
                startDate = customerOrdersStartDate;
                endDate = customerOrdersEndDate;
                reportSubtype = customerOrdersReportSubtype;
                break;
        }

        Date start = null, end = null;

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

        switch (reportType) {
            case 5: // Products
                try {
                   if (startDate == "") {
                      start = dateFormat.parse("01/01/1970");
                   } else {
                      start = dateFormat.parse(startDate);
                   }

                   if (endDate == "") {
                      end = dateFormat.parse("01/01/2100");
                   } else {
                      end = dateFormat.parse(endDate);
                   }
                } catch (ParseException e) {
                   e.printStackTrace();
                }

                if (start.before(end)) {
                } else {
                    end = start;
                }
                break;
            case 6: // Packing
                try {
                    if (startDate == "") {
                        start = dateFormat.parse(new SimpleDateFormat("MM/dd/yyyy").format(new Date()));
                    } else {
                        start = dateFormat.parse(startDate);
                    }

                    if (endDate == "") {
                        DateTime tomorrow = new DateTime(start).plusDays(1);
                        end = dateFormat.parse(new SimpleDateFormat("MM/dd/yyyy").format(tomorrow.toDate()));
                    } else {
                        end = dateFormat.parse(endDate);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                if (start.before(end)) {

                } else {
                    end = start;
                }
                break;
            default: try {
                if (startDate == "") {
                    start = dateFormat.parse("01/01/1970");
                } else {
                    start = dateFormat.parse(startDate);
                }

                if (endDate == "") {
                    end = dateFormat.parse(new SimpleDateFormat("MM/dd/yyyy").format(new Date()));
                } else {
                    end = dateFormat.parse(endDate);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }

                if (start.before(end)) {

                } else {
                    end = start;
                }
                break;
        }

        dateFormat.applyPattern("yyyy-MM-dd");
        endDate = dateFormat.format(end);
        startDate = dateFormat.format(start);

        ExcelStructure excelStructure;

        switch (reportType) {
            case 0: // Customer orders
                excelStructure = generateCustomerOrdersReport(reportSubtype, start, end, request);
                break;
            case 1: // Supplier orders
                excelStructure = generateSupplierOrdersReport(reportSubtype, start, end, soSupplierId, request);
                break;
            case 2: // Customers
                excelStructure = generateCustomerReport(reportSubtype, start, end, "0", request);
                break;
            case 5: // Products
                excelStructure = generateProductsReport(reportSubtype, start, end, productsProductId, request);
                excelStructure.header = new String[]{"מספר סידורי", "תאריך משלוח", "שם לקוח", "מספר הזמנה", "מספר שילוח", "אורז", "מספר קופון", "תאור", "כמות", "סכום", "סיבה", "סוג זיכוי", "עובד מבצע", "תאריך"};
                break;
            case 6: // Packing
                switch (reportSubtype) {
                    case 0: //
                        excelStructure = generatePackingReport(reportSubtype, start, end, 0, request);
                        excelStructure.header = new String[]{"מספר סידורי", "תאור מוצר", "קטגוריה", "סוג אריזה", "קוד משקל"};
                        break;
                    case 1: // Weight code
                        excelStructure = generatePackingReport(reportSubtype, start, end, 0, request);
                        excelStructure.header = new String[]{"תאור מוצר", "קוד משקל", "כמות מוזמנת"};
                        break;
                    case 5: // Quantization
                        excelStructure = generatePackingReport(reportSubtype, start, end, 0, request);
                        excelStructure.header = new String[]{"תאור מוצר", "כמות מוזמנת"};
                        break;
                    case 2:
                        excelStructure = generatePackingReport(reportSubtype, start, end, 0, request);
                        break;
                    case 3:
                        excelStructure = generatePackingReport(reportSubtype, start, end, packingProductId, request);
                        break;
                    default:
                        excelStructure = new ExcelStructure();
                        excelStructure.header = new String[]{""};
                        break;
                }
                break;
            case 7: // Logistics
                excelStructure = generateLogisticsReport(reportSubtype, start, end, deliveryWindow, request);
                switch (reportSubtype) {
                    case 0: // Short DHL report
                        excelStructure.title = new String[]{"דוח דהל"};
                        excelStructure.header = new String[]{"מספר מזהה", "מספר חבר", "טלפון נוסף", "טלפון", "קוד שער", "דירה", "ישוב", "רחוב", "שם משפחה", "שם פרטי", "מספר הזמנה"};
                        break;
                    case 1: // Long DHL report
                        excelStructure.title = new String[]{"דוח דהל מפורט"};
                        excelStructure.header = new String[]{"מספר מזהה", "מספר חבר", "טלפון נוסף", "טלפון", "קוד שער", "דירה", "ישוב", "רחוב", "שם משפחה", "שם פרטי", "מספר הזמנה", "הערות הזמנה", "הערות לקוח"};
                        break;
                    case 2: // DHL format report
                        excelStructure.header = new String[]{"מספר דרישה", "תאריך אספקה", "סוג דרישה", "סוג הזמנה", "עדיפות", "מסלול", "ס. תכנון", "ס. הובלה", "מרכז", "נקוד", "קוד חברה", "מספר לקוח", "סוג לקוח", "שם לקוח", "קבוצת לקוח", "קוד אתר", "שם אתר", "שם איש קשר 2", "רחוב", "מספר בית", "הערות", "עיר", "מיקוד", "טלפון 1", "טלפון 2", "זמן שירות", "מספר יחידות", "משטחים", "משקל", "נפח", "ערך", "הערות", "הערות לקוח"};
                        break;

                    case 3: // Boxes report
                        excelStructure.header = new String[]{"מספר הזמנה", "אזור חלוקה", "מספר חבילה", "מספר ארגזים", "תאריך משלוח"};
                        break;
                    case 4: // Boxes report filtered
                        excelStructure.header = new String[]{"מספר הזמנה", "אזור חלוקה", "מספר חבילה", "מספר ארגזים", "תאריך משלוח"};
                        break;
                    case 5: // Boxes report daily
                        excelStructure.header = new String[]{"מספר הזמנה", "אזור חלוקה", "מספר חבילה", "מספר ארגזים", "תאריך משלוח"};
                        break;
                    case 6: // Mapotempo format report
                    case 8:
                        fileType = 1;
                        excelStructure.header = new String[]{"reference", "name", "street", "detail", "postalcode", "city", "country", "lat", "lng", "geocoding accuracy", "geocoding level", "comment", "phone", "tags", "without visit", "reference visit", "duration", "open 1", "close 1", "open 2", "close 2", "priority", "tags visit", "quantity[number of stops]", "operation[number of stops]", "quantity[Palets]", "operation[Palets]", "quantity[Unit]", "operation[Unit]", "quantity[Weight]", "operation[Weight]"};
                        break;
                    case 7: // Drivers report
                        excelStructure.header = new String[]{"מספר אריזה", "שם", "כתובת", "ישוב", "דירה", "קוד כניסה", "טלפון", "טלפון נוסף", "הערות", "מספר הזמנה"};
                        excelLayout = 1;
                        break;
                    default: // Should not be here
                        excelStructure.title = new String[]{""};
                        excelStructure.header = new String[]{""};
                        break;
                }
                break;
            case 8: // Logistics
                excelStructure = generatePredictionsReport(reportSubtype, start, end, supplierId, categoryId, request);
                switch (reportSubtype) {
                    case 0: // Buying report by supplier
                        excelStructure.header = new String[]{"תאור", "קוד משקל", "יחידת משקל", "כמות", "משקל", "מחיר מכירה", "מספר הזמנות"};
                        break;
                    case 1: // Selling report
                        excelStructure.header = new String[]{"תאור", "קוד משקל", "יחידת משקל", "כמות", "משקל", "מחיר מכירה", "מספר הזמנות"};
                        break;
                    default:
                        excelStructure.header = new String[]{""};
                        break;
                }
                break;
            default:
                excelStructure = new ExcelStructure();
                excelStructure.header = new String[]{""};
                excelStructure.content = new Object[1][1];
                break;
        }

//            model.addAttribute("primary", reportType);
//            model.addAttribute("secondary", reportSubtype);
//            return "redirect:" + "/admin/ReportsHub?primary=" + reportType + "&secondary=" + reportSubtype;


        byte[] excel = generateExcelFile(excelStructure, excelLayout);
        if (reportMethod == 0) {
            if (fileType == 0) { // Excel
                response.setHeader("Content-Disposition", "attachment; filename=\"" + excelStructure.filename + "\"");
                response.setContentLength(excel.length);

                response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                return excel;
            } else { // CSV
                StringBuilder stringBuilder = new StringBuilder();
                if (excelStructure.header.length > 0) {
                    for (int i = 0; i < excelStructure.header.length - 1; i++) {
                        stringBuilder.append(excelStructure.header[i]).append(",");
                    }
                    stringBuilder.append(excelStructure.header[excelStructure.header.length - 1]).append("\n");
                }

                if (excelStructure.content.length > 0) {
                    for (int i = 0; i < excelStructure.content.length; i++) {
                        for (int j = 0; j < excelStructure.content[i].length -1; j++) {
                            String outputString = excelStructure.content[i][j].toString();
                            boolean fix = false;
                            if (outputString.indexOf('"') >= 0) {
                                fix = true;
                                outputString = outputString.replaceAll("\"","\"\"");
                            }
                            if (outputString.indexOf(',') >= 0) {
                                fix = true;
                            }
                            if (fix) {
                                outputString = "\"" + outputString + "\"";
                            }
                            stringBuilder.append(outputString).append(",");
                        }
                        String outputString = excelStructure.content[i][excelStructure.content[i].length - 1].toString();
                        boolean fix = false;
                        if (outputString.indexOf('"') >= 0) {
                            fix = true;
                            outputString.replaceAll("\"","\"\"");
                        }
                        if (outputString.indexOf(',') >= 0) {
                            fix = true;
                        }
                        if (fix) {
                            outputString = "\"" + outputString + "\"";
                        }
                        stringBuilder.append(outputString).append(",");
                        stringBuilder.append(outputString).append("\n");
                    }
                }

                response.setHeader("Content-Disposition", "attachment; filename=\""+ excelStructure.filename +"\"");
                response.setContentLength(stringBuilder.length());
                response.setContentType("text/csv");
                return stringBuilder.toString();
            }
        } else {
            String subject;
            String html;
            switch (reportType) {
                case 0:
                    subject = "שוקבוק - דוח הזמנת לקוחות";
                    html = "דוח הזמנת לקוחות מצורף";
                    break;
                case 2:
                    subject = "שוקבוק - דוח לקוחות";
                    html = "דוח לקוחות מצורף";
                    break;
                case 6:
                    subject = "שוקבוק - דוח אריזה";
                    html = "דוח אריזה מצורף";
                    break;
                case 7:
                    subject = "שוקבוק - דוח משלוח";
                    html = "דוח משלוח מצורף";
                    break;
                case 8:
                    subject = "שוקבוק - דוח תחזית";
                    html = "דוח תחזית מצורף";
                    break;
                default:
                    subject = "שוקבוק - סוג דוח לא נבחר";
                    html = "לא נמצא דוח לצירוף";
                    break;
            }
            common.sendEmailWithAttachment("customer_support", email, subject, html, excel, excelStructure.filename, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

            return null;
        }
    }

    private static class ExcelStructure {
        String[] header;
        String[] title;
        String filename = "excel.xlsx";
        Object[][] content;
    }

    @RequestMapping(value = "/admin/reports/reportResults", method = RequestMethod.GET)
    public Object reportResults(@RequestParam(value = "start_date", required = false) String startDate,
                                @RequestParam(value = "end_date", required = false) String endDate,
                                @RequestParam(value = "delivery_window", required = false) Integer deliveryWindow,
                                @RequestParam(value = "supplier_id", required = false) Integer supplierId,
                                @RequestParam(value = "category_id", required = false) Integer categoryId,
                                @RequestParam(value = "product_id", required = false) Integer productId,
                                @RequestParam(value = "primary") Integer reportType,
                                @RequestParam(value = "secondary") Integer reportSubtype,
                                @RequestParam(value = "customer_barcode", required = false) String customerBarcode,
                                Model model, HttpServletRequest request) {

        if (customerBarcode == null) {
            customerBarcode = "0";
        }
        Date start = null, end = null;

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

        switch (reportType) {
            case 5: // Products
                try {
                    if (startDate == "") {
                        start = dateFormat.parse("01/01/1970");
                    } else {
                        start = dateFormat.parse(startDate);
                    }

                    if (endDate == "") {
                        end = dateFormat.parse("01/01/2100");
                    } else {
                        end = dateFormat.parse(endDate);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                if (start.before(end)) {
                } else {
                    end = start;
                }
                break;
            case 6: // Packing
                    try {
                       if (startDate == "") {
                           start = dateFormat.parse(new SimpleDateFormat("MM/dd/yyyy").format(new Date()));
                       } else {
                           start = dateFormat.parse(startDate);
                       }

                       if (endDate == "") {
                           DateTime tomorrow = new DateTime(start).plusDays(1);
                           end = dateFormat.parse(new SimpleDateFormat("MM/dd/yyyy").format(tomorrow.toDate()));
                       } else {
                           end = dateFormat.parse(endDate);
                       }
                   } catch (ParseException e) {
                       e.printStackTrace();
                   }

                   if (start.before(end)) {

                   } else {
                       end = start;
                   }
                   break;
            default: try {
                        if (startDate == "") {
                           start = dateFormat.parse("01/01/1970");
                        } else {
                           start = dateFormat.parse(startDate);
                        }

                        if (endDate == "") {
                           end = dateFormat.parse(new SimpleDateFormat("MM/dd/yyyy").format(new Date()));
                        } else {
                           end = dateFormat.parse(endDate);
                        }
                     } catch (ParseException e) {
                        e.printStackTrace();
                     }

                     if (start.before(end)) {

                     } else {
                       end = start;
                     }
                     break;
        }

        dateFormat.applyPattern("yyyy-MM-dd");
        endDate = dateFormat.format(end);
        startDate = dateFormat.format(start);

        ExcelStructure excelStructure;


        switch (reportType) {
            case 0: // Orders report
                excelStructure = generateCustomerOrdersReport(reportSubtype, start, end, request);
//                    excelStructure.header = new String [] {"מספר הזמנה", "מספר קדמי", "שם לקוח", "דואר אלקטרוני", "טלפון", "רחוב", "עיר","תאריך משלוח", "סכום הזמנה", "סטטוס"};
                break;
            case 1: // Supplier orders report
                excelStructure = generateSupplierOrdersReport(reportSubtype, start, end, supplierId, request);
                break;
            case 2: // Customers report
                switch (reportSubtype) {
                    case 0:
                    case 1: //  summary reports
                        excelStructure = generateCustomerReport(reportSubtype, start, end, "0", request);
//                            excelStructure.header = new String[]{"מספר לקוח", "שם לקוח", "דואר אלקטרוני", "טלפון", "טלפון נוסף", "רחוב", "עיר", "מספר הזמנות", "תאריך הזמנה אחרונה", "סכום הזמנה", "סכום הזמנה ממוצעת"};
                        break;
                    case 2:
                        excelStructure = generateCustomerReport(reportSubtype, start, end, customerBarcode, request);
//                            excelStructure.header = new String[]{"מספר הזמנה", "תאריך משלוח", "סכום הזמנה", "סטטוס", "סטטוס גביה"};
                        break;
                    case 3:
                        excelStructure = generateCustomerReport(reportSubtype, start, end, customerBarcode, request);
//                            excelStructure.header = new String[]{"מספר קטלוגי", "תאור מוצר", "עלות ממוצעת", "כמות", "עלות כוללת", "מספר הזמנות"};
                        break;
                    case 4:
                        excelStructure = generateCustomerReport(reportSubtype, start, end, "0", request);
                        break;
                    case 5: // Customer zone report
                        excelStructure = generateCustomerReport(reportSubtype, start, end, "0", request);
                        break;
                    case 6: // Customer preservation
                        excelStructure = generateCustomerReport(reportSubtype, start, end, "0", request);
                        break;

                    default:
                        excelStructure = new ExcelStructure();
                        excelStructure.header = new String[]{""};
                        excelStructure.content = new Object[1][1];
                        break;
                }
                break;
            case 5: // Products
                excelStructure = generateProductsReport(reportSubtype, start, end, productId, request);
                excelStructure.header = new String[]{"מספר סידורי", "תאריך משלוח", "שם לקוח", "מספר הזמנה", "מספר שילוח", "אורז", "מספר קופון", "תאור", "כמות", "סכום", "סיבה", "סוג זיכוי", "עובד מבצע", "תאריך"};
                break;
            case 6: // Packing
                switch (reportSubtype) {
                    case 0: //
                        excelStructure = generatePackingReport(reportSubtype, start, end, 0, request);
                        excelStructure.header = new String[]{"מספר סידורי", "תאור מוצר", "קטגוריה", "סוג אריזה", "קוד משקל"};
                        break;
                    case 1:
                        excelStructure = generatePackingReport(reportSubtype, start, end, 0, request);
                        excelStructure.header = new String[]{"תאור מוצר", "קוד משקל", "כמות מוזמנת"};
                        break;
                    case 5:
                        excelStructure = generatePackingReport(reportSubtype, start, end, 0, request);
                        excelStructure.header = new String[]{"תאור מוצר", "כמות מוזמנת"};
                        break;
                    case 2:
                        excelStructure = generatePackingReport(reportSubtype, start, end, 0, request);
                        break;
                    case 3:
                        excelStructure = generatePackingReport(reportSubtype, start, end, productId, request);
                        break;
                    case 4:
                        excelStructure = generatePackingReport(reportSubtype, start, end, 0, request);
                        break;
                    default:
                        excelStructure = new ExcelStructure();
                        excelStructure.header = new String[]{""};
                        break;
                }
                break;
            case 7: // Logistics report
                excelStructure = generateLogisticsReport(reportSubtype, start, end, deliveryWindow, request);
                switch (reportSubtype) {
                    case 0: // Short DHL report
                        excelStructure.header = new String[]{"מספר מזהה", "מספר חבר", "טלפון נוסף", "טלפון", "קוד שער", "דירה", "ישוב", "רחוב", "שם משפחה", "שם פרטי", "מספר הזמנה"};
                        break;
                    case 1: // Long DHL report
                        excelStructure.header = new String[]{"מספר מזהה", "מספר חבר", "טלפון נוסף", "טלפון", "קוד שער", "דירה", "ישוב", "רחוב", "שם משפחה", "שם פרטי", "מספר הזמנה", "הערות הזמנה", "הערות לקוח"};
                        break;
                    case 2: // DHL format report
                        excelStructure.header = new String[]{"מספר דרישה", "תאריך אספקה", "סוג דרישה", "סוג הזמנה", "עדיפות", "מסלול", "ס. תכנון", "ס. הובלה", "מרכז", "נקוד", "קוד חברה", "מספר לקוח", "סוג לקוח", "שם לקוח", "קבוצת לקוח", "קוד אתר", "שם אתר", "שם איש קשר 2", "רחוב", "מספר בית", "הערות", "עיר", "מיקוד", "טלפון 1", "טלפון 2", "שמן שירות", "מספר יחידות", "משטחים", "משקל", "נפח", "ערך", "הערות", "הערות לקוח"};
                        break;
                    case 3: // Boxes report
                        excelStructure.header = new String[]{"מספר הזמנה", "אזור חלוקה", "מספר חבילה", "מספר ארגזים", "תאריך משלוח"};
                        break;
                    case 4: // Boxes report filtered
                        excelStructure.header = new String[]{"מספר הזמנה", "אזור חלוקה", "מספר חבילה", "מספר ארגזים", "תאריך משלוח"};
                        break;
                    case 5: // Boxes report daily
                        excelStructure.header = new String[]{"מספר הזמנה", "אזור חלוקה", "מספר חבילה", "מספר ארגזים", "תאריך משלוח"};
                        break;
                    case 6: // Mapotempo format report
                    case 8: // Mapotempo format report
                        excelStructure.header = new String[]{"reference", "name", "street", "detail", "postalcode", "city", "country", "lat", "lng", "geocoding accuracy", "geocoding level", "comment", "phone", "tags", "without visit", "reference visit", "duration", "open 1", "close 1", "open 2", "close 2", "priority", "tags visit", "quantity[number of stops]", "operation[number of stops]", "quantity[Palets]", "operation[Palets]", "quantity[Unit]", "operation[Unit]", "quantity[Weight]", "operation[Weight]"};
                        // excelStructure.header = new String[]{"מסלול", "רכב", "active", "סיג עצירה", "התייחסות", "שם", "רחוב", "פרטים", "מיקוד", "עיר", "ארץ", "רוחב", "lng", "רמת דיוק הגאוקודינג", "רמת הגאוקודינג", "טלפון", "הערות", "תגים", "מזהה לביקור", "פתוח 1", "סגור 1", "open 2", "סגור 2", "priority", "תגי ביקורים", "משך הביקור", "כמות[Palets]", "operation[Palets]", "כמות[Unit]", "operation[Unit]", "כמות[Weight]", "operation[Weight]", "ללא ביקור", "מספר לקוח", "טלפון שני", "קוד כניסה", "תאריך משלוח"};
                        break;
                    case 7: // Drivers report
                        excelStructure.header = new String[]{"מספר אריזה", "שם", "כתובת", "ישוב", "דירה", "קוד כניסה", "טלפון", "טלפון נוסף", "הערות", "מספר הזמנה"};
                        break;
                    default:
                        excelStructure.header = new String[]{""};
                        break;
                }
                break;
            case 8: // Logistics report
                excelStructure = generatePredictionsReport(reportSubtype, start, end, supplierId, categoryId, request);
                switch (reportSubtype) {
                    case 0: // Buying report by supplier
                        excelStructure.header = new String[]{"תאור", "קוד משקל", "יחידת משקל", "כמות", "משקל", "מחיר מכירה", "מספר הזמנות"};
                        break;
                    case 1: // Selling report
                        excelStructure.header = new String[]{"תאור", "קוד משקל", "יחידת משקל", "כמות", "משקל", "מחיר מכירה", "מספר הזמנות"};
                        break;
                    default:
                        excelStructure.header = new String[]{""};
                        break;
                }
                break;
            default: // Should not be here
                excelStructure = new ExcelStructure();
                excelStructure.header = new String[]{""};
                excelStructure.content = new Object[1][1];
        }

        model.addAttribute("header", excelStructure.header);
        if (excelStructure.content == null) {
            model.addAttribute("numberOfRows", 0);

        } else {
            model.addAttribute("numberOfRows", excelStructure.content.length);
        }
        model.addAttribute("content", excelStructure.content);
        return "ReportResults";
    }

    public ExcelStructure generateCustomerOrdersReport(Integer type, Date startDate, Date endDate, HttpServletRequest request) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        ExcelStructure excelStructure = new ExcelStructure();

        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH) + 1;
        int day = cal.get(Calendar.DAY_OF_MONTH);

        StringBuilder dateStr = new StringBuilder();
        dateStr.append(day).append("_").append(month).append("_").append(year);
        excelStructure.filename = "customerOrdersReport_" + dateStr.toString() + ".xlsx";

        List<Order> orders;
        Set<Integer> customerIds;
        Map<Integer, Customer> customerMap;

        int index = 0;

        switch (type) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4: // By open pending and delivered orders
                excelStructure.header = new String[]{"מספר הזמנה", "מספר קדמי", "שם לקוח", "דואר אלקטרוני", "טלפון", "רחוב", "עיר", "תאריך משלוח", "סכום הזמנה", "סטטוס"};
                excelStructure.title = new String[]{"דוח הזמנות - פתוחות ", "מתאריך:", dateFormat.format(startDate), "עד תאריך:", dateFormat.format(endDate)};

                orders = ordersDao.findAllOrderByStatusBetweenDatesByIdDesc(type + 1, startDate, endDate);
                customerIds = new HashSet<>();
                for (Order order : orders) {
                    customerIds.add(order.getCustomerId());
                }
                customerMap = new HashMap<>();
                for (Customer customer : customersDao.findAll(customerIds)) {
                    customerMap.put(customer.getId(), customer);
                }
                excelStructure.content = new Object[orders.size()][10];
                dateFormat.applyPattern("dd/MM/yyyy");

                for (Order order : orders) {
                    excelStructure.content[index][0] = order.getId();
                    excelStructure.content[index][1] = order.getWpId();
                    excelStructure.content[index][2] = order.getFirstName() + " " + order.getLastName();
                    excelStructure.content[index][3] = customerMap.get(order.getCustomerId()).getEmail();
                    excelStructure.content[index][4] = customerMap.get(order.getCustomerId()).getPhoneExt() + "-" + customerMap.get(order.getCustomerId()).getPhoneNo();
                    excelStructure.content[index][5] = order.getStreet() + " " + order.getStreetNo();
                    excelStructure.content[index][6] = order.getCity();
                    excelStructure.content[index][7] = dateFormat.format(order.getDeliveryDate());
                    excelStructure.content[index][8] = order.getSum();
                    excelStructure.content[index][9] = order.getStatus().heb;

                    index++;
                }
                break;

            case 5: // Attention
                excelStructure.header = new String[]{"מספר הזמנה", "שם לקוח", "דואר אלקטרוני", "טלפון", "רחוב", "עיר", "תאריך משלוח", "סכום הזמנה", "התראה", "סיבת התראה"};
                excelStructure.title = new String[]{"דוח הזמנות - התראות", "מתאריך:", dateFormat.format(startDate), "עד תאריך:", dateFormat.format(endDate)};

                orders = ordersDao.findAllBetweenDeliveryDateWithCustomerAttentionOrderByParam(startDate, endDate, "id");
                customerIds = new HashSet<>();
                for (Order order : orders) {
                    customerIds.add(order.getCustomerId());
                }
                customerMap = new HashMap<>();
                for (Customer customer : customersDao.findAll(customerIds)) {
                    customerMap.put(customer.getId(), customer);
                }
                excelStructure.content = new Object[orders.size()][10];
                dateFormat.applyPattern("dd/MM/yyyy");

                for (Order order : orders) {
                    if (customerMap.get(order.getCustomerId()).getAttentionFlag().ordinal() > 0) {
                        excelStructure.content[index][0] = order.getId();
                        excelStructure.content[index][1] = order.getFirstName() + " " + order.getLastName();
                        excelStructure.content[index][2] = customerMap.get(order.getCustomerId()).getEmail();
                        excelStructure.content[index][3] = customerMap.get(order.getCustomerId()).getPhoneExt() + "-" + customerMap.get(order.getCustomerId()).getPhoneNo();
                        excelStructure.content[index][4] = order.getStreet() + " " + order.getStreetNo();
                        excelStructure.content[index][5] = order.getCity();
                        excelStructure.content[index][6] = dateFormat.format(order.getDeliveryDate());
                        excelStructure.content[index][7] = order.getSum();
                        excelStructure.content[index][8] = customerMap.get(order.getCustomerId()).getAttentionFlag().heb;
                        excelStructure.content[index][9] = customerMap.get(order.getCustomerId()).getAttentionCause();

                        index++;
                    }
                }
                break;

            case 6: // Duplicates
                excelStructure.header = new String[]{"מספר הזמנה", "מספר קדמי", "שם לקוח", "דואר אלקטרוני", "טלפון", "רחוב", "עיר", "תאריך משלוח", "סכום הזמנה", "סטטוס"};
                excelStructure.title = new String[]{"דוח הזמנות - כפולות "};

                orders = ordersDao.findAllOpenWithSameCustomerId();

                int dupMatchPartial;
                int info;
                int count = 0;
                Map<Integer, Integer> pointers = new HashMap<>();

                for (index = 0; index < orders.size() -1; index++) {
                    Integer dupMatch = 0;
                    dupMatchPartial = 1;
                    info = 0;
                    while (dupMatchPartial == 1 && info < 6) {
                        switch (info) {
                            case 0:
                                if (!Objects.equals(orders.get(index).getCity(), orders.get(index + 1).getCity())) {
                                    dupMatchPartial = 0;
                                }
                                break;
                            case 1:
                                if (!Objects.equals(orders.get(index).getStreet(), orders.get(index + 1).getStreet())) {
                                    dupMatchPartial = 0;
                                }
                                break;
                            case 2:
                                if (!Objects.equals(orders.get(index).getStreetNo(), orders.get(index + 1).getStreetNo())) {
                                    dupMatchPartial = 0;
                                }
                                break;
                            case 3:
                                if (!Objects.equals(orders.get(index).getApartment(), orders.get(index + 1).getApartment())) {
                                    dupMatchPartial = 0;
                                }
                                break;
                            case 4:
                                if (!Objects.equals(orders.get(index).getFloorNo(), orders.get(index + 1).getFloorNo())) {
                                    dupMatchPartial = 0;
                                }
                                break;
                            case 5:
                                dupMatch = 1;
                        }
                        info++;
                    }

                    /* if (dupMatch == 0) {
                        orders = ordersDao.findAllPendingWithSameCustomerId();
                        while (dupMatch == 0 && index < orders.size() - 1) {
                            dupMatchPartial = 1;
                            info = 0;
                            while (dupMatchPartial == 1 && info < 6) {
                                switch (info) {
                                    case 0:
                                        if (!Objects.equals(orders.get(index).getCity(), orders.get(index + 1).getCity())) {
                                            dupMatchPartial = 0;
                                        }
                                        break;
                                    case 1:
                                        if (!Objects.equals(orders.get(index).getStreet(), orders.get(index + 1).getStreet())) {
                                            dupMatchPartial = 0;
                                        }
                                        break;
                                    case 2:
                                        if (!Objects.equals(orders.get(index).getStreetNo(), orders.get(index + 1).getStreetNo())) {
                                            dupMatchPartial = 0;
                                        }
                                        break;
                                    case 3:
                                        if (!Objects.equals(orders.get(index).getApartment(), orders.get(index + 1).getApartment())) {
                                            dupMatchPartial = 0;
                                        }
                                        break;
                                    case 4:
                                        if (!Objects.equals(orders.get(index).getFloorNo(), orders.get(index + 1).getFloorNo())) {
                                            dupMatchPartial = 0;
                                        }
                                        break;
                                    case 5:
                                        dupMatch = 1;
                                }
                                info++;
                            }
                        }
                    } */

                    if (dupMatch == 1) {
                        pointers.put(count, index);
                        count++;
                    }
                }

                excelStructure.content = new Object[count*2][10];

                for (int arrayIndex = 0; arrayIndex < count; arrayIndex++) {
                    dateFormat.applyPattern("dd/MM/yyyy");
                    index = pointers.get(arrayIndex);
                    excelStructure.content[2 * arrayIndex][0] = orders.get(index).getId();
                    excelStructure.content[2 * arrayIndex][1] = orders.get(index).getWpId();
                    excelStructure.content[2 * arrayIndex][2] = orders.get(index).getFirstName() + " " + orders.get(index).getLastName();
                    excelStructure.content[2 * arrayIndex][3] = customersDao.findOne(orders.get(index).getCustomerId()).getEmail();
                    excelStructure.content[2 * arrayIndex][4] = customersDao.findOne(orders.get(index).getCustomerId()).getPhoneExt() + "-" + customersDao.findOne(orders.get(index).getCustomerId()).getPhoneNo();
                    excelStructure.content[2 * arrayIndex][5] = orders.get(index).getStreet() + " " + orders.get(index).getStreetNo();
                    excelStructure.content[2 * arrayIndex][6] = orders.get(index).getCity();
                    excelStructure.content[2 * arrayIndex][7] = dateFormat.format(orders.get(index).getDeliveryDate());
                    excelStructure.content[2 * arrayIndex][8] = orders.get(index).getSum();
                    excelStructure.content[2 * arrayIndex][9] = orders.get(index).getStatus().heb;
                    excelStructure.content[2 * arrayIndex + 1][0] = orders.get(index + 1).getId();
                    excelStructure.content[2 * arrayIndex + 1][1] = orders.get(index + 1).getWpId();
                    excelStructure.content[2 * arrayIndex + 1][2] = orders.get(index + 1).getFirstName() + " " + orders.get(index + 1).getLastName();
                    excelStructure.content[2 * arrayIndex + 1][3] = customersDao.findOne(orders.get(index + 1).getCustomerId()).getEmail();
                    excelStructure.content[2 * arrayIndex + 1][4] = customersDao.findOne(orders.get(index + 1).getCustomerId()).getPhoneExt() + "-" + customersDao.findOne(orders.get(index + 1).getCustomerId()).getPhoneNo();
                    excelStructure.content[2 * arrayIndex + 1][5] = orders.get(index + 1).getStreet() + " " + orders.get(index + 1).getStreetNo();
                    excelStructure.content[2 * arrayIndex + 1][6] = orders.get(index + 1).getCity();
                    excelStructure.content[2 * arrayIndex + 1][7] = dateFormat.format(orders.get(index + 1).getDeliveryDate());
                    excelStructure.content[2 * arrayIndex + 1][8] = orders.get(index + 1).getSum();
                    excelStructure.content[2 * arrayIndex + 1][9] = orders.get(index + 1).getStatus().heb;
                }
                break;

            default: // Do nothing
                excelStructure.content = new Object[1][9];
                break;
        }

        return excelStructure;
    }

    public ExcelStructure generateSupplierOrdersReport(Integer type, Date startDate, Date endDate, Integer supplierId, HttpServletRequest request) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        ExcelStructure excelStructure = new ExcelStructure();
        excelStructure.header = new String[]{"מספר הזמנה", "מספר קדמי", "שם לקוח", "דואר אלקטרוני", "טלפון", "רחוב", "עיר", "תאריך משלוח", "סכום הזמנה", "סטטוס"};
        excelStructure.title = new String[]{"דוח הזמנות - פתוחות ", "מתאריך:", dateFormat.format(startDate), "עד תאריך:", dateFormat.format(endDate)};

        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH) + 1;
        int day = cal.get(Calendar.DAY_OF_MONTH);

        StringBuilder dateStr = new StringBuilder();
        dateStr.append(day).append("_").append(month).append("_").append(year);
        excelStructure.filename = "customerOrdersReport_" + dateStr.toString() + ".xlsx";

        int index = 0;
        List<SupplierOrder> supplierOrders;

        switch (type) {
            case 0: // By supplier id

                if (supplierId == null) {
                    supplierOrders = supplierOrdersDao.findAll();
                } else {
                    supplierOrders = supplierOrdersDao.findAllWithSupplierId(supplierId);
                }

                excelStructure.content = new Object[supplierOrders.size()][4];
                dateFormat.applyPattern("dd/MM/yyyy");

                for (SupplierOrder supplierOrder : supplierOrders) {
                    Supplier supplier = suppliersDao.findOne(supplierOrder.getSupplierId());
                    excelStructure.content[index][0] = supplier.getName();
                    excelStructure.content[index][1] = supplierOrder.getBarcode();
                    excelStructure.content[index][2] = dateFormat.format(supplierOrder.getCreatedDate());
                    excelStructure.content[index][3] = supplierOrder.getSum();

                    index++;
                }
                break;

            default: // Do nothing
                excelStructure.content = new Object[1][4];
                break;
        }

        return excelStructure;
    }

    public ExcelStructure generateCustomerReport(Integer type, Date startDate, Date endDate, String customerId, HttpServletRequest request) {

        ExcelStructure excelStructure = new ExcelStructure();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        excelStructure.header = new String[]{"מספר לקוח", "שם לקוח", "דואר אלקטרוני", "טלפון", "טלפון נוסף", "רחוב", "עיר", "מספר הזמנות", "תאריך הזמנה אחרונה", "סכום הזמנה", "סכום הזמנה ממוצעת"};
        excelStructure.title = new String[]{"דוח לקוחות - לפי לקוח ", "מתאריך:", dateFormat.format(startDate), "עד תאריך:", dateFormat.format(endDate)};

        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH) + 1;
        int day = cal.get(Calendar.DAY_OF_MONTH);

        StringBuilder dateStr = new StringBuilder();
        dateStr.append(day).append("_").append(month).append("_").append(year);
        excelStructure.filename = "customersReport_" + dateStr.toString() + ".xlsx";

        int index = 0;
        int contentIndex = 0;

        List<Order> orders;
        List<Customer> customers;
        Customer singleCustomer;
        Product singleProduct;
        Map<String, Object[]> column = new HashMap<>();
        Object[] singleColumn;

        switch (type) {
            case 0: // By customer enrolment date
                customers = customersDao.findAllBetweenDates(startDate, endDate);
                excelStructure.content = new Object[customers.size()][11];

                dateFormat.applyPattern("dd/MM/yyyy");

                for (Customer customer : customers) {
                    excelStructure.content[index][0] = customer.getBarcode();
                    excelStructure.content[index][1] = customer.getFirstName() + " " + customer.getLastName();
                    excelStructure.content[index][2] = customer.getEmail();
                    excelStructure.content[index][3] = customer.getPhoneExt() + "-" + customer.getPhoneNo();
                    excelStructure.content[index][4] = customer.getPhone2Ext() + "-" + customer.getPhone2No();
                    excelStructure.content[index][5] = customer.getStreet() + " " + customer.getStreetNo();
                    excelStructure.content[index][6] = customer.getCity();
                    orders = ordersDao.findAllByCustomerIdDesc(customer.getId());
                    excelStructure.content[index][7] = orders.size();
                    if (orders.size() > 0) {
                        Order order = ordersDao.findOneByCustomerIdDesc(customer.getId());
                        excelStructure.content[index][8] = dateFormat.format(order.getDeliveryDate());
                        excelStructure.content[index][9] = Float.toString(order.getSum());
                        double totalSum = 0.0;
                        for (Order ordersTosum : orders) {
                            totalSum += ordersTosum.getSum();
                        }
                        DecimalFormat twoDecimals = new DecimalFormat("#.00");
                        excelStructure.content[index][10] = twoDecimals.format(totalSum / orders.size());
                    } else {

                        excelStructure.content[index][8] = "";
                        excelStructure.content[index][9] = "";
                        excelStructure.content[index][10] = "";
                    }

                    index++;
                }

                break;

            case 1: // By orders
                excelStructure.title[0] = "דוח לקוחות - לפי הזמנות";
                orders = ordersDao.findAllDeliveredBetweenDatesGroupByCustomerId(startDate, endDate);
                excelStructure.content = new Object[orders.size()][11];

                dateFormat.applyPattern("dd/MM/yyyy");

                for (Order order : orders) {
                    Customer customer = customersDao.findOne(order.getCustomerId());
                    excelStructure.content[index][0] = customer.getBarcode();
                    excelStructure.content[index][1] = customer.getFirstName() + " " + customer.getLastName();
                    excelStructure.content[index][2] = customer.getEmail();
                    excelStructure.content[index][3] = customer.getPhoneExt() + "-" + customer.getPhoneNo();
                    excelStructure.content[index][4] = customer.getPhone2Ext() + "-" + customer.getPhone2No();
                    excelStructure.content[index][5] = customer.getStreet() + " " + customer.getStreetNo();
                    excelStructure.content[index][6] = customer.getCity();
                    List<Order> orders1 = ordersDao.findAllByCustomerIdDesc(customer.getId());
                    excelStructure.content[index][7] = orders1.size();
                    if (orders1.size() > 0) {
                        Order lastOrder = ordersDao.findOneByCustomerIdDesc(customer.getId());
                        excelStructure.content[index][8] = dateFormat.format(lastOrder.getDeliveryDate());
                        excelStructure.content[index][9] = Float.toString(lastOrder.getSum());
                        double totalSum = 0.0;
                        for (Order ordersTosum : orders1) {
                            totalSum += ordersTosum.getSum();
                        }
                        DecimalFormat twoDecimals = new DecimalFormat("#.00");
                        excelStructure.content[index][10] = twoDecimals.format(totalSum / orders1.size());
                    } else {

                        excelStructure.content[index][8] = "";
                        excelStructure.content[index][9] = "";
                        excelStructure.content[index][10] = "";
                    }

                    index++;
                }
                break;

            case 2: // Customer orders
                singleCustomer = customersDao.findOneByBarcode(customerId);
                if (customerId == "0") {
                    orders = ordersDao.findAllBetweenDates(startDate, endDate);
                } else {
                    orders = ordersDao.findAllByCustomerIdWithDates(singleCustomer.getId(), startDate, endDate);
                }
                excelStructure.content = new Object[orders.size()][5];
                for (Order order : orders) {
                    excelStructure.content[index][0] = order.getId();
                    excelStructure.content[index][1] = order.getDeliveryDate();
                    excelStructure.content[index][2] = order.getSum();
                    excelStructure.content[index][3] = order.getStatus().heb;
                    excelStructure.content[index][4] = order.getClearingStatus().heb;

                    index++;
                }

                break;

            case 3:
                singleCustomer = customersDao.findOneByBarcode(customerId);
                if (customerId == "") {
                    orders = ordersDao.findAllBetweenDates(startDate, endDate);
                } else {
                    orders = ordersDao.findAllByCustomerIdWithDates(singleCustomer.getId(), startDate, endDate);
                }

                if (orders.size() > 0) {
                    for (Order order : orders) {
                        index++;
                        if (index % 10 == 0) {
                        }
                        if (order.getStatus() == Order.OrderStatuses.SHIPMENT || order.getStatus() == Order.OrderStatuses.IN_ORDER) {
                            for (OrderProduct orderProduct : orderProductDao.findAllByOrderId(order.getId())) {

                                singleProduct = productsDao.findOne(orderProduct.getProductId());
                                singleColumn = column.get(orderProduct.getItemId().toString());
                                if (singleColumn == null) {
                                    column.put(orderProduct.getItemId().toString(), new Object[]{itemsDao.getOne(orderProduct.getItemId()).getBarcode(), singleProduct.getNameHeb(), U.var.parseInt(orderProduct.getQuantityNeeded()), orderProduct.getUnitPrice() * orderProduct.getQuantityNeeded(), 1});
                                } else {
                                    Integer iterations = U.var.parseInt(singleColumn[4]) + 1;
                                    Integer qty = U.var.parseInt(singleColumn[2]) + U.var.parseInt(orderProduct.getQuantityNeeded());
                                    Float totalPrice = U.var.parseFloat(singleColumn[3]) + U.var.parseFloat(orderProduct.getUnitPrice()) * U.var.parseInt(orderProduct.getQuantityNeeded());
                                    column.put(orderProduct.getItemId().toString(), new Object[]{itemsDao.getOne(orderProduct.getItemId()).getBarcode(), singleProduct.getNameHeb(), qty, totalPrice, iterations});
                                }
                            }
                        }
                    }

                    DecimalFormat decimalFormat = new DecimalFormat("#.##");
                    excelStructure.content = new Object[column.size()][6];
                    index = 0;
                    for (Map.Entry<String, Object[]> entry : column.entrySet()) {
                        excelStructure.content[index][0] = entry.getValue()[0];
                        excelStructure.content[index][1] = entry.getValue()[1];
                        excelStructure.content[index][2] = decimalFormat.format(U.var.parseFloat(entry.getValue()[3]) / U.var.parseFloat(entry.getValue()[2]));
                        excelStructure.content[index][3] = entry.getValue()[2];
                        excelStructure.content[index][4] = decimalFormat.format(entry.getValue()[3]);
                        excelStructure.content[index][5] = entry.getValue()[4];
                        index++;
                    }
                } else {
                    excelStructure.content = new Object[1][6];
                }
                break;

            case 4: // Customers with attention flags
                excelStructure.header = new String[]{"מספר לקוח", "שם לקוח", "דואר אלקטרוני", "טלפון", "טלפון נוסף", "רחוב", "עיר", "התראה", "סיבת התראה"};
                excelStructure.title = new String[]{"דוח לקוחות - התראות", "מתאריך:", dateFormat.format(startDate), "עד תאריך:", dateFormat.format(endDate)};

                customers = customersDao.findAllBetweenDatesWithAttentionFlag(startDate, endDate);
                excelStructure.content = new Object[customers.size()][9];

                dateFormat.applyPattern("dd/MM/yyyy");

                for (Customer customer : customers) {
                    excelStructure.content[index][0] = customer.getBarcode();
                    excelStructure.content[index][1] = customer.getFirstName() + " " + customer.getLastName();
                    excelStructure.content[index][2] = customer.getEmail();
                    excelStructure.content[index][3] = customer.getPhoneExt() + "-" + customer.getPhoneNo();
                    excelStructure.content[index][4] = customer.getPhone2Ext() + "-" + customer.getPhone2No();
                    excelStructure.content[index][5] = customer.getStreet() + " " + customer.getStreetNo();
                    excelStructure.content[index][6] = customer.getCity();
                    excelStructure.content[index][7] = customer.getAttentionFlag().heb;
                    excelStructure.content[index][8] = customer.getAttentionCause();

                    index++;
                }

                break;

            case 5: // Customer zone report
                excelStructure.header = new String[]{"מספר לקוח", "שם לקוח", "דואר אלקטרוני", "טלפון", "טלפון נוסף", "רחוב", "עיר", "אזור", "מספר הזמנות"};
                Integer zone0 = 0; Integer zone1 = 0; Integer zone2 = 0;
                customers = customersDao.findAllBetweenDates(startDate, endDate);
                excelStructure.content = new Object[customers.size()][9];

                for (Customer customer : customers) {
                    excelStructure.content[index][0] = customer.getBarcode();
                    excelStructure.content[index][1] = customer.getFirstName() + " " + customer.getLastName();
                    excelStructure.content[index][2] = customer.getEmail();
                    excelStructure.content[index][3] = customer.getPhoneExt() + "-" + customer.getPhoneNo();
                    excelStructure.content[index][4] = customer.getPhone2Ext() + "-" + customer.getPhone2No();
                    excelStructure.content[index][5] = customer.getStreet() + " " + customer.getStreetNo();
                    excelStructure.content[index][6] = customer.getCity();
                    Integer zone = U.var.parseInt(customer.getBarcode().substring(3,4));
                    excelStructure.content[index][7] = zone;
                    orders = ordersDao.findAllByCustomerIdDesc(customer.getId());
                    excelStructure.content[index][8] = orders.size();

                    index++;
                }

                excelStructure.title = new String[]{"דוח לקוחות - התראות", "מתאריך:", dateFormat.format(startDate), "עד תאריך:", dateFormat.format(endDate), "מרכז:", zone0.toString(), "דרום:", zone1.toString(), "צפון:", zone2.toString()};
                break;
            case 6: // Customer zone report
                excelStructure.header = new String[]{"מספר לקוח", "שם לקוח", "דואר אלקטרוני", "טלפון", "טלפון נוסף", "רחוב", "עיר", "אזור", "מספר הזמנות", "תאריך הצטרפות", "תאריך הזמנה אחרונה", "תאריך הזמנה לפני אחרונה", "סכום הזמנה ממוצע", "מספר נקודות בצבירה", "הערות"};
                customers = customersDao.findAllBetweenDates(startDate, endDate);
                excelStructure.content = new Object[customers.size()][15];
                for (Customer customer : customers) {
                    excelStructure.content[index][0] = customer.getBarcode();
                    excelStructure.content[index][1] = customer.getFirstName() + " " + customer.getLastName();
                    excelStructure.content[index][2] = customer.getEmail();
                    excelStructure.content[index][3] = customer.getPhoneExt() + "-" + customer.getPhoneNo();
                    excelStructure.content[index][4] = customer.getPhone2Ext() + "-" + customer.getPhone2No();
                    excelStructure.content[index][5] = customer.getStreet() + " " + customer.getStreetNo();
                    excelStructure.content[index][6] = customer.getCity();
                    Integer zone = U.var.parseInt(customer.getBarcode().substring(3,4));
                    excelStructure.content[index][7] = zone;
                    orders = ordersDao.findAllDeliveredByCustomerIdDesc(customer.getId());
                    excelStructure.content[index][8] = orders.size();
                    excelStructure.content[index][9] = dateFormat.format(customer.getCreatedDate());
                    if (orders.size() > 0) {
                        excelStructure.content[index][10] = dateFormat.format(orders.get(0).getDeliveryDate());
                    } else {
                        excelStructure.content[index][10] = " ";
                    }
                    if (orders.size() > 1) {
                        excelStructure.content[index][11] = dateFormat.format(orders.get(1).getDeliveryDate());
                    } else {
                        excelStructure.content[index][11] = " ";
                    }
                    if (orders.size() > 0) {
                        Float sum = 0F;
                        for (Order order : orders) {
                            sum += order.getSum();
                        }
                        excelStructure.content[index][12] = sum/orders.size();
                    } else {
                        excelStructure.content[index][12] = " ";
                    }
                    excelStructure.content[index][13] = customer.getCreditPoints();
                    excelStructure.content[index][14] = " ";

                    index++;
                }
                break;

            default: // Do nothing
                excelStructure.content = new Object[1][11];
                break;
        }

        return excelStructure;
    }

    public ExcelStructure generateProductsReport(Integer type, Date startDate, Date endDate, Integer productId, HttpServletRequest request) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        ExcelStructure excelStructure = new ExcelStructure();
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH) + 1;
        int day = cal.get(Calendar.DAY_OF_MONTH);

        int index;

        StringBuilder dateStr = new StringBuilder();
        dateStr.append(day).append("_").append(month).append("_").append(year);

        switch (type) {
            case 0: // Refunds
                List<Refund> refunds = refundsDao.findAllBetweenDates(startDate, endDate);
                excelStructure.title = new String[]{"דוח זיכויים", dateFormat.format(new Date())};
                excelStructure.filename = "productsReport_" + dateStr.toString() + ".xlsx";
                excelStructure.content = new Object[refunds.size()][14];

                index = 0;
                for (Refund refund : refunds) {
                    Customer customer = customersDao.findOne(refund.getCustomerId());
                    Order order = ordersDao.findOne(refund.getOrderId());
                    Coupon coupon = couponsDao.getOne(refund.getCouponId());
                    excelStructure.content[index][0] = refund.getId();
                    if (refund.getOrderId() != 0) {
                        excelStructure.content[index][1] = dateFormat.format(order.getDeliveryDate());
                    } else {
                        excelStructure.content[index][1] = "";
                    }
                    excelStructure.content[index][2] = customer.getFirstName() + " " + customer.getLastName();
                    excelStructure.content[index][3] = refund.getOrderId();
                    if (refund.getOrderId() != 0) {
                        excelStructure.content[index][4] = order.getDhlPackageNumber();
                    } else {
                        excelStructure.content[index][4] = "";
                    }
                    if (order.getPackingWorkerId() != 0) {
                        excelStructure.content[index][5] = workersDao.getOne(order.getPackingWorkerId()).getSbId();
                    } else {
                        excelStructure.content[index][5] = "";
                    }
                    if (refund.getCouponId() != 0) {
                        excelStructure.content[index][6] = couponsDao.getOne(refund.getCouponId()).getTitle();
                    } else {
                        excelStructure.content[index][6] = "";
                    }
                    excelStructure.content[index][7] = refund.getDetails();
                    excelStructure.content[index][8] = refund.getQuantity();
                    excelStructure.content[index][9] = refund.getAmount();
                    excelStructure.content[index][10] = refund.getRootCause().heb;
                    excelStructure.content[index][11] = refund.getType().heb;
                    if (refund.getWorkerId() != 0) {
                        excelStructure.content[index][12] = workersDao.getOne(refund.getWorkerId()).getSbId();
                    } else {
                        excelStructure.content[index][12] = "";
                    }
                    excelStructure.content[index][13] = dateFormat.format(refund.getCreatedDate());

                    index++;
                }
                break;
            default: // Do nothing
                excelStructure.content = new Object[1][4];
                excelStructure.filename = "packingError_" + dateStr.toString() + ".xlsx";
                break;
        }

        return excelStructure;
    }

    public ExcelStructure generatePackingReport(Integer type, Date startDate, Date endDate, Integer productId, HttpServletRequest request) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        ExcelStructure excelStructure = new ExcelStructure();
        List<Worker> packingWorkers;
        Map<Integer, Worker> workerMap;
        List<Order> orders;

        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH) + 1;
        int day = cal.get(Calendar.DAY_OF_MONTH);

        StringBuilder dateStr = new StringBuilder();
        dateStr.append(day).append("_").append(month).append("_").append(year);
        int index = 0;
        Product product;
        List<Object[]> weightReport;

        switch (type) {
            case 0: // All product weight code
                List<Object[]> allWeightReport = productsDao.weightReportAllProducts();
                excelStructure.title = new String[]{"דוח אריזה - כל המוצרים", dateFormat.format(new Date())};
                excelStructure.filename = "packingReport_" + dateStr.toString() + ".xlsx";
                excelStructure.content = new Object[allWeightReport.size()][5];

                index = 0;
                for (Object[] line : allWeightReport) {
                    excelStructure.content[index][0] = line[0];
                    excelStructure.content[index][1] = line[1];
                    excelStructure.content[index][2] = categoriesDao.findOne(U.var.parseInt(line[2])).getName();
                    excelStructure.content[index][3] = line[3];
                    excelStructure.content[index][4] = line[4];

                    index++;
                }
                break;

            case 1: // Product weight code
                if ("0".equals(globalsDao.findOne("packing_weight_code_gen").getValue())) {
                    weightReport = productsDao.weightReportPendingOnly();
                } else {
                    weightReport = productsDao.weightReportPendingOrHarvesting();
                }
                excelStructure.title = new String[]{"דוח אריזה - קוד משקל", dateFormat.format(new Date())};
                excelStructure.filename = "packingReport_" + dateStr.toString() + ".xlsx";
                excelStructure.content = new Object[weightReport.size()][3];

                index = 0;
                for (Object[] line : weightReport) {
                    //excelStructure.content[index][0] = line[0];
                    String supplierName = line[5].toString().split("-")[0];
                    if ("".equals(line[2].toString())) {
                        excelStructure.content[index][0] = line[1] + " - " + supplierName;
                    } else {
                        excelStructure.content[index][0] = line[1] + " - " + line[2] + " - " + supplierName;
                    }
                    product = productsDao.findOne(U.var.parseInt(line[0]));
                    // excelStructure.content[index][2] = categoriesDao.findOne(product.getCategoryId()).getName();
                    // excelStructure.content[index][3] = product.getPackingAction().heb;
                    excelStructure.content[index][1] = U.var.parseInt(line[3]);
                    excelStructure.content[index][2] = line[4];

                    index++;
                }
                break;

            case 5: // Product quantization
                if ("0".equals(globalsDao.findOne("packing_weight_code_gen").getValue())) {
                    weightReport = productsDao.weightReportPendingOnlyQuantization();
                } else {
                    weightReport = productsDao.weightReportPendingOrHarvestingQuantization();
                }
                excelStructure.title = new String[]{"דוח אריזה - קוונטה", dateFormat.format(new Date())};
                excelStructure.filename = "packingReport_" + dateStr.toString() + ".xlsx";
                excelStructure.content = new Object[weightReport.size()][2];

                index = 0;
                for (Object[] line : weightReport) {
                    //excelStructure.content[index][0] = line[0];
                    String supplierName = line[5].toString().split("-")[0];
                    if ("".equals(line[2].toString())) {
                        excelStructure.content[index][0] = line[1] + " - " + supplierName;
                    } else {
                        excelStructure.content[index][0] = line[1] + " - " + line[2] + " - " + supplierName;
                    }
                    product = productsDao.findOne(U.var.parseInt(line[0]));
                    // excelStructure.content[index][2] = categoriesDao.findOne(product.getCategoryId()).getName();
                    // excelStructure.content[index][3] = product.getPackingAction().heb;
                    excelStructure.content[index][1] = line[4];

                    index++;
                }
                break;

            case 2:  // Workers report
                excelStructure.title = new String[]{"דוח אריזה - עובדים", dateFormat.format(startDate), "", dateFormat.format(endDate)};
                excelStructure.header = new String[]{"מספר עובד", "שם", "הזמנות", "ארגזים", "ממוצע ארגזים", "סכום הזמנות", "סכום ממוצע", "זמן אריזה", "ממוצע זמן אריזה", "הכנסה לזמן"};

                packingWorkers = workersDao.findAllPackingWorkers();
                workerMap = new HashMap<>();
                for (Worker worker : workersDao.findAllPackingWorkers()) {
                    workerMap.put(worker.getId(), worker);
                }

                excelStructure.content = new Object[packingWorkers.size()][10];
                orders = ordersDao.findAllBetweenDates(startDate, endDate);

                for (Worker worker : packingWorkers) {
                    Integer numberOfBoxes = 0;
                    Integer numberOfOrders = 0;
                    Float   sumOfOrders = 0F;
                    Long packingTime = 0L;
                    for (Order order : orders) {
                        if (worker.getId() == order.getPackingWorkerId()) {
                            numberOfBoxes += order.getBoxes();
                            sumOfOrders += order.getSum()-18;
                            numberOfOrders++;
                            long diff = order.getLastPackingTimestamp().getTime() - order.getStartPackingTimestamp().getTime();
                            packingTime += diff / 1000;
                        }
                    }


                    if (numberOfOrders != 0) {
                       excelStructure.content[index][0] = worker.getId();
                       excelStructure.content[index][1] = worker.getSbId();
                       excelStructure.content[index][2] = numberOfOrders;
                       excelStructure.content[index][3] = numberOfBoxes;
                       excelStructure.content[index][5] = String.format ("%,.2f", sumOfOrders);
                       excelStructure.content[index][7] = packingTime / 60 + ":" + packingTime % 60;
                       excelStructure.content[index][4] = String.format ("%,.2f", ((float)numberOfBoxes) / numberOfOrders);
                       excelStructure.content[index][6] = String.format ("%,.2f", sumOfOrders / numberOfOrders);
                       excelStructure.content[index][8] = (packingTime / (numberOfOrders * 60)) + ":" + (packingTime / numberOfOrders) % 60;
                       excelStructure.content[index][9] = String.format ("%,.2f", sumOfOrders / packingTime);
                    }

                    index++;
                }

                break;
            case 3:  // Product report
                excelStructure.header = new String[]{"מספר הזמנה", "מספר דהל", "שם לקוח", "כמות מוזמנת", "כמות שסופקה", "עובד"};
                List<Object[]> data = orderProductDao.getPackingOrderProducts(productId, startDate, endDate);
                excelStructure.content = new Object[data.size()][6];
                for (Object[] line : data) {
                    excelStructure.content[index][0] = line[0];
                    excelStructure.content[index][1] = line[1];
                    excelStructure.content[index][2] = line[2] + " " + line[3];
                    excelStructure.content[index][3] = line[4];
                    excelStructure.content[index][4] = line[5];
                    excelStructure.content[index][5] = line[6];

                    index++;
                }

                break;
            case 4:  // Workers real time report
                excelStructure.title = new String[]{"דוח אריזה - עובדים", dateFormat.format(startDate), "", dateFormat.format(endDate)};
                excelStructure.header = new String[]{"מספר עובד", "שם", "הזמנות", "ארגזים", "ממוצע ארגזים", "סכום הזמנות", "סכום ממוצע", "זמן אריזה", "ממוצע זמן אריזה", "הכנסה לזמן"};

                packingWorkers = workersDao.findAllPackingWorkers();
                workerMap = new HashMap<>();
                for (Worker worker : workersDao.findAllPackingWorkers()) {
                    workerMap.put(worker.getId(), worker);
                }

                excelStructure.content = new Object[packingWorkers.size()][10];
                orders = ordersDao.findAllBetweenDatesRealTime();

                for (Worker worker : packingWorkers) {
                    Integer numberOfBoxes = 0;
                    Integer numberOfOrders = 0;
                    Float   sumOfOrders = 0F;
                    Long packingTime = 0L;
                    for (Order order : orders) {
                        if (worker.getId() == order.getPackingWorkerId()) {
                            numberOfBoxes += order.getBoxes();
                            sumOfOrders += order.getSum()-18;
                            numberOfOrders++;
                            long diff = order.getLastPackingTimestamp().getTime() - order.getStartPackingTimestamp().getTime();
                            packingTime += diff / 1000;
                        }
                    }


                    if (numberOfOrders != 0) {
                        excelStructure.content[index][0] = worker.getId();
                        excelStructure.content[index][1] = worker.getSbId();
                        excelStructure.content[index][2] = numberOfOrders;
                        excelStructure.content[index][3] = numberOfBoxes;
                        excelStructure.content[index][5] = String.format ("%,.2f", sumOfOrders);
                        excelStructure.content[index][7] = packingTime / 60 + ":" + packingTime % 60;
                        excelStructure.content[index][4] = String.format ("%,.2f", ((float)numberOfBoxes) / numberOfOrders);
                        excelStructure.content[index][6] = String.format ("%,.2f", sumOfOrders / numberOfOrders);
                        excelStructure.content[index][8] = (packingTime / (numberOfOrders * 60)) + ":" + (packingTime / numberOfOrders) % 60;
                        excelStructure.content[index][9] = String.format ("%,.2f", sumOfOrders / packingTime);
                    }

                    index++;
                }

                break;
            default: // Do nothing
                excelStructure.content = new Object[1][4];
                excelStructure.filename = "packingError_" + dateStr.toString() + ".xlsx";
                break;
        }

        return excelStructure;
    }

    public ExcelStructure generateLogisticsReport(Integer type, Date startDate, Date endDate, Integer deliveryWindow, HttpServletRequest request) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        ExcelStructure excelStructure = new ExcelStructure();


        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH) + 1;
        int day = cal.get(Calendar.DAY_OF_MONTH);

        StringBuilder dateStr = new StringBuilder();
        dateStr.append(day).append("_").append(month).append("_").append(year);
        List<Order> orders;
        Customer customer;
        String appartment;
        int index = 0;


        switch (type) {
            case 0: // DHL report
                orders = ordersDao.findAllHarvestingOrder();
                excelStructure.filename = "dhlReport_" + dateStr.toString() + ".xlsx";
                excelStructure.content = new Object[orders.size()][11];

                index = 0;
                for (Order order : orders) {
                    appartment = "";
                    customer = customersDao.findOne(order.getCustomerId());
                    excelStructure.content[index][0] = order.getId();
                    excelStructure.content[index][1] = customer.getBarcode();
                    excelStructure.content[index][2] = customer.getPhone2Ext() + customer.getPhone2No();
                    excelStructure.content[index][3] = customer.getPhoneExt() + customer.getPhoneNo();
                    excelStructure.content[index][4] = order.getEntryCode();
                    if (!U.var.isEmptyStr(order.getApartment())) {
                        appartment = "ד. " + order.getApartment() + " ";
                    }
                    if (!U.var.isEmptyStr(order.getFloorNo())) {
                        appartment = appartment + "ק. " + order.getFloorNo();
                    }
                    excelStructure.content[index][5] = appartment;
                    excelStructure.content[index][6] = order.getCity();
                    excelStructure.content[index][7] = order.getStreet() + " " + order.getStreetNo();
                    excelStructure.content[index][8] = order.getLastName();
                    excelStructure.content[index][9] = order.getFirstName();
                    excelStructure.content[index][10] = common.createOrderCode(order.getId());

                    index++;
                }
                break;

            case 1: // DHL extended report
                orders = ordersDao.findAllHarvestingOrder();
                excelStructure.filename = "dhlExtendedReport_" + dateStr.toString() + ".xlsx";
                excelStructure.content = new Object[orders.size()][13];

                index = 0;
                for (Order order : orders) {
                    appartment = "";
                    customer = customersDao.findOne(order.getCustomerId());
                    excelStructure.content[index][0] = order.getId();
                    excelStructure.content[index][1] = customer.getBarcode();
                    excelStructure.content[index][2] = customer.getPhone2Ext() + customer.getPhone2No();
                    excelStructure.content[index][3] = customer.getPhoneExt() + customer.getPhoneNo();
                    excelStructure.content[index][4] = order.getEntryCode();
                    if (!U.var.isEmptyStr(order.getApartment())) {
                        appartment = "ד. " + order.getApartment() + " ";
                    }
                    if (!U.var.isEmptyStr(order.getFloorNo())) {
                        appartment = appartment + "ק. " + order.getFloorNo();
                    }
                    excelStructure.content[index][5] = appartment;
                    excelStructure.content[index][6] = order.getCity();
                    excelStructure.content[index][7] = order.getStreet() + " " + order.getStreetNo();
                    excelStructure.content[index][8] = order.getLastName();
                    excelStructure.content[index][9] = order.getFirstName();
                    excelStructure.content[index][10] = common.createOrderCode(order.getId());
                    excelStructure.content[index][11] = order.getRemarks();
                    excelStructure.content[index][12] = customer.getRemarks();

                    index++;
                }
                break;

            case 2:

                JSONObject customersCodes = new JSONObject();
                JSONObject logisticCenter = new JSONObject().
                        put("להבים", "DAROM").put("עומר", "DAROM").put("יבנה", "DAROM").put("להב", "DAROM").put("באר שבע", "DAROM").
                        put("בית קמה", "DAROM").put("גן יבנה", "DAROM").put("כפר אביב", "DAROM").put("כפר ורבורג", "DAROM").put("מושב שפיר", "DAROM").
                        put("שפיר", "DAROM").put("תלמי יחיאל", "DAROM").put("ניר בנים", "DAROM").put("גדרה", "DAROM").put("אשקלון", "DAROM").
                        put("משמר הנגב", "DAROM").put("אשדוד", "DAROM").put("באר טוביה", "DAROM").put("כרמים", "DAROM").put("דבירה", "DAROM").
                        put("ערוגות", "DAROM").put("באר מילכה", "DAROM").put("ירוחם", "DAROM").put("עומר פארק תעשיה", "DAROM").put("קיבוץ בית קמה", "DAROM").
                        put("קיבוץ להב", "DAROM").put("רבדים", "DAROM").put("בני עי״ש", "DAROM").put("לכיש", "DAROM").put("מיתר", "DAROM").
                        put("שדה צבי", "DAROM").put("קדרון", "DAROM").put("דבירה", "DAROM").put("יד מרדכי", "DAROM").put("שקף", "DAROM").
                        put("אורות", "DAROM").put("שובל", "DAROM").put("כפר אחים", "DAROM").put("בני עייש", "DAROM").put("בני דרום", "DAROM").put("בית גמליאל", "DAROM").
                        put("חיפה", "HH").put("זכרון יעקב", "HH").put("קיסריה", "HH").put("קיסריה א.ת.", "HH").put("קרית חיים חיפה", "HH").
                        put("קרית ים", "HH").put("קרית חיים", "HH").put("קרית מוצקין", "HH").put("קרית ביאליק", "HH").put("קרית אתא", "HH").
                        put("קרית טבעון", "HH").put("קריית ים", "HH").put("קריית חיים", "HH").put("קריית מוצקין", "HH").put("קריית ביאליק", "HH").
                        put("קריית אתא", "HH").put("קריית טבעון", "HH").put("כפר חסידים", "HH").put("רמת ישי", "HH").put("רמת יוחנן", "HH").
                        put("בית שערים", "HH").put("כפר ביאליק", "HH").put("אלונים", "HH").put("יוקנעם", "HH").put("יוקנעם עילית", "HH").
                        put("כפר יהושע", "HH").put("בית אורן", "HH").put("יגור", "HH").put("הסוללים", "HH").put("קיבוץ הסוללים", "HH").
                        put("נהריה", "HH").put("ראש הנקרה", "HH").put("יקנעם", "HH").put("יקנעם עילית", "HH").put("בית לחם הגלילית", "HH").
                        put("זרזיר", "HH").put("ציפורי", "HH").put("צפורי", "HH").put("גבעת האלה", "HH").put("שמשית", "HH").put("אלון הגליל", "HH").
                        put("לימן", "HH").put("גשר הזיו", "HH").put("סער", "HH").put("עברון", "HH").put("מזרעה", "HH").put("רגבה", "HH").
                        put("לוחמי הגטאות", "HH").put("שמרת", "HH").put("בוסתן הגליל", "HH").put("עכו", "HH").put("עין המפרץ", "HH").put("כפר מסריק", "HH").
                        put("בן עמי", "HH").put("כברי", "HH").put("מצובה", "HH").put("שלומי", "HH").put("בצת", "HH").put("החותרים", "HH").
                        put("צרופה", "HH").put("עין איילה", "HH").put("עופר", "HH").put("מעיין צבי", "HH").put("גבעת נילי", "HH").put("עמיקם", "HH").
                        put("אביאל", "HH").put("רגבים", "HH").put("אלוני יצחק", "HH").put("כפר גליקסון", "HH").put("משמרות", "HH").put("כפר פינס", "HH").
                        put("ברקאי", "HH").put("נשר", "HH").put("יקנעם", "HH").put("פרדס חנה", "HH").put("פרדס חנה כרכור", "HH").put("חריש", "HH").put("חדרה", "HH").put("עפולה", "HH").
                        put("מגדל תפן", "HH").put("נופית", "HH").
                        put("תל מונד", "NS").put("הרצליה", "NS").put("הוד השרון", "NS").put("רעננה", "NS").put("נתניה", "NS").
                        put("אומץ", "NS").put("גבעת שפירא", "NS").put("בצרה", "NS").put("געש", "NS").put("גנות הדר", "NS").put("נווה ירק", "NS").
                        put("עין ורד", "NS").put("צור יגאל", "NS").put("קיבוץ בחן", "NS").put("בחן", "NS").put("מאור", "NS").
                        put("בת חפר", "NS").put("כפר סבא", "NS").put("חרות", "NS").put("עין וורד", "NS").put("בית חפר שער יצחק", "NS").put("בית יצחק שער חפר", "NS").
                        put("קדימה", "NS").put("בת-חפר", "NS").put("רשפון", "NS").put("מושב בצרה", "NS").put("חגלה", "NS").
                        put("יד חנה", "NS").put("עין שריד", "NS").put("יד שרה", "NS").put("כפר נטר", "NS").put("שער אפרים", "NS").
                        put("קיבוץ אייל", "NS").put("רמת השרון", "NS").put("קדימה צורן", "NS").
                        put("הרצליה פיתוח", "NS").put("בני דרור", "NS").put("אבן יהודה", "NS").put("נורדיה", "NS").
                        put("בית יהושוע", "NS").put("צור יצחק", "NS").put("ירקונה", "NS").put("כפר יונה", "NS").
                        put("מדרשת בן גוריון", "YY").put("מדרשת בן-גוריון", "YY").put("טללים", "YY").put("אשלים", "YY").put("שדה בוקר", "YY").
                        put("מצפה רמון", "YY").put("רביבים", "YY").
                        put("אלפי מנשה", "ZZ").put("נבטים", "ZZ").put("בסיס נבטים", "ZZ").put("ברקן", "ZZ").put("אריאל", "ZZ");

                orders = ordersDao.findAllHarvestingOrder();
                excelStructure.filename = "dhlFormatReport_" + dateStr.toString() + ".xlsx";
                excelStructure.content = new Object[orders.size()][33];

                index = 0;
                for (Order order : orders) {
                    appartment = "";
                    customer = customersDao.findOne(order.getCustomerId());
                    String customerBarcode= customer.getBarcode();
                    excelStructure.content[index][0] = order.getId();
                    excelStructure.content[index][1] = dateFormat.format(order.getDeliveryDate());
                    excelStructure.content[index][2] = "D";
                    excelStructure.content[index][3] = "CUST";
                    excelStructure.content[index][4] = "1";
                    excelStructure.content[index][5] = "";
                    excelStructure.content[index][6] = "";
                    excelStructure.content[index][7] = "";

                    String center;
                    if (logisticCenter.has(order.getCity())) {
                        center = logisticCenter.get(order.getCity()).toString();
                    } else {
                        center = "DGF";
                    }
                    excelStructure.content[index][8] = center;
                    excelStructure.content[index][9] = "";
                    excelStructure.content[index][10] = "DGF";
                    if (customersCodes.has(customerBarcode)) {
                        Integer customerIndex = U.var.parseInt(customersCodes.getString(customerBarcode)) + 1;
                        customersCodes.put(customer.getBarcode(), customerIndex.toString());
                        excelStructure.content[index][11] = customer.getBarcode() + "-" + customerIndex.toString();
                    } else {
                        customersCodes.put(customer.getBarcode(), "1");
                        excelStructure.content[index][11] = customer.getBarcode();
                    }

                    excelStructure.content[index][12] = "Customer";
                    excelStructure.content[index][13] = order.getLastName() + " " + order.getFirstName();
                    excelStructure.content[index][14] = "";
                    excelStructure.content[index][15] = excelStructure.content[index][11];
                    excelStructure.content[index][16] = order.getLastName() + " " + order.getFirstName();
                    excelStructure.content[index][17] = "";
                    excelStructure.content[index][18] = order.getStreet() + " " + order.getStreetNo();
                    excelStructure.content[index][19] = "";
                    excelStructure.content[index][20] = order.getEntryCode();
                    excelStructure.content[index][21] = order.getCity();
                    excelStructure.content[index][22] = "";
                    excelStructure.content[index][23] = customer.getPhoneExt() + customer.getPhoneNo();
                    excelStructure.content[index][24] = customer.getPhone2Ext() + customer.getPhone2No();
                    excelStructure.content[index][25] = "7";
                    excelStructure.content[index][26] = "1";
                    excelStructure.content[index][27] = "1";
                    excelStructure.content[index][28] = "1";
                    excelStructure.content[index][29] = "1";
                    excelStructure.content[index][30] = "1";
                    if (!U.var.isEmptyStr(order.getApartment())) {
                        appartment = "ד. " + order.getApartment() + " ";
                    }
                    if (!U.var.isEmptyStr(order.getFloorNo())) {
                        appartment = appartment + "ק. " + order.getFloorNo();
                    }
                    excelStructure.content[index][31] = appartment;

                    String remarks = "";
                    if (!U.var.isEmptyStr(order.getRemarks())) {
                        remarks = order.getRemarks() + " ";
                    }
                    if (!U.var.isEmptyStr(customer.getRemarks())) {
                        remarks = remarks + customer.getRemarks();
                    }

                    excelStructure.content[index][32] = remarks;

                    index++;
                }
                break;

            case 6: // Mapotempo

                if (U.var.parseInt(globalsDao.findOne("debug_mode").getValue()) == 1) {
                    orders = ordersDao.findAllHarvestingOrderByDeliveryWindow(deliveryWindow, 1);
                } else {
                    orders = ordersDao.findAllHarvestingOrderByDeliveryWindow(deliveryWindow, 5);
                }
                if (deliveryWindow == 0) {
                    excelStructure.filename = "mapotempoFormatReport_" + dateStr.toString() + "_m_.csv";
                } else {
                    excelStructure.filename = "mapotempoFormatReport_" + dateStr.toString() + "_e_.csv";
                }

                excelStructure.content = new Object[orders.size()][31];


                index = 0;
                for (Order order : orders) {

                    customer = customersDao.findOne(order.getCustomerId());

                    excelStructure.content[index][0] = order.getId();
                    excelStructure.content[index][1] = order.getFirstName() + " " + order.getLastName();
                    excelStructure.content[index][2] = order.getStreet() + " " + order.getStreetNo();

                    appartment = "";
                    if (!U.var.isEmptyStr(order.getApartment())) {
                        appartment = "ד. " + order.getApartment() + " ";
                    }
                    if (!U.var.isEmptyStr(order.getFloorNo())) {
                        appartment = appartment + "ק. " + order.getFloorNo();
                    }
                    excelStructure.content[index][3] = appartment;
                    excelStructure.content[index][4] = "";
                    excelStructure.content[index][5] = order.getCity();
                    excelStructure.content[index][6] = "ישראל";

                    boolean sameAddress = true;

                    excelStructure.content[index][7] = "";
                    excelStructure.content[index][8] = "";
                    excelStructure.content[index][9] = "";
                    excelStructure.content[index][10] = "";

                    if (customer.getForceGeotag() > 0) { // If force geotag
                        GeoPoint geoPoint = geoPoinsDao.findOne(customer.getForceGeotag());
                        excelStructure.content[index][7] = geoPoint.getLatitude().toString();
                        excelStructure.content[index][8] = geoPoint.getLongitude().toString();
                        excelStructure.content[index][9] = "";
                        excelStructure.content[index][10] = "";
                    }

                    List<GeoPoint> geoPoints = geoPoinsDao.findAllByCustomerId(customer.getId());
                    String timeConstraints = "NA";
                    if ((geoPoints.size() > 0) && customer.getForceGeotag() == 0) { // If customer has a geopoint if same address get geopoint
                        for (GeoPoint customerGeoPoint : geoPoints) {
                            sameAddress = true;
                            if (!order.getCity().equals(customerGeoPoint.getCity())) {
                                sameAddress = false;
                            }
                            if (!order.getStreet().equals(customerGeoPoint.getStreet())) {
                                sameAddress = false;
                            }
                            if (!order.getStreetNo().equals(customerGeoPoint.getStreetNumber())) {
                                sameAddress = false;
                            }
                            if (sameAddress) {
                                excelStructure.content[index][7] = customerGeoPoint.getLatitude().toString();
                                excelStructure.content[index][8] = customerGeoPoint.getLongitude().toString();
                                excelStructure.content[index][9] = "";
                                excelStructure.content[index][10] = "";
                                timeConstraints = customerGeoPoint.getTimeConstraints();
                            }
                        }
                    }


                    String remarks = "";
                    if (!U.var.isEmptyStr(order.getRemarks())) {
                        remarks = order.getRemarks() + " ";
                    }
                    if (!U.var.isEmptyStr(customer.getRemarks())) {
                        remarks = remarks + customer.getRemarks();
                    }
                    excelStructure.content[index][11] = remarks;
                    excelStructure.content[index][12] = customer.getPhoneExt() + "-" + customer.getPhoneNo();

                    String center;
                    CityGroup cityGroup = cityGroupsDoa.findOneByCity(order.getCity());
                    if (cityGroup == null) {
                        center = "None";
                    } else {
                        center = cityGroup.getDescription();
                    }
                    excelStructure.content[index][13] = center;
                    excelStructure.content[index][14] = "";
                    excelStructure.content[index][15] = "";
                    if ((cityGroup != null) && (cityGroup.getId() == 14) && (customer.getCouponGroup() == 2)) {
                        excelStructure.content[index][16] = "00:01:00";
                    } else if ((cityGroup != null) && (cityGroup.getId() == 14)) {
                        excelStructure.content[index][16] = "00:05:00";
                    } else {
                        excelStructure.content[index][16] = "00:10:00";
                    }

                    if (timeConstraints.equals("NA")) {
                        if (order.getDeliveryWindow() == 0) {
                            excelStructure.content[index][17] = "08:00";
                            excelStructure.content[index][18] = "17:00";
                        } else {
                            excelStructure.content[index][17] = "15:00";
                            excelStructure.content[index][18] = "23:30";
                        }
                    } else {
                        String[] hours;
                        String[] times = timeConstraints.split(";");
                        if (order.getDeliveryWindow() == 0) {
                            hours = times[0].split("-");
                        } else {
                            hours = times[1].split("-");
                        }
                        excelStructure.content[index][17] = hours[0];
                        excelStructure.content[index][18] = hours[1];
                    }
                    excelStructure.content[index][19] = "";
                    excelStructure.content[index][20] = "";
                    excelStructure.content[index][21] = "";
                    excelStructure.content[index][22] = "";
                    excelStructure.content[index][23] = "1";
                    excelStructure.content[index][24] = "";
                    excelStructure.content[index][25] = "";
                    excelStructure.content[index][26] = "";

                    Integer boxes = U.var.parseInt(Math.ceil(order.getSum() / 250.0));
                    excelStructure.content[index][27] = boxes;
                    excelStructure.content[index][28] = "";
                    excelStructure.content[index][29] = "";
                    excelStructure.content[index][30] = "";

                    index++;
                }
                break;

            case 8: // Mapotempo summary

                List<Integer> customerIds = ordersDao.findAllCustomerIdsDeliveredBetweenDates(startDate, endDate);
                excelStructure.filename = "mapotempoSummaryReport_" + dateStr.toString() + ".csv";

                excelStructure.content = new Object[customerIds.size()][31];

                index = 0;
                for (Integer id : customerIds) {

                    Order order = ordersDao.findOneByCustomerIdDesc(id);
                    customer = customersDao.findOne(id);

                    excelStructure.content[index][0] = order.getId();
                    excelStructure.content[index][1] = order.getFirstName() + " " + order.getLastName();
                    excelStructure.content[index][2] = order.getStreet() + " " + order.getStreetNo();

                    appartment = "";
                    if (!U.var.isEmptyStr(order.getApartment())) {
                        appartment = "ד. " + order.getApartment() + " ";
                    }
                    if (!U.var.isEmptyStr(order.getFloorNo())) {
                        appartment = appartment + "ק. " + order.getFloorNo();
                    }
                    excelStructure.content[index][3] = appartment;
                    excelStructure.content[index][4] = "";
                    excelStructure.content[index][5] = order.getCity();
                    excelStructure.content[index][6] = "ישראל";

                    boolean sameAddress = true;

                    excelStructure.content[index][7] = "";
                    excelStructure.content[index][8] = "";
                    excelStructure.content[index][9] = "";
                    excelStructure.content[index][10] = "";

                    if (customer.getForceGeotag() > 0) { // If force geotag
                        GeoPoint geoPoint = geoPoinsDao.findOne(customer.getForceGeotag());
                        excelStructure.content[index][7] = geoPoint.getLatitude().toString();
                        excelStructure.content[index][8] = geoPoint.getLongitude().toString();
                        excelStructure.content[index][9] = "";
                        excelStructure.content[index][10] = "";
                    }

                    List<GeoPoint> geoPoints = geoPoinsDao.findAllByCustomerId(customer.getId());
                    String timeConstraints = "NA";
                    if ((geoPoints.size() > 0) && customer.getForceGeotag() == 0) { // If customer has a geopoint if same address get geopoint
                        for (GeoPoint customerGeoPoint : geoPoints) {
                            sameAddress = true;
                            if (!order.getCity().equals(customerGeoPoint.getCity())) {
                                sameAddress = false;
                            }
                            if (!order.getStreet().equals(customerGeoPoint.getStreet())) {
                                sameAddress = false;
                            }
                            if (!order.getStreetNo().equals(customerGeoPoint.getStreetNumber())) {
                                sameAddress = false;
                            }
                            if (sameAddress) {
                                excelStructure.content[index][7] = customerGeoPoint.getLatitude().toString();
                                excelStructure.content[index][8] = customerGeoPoint.getLongitude().toString();
                                excelStructure.content[index][9] = "";
                                excelStructure.content[index][10] = "";
                                timeConstraints = customerGeoPoint.getTimeConstraints();
                            }
                        }
                    }

                    String remarks = "";
                    if (!U.var.isEmptyStr(order.getRemarks())) {
                        remarks = order.getRemarks() + " ";
                    }
                    if (!U.var.isEmptyStr(customer.getRemarks())) {
                        remarks = remarks + customer.getRemarks();
                    }
                    excelStructure.content[index][11] = remarks;
                    excelStructure.content[index][12] = customer.getPhoneExt() + "-" + customer.getPhoneNo();

                    String center;
                    CityGroup cityGroup = cityGroupsDoa.findOneByCity(order.getCity());
                    if (cityGroup == null) {
                        center = "None";
                    } else {
                        center = cityGroup.getDescription();
                    }
                    excelStructure.content[index][13] = center;
                    excelStructure.content[index][14] = "";
                    excelStructure.content[index][15] = "";
                    excelStructure.content[index][16] = "00:05:00";

                    excelStructure.content[index][17] = "08:00";
                    excelStructure.content[index][18] = "17:00";
                    excelStructure.content[index][19] = "";
                    excelStructure.content[index][20] = "";
                    excelStructure.content[index][21] = "";
                    excelStructure.content[index][22] = "";
                    excelStructure.content[index][23] = "1";
                    excelStructure.content[index][24] = "";
                    excelStructure.content[index][25] = "";
                    excelStructure.content[index][26] = "";

                    Integer boxes = U.var.parseInt(Math.ceil(order.getSum() / 250.0));
                    excelStructure.content[index][27] = boxes;
                    excelStructure.content[index][28] = "";
                    excelStructure.content[index][29] = "";
                    excelStructure.content[index][30] = "";

                    index++;
                }
                break;

            case 3:
            case 4:
            case 5:
                orders = null;
                if ((type == 3) || (type == 5)) {
                    if (dateFormat.format(endDate).equals(dateFormat.format(new Date())) || (type == 5)) {
                        orders = ordersDao.findAllWithStatusWithDeliveryDateOrderByParam(Order.OrderStatuses.SHIPMENT.ordinal(), startDate, "dhl_package_number");
                    } else {
                        orders = ordersDao.findAllWithStatusBetweenDeliveryDateOrderByParam(Order.OrderStatuses.SHIPMENT.ordinal(), startDate, endDate, "dhl_package_number");
                    }
                } else if (type == 4) {
                    if (dateFormat.format(endDate).equals(dateFormat.format(new Date()))) {
                        orders = ordersDao.findFilteredWithStatusWithDeliveryDateOrderByParam(Order.OrderStatuses.SHIPMENT.ordinal(), startDate, "dhl_package_number");
                    } else {
                        orders = ordersDao.findFilteredWithStatusBetweenDeliveryDateOrderByParam(Order.OrderStatuses.SHIPMENT.ordinal(), startDate, endDate, "dhl_package_number");
                    }
                }

                excelStructure.content = new Object[orders.size()][5];
                excelStructure.filename = "boxesReport_" + dateStr.toString() + ".xlsx";
                index = 0;
                int unifiedOrders = 0;
                int singleOrders = 0;
                int doubleOrders = 0;
                int tripleOrders = 0;
                int largeOrders = 0;
                int totalBoxes = 0;

                for (Order order : orders) {
                    customer = customersDao.findOne(order.getCustomerId());
                    excelStructure.content[index][0] = order.getId();
                    excelStructure.content[index][1] = customer.getBarcode().substring(3, 4);
                    excelStructure.content[index][2] = order.getDhlPackageNumber();
                    excelStructure.content[index][3] = U.var.parseInt(order.getBoxes());
                    excelStructure.content[index][4] = dateFormat.format(order.getDeliveryDate());


                    switch (order.getBoxes()) { // Set summary of boxes
                        case 0:
                            unifiedOrders++;
                            break;
                        case 1:
                            singleOrders++;
                            totalBoxes++;
                            break;
                        case 2:
                            doubleOrders++;
                            totalBoxes = totalBoxes + 2;
                            break;
                        case 3:
                            tripleOrders++;
                            totalBoxes = totalBoxes + 3;
                            break;
                        default:
                            largeOrders++;
                            totalBoxes = totalBoxes + order.getBoxes();
                            break;
                    }

                    index++;
                }

                Float percentage = (U.var.parseFloat(totalBoxes) - U.var.parseFloat(singleOrders)) / U.var.parseFloat(singleOrders);

                excelStructure.title = new String[]{"דוח ארגזים ", "תאריך משלוח", dateFormat.format(startDate), "",
                        "מספר ארגזים", Integer.toString(totalBoxes),
                        "אחוז כפולות", Float.toString(percentage),
                        "מאוחדות", Integer.toString(unifiedOrders),
                        "בודדות", Integer.toString(singleOrders),
                        "כפולות", Integer.toString(doubleOrders),
                        "משולשות", Integer.toString(tripleOrders),
                        "גדולות", Integer.toString(largeOrders)};
                break;

            case 7 : // Drivers report
                Date start = new Date();
                try {
                    start = dateFormat.parse("01/01/1970");
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (startDate.equals(start)) {
                    Long time = new Date().getTime();
                    Date now = new Date(time - (time % (24 * 60 * 60 * 1000) - 0 * 60 * 60 * 1000));
                    startDate = new Date(now.getTime() + 24*60*60*1000);
                }
                List<Order> morningOrders = ordersDao.findAllPendingOrderByDeliveryWindowReadyToDhlOrdered(0, startDate);
                List<Order> eveningOrders = ordersDao.findAllPendingOrderByDeliveryWindowReadyToDhlOrdered(1, startDate);
                List<Routes> routes = routesDao.findAllPendingByDeliveryDate(startDate);

                Integer numberOfRoutes= ordersDao.findAllPendingOrderPrifix(0).size();
                numberOfRoutes += ordersDao.findAllPendingOrderPrifix(1).size();
                List<Order> combinedOrders = new ArrayList<Order>();
                combinedOrders.addAll(eveningOrders);
                combinedOrders.addAll(morningOrders);
                excelStructure.content = new Object[combinedOrders.size()+routes.size()+4][10];
                excelStructure.filename = "driversReport_" + dateStr.toString() + ".xlsx";

                index = 0;
                Character pallete = ' ';
                Character lastPallete = ' ';
                for (Order order : combinedOrders) {

                    pallete = order.getDhlPackageNumber().charAt(0);
                    if (!pallete.equals(lastPallete)) {
                        excelStructure.content[index][0] = "";
                        excelStructure.content[index][1] = "שם הנהג:";
                        RouteStop stop = routeStopDao.findOneByOrderId(order.getId());
                        Routes route = routesDao.findOne(stop.getRouteId());
                        excelStructure.content[index][2] = route.getDriverName();
                        excelStructure.content[index][3] = "תאריך:";
                        Date deliveryDate = order.getDeliveryWindow() == 1 ? new DateTime(order.getDeliveryDate()).minusDays(1).toDate() : order.getDeliveryDate();

                        excelStructure.content[index][4] = new SimpleDateFormat("dd/MM/yyyy").format(deliveryDate);
                        excelStructure.content[index][5] = "";
                        if (order.getDeliveryWindow() == 1) {
                            excelStructure.content[index][6] = "משמרת ערב";
                        } else {
                            excelStructure.content[index][6] = "משמרת בוקר";
                        }
                        excelStructure.content[index][7] = "";
                        excelStructure.content[index][8] = "";
                        excelStructure.content[index][9] = "";
                        index++;
                    }

                    customer = customersDao.findOne(order.getCustomerId());

                    excelStructure.content[index][0] = order.getDhlPackageNumber();
                    excelStructure.content[index][1] = order.getFirstName() + " " + order.getLastName();
                    excelStructure.content[index][2] = order.getStreet() + " " + order.getStreetNo();
                    excelStructure.content[index][3] = order.getCity();

                    appartment = "";
                    if (!U.var.isEmptyStr(order.getApartment())) {
                        appartment = "ד. " + order.getApartment() + " ";
                    }
                    if (!U.var.isEmptyStr(order.getFloorNo())) {
                        appartment = appartment + "ק. " + order.getFloorNo();
                    }

                    excelStructure.content[index][4] = appartment;
                    excelStructure.content[index][5] = order.getEntryCode();
                    excelStructure.content[index][6] = customer.getPhoneExt() + "-" + customer.getPhoneNo();
                    excelStructure.content[index][7] = customer.getPhone2Ext() + "-" + customer.getPhone2No();

                    String remarks = "";
                    if (!U.var.isEmptyStr(order.getRemarks())) {
                        remarks = order.getRemarks() + "   ";
                    }
                    if (!U.var.isEmptyStr(customer.getRemarks())) {
                        remarks = remarks + customer.getRemarks();
                    }
                    excelStructure.content[index][8] = remarks;
                    excelStructure.content[index][9] = order.getId();

                    lastPallete = order.getDhlPackageNumber().charAt(0);
                    index++;
                }

                break;
            default: // Do nothing
                excelStructure.content = new Object[1][11];
                excelStructure.filename = "dhlError_" + dateStr.toString() + ".xlsx";
                break;
        }

        return excelStructure;
    }

    public ExcelStructure generatePredictionsReport(Integer type, Date startDate, Date endDate, Integer supplierId, Integer categoryId, HttpServletRequest request) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date earliestDate = null;
        try {
            earliestDate = dateFormat.parse("02/02/1970");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        ExcelStructure excelStructure = new ExcelStructure();

        Map<String, Object[]> column = new HashMap<>();
        Product singleProduct;
        Object[] singleColumn;


        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH) + 1;
        int day = cal.get(Calendar.DAY_OF_MONTH);

        StringBuilder dateStr = new StringBuilder();
        dateStr.append(day).append("_").append(month).append("_").append(year);
        String fileName;
        List<Order> orders;
        Customer customer;
        String appartment;
        int index = 0;


        switch (type) {
            case 0: // Sell prediction by supplier
            case 1: // Sell prediction by category
                excelStructure.title = new String[]{"דוח תחזית קניה מספקים"};
                if (startDate.before(earliestDate)) {
                    orders = ordersDao.findAllOpen();
                } else {
                    orders = ordersDao.findAllOpenWithDeliveryDate(startDate);
                }
                fileName = "predictionReport_" + dateStr.toString() + ".xlsx";
                excelStructure.content = new Object[orders.size()][6];

                index = 0;
                if (orders.size() > 0) {
                    for (Order order : orders) {
                        index++;
                        if (index % 1 == 0) {
                        }
                        for (OrderProduct orderProduct : orderProductDao.findAllByOrderId(order.getId())) {

                            boolean productMatch = false;
                            singleProduct = productsDao.findOne(orderProduct.getProductId());

                            switch (type) {
                                case 0: // Report by supplier
                                    categoryId = 0;
                                    if (supplierId != null) {
                                        List<Item> items = itemsDao.findAllByProductIdWithParam(orderProduct.getProductId());
                                        for (Item item : items) {
                                            productMatch = item.getSupplierId() == supplierId;
                                        }
                                    } else {
                                        productMatch = true;
                                    }
                                    break;

                                case 1: // Report by category
                                    supplierId = 0;
                                    if (categoryId != null) {
                                        productMatch = singleProduct.getCategoryId() == categoryId;
                                    } else {
                                        productMatch = true;
                                    }
                                    break;

                                default:
                                    break;
                            }

                            if (((supplierId == null) || (categoryId == null)) || productMatch) {
                                singleColumn = column.get(singleProduct.getId().toString());
                                if (singleColumn == null) {
                                    column.put(singleProduct.getId().toString(), new Object[]{singleProduct.getNameHeb(), singleProduct.getPackingWeightCode(), singleProduct.getPackageType().heb, U.var.parseInt(orderProduct.getQuantityNeeded()), singleProduct.getUnitWeight() * orderProduct.getQuantityNeeded(), orderProduct.getUnitPrice() * orderProduct.getQuantityNeeded(), 1});
                                } else {
                                    Integer iterations = U.var.parseInt(singleColumn[6]) + 1;
                                    Integer qty = U.var.parseInt(singleColumn[3]) + U.var.parseInt(orderProduct.getQuantityNeeded());
                                    Float totalWeight = U.var.parseFloat(singleColumn[4]) + U.var.parseFloat(singleProduct.getUnitWeight() * orderProduct.getQuantityNeeded());
                                    Float totalPrice = U.var.parseFloat(singleColumn[5]) + U.var.parseFloat(orderProduct.getUnitPrice()) * U.var.parseInt(orderProduct.getQuantityNeeded());
                                    column.put(singleProduct.getId().toString(), new Object[]{singleProduct.getNameHeb(), singleProduct.getPackingWeightCode(), singleProduct.getPackageType().heb, qty, totalWeight, totalPrice, iterations});
                                }
                            }
                        }
                    }

                    DecimalFormat decimalFormat = new DecimalFormat("#.##");
                    excelStructure.content = new Object[column.size()][7];
                    index = 0;
                    for (Map.Entry<String, Object[]> entry : column.entrySet()) {
                        excelStructure.content[index][0] = entry.getValue()[0];
                        excelStructure.content[index][1] = entry.getValue()[1];
                        excelStructure.content[index][2] = entry.getValue()[2];
                        excelStructure.content[index][3] = entry.getValue()[3];
                        excelStructure.content[index][4] = decimalFormat.format(entry.getValue()[4]);
                        excelStructure.content[index][5] = decimalFormat.format(entry.getValue()[5]);
                        excelStructure.content[index][6] = entry.getValue()[6];
                        index++;
                    }
                } else {
                    excelStructure.content = new Object[1][6];
                }
                break;

            default: // Do nothing
                excelStructure.title = new String[]{""};
                excelStructure.header = new String[]{};
                excelStructure.content = new Object[1][11];
                fileName = "predictionError_" + dateStr.toString() + ".xlsx";
                break;
        }

        return excelStructure;
    }

    public byte[] generateExcelFile(ExcelStructure excelStructure, int layout) {

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Shookbook Report");

        if (layout == 1) {
            sheet.getPrintSetup().setLandscape(true);
        }

        XSSFCellStyle titleStyle = workbook.createCellStyle();
        XSSFCellStyle headerStyle = workbook.createCellStyle();
        XSSFCellStyle contentStyle = workbook.createCellStyle();
        XSSFCellStyle contentOddStyle = workbook.createCellStyle();

        XSSFFont titleFont = workbook.createFont();
        XSSFFont headerFont = workbook.createFont();
        XSSFFont contentFont = workbook.createFont();

        byte[] rgb = new byte[3];
        rgb[0] = (byte) 240; // red
        rgb[1] = (byte) 240; // green
        rgb[2] = (byte) 240; // blue
        XSSFColor veryLightGray = new XSSFColor(rgb); // #f2dcdb

        short gray = IndexedColors.GREY_50_PERCENT.getIndex();
        short lightGray = IndexedColors.GREY_25_PERCENT.getIndex();
        short darkGray = IndexedColors.GREY_80_PERCENT.getIndex();
        short white = IndexedColors.WHITE.getIndex();

//            Map<String, CellStyle> styles = new HashMap<String, CellStyle>();


        titleStyle.setAlignment(CellStyle.ALIGN_CENTER);
        titleStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        titleStyle.setFillForegroundColor(white);
        titleStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
        titleStyle.setWrapText(false);

        titleFont.setFontHeightInPoints((short) 14);
        titleFont.setColor(darkGray);

        titleStyle.setFont(titleFont);

        headerStyle.setAlignment(CellStyle.ALIGN_CENTER);
        headerStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        headerStyle.setFillForegroundColor(gray);
        headerStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
        headerStyle.setWrapText(false);

        headerFont.setFontHeightInPoints((short) 14);
        headerFont.setColor(darkGray);

        headerStyle.setFont(headerFont);

        contentStyle.setAlignment(CellStyle.ALIGN_CENTER);
        contentStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        contentStyle.setFillForegroundColor(white);
        contentStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
        contentStyle.setWrapText(false);

        contentFont.setFontHeightInPoints((short) 14);
        contentFont.setColor(darkGray);

        contentStyle.setFont(contentFont);

        contentOddStyle.setAlignment(CellStyle.ALIGN_CENTER);
        contentOddStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        contentOddStyle.setFillForegroundColor(veryLightGray);
        contentOddStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
        contentOddStyle.setWrapText(false);

        contentOddStyle.setFont(contentFont);

        int rowNumber = 0;
        int columnNumber = 0;

        Row row = sheet.createRow(rowNumber++);
        row.setHeightInPoints(40);

        if (excelStructure.title != null) {
            for (Object field : excelStructure.title) {
                Cell cell = row.createCell(columnNumber++);
                if (field instanceof String) {
                    cell.setCellValue((String) field);
                } else if (field instanceof Integer) {
                    cell.setCellValue((Integer) field);
                } else if (field instanceof Float) {
                    cell.setCellValue((Float) field);
                } else if (field instanceof Double) {
                    cell.setCellValue((Double) field);
                }
                cell.setCellStyle(titleStyle);
            }
            row = sheet.createRow(rowNumber++);
            columnNumber = 0;
        }

        row.setHeightInPoints(30);

        for (Object field : excelStructure.header) {
            Cell cell = row.createCell(columnNumber++);
            if (field instanceof String) {
                cell.setCellValue((String) field);
            } else if (field instanceof Integer) {
                cell.setCellValue((Integer) field);
            }

            cell.setCellStyle(headerStyle);
        }

        for (Object[] line : excelStructure.content) {
            row = sheet.createRow(rowNumber++);
            row.setHeightInPoints(30);

            if (U.var.parseInt(rowNumber % 2) == 1) {
            } else {
            }

            columnNumber = 0;
            for (Object field : line) {
                Cell cell = row.createCell(columnNumber++);
                if (field instanceof String) {
                    cell.setCellValue((String) field);
                } else if (field instanceof Integer) {
                    cell.setCellValue((Integer) field);
                } else if (field instanceof Float) {
                    cell.setCellValue((Float) field);
                } else if (field instanceof Double) {
                    cell.setCellValue((Double) field);
                }

                if (U.var.parseInt(rowNumber % 2) == 1) {
                    cell.setCellStyle(contentStyle);
                } else {
                    cell.setCellStyle(contentOddStyle);
                }
            }
        }

        for (int i = 0; i < excelStructure.content.length; i++) {
            sheet.autoSizeColumn(i);
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            workbook.write(baos);
            baos.flush();
            baos.close();
        } catch (IOException ex) {
            logger.error("error writing to excel", ex);
        }
        return baos.toByteArray();
    }

    @RequestMapping(value = "/admin/reports/ReportOrdersByCustomerId")
    public String ordersByCustomerIdGraph(Model model) {

        List<Map<String, Object>> customerList = new LinkedList<>();
        for (Customer customer : customersDao.findAll()) {
            Map<String, Object> column = new HashMap<>();
            column.put("name", customer.getFirstName() + " " + customer.getLastName());
            column.put("barcode", customer.getBarcode());
            column.put("id", customer.getId());
            customerList.add(column);
        }
        model.addAttribute("customerList", customerList);

        return "ReportOrdersByCustomerId";

    }

    @RequestMapping(value = "/admin/reports/ReportOrdersByCustomerId/presentData", method = RequestMethod.POST)
    public String ordersByCustomerIdPresentData(@RequestParam(value = "customerBarcode") String customerBarcode,
                                                @RequestParam(value = "startDate") String startDate,
                                                @RequestParam(value = "endDate") String endDate,
                                                Model model, HttpServletRequest request) {
        int customerId = customersDao.getCustomerIdByBarcode(customerBarcode);
        if (!U.var.parseBoolean(request.getSession().getAttribute("login"))) {
            return "redirect:/admin/login";
        }
        Date start = null, end = null;

        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
        try {
            start = f.parse(startDate);
            end = f.parse(endDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (start.before(end)) {
            Customer customer = customersDao.findOne(U.var.parseInt(customerId));
            List<Order> orders = ordersDao.findAllBetweenDatesAndCustomerId(start, end, customer.getId());
            List<Map<String, Object>> columns = new LinkedList<>();
            float totalSum = 0;
            for (Order order : orders) {
                totalSum += order.getSum();
            }
            for (Order order : orders) {
                Map<String, Object> column = new HashMap<>();

                column.put("orderNumber", common.createOrderCode(order.getId()));
                column.put("orderId", order.getId());
                column.put("orderSum", order.getSum());
                column.put("orderStatus", order.getStatus().heb);
                column.put("orderShippingDate", new SimpleDateFormat("dd-MM-yyyy").format(order.getShippingDate()));

                columns.add(column);
            }
            DecimalFormat dtime = new DecimalFormat("#.##");
            model.addAttribute("columns", columns);
            model.addAttribute("totalSum", dtime.format(totalSum));
            model.addAttribute("customerName", customer.getFirstName() + " " + customer.getLastName());
            model.addAttribute("customerBarcode", customer.getBarcode());
            model.addAttribute("customerId", customer.getId());
            model.addAttribute("startDate", startDate);
            model.addAttribute("endDate", endDate);
            return "ReportOrdersByCustomerIdPresentData";
        } else
            return "datesError";
    }

    @ResponseBody
    @RequestMapping(value = "/admin/reports/ReportOrdersByCustomerId/presentData/saveCSVFile", method = RequestMethod.GET)
    public Object reportOrdersByCustomerIdSendCSVFile(@RequestParam(value = "customerId") String customerId,
                                                      @RequestParam(value = "startDate") String startDate,
                                                      @RequestParam(value = "endDate") String endDate,
                                                      Model model, HttpServletResponse response) {
//            if (!U.var.parseBoolean(request.getSession().getAttribute("login")))
//            {
//                return "redirect:/admin/login";
//            }

        char NEW_LINE_SEPARATOR = '\n';
        char COMMA_DELIMITER = ',';
        Writer fileWriter = null;
        StringBuilder sBuilder = new StringBuilder();

        //Handling dates :
        Date start = null, end = null;

        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
        try {
            start = f.parse(startDate);
            end = f.parse(endDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        //Create CSV File :
        try {
//                File fileDir = new File("OrdersByCustomerId_Report.csv");
//                fileWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileDir), "UTF8"));

            Customer customer = customersDao.findOne(U.var.parseInt(customerId));
            sBuilder.append("דו״ח הזמנות לפי מספר לקוח");
            sBuilder.append(NEW_LINE_SEPARATOR);
            sBuilder.append("מספר הלקוח,שם הלקוח");
            sBuilder.append(NEW_LINE_SEPARATOR);


            sBuilder.append(customer.getBarcode());
            sBuilder.append(COMMA_DELIMITER);
            sBuilder.append(customer.getFirstName() + " " + customer.getLastName());
            sBuilder.append(COMMA_DELIMITER);
            sBuilder.append(NEW_LINE_SEPARATOR);


            sBuilder.append("מספר הזמנה, תאריך משלוח,סכום הזמנה, מצב הזמנה");
            sBuilder.append(NEW_LINE_SEPARATOR);


            List<Order> orders = ordersDao.findAllBetweenDatesAndCustomerId(start, end, customer.getId());

            float totalSum = 0;
            for (Order order : orders) {
                totalSum += order.getSum();
            }
            for (Order order : orders) {

                sBuilder.append(common.createOrderCode(order.getId()));
                sBuilder.append(COMMA_DELIMITER);
                sBuilder.append(new SimpleDateFormat("dd-MM-yyyy").format(order.getShippingDate()));
                sBuilder.append(COMMA_DELIMITER);
                sBuilder.append(order.getSum() + "");
                sBuilder.append(COMMA_DELIMITER);
                sBuilder.append(order.getStatus().heb);
                sBuilder.append(NEW_LINE_SEPARATOR);

            }
            DecimalFormat dtime = new DecimalFormat("#.##");

            sBuilder.append("סה״כ: ");
            sBuilder.append(dtime.format(totalSum));
            sBuilder.append(NEW_LINE_SEPARATOR);


            System.out.println("CSV file was created successfully !!!");

        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            e.printStackTrace();
        }


        response.setHeader("Content-Disposition", "attachment; filename=\"OrdersByCustomerId_Report.csv\"");
        response.setContentLength(sBuilder.length());
        response.setContentType("text/csv");
        return sBuilder.toString();

        //return "redirect:/admin/reports/ReportOrdersByCustomerId";
    }


    @RequestMapping(value = "/admin/reports/ReportProductByCustomerId", method = RequestMethod.GET)
    public Object reportProductByCustomerId(Model model, HttpServletRequest request) {

        List<Map<String, Object>> customerList = new LinkedList<>();
        for (Customer customer : customersDao.findAll()) {
            Map<String, Object> column = new HashMap<>();
            column.put("name", customer.getFirstName() + " " + customer.getLastName());
            column.put("id", customer.getId());
            column.put("barcode", customer.getBarcode());
            customerList.add(column);
        }
        model.addAttribute("customerList", customerList);

        return "ReportProductByCustomerId";
    }

    @RequestMapping(value = "/admin/reports/ReportProductByCustomerId/presentData", method = RequestMethod.POST)
    public String productByCustomerIdPresentData(@RequestParam(value = "customerBarcode") String customerBarcode,
                                                 @RequestParam(value = "startDate") String startDate,
                                                 @RequestParam(value = "endDate") String endDate,
                                                 Model model, HttpServletRequest request) {
        int customerId = customersDao.getCustomerIdByBarcode(customerBarcode);
        if (!U.var.parseBoolean(request.getSession().getAttribute("login"))) {
            return "redirect:/admin/login";
        }
        float totalSum = 0;
        DecimalFormat dtime = new DecimalFormat("#.##");
        Date start = null, end = null;
        Set<Integer> productIds = new HashSetIgnoresNulls<>();
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");

        try {
            start = f.parse(startDate);
            end = f.parse(endDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (start.before(end)) {

            Customer customer = customersDao.findOne(U.var.parseInt(customerId));
            List<Order> orders = ordersDao.findAllBetweenDatesAndCustomerId(start, end, customer.getId());
            List<Map<String, Object>> columns = new LinkedList<>();


            for (Order order : orders) {
                totalSum += order.getSum();
                for (OrderProduct orderProduct : orderProductDao.findAllByOrderId(order.getId())) {
                    Product product = productsDao.findOne(orderProduct.getProductId());
                    Item item = null;
                    if (orderProduct.getItemId() != null)
                        item = itemsDao.findOne(orderProduct.getItemId());
                    Map<String, Object> column = new HashMap<>();
                    column.put("itemId", (item != null ? item.getBarcode() : "-----"));
                    column.put("productDec", product.getNameHeb());
                    column.put("sellingCost", orderProduct.getUnitPrice());
                    column.put("quantity", orderProduct.getQuantityNeeded());
                    column.put("packageUnit", product.getPackageType().heb);
                    column.put("averagePrice", dtime.format(orderProductDao.getProductAveragePrice(U.var.parseInt(customerId), orderProduct.getProductId(), start, end)
                            / orderProductDao.getProductCountPerCustomer(U.var.parseInt(customerId), orderProduct.getProductId(), start, end)));

                    columns.add(column);

                }

            }

            model.addAttribute("totalSum", dtime.format(totalSum));
            model.addAttribute("customerName", customer.getFirstName() + " " + customer.getLastName());
            model.addAttribute("columns", columns);
            model.addAttribute("customerBarcode", customer.getBarcode());
            model.addAttribute("customerId", customer.getId());
            model.addAttribute("startDate", startDate);
            model.addAttribute("endDate", endDate);
            return "ReportProductByCustomerIdPresentData";
        } else
            return "datesError";
    }

    @ResponseBody
    @RequestMapping(value = "/admin/reports/ReportProductByCustomerId/presentData/saveCSVFile", method = RequestMethod.GET)
    public Object reportProductsByCustomerIdSendCSVFile(@RequestParam(value = "customerId") String customerId,
                                                        @RequestParam(value = "startDate") String startDate,
                                                        @RequestParam(value = "endDate") String endDate,
                                                        Model model, HttpServletResponse response) {

        String NEW_LINE_SEPARATOR = "\n";
        String COMMA_DELIMITER = ",";
        Writer fileWriter = null;
        StringBuilder sBuilder = new StringBuilder();
        float totalSum = 0;
        Customer customer = customersDao.findOne(U.var.parseInt(customerId));
        DecimalFormat dtime = new DecimalFormat("#.##");

        //Handling dates :
        Date start = null, end = null;

        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
        try {
            start = f.parse(startDate);
            end = f.parse(endDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        //Create CSV File :
        try {
            File fileDir = new File("ProductByCustomerId_Report.csv");
            fileWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileDir), "UTF8"));


            sBuilder.append("דו״ח מוצר לפי מספר לקוח");
            sBuilder.append(NEW_LINE_SEPARATOR);
            sBuilder.append("מספר הלקוח,שם הלקוח");
            sBuilder.append(NEW_LINE_SEPARATOR);

            sBuilder.append(customer.getBarcode());
            sBuilder.append(COMMA_DELIMITER);
            sBuilder.append(customer.getFirstName() + " " + customer.getLastName());
            sBuilder.append(COMMA_DELIMITER);
            sBuilder.append(NEW_LINE_SEPARATOR);

            sBuilder.append("מספר קטלוגי, תיאור מוצר,עלות מכירה,כמות, יחידת מידה, מחיר ממוצע");
            sBuilder.append(NEW_LINE_SEPARATOR);

            for (Order order : ordersDao.findAllBetweenDatesAndCustomerId(start, end, customer.getId())) {
                totalSum += order.getSum();
                for (OrderProduct orderProduct : orderProductDao.findAllByOrderId(order.getId())) {
                    Product product = productsDao.findOne(orderProduct.getProductId());
                    Item item = null;
                    if (orderProduct.getItemId() != null)
                        item = itemsDao.findOne(orderProduct.getItemId());

                    sBuilder.append(item.getBarcode());
                    sBuilder.append(COMMA_DELIMITER);
                    sBuilder.append(product.getNameHeb().replaceAll(",", " "));
                    sBuilder.append(COMMA_DELIMITER);
                    sBuilder.append(product.getPrice() + "");
                    sBuilder.append(COMMA_DELIMITER);
                    sBuilder.append(orderProduct.getQuantity() + "");
                    sBuilder.append(COMMA_DELIMITER);
                    sBuilder.append(product.getPackageType().heb);
                    sBuilder.append(COMMA_DELIMITER);
                    sBuilder.append(dtime.format(orderProductDao.getProductAveragePrice(U.var.parseInt(customerId), orderProduct.getProductId(), start, end)
                            / orderProductDao.getProductCountPerCustomer(U.var.parseInt(customerId), orderProduct.getProductId(), start, end)));
                    sBuilder.append(COMMA_DELIMITER);
                    sBuilder.append(NEW_LINE_SEPARATOR);

                }

            }


            sBuilder.append("סה״כ: ");
            sBuilder.append(dtime.format(totalSum));
            sBuilder.append(NEW_LINE_SEPARATOR);


            System.out.println("CSV file was created successfully !!!");

        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            e.printStackTrace();
        }

        response.setHeader("Content-Disposition", "attachment; filename=\"ProductByCustomerId_Report.csv\"");
        response.setContentLength(sBuilder.length());
        response.setContentType("text/csv");
        return sBuilder.toString();

    }


    @RequestMapping(value = "/admin/reports/ReportWeightByCustomerId", method = RequestMethod.GET)
    public Object reportWeightByCustomerId(Model model, HttpServletRequest request) {

        List<Map<String, Object>> customerList = new LinkedList<>();
        for (Customer customer : customersDao.findAll()) {
            Map<String, Object> column = new HashMap<>();
            column.put("name", customer.getFirstName() + " " + customer.getLastName());
            column.put("id", customer.getId());
            column.put("barcode", customer.getBarcode());
            customerList.add(column);
        }
        model.addAttribute("customerList", customerList);

        return "ReportWeightByCustomerId";
    }

    @RequestMapping(value = "/admin/reports/ReportWeightByCustomerId/presentData", method = RequestMethod.POST)
    public String weightByCustomerIdPresentData(@RequestParam(value = "customerBarcode") String customerBarcode,
                                                @RequestParam(value = "startDate") String startDate,
                                                @RequestParam(value = "endDate") String endDate,
                                                Model model, HttpServletRequest request) {
        int customerId = customersDao.getCustomerIdByBarcode(customerBarcode);
        if (!U.var.parseBoolean(request.getSession().getAttribute("login"))) {
            return "redirect:/admin/login";
        }
        Date start = null, end = null;
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
        DecimalFormat dtime = new DecimalFormat("#.##");
        try {
            start = f.parse(startDate);
            end = f.parse(endDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (start.before(end)) {
            Customer customer = customersDao.findOne(U.var.parseInt(customerId));
            List<Order> orders = ordersDao.findAllBetweenDatesAndCustomerId(start, end, customer.getId());
            List<Map<String, Object>> columns = new LinkedList<>();
            for (Order order : orders) {
                float OrderTotalWeight = 0;
                Map<String, Object> column = new HashMap<>();
                column.put("orderNumber", common.createOrderCode(order.getId()));
                column.put("orderId", order.getId());
                column.put("shipmentDate", new SimpleDateFormat("dd-MM-yyyy").format(order.getShippingDate()));
                column.put("boxes", order.getBoxes());
                column.put("refrigerate", (order.getRefrigerated() == 0 ? "לא" : "כן"));
                for (OrderProduct orderProduct : orderProductDao.findAllByOrderId(order.getId())) {
                    OrderTotalWeight += itemsDao.getOne(orderProduct.getItemId()).getUnitWeight();
                }
                column.put("orderWeight", dtime.format(OrderTotalWeight));


                columns.add(column);
            }
            Set<Integer> itemIds = new HashSetIgnoresNulls<>();
            for (Order order : orders) {
                for (OrderProduct orderProduct : orderProductDao.findAllByOrderId(order.getId())) {
                    itemIds.add(orderProduct.getItemId());
                }
            }
            List<Map<String, Object>> columns2 = new LinkedList<>();
            for (Iterator<Integer> it = itemIds.iterator(); it.hasNext(); ) {
                Map<String, Object> column = new HashMap<>();

                Item item = itemsDao.findOne(it.next());
                Product product = productsDao.findOne(item.getProductId());
                int itemTotalQuantity = 0;
                for (Order order : orders) {

                    for (OrderProduct orderProduct : orderProductDao.findAllByOrderId(order.getId())) {
                        if (orderProduct.getItemId() == item.getId()) {
                            itemTotalQuantity += orderProduct.getQuantity();
                        }
                    }


                }
                column.put("productDec", product.getNameHeb());
                column.put("catalogNo", item.getBarcode());
                column.put("unitWeight", (item != null ? item.getUnitWeight() : "----"));
                column.put("packageUnit", product.getPackageType().heb);
                column.put("itemTotalQuantity", itemTotalQuantity);
                column.put("totalWeight", dtime.format(itemTotalQuantity * item.getUnitWeight()));

                columns2.add(column);
            }


            model.addAttribute("columns2", columns2);
            model.addAttribute("columns", columns);
            model.addAttribute("customerName", customer.getFirstName() + " " + customer.getLastName());
            model.addAttribute("customerBarcode", customer.getBarcode());
            model.addAttribute("customerId", customer.getId());
            model.addAttribute("startDate", startDate);
            model.addAttribute("endDate", endDate);
            return "ReportWeightByCustomerIdPresentData";
        } else
            return "datesError";
    }

    @ResponseBody
    @RequestMapping(value = "/admin/reports/ReportWeightByCustomerId/presentData/saveCSVFile", method = RequestMethod.GET)
    public Object reportWeightByCustomerIdSendCSVFile(@RequestParam(value = "customerId") String customerId,
                                                      @RequestParam(value = "startDate") String startDate,
                                                      @RequestParam(value = "endDate") String endDate,
                                                      Model model, HttpServletResponse response) {

        String NEW_LINE_SEPARATOR = "\n";
        String COMMA_DELIMITER = ",";

        Customer customer = customersDao.findOne(U.var.parseInt(customerId));
        DecimalFormat dtime = new DecimalFormat("#.##");
        StringBuilder sBuilder = new StringBuilder();
        //Handling dates :
        Date start = null, end = null;

        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
        try {
            start = f.parse(startDate);
            end = f.parse(endDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        //Create CSV File :
        try {
            sBuilder.append("דו״ח משקל לפי מספר לקוח");
            sBuilder.append(NEW_LINE_SEPARATOR);
            sBuilder.append("מספר הלקוח,שם הלקוח");
            sBuilder.append(NEW_LINE_SEPARATOR);

            sBuilder.append(customer.getBarcode());
            sBuilder.append(COMMA_DELIMITER);
            sBuilder.append(customer.getFirstName() + " " + customer.getLastName());
            sBuilder.append(COMMA_DELIMITER);
            sBuilder.append(NEW_LINE_SEPARATOR);

            sBuilder.append("מספר הזמנה, תאריך משלוח,מספר ארגזים,הובלה בקירור,משקל הזמנה(ק״ג)");
            sBuilder.append(NEW_LINE_SEPARATOR);

            List<Order> orders = ordersDao.findAllBetweenDatesAndCustomerId(start, end, customer.getId());

            for (Order order : orders) {
                float OrderTotalWeight = 0;

                sBuilder.append(common.createOrderCode(order.getId()));
                sBuilder.append(COMMA_DELIMITER);
                sBuilder.append(new SimpleDateFormat("dd-MM-yyyy").format(order.getShippingDate()));
                sBuilder.append(COMMA_DELIMITER);
                sBuilder.append(order.getBoxes() + "");
                sBuilder.append(COMMA_DELIMITER);
                sBuilder.append((order.getRefrigerated() == 0 ? "לא" : "כן"));
                sBuilder.append(COMMA_DELIMITER);
                for (OrderProduct orderProduct : orderProductDao.findAllByOrderId(order.getId())) {
                    OrderTotalWeight += itemsDao.getOne(orderProduct.getItemId()).getUnitWeight();
                }
                sBuilder.append(COMMA_DELIMITER);
                sBuilder.append(dtime.format(OrderTotalWeight));
                sBuilder.append(NEW_LINE_SEPARATOR);

            }
            sBuilder.append(NEW_LINE_SEPARATOR);
            sBuilder.append(NEW_LINE_SEPARATOR);
            sBuilder.append("מספר קטלוגי, תיאור מוצר,משקל יחידה,יחידת מידה,כמות,משקל כולל(ק״ג)");
            sBuilder.append(NEW_LINE_SEPARATOR);

            Set<Integer> itemIds = new HashSetIgnoresNulls<>();
            for (Order order : orders) {
                for (OrderProduct orderProduct : orderProductDao.findAllByOrderId(order.getId())) {
                    itemIds.add(orderProduct.getItemId());
                }
            }

            for (Iterator<Integer> it = itemIds.iterator(); it.hasNext(); ) {

                Item item = itemsDao.findOne(it.next());
                Product product = productsDao.findOne(item.getProductId());
                int itemTotalQuantity = 0;
                for (Order order : orders) {

                    for (OrderProduct orderProduct : orderProductDao.findAllByOrderId(order.getId())) {
                        if (orderProduct.getItemId() == item.getId()) {
                            itemTotalQuantity += orderProduct.getQuantity();
                        }
                    }


                }
                sBuilder.append(item.getBarcode());
                sBuilder.append(COMMA_DELIMITER);
                sBuilder.append(product.getNameHeb().replaceAll(",", " "));
                sBuilder.append(COMMA_DELIMITER);
                sBuilder.append(item.getUnitWeight() + "");
                sBuilder.append(COMMA_DELIMITER);
                sBuilder.append(product.getPackageType().heb);
                sBuilder.append(COMMA_DELIMITER);
                sBuilder.append(itemTotalQuantity + "");
                sBuilder.append(COMMA_DELIMITER);
                sBuilder.append(dtime.format(itemTotalQuantity * item.getUnitWeight()));
                sBuilder.append(NEW_LINE_SEPARATOR);

            }


            System.out.println("CSV file was created successfully !!!");

        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            e.printStackTrace();
        }


        response.setHeader("Content-Disposition", "attachment; filename=\"WeightByCustomerId_Report.csv\"");
        response.setContentLength(sBuilder.length());
        response.setContentType("text/csv");
        return sBuilder.toString();


    }


    @RequestMapping(value = "/admin/reports/ReportSupplierForBuying", method = RequestMethod.GET)
    public Object reportSupplierForBuying(Model model, HttpServletRequest request) {

        List<Map<String, Object>> supplierList = new LinkedList<>();
        for (Supplier supplier : suppliersDao.findAll()) {
            Map<String, Object> column = new HashMap<>();
            column.put("name", supplier.getName());
            column.put("id", supplier.getId());
            supplierList.add(column);
        }
        model.addAttribute("supplierList", supplierList);
        return "ReportSupplierForBuying";
    }

    @RequestMapping(value = "/admin/reports/ReportSupplierForBuying/submit", method = RequestMethod.POST)
    public Object reportSupplierBuyingSubmit(
            @RequestParam(value = "supplierIds") Integer supplierId,
            @RequestParam(value = "startDate") String startDate,
            @RequestParam(value = "endDate") String endDate,
            HttpServletRequest request,
            Model model) {

//            List<String> suppliersList = Arrays.asList(supplierIds.split(","));
        float totalSum = 0;
        Date start = null, end = null;
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
        try {
            start = f.parse(startDate);
            end = f.parse(endDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (start.before(end)) {

            List<Map<String, Object>> supplierOrdersDetails = new LinkedList<>();
            ;


            for (SupplierOrder supplierOrder : supplierOrdersDao.findAllBetweenDatesAndSupplierId(start, end, supplierId)) {
                Map<String, Object> supplierOrders = new HashMap<>();
                supplierOrders.put("orderNumber", common.createSupplierOrderSBID(supplierId, supplierOrder.getId()));
                supplierOrders.put("orderId", supplierOrder.getId());
                supplierOrders.put("shipmentDate", new SimpleDateFormat("dd/MM/yyyy").format(supplierOrder.getCreatedDate()));
                supplierOrders.put("orderSum", supplierOrder.getSum());
                supplierOrders.put("orderStatus", supplierOrder.getStatus().heb);
                totalSum += supplierOrder.getSum();
                supplierOrdersDetails.add(supplierOrders);

            }

            model.addAttribute("totalSum", totalSum);
            model.addAttribute("supplierOrdersDetails", supplierOrdersDetails);
            model.addAttribute("supplierBarcode", common.pedSupplierNumber(supplierId));
            model.addAttribute("supplierId", suppliersDao.findOne(supplierId).getId());
            model.addAttribute("supplierName", suppliersDao.findOne(supplierId).getName());
            model.addAttribute("startDate", startDate);
            model.addAttribute("endDate", endDate);


            return "ReportSupplierForBuyingCompareSuppliers";
        } else
            return "wrongInputError";
    }


    @ResponseBody
    @RequestMapping(value = "/admin/reports/ReportSupplierForBuying/submit/saveCSVFile", method = RequestMethod.GET)
    public Object reportSupplierForBuyingSendCSVFile(@RequestParam(value = "supplierId") String supplierId,
                                                     @RequestParam(value = "startDate") String startDate,
                                                     @RequestParam(value = "endDate") String endDate,
                                                     Model model, HttpServletResponse response) {

        String NEW_LINE_SEPARATOR = "\n";
        String COMMA_DELIMITER = ",";

        DecimalFormat dtime = new DecimalFormat("#.##");
        StringBuilder sBuilder = new StringBuilder();
        //Handling dates :
        Date start = null, end = null;

        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
        try {
            start = f.parse(startDate);
            end = f.parse(endDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        //Create CSV File :
        try {

            float totalSum = 0;
            Supplier supplier = suppliersDao.findOne(U.var.parseInt(supplierId));

            sBuilder.append("דו״ח ספק לקנייה");
            sBuilder.append(NEW_LINE_SEPARATOR);
            sBuilder.append("מספר ספק,שם הספק");
            sBuilder.append(NEW_LINE_SEPARATOR);

            sBuilder.append(common.pedSupplierNumber(supplier.getId()));
            sBuilder.append(COMMA_DELIMITER);
            sBuilder.append(supplier.getName());
            sBuilder.append(COMMA_DELIMITER);
            sBuilder.append(NEW_LINE_SEPARATOR);

            sBuilder.append("מספר הזמנה, תאריך משלוח,סכום הזמנה,מצב הזמנה");
            sBuilder.append(NEW_LINE_SEPARATOR);


            for (SupplierOrder supplierOrder : supplierOrdersDao.findAllBetweenDatesAndSupplierId(start, end, supplier.getId())) {

                sBuilder.append(common.createSupplierOrderSBID(supplier.getId(), supplierOrder.getId()));
                sBuilder.append(COMMA_DELIMITER);
                sBuilder.append(new SimpleDateFormat("dd/MM/yyyy").format(supplierOrder.getCreatedDate()));
                sBuilder.append(COMMA_DELIMITER);
                sBuilder.append(supplierOrder.getSum() + "");
                sBuilder.append(COMMA_DELIMITER);
                sBuilder.append(supplierOrder.getStatus().heb);
                sBuilder.append(COMMA_DELIMITER);
                totalSum += supplierOrder.getSum();
                sBuilder.append(NEW_LINE_SEPARATOR);

            }

            sBuilder.append("סה״כ: ");
            sBuilder.append(dtime.format(totalSum));
            sBuilder.append(NEW_LINE_SEPARATOR);

            System.out.println("CSV file was created successfully !!!");

        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            e.printStackTrace();
        }

        response.setHeader("Content-Disposition", "attachment; filename=\"SupplierForBuying_Report.csv\"");
        response.setContentLength(sBuilder.length());
        response.setContentType("text/csv");
        return sBuilder.toString();
    }


    @RequestMapping(value = "/admin/reports/ReportSupplierForWeight", method = RequestMethod.GET)
    public Object reportSupplierForWeight(Model model, HttpServletRequest request) {

        List<Map<String, Object>> supplierList = new LinkedList<>();
        for (Supplier supplier : suppliersDao.findAll()) {
            Map<String, Object> column = new HashMap<>();
            column.put("name", supplier.getName());
            column.put("id", supplier.getId());
            supplierList.add(column);
        }
        model.addAttribute("supplierList", supplierList);
        return "ReportSupplierForWeight";

    }

    @RequestMapping(value = "/admin/reports/ReportSupplierForWeight/submit", method = RequestMethod.POST)
    public Object reportSupplierForWeightSubmit(@RequestParam(value = "supplierIds") Integer supplierId,
                                                @RequestParam(value = "startDate") String startDate,
                                                @RequestParam(value = "endDate") String endDate,
                                                Model model, HttpServletRequest request) {
        float orderWeight = 0;
        Date start = null, end = null;
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
        DecimalFormat dtime = new DecimalFormat("#.##");
        try {
            start = f.parse(startDate);
            end = f.parse(endDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (start.before(end)) {

            List<Map<String, Object>> orderDetails = new LinkedList<>();
            List<Map<String, Object>> supplierProductLists = new LinkedList<>();

            for (SupplierOrder supplierOrder : supplierOrdersDao.findAllBetweenDatesAndSupplierId(start, end, supplierId)) {
                Map<String, Object> order = new HashMap<>();

                order.put("orderNumber", common.createSupplierOrderSBID(supplierId, supplierOrder.getId()));
                order.put("orderId", supplierOrder.getId());
                order.put("createdDate", new SimpleDateFormat("dd-MM-yyyy").format(supplierOrder.getCreatedDate()));

                for (SupplierOrderItem supplierOrderItem : supplierOrderItemDao.findAllByOrderId(supplierOrder.getId())) {
                    orderWeight += supplierOrderItem.getQuantity() * itemsDao.getOne(supplierOrderItem.getItemId()).getUnitWeight();
                }
                order.put("orderWeight", dtime.format(orderWeight));
                orderWeight = 0;
                orderDetails.add(order);
            }


            Set<Integer> itemIds = new HashSetIgnoresNulls<>();
            for (SupplierOrder supplierOrder : supplierOrdersDao.findAllBetweenDatesAndSupplierId(start, end, supplierId)) {
                for (SupplierOrderItem supplierOrderItem : supplierOrderItemDao.findAllByOrderId(supplierOrder.getId()))
                    itemIds.add(supplierOrderItem.getItemId());
            }

            for (Iterator<Integer> it = itemIds.iterator(); it.hasNext(); ) {
                float itemQuantity = 0;
                Item item = itemsDao.findOne(it.next());
                Product product = productsDao.findOne(item.getProductId());

                Map<String, Object> supplierProducts = new HashMap<>();


                for (SupplierOrder supplierOrder : supplierOrdersDao.findAllBetweenDatesAndSupplierId(start, end, supplierId)) {
                    for (SupplierOrderItem supplierOrderItem : supplierOrderItemDao.findAllByOrderId(supplierOrder.getId())) {

                        if (supplierOrderItem.getItemId() == item.getId()) {
                            itemQuantity += supplierOrderItem.getQuantity();
                        }

                    }

                }
                supplierProducts.put("catalogNo", item.getBarcode());
                supplierProducts.put("productDec", product.getNameHeb());
                supplierProducts.put("unitWeight", item.getUnitWeight());
                supplierProducts.put("packageUnit", product.getPackageType().heb);
                supplierProducts.put("quantity", dtime.format(itemQuantity));
                supplierProducts.put("totalWeight", dtime.format(itemQuantity * item.getUnitWeight()));

                supplierProductLists.add(supplierProducts);
            }
            model.addAttribute("supplierName", suppliersDao.findOne(supplierId).getName());
            model.addAttribute("supplierBarcode", common.pedSupplierNumber(supplierId));
            model.addAttribute("supplierId", supplierId);
            model.addAttribute("orderDetails", orderDetails);
            model.addAttribute("supplierProductLists", supplierProductLists);
            model.addAttribute("startDate", startDate);
            model.addAttribute("endDate", endDate);

            return "ReportSupplierForWeightCompareSuppliers";
        } else
            return "wrongInputError";

    }

    @ResponseBody
    @RequestMapping(value = "/admin/reports//ReportSupplierForWeight/submit/saveCSVFile", method = RequestMethod.GET)
    public Object reportSupplierForWeightSendCSVFile(@RequestParam(value = "supplierId") String supplierId,
                                                     @RequestParam(value = "startDate") String startDate,
                                                     @RequestParam(value = "endDate") String endDate,
                                                     Model model, HttpServletResponse response) {

        String NEW_LINE_SEPARATOR = "\n";
        String COMMA_DELIMITER = ",";
        StringBuilder sBuilder = new StringBuilder();
        DecimalFormat dtime = new DecimalFormat("#.##");
        float orderWeight = 0;
        //Handling dates :
        Date start = null, end = null;

        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
        try {
            start = f.parse(startDate);
            end = f.parse(endDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        //Create CSV File :
        try {
            Supplier supplier = suppliersDao.findOne(U.var.parseInt(supplierId));
            sBuilder.append("דו״ח ספק למשקל");
            sBuilder.append(NEW_LINE_SEPARATOR);
            sBuilder.append("מספר ספק,שם הספק");
            sBuilder.append(NEW_LINE_SEPARATOR);

            sBuilder.append(common.pedSupplierNumber(supplier.getId()));
            sBuilder.append(COMMA_DELIMITER);
            sBuilder.append(supplier.getName());
            sBuilder.append(COMMA_DELIMITER);
            sBuilder.append(NEW_LINE_SEPARATOR);

            sBuilder.append("מספר הזמנה, תאריך משלוח,משקל הזמנה(ק״ג)");
            sBuilder.append(NEW_LINE_SEPARATOR);


            for (SupplierOrder supplierOrder : supplierOrdersDao.findAllBetweenDatesAndSupplierId(start, end, supplier.getId())) {


                sBuilder.append(common.createSupplierOrderSBID(supplier.getId(), supplierOrder.getId()));
                sBuilder.append(COMMA_DELIMITER);
                sBuilder.append(new SimpleDateFormat("dd-MM-yyyy").format(supplierOrder.getCreatedDate()));

                for (SupplierOrderItem supplierOrderItem : supplierOrderItemDao.findAllByOrderId(supplierOrder.getId())) {
                    orderWeight += supplierOrderItem.getQuantity() * itemsDao.getOne(supplierOrderItem.getItemId()).getUnitWeight();
                }
                sBuilder.append(COMMA_DELIMITER);
                sBuilder.append(dtime.format(orderWeight));
                orderWeight = 0;
                sBuilder.append(NEW_LINE_SEPARATOR);

            }
            sBuilder.append(NEW_LINE_SEPARATOR);
            sBuilder.append(NEW_LINE_SEPARATOR);

            sBuilder.append("מספר קטלוגי, תיאור מוצר,משקל יחידה,יחידת מידה,כמות,משקל כולל(ק״ג)");
            sBuilder.append(NEW_LINE_SEPARATOR);


            Set<Integer> itemIds = new HashSetIgnoresNulls<>();
            for (SupplierOrder supplierOrder : supplierOrdersDao.findAllBetweenDatesAndSupplierId(start, end, supplier.getId())) {
                for (SupplierOrderItem supplierOrderItem : supplierOrderItemDao.findAllByOrderId(supplierOrder.getId()))
                    itemIds.add(supplierOrderItem.getItemId());
            }

            for (Iterator<Integer> it = itemIds.iterator(); it.hasNext(); ) {
                float itemQuantity = 0;
                Item item = itemsDao.findOne(it.next());
                Product product = productsDao.findOne(item.getProductId());

                Map<String, Object> supplierProducts = new HashMap<>();


                for (SupplierOrder supplierOrder : supplierOrdersDao.findAllBetweenDatesAndSupplierId(start, end, supplier.getId())) {
                    for (SupplierOrderItem supplierOrderItem : supplierOrderItemDao.findAllByOrderId(supplierOrder.getId())) {

                        if (supplierOrderItem.getItemId() == item.getId()) {
                            itemQuantity += supplierOrderItem.getQuantity();
                        }

                    }

                }
                sBuilder.append(item.getBarcode());
                sBuilder.append(COMMA_DELIMITER);
                sBuilder.append(product.getNameHeb().replaceAll(",", " "));
                sBuilder.append(COMMA_DELIMITER);
                sBuilder.append(item.getUnitWeight() + "");
                sBuilder.append(COMMA_DELIMITER);
                sBuilder.append(product.getPackageType().heb);
                sBuilder.append(COMMA_DELIMITER);
                sBuilder.append(dtime.format(itemQuantity));
                sBuilder.append(COMMA_DELIMITER);
                sBuilder.append(dtime.format(itemQuantity * item.getUnitWeight()));
                sBuilder.append(NEW_LINE_SEPARATOR);

            }


            System.out.println("CSV file was created successfully !!!");

        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            e.printStackTrace();
        }
        response.setHeader("Content-Disposition", "attachment; filename=\"SupplierForWeight_Report.csv\"");
        response.setContentLength(sBuilder.length());
        response.setContentType("text/csv");
        return sBuilder.toString();
    }


    @RequestMapping(value = "/admin/reports/ReportProductByTime", method = RequestMethod.GET)
    public Object productByTimeCompareCatalogNo(Model model, HttpServletRequest request) {
        List<Map<String, Object>> productList = new LinkedList<>();
        for (Product product : productsDao.findAll()) {
            Map<String, Object> column = new HashMap<>();
            column.put("name", product.getId() + " - " + product.getNameHeb());
            column.put("id", product.getId());
            productList.add(column);
        }
        model.addAttribute("productList", productList);

        return "ReportProductByTime";
    }

    @RequestMapping(value = "/admin/reports/ReportProductByTime/submit", method = RequestMethod.POST)
    public Object productByTimeCompareCatalogNoSubmit(@RequestParam(value = "productIds") Integer productId,
                                                      @RequestParam(value = "startDate") String startDate,
                                                      @RequestParam(value = "endDate") String endDate,
                                                      Model model, HttpServletRequest request) {
        Date start = null, end = null;

        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
        try {
            start = f.parse(startDate);
            end = f.parse(endDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (start.before(end)) {


            List<Map<String, Object>> items = new LinkedList<>();
            List<Map<String, Object>> details = new LinkedList<>();

            Set<String> months = new HashSetIgnoresNulls<>();
            List<Order> orders = ordersDao.findAllBetweenDatesAndProductId(start, end, productId);


            for (Order order : orders) {
                for (OrderProduct orderProduct : orderProductDao.findAllByOrderId(order.getId()))
                    if (orderProduct.getProductId() == productId) {
                        Map<String, Object> orderDetails = new HashMap<>();

                        orderDetails.put("orderNumber", common.createOrderCode(order.getId()));
                        orderDetails.put("orderId", order.getId());
                        orderDetails.put("orderCreatedDate", new SimpleDateFormat("dd-MM-yyyy").format(order.getCreatedDate()));
                        orderDetails.put("productQuantity", orderProduct.getQuantity());
                        orderDetails.put("customerName", customersDao.getOne(order.getCustomerId()).getFirstName() + " "
                                + customersDao.getOne(order.getCustomerId()).getLastName());
                        orderDetails.put("customerNumber", customersDao.getOne(order.getCustomerId()).getBarcode());
//                            months.add(new SimpleDateFormat("MMM").format(order.getCreatedDate()));
                        details.add(orderDetails);
                    }
            }

            List<List<Map<String, Object>>> listOfLists = new LinkedList<>();
            for (Iterator<String> mIterator = months.iterator(); mIterator.hasNext(); ) {

                String s = mIterator.next();
                Set<Integer> itemIds = new HashSetIgnoresNulls<>();
                for (Order order : orders) {
                    if (s.equals(new SimpleDateFormat("MMM").format(order.getCreatedDate())))
                        for (OrderProduct orderProduct : orderProductDao.findAllByOrderId(order.getId())) {
                            if (orderProduct.getProductId() == productId)
                                itemIds.add(orderProduct.getItemId());
                        }
                }

                for (Iterator<Integer> it = itemIds.iterator(); it.hasNext(); ) {
                    Item item = itemsDao.findOne(it.next());
                    Product product = productsDao.findOne(item.getProductId());
                    if (product.getId() == productId) {
                        Map<String, Object> itemDetails = new HashMap<>();
                        itemDetails.put("catalogNo", item.getBarcode());
                        itemDetails.put("productDec", product.getNameHeb());
                        itemDetails.put("buyingCost", item.getBuyingCost());
                        itemDetails.put("sellingCost", product.getPrice());
                        itemDetails.put("grossProfit", item.getGrossProfit());
                        items.add(itemDetails);
                    }

                }
                System.out.println("month in the list : " + s);
                listOfLists.add(items);
            }
//                System.out.println("listOfLists Size :  : " +listOfLists.size());
            model.addAttribute("listOfLists", listOfLists);
            model.addAttribute("details", details);
            model.addAttribute("startDate", startDate);
            model.addAttribute("endDate", endDate);
            model.addAttribute("productName", productsDao.findOne(productId).getNameHeb());
            model.addAttribute("start", new SimpleDateFormat("dd-MM-yyyy").format(start));
            model.addAttribute("end", new SimpleDateFormat("dd-MM-yyyy").format(end));
            model.addAttribute("productId", productId);
            return "ReportProductByTimeCompareCatalogNo";
        } else
            return "wrongInputError";

    }


    @ResponseBody
    @RequestMapping(value = "/admin/reports/ReportProductByTime/submit/saveCSVFile", method = RequestMethod.GET)
    public Object reportProductByTimeSendCSVFile(@RequestParam(value = "productId") Integer productId,
                                                 @RequestParam(value = "startDate") String startDate,
                                                 @RequestParam(value = "endDate") String endDate,
                                                 Model model, HttpServletResponse response) {


        String NEW_LINE_SEPARATOR = "\n";
        String COMMA_DELIMITER = ",";
        StringBuilder sBuilder = new StringBuilder();
        DecimalFormat dtime = new DecimalFormat("#.##");
        float orderWeight = 0;
        //Handling dates :
        Date start = null, end = null;

        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
        try {
            start = f.parse(startDate);
            end = f.parse(endDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        //Create CSV File :
        try {

            sBuilder.append("השוואת מספר קטלוגי");
            sBuilder.append(NEW_LINE_SEPARATOR);
            sBuilder.append("מתאריך ");
            sBuilder.append(new SimpleDateFormat("dd-MM-yyyy").format(start));
            sBuilder.append("עד תאריך ");
            sBuilder.append(new SimpleDateFormat("dd-MM-yyyy").format(end));
            sBuilder.append(NEW_LINE_SEPARATOR);
            sBuilder.append("מספר הזמנה, שם הלקוח ,מספר הלקוח,תאריך קבלת הזמנה,כמות בהזמנה");
            sBuilder.append(NEW_LINE_SEPARATOR);

            List<Map<String, Object>> items = new LinkedList<>();

            Set<Integer> itemIds = new HashSetIgnoresNulls<>();

            for (Order order : ordersDao.findAllBetweenDatesAndProductId(start, end, productId)) {
                for (OrderProduct orderProduct : orderProductDao.findAllByOrderId(order.getId()))
                    if (orderProduct.getProductId() == productId)
                        itemIds.add(orderProduct.getItemId());
            }

            List<Order> orders = ordersDao.findAllBetweenDatesAndProductId(start, end, productId);

            for (Order order : orders) {
                for (OrderProduct orderProduct : orderProductDao.findAllByOrderId(order.getId()))
                    if (orderProduct.getProductId() == productId) {
                        Map<String, Object> orderDetails = new HashMap<>();

//                            sBuilder.append( order.getId());
//                            sBuilder.append(COMMA_DELIMITER);
                        sBuilder.append(common.createOrderCode(order.getId()));
                        sBuilder.append(COMMA_DELIMITER);
                        sBuilder.append(customersDao.getOne(order.getCustomerId()).getFirstName() + " "
                                + customersDao.getOne(order.getCustomerId()).getLastName());
                        sBuilder.append(COMMA_DELIMITER);
                        sBuilder.append(customersDao.getOne(order.getCustomerId()).getBarcode());
                        sBuilder.append(COMMA_DELIMITER);
                        sBuilder.append(new SimpleDateFormat("dd-MM-yyyy").format(order.getCreatedDate()));
                        sBuilder.append(COMMA_DELIMITER);
                        sBuilder.append(orderProduct.getQuantity());
                        sBuilder.append(COMMA_DELIMITER);
                        sBuilder.append(NEW_LINE_SEPARATOR);


//                            months.add(new SimpleDateFormat("MMM").format(order.getCreatedDate()));

                    }
            }

            System.out.println("CSV file was created successfully !!!");

        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            e.printStackTrace();
        }

        response.setHeader("Content-Disposition", "attachment; filename=\"ProductByTime_Report.csv\"");
        response.setContentLength(sBuilder.length());
        response.setContentType("text/csv");
        return sBuilder.toString();
    }

    @RequestMapping(value = "/admin/reports/ReportStock", method = RequestMethod.GET)
    public Object stockReport(Model model, HttpServletRequest request) {
        if (!U.var.parseBoolean(request.getSession().getAttribute("login"))) {
            return "redirect:/admin/login";
        }
        List<Order> openOrders = ordersDao.findAllOpen();

        Map<Integer, Map<Integer, Integer>> productsOrderedQuantity = new HashMap<>();

        List<Map<String, Object>> columns = new LinkedList<>();
        for (Order openOrder : openOrders) {
            List<OrderProduct> orderProducts = orderProductDao.findAllByOrderId(openOrder.getId());
            //Creating a set of product to avoid duplicate
            Set<Integer> productIds = new HashSetIgnoresNulls<>();
            for (OrderProduct orderProduct : orderProducts) {
                productIds.add(orderProduct.getProductId());
            }
            for (Iterator<Integer> it = productIds.iterator(); it.hasNext(); ) {
                int productId = it.next();
                Map<Integer, Integer> p = new HashMap<>();
                p.put(productId, orderProductDao.getTotalQuantityOfProductFromAllOrders(productId));
                productsOrderedQuantity.put(productId, p);
            }
            System.out.println("productsOrderedQuantity - SIZE : " + productsOrderedQuantity.size());

        }

        List<Order> orders = ordersDao.findAll();
        Set<Integer> itemIds = new HashSetIgnoresNulls<>();
        for (Order order : orders) {
            for (OrderProduct orderProduct : orderProductDao.findAllByOrderId(order.getId())) {
                if (orderProduct.getItemId() != null)
                    itemIds.add(itemsDao.findOne(orderProduct.getItemId()).getId());
            }
        }


        for (Iterator<Integer> it = itemIds.iterator(); it.hasNext(); ) {
            Item item = itemsDao.findOne(it.next());
            Map<String, Object> column = new HashMap<>();
            Product product = productsDao.findOne(item.getProductId());

            column.put("catalogNo", item.getBarcode());
            column.put("supplierName", suppliersDao.findOne(item.getSupplierId()).getName());
            column.put("productName", product.getNameHeb());
            column.put("productUnit", product.getPackageType().heb);
            column.put("stock", item.getInventory());
            columns.add(column);
        }


        model.addAttribute("columns", columns);

        return "ReportStock";
    }

    @ResponseBody
    @RequestMapping(value = "/admin/reports/admin/reports/ReportStock/saveCSVFile", method = RequestMethod.GET)
    public Object reportStockSendCSVFile(Model model, HttpServletResponse response) {


        String NEW_LINE_SEPARATOR = "\n";
        String COMMA_DELIMITER = ",";

        StringBuilder sBuilder = new StringBuilder();

        //Create CSV File :
        try {

            sBuilder.append("דו״ח מלאי");
            sBuilder.append(NEW_LINE_SEPARATOR);
            sBuilder.append("מספר קטלוגי, שם ספק ,תאור מוצר,יחידת מידה,כמות זמינה");
            sBuilder.append(NEW_LINE_SEPARATOR);


            List<Order> openOrders = ordersDao.findAllOpen();

            Map<Integer, Map<Integer, Integer>> productsOrderedQuantity = new HashMap<>();


            for (Order openOrder : openOrders) {
                //Creating a set of product to avoid duplicate
                Set<Integer> productIds = new HashSetIgnoresNulls<>();
                for (OrderProduct orderProduct : orderProductDao.findAllByOrderId(openOrder.getId())) {
                    productIds.add(orderProduct.getProductId());
                }
                for (Iterator<Integer> it = productIds.iterator(); it.hasNext(); ) {
                    int productId = it.next();
                    Map<Integer, Integer> p = new HashMap<>();
                    p.put(productId, orderProductDao.getTotalQuantityOfProductFromAllOrders(productId));
                    productsOrderedQuantity.put(productId, p);
                }

            }


            Set<Integer> itemIds = new HashSetIgnoresNulls<>();
            for (Order order : ordersDao.findAll()) {
                for (OrderProduct orderProduct : orderProductDao.findAllByOrderId(order.getId())) {
                    if (orderProduct.getItemId() != null)
                        itemIds.add(itemsDao.findOne(orderProduct.getItemId()).getId());
                }
            }


            for (Iterator<Integer> it = itemIds.iterator(); it.hasNext(); ) {
                Item item = itemsDao.findOne(it.next());
                Product product = productsDao.findOne(item.getProductId());


                sBuilder.append(item.getBarcode());
                sBuilder.append(COMMA_DELIMITER);
                sBuilder.append(suppliersDao.findOne(item.getSupplierId()).getName());
                sBuilder.append(COMMA_DELIMITER);
                sBuilder.append(product.getNameHeb().replace(",", " "));
                sBuilder.append(COMMA_DELIMITER);
                sBuilder.append(product.getPackageType().heb);
                sBuilder.append(COMMA_DELIMITER);
                sBuilder.append(item.getInventory());
                sBuilder.append(NEW_LINE_SEPARATOR);


            }

            System.out.println("CSV file was created successfully !!!");

        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            e.printStackTrace();
        }

        response.setHeader("Content-Disposition", "attachment; filename=\"Stock_Report.csv\"");
        response.setContentLength(sBuilder.length());
        response.setContentType("text/csv");
        return sBuilder.toString();
    }

    @RequestMapping(value = "/admin/reports/ReportOrderedItems", method = RequestMethod.GET)
    public Object reportOrderedItems(Model model, HttpServletRequest request) {
        return "ReportOrderedItems";
    }

    @RequestMapping(value = "/admin/reports/ReportOrderedItems/submit", method = RequestMethod.POST)
    public Object reportOrderedItemsSubmit(@RequestParam(value = "startDate") String startDate,
                                           @RequestParam(value = "endDate") String endDate,
                                           Model model, HttpServletRequest request) {
        Date start = null, end = null;
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
        try {
            start = f.parse(startDate);
            end = f.parse(endDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (start.before(end)) {

            List<Map<String, Object>> items = new LinkedList<>();

            for (Object[] row : supplierOrdersDao.findAllOrderedItemsReport(start, end)) {
//                    i.id, p.name_heb, SUM(op.quantity_needed) total, o.barcode, i.barcode, s.name, p.unit_type
                HashMap<String, Object> item = new HashMap<>();
                item.put("catalogNo", row[4]);
                item.put("orderNumber", row[3]);
                item.put("supplierName", row[5]);
                item.put("productDec", row[1]);
                item.put("unitType", row[6]);
                item.put("orderedQuantity", row[2]);
                items.add(item);
            }


            model.addAttribute("items", items);
            model.addAttribute("startDate", startDate);
            model.addAttribute("endDate", endDate);
            return "ReportOrderedItemsPresentData";


        } else
            return "datesError";

    }

    @ResponseBody
    @RequestMapping(value = "/admin/reports/ReportOrderedItems/submit/saveCSVFile", method = RequestMethod.GET)
    public Object reportOrderedItemsSendCSVFile(@RequestParam(value = "startDate") String startDate,
                                                @RequestParam(value = "endDate") String endDate,
                                                Model model, HttpServletResponse response) {

        String NEW_LINE_SEPARATOR = "\n";
        String COMMA_DELIMITER = ",";
        StringBuilder sBuilder = new StringBuilder();
        DecimalFormat dtime = new DecimalFormat("#.##");
        float orderWeight = 0;
        //Handling dates :
        Date start = null, end = null;

        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
        try {
            start = f.parse(startDate);
            end = f.parse(endDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        //Create CSV File :
        try {
            sBuilder.append("דו״ח פריטים שהוזמנו");
            sBuilder.append(NEW_LINE_SEPARATOR);
            sBuilder.append("מספר קטלוגי, מספר הזמנה ,שם ספק,תאור מוצר,יחידת מידה,כמות שהוזמנה");
            sBuilder.append(NEW_LINE_SEPARATOR);

            for (SupplierOrder supplierOrder : supplierOrdersDao.findAllInOrderBetweenDates(start, end)) {
                for (SupplierOrderItem supplierOrderItem : supplierOrderItemDao.findAllByOrderId(supplierOrder.getId())) {

                    Item item = itemsDao.findOne(supplierOrderItem.getItemId());
                    Product product = productsDao.findOne(item.getProductId());

                    sBuilder.append(item.getBarcode());
                    sBuilder.append(COMMA_DELIMITER);
                    sBuilder.append(common.createSupplierOrderSBID(supplierOrder.getSupplierId(), supplierOrder.getId()));
                    sBuilder.append(COMMA_DELIMITER);
                    sBuilder.append(suppliersDao.findOne(item.getSupplierId()).getName());
                    sBuilder.append(COMMA_DELIMITER);
                    sBuilder.append(product.getNameHeb().replaceAll(",", " "));
                    sBuilder.append(COMMA_DELIMITER);
                    sBuilder.append(product.getPackageType().heb);
                    sBuilder.append(COMMA_DELIMITER);
                    sBuilder.append(supplierOrderItemDao.getOrderedItemQuantity(item.getId()) + "");
                    sBuilder.append(NEW_LINE_SEPARATOR);

                }
            }


            System.out.println("CSV file was created successfully !!!");

        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            e.printStackTrace();
        }

        response.setHeader("Content-Disposition", "attachment; filename=\"OrderedItems_Report.csv\"");
        response.setContentLength(sBuilder.length());
        response.setContentType("text/csv");
        return sBuilder.toString();

    }

    @RequestMapping(value = "/admin/reports/ReportBuyingPrediction", method = RequestMethod.GET)
    public Object reportBuyingPrediction(Model model, HttpServletRequest request) {
        List<Map<String, Object>> categories = new LinkedList<>();

        for (Category category : categoriesDao.findAll()) {
            Map<String, Object> column = new HashMap<>();
            column.put("name", category.getName());
            column.put("id", category.getId());
            categories.add(column);
        }
        model.addAttribute("categories", categories);
        return "ReportBuyingPrediction";
    }

    @RequestMapping(value = "/admin/reports/ReportBuyingPrediction/presentData", method = RequestMethod.POST)
    public Object reportBuyingPredictionPresentData(@RequestParam(value = "categoryId") Integer categoryId,
                                                    Model model, HttpServletRequest request) {
        if (!U.var.parseBoolean(request.getSession().getAttribute("login"))) {
            return "redirect:/admin/login";
        }

        List<Map<String, Object>> products = new LinkedList<>();

        for (Object[] row : supplierOrdersDao.findAllBuyingPredictionReport(categoryId)) {
            HashMap<String, Object> productDetails = new HashMap<>();
            productDetails.put("productNumber", row[0]);
            productDetails.put("productName", row[1]);
            productDetails.put("unit", Product.PackageTypes.values()[U.var.parseInt(row[2])].heb);
            productDetails.put("weightCode", row[3]);
            productDetails.put("productQuantity", row[4]);
            productDetails.put("totalBuyingWeight", row[5]);
            products.add(productDetails);
        }


        model.addAttribute("products", products);
        model.addAttribute("categoryId", categoryId.toString());

        return "ReportBuyingPredictionPresentData";
    }

    @ResponseBody
    @RequestMapping(value = "/admin/reports/ReportBuyingPrediction/presentData/saveCSVFile", method = RequestMethod.GET)
    public Object reportBuyingPredictionSendCSVFile(@RequestParam(value = "categoryId") Integer categoryId,
                                                    Model model, HttpServletResponse response) {

        String NEW_LINE_SEPARATOR = "\n";
        String COMMA_DELIMITER = ",";
        StringBuilder sBuilder = new StringBuilder();
        DecimalFormat dtime = new DecimalFormat("#.##");


        //Create CSV File :
        try {
            sBuilder.append("דו״ח תחזית קניה");
            sBuilder.append(NEW_LINE_SEPARATOR);
            sBuilder.append("מספר מוצר, תיאור מוצר ,יחידת משקל, קוד משקל,כמות לקניה, משקל לקניה");
            sBuilder.append(NEW_LINE_SEPARATOR);

            for (Object[] row : supplierOrdersDao.findAllBuyingPredictionReport(categoryId)) {
                sBuilder.append(row[0]).append(',').append(row[1]).append(',').append(Product.PackageTypes.values()[U.var.parseInt(row[2])].heb).append(',').append(row[3]).append(',').append(row[4]).append(',').append(row[5]).append(NEW_LINE_SEPARATOR);
            }

            System.out.println("CSV file was created successfully !!!");

        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            e.printStackTrace();
        }

        response.setHeader("Content-Disposition", "attachment; filename=\"BuyingPrediction_Report.csv\"");
        response.setContentLength(sBuilder.length());
        response.setContentType("text/csv");
        return sBuilder.toString();
    }

    private static JSONObject createGraphElement(String label, List<Float> src) throws JSONException {
        JSONObject ret = new JSONObject();
        ret.put("label", label);
        JSONArray data = new JSONArray();
        for (int i = 0; i < src.size(); i++) {
            data.put(new JSONArray(new float[]{i, src.get(i)}));
        }
        ret.put("data", data);
        return ret;
    }

    private static JSONObject createGraphElementInt(String label, List<Integer> src) throws JSONException {
        JSONObject ret = new JSONObject();
        ret.put("label", label);
        JSONArray data = new JSONArray();
        for (int i = 0; i < src.size(); i++) {
            data.put(new JSONArray(new int[]{i, src.get(i)}));
        }
        ret.put("data", data);
        return ret;
    }

    private String scramble(String text) {

        List<Character> list = new ArrayList<Character>();
        for(char c : text.toCharArray()) {
            list.add(c);
        }
        Collections.shuffle(list);
        StringBuilder shuffledString = new StringBuilder();
        for(char c : list) {
            shuffledString.append(c);
        }
        String scrambledText = shuffledString.toString();

        return scrambledText;
    }


}
