package com.shookbook.admin;


import com.shookbook.dao.shookbook.*;
import com.shookbook.dao.wordpress.WpOrderItemsDao;
import com.shookbook.dao.wordpress.WpPostmetaDao;
import com.shookbook.dao.wordpress.WpPostsDao;
import com.shookbook.dao.wordpress.WpUsermetaDao;
import com.shookbook.entities.shookbook.*;
import com.shookbook.utils.U;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import javax.servlet.http.HttpServletRequest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Controller
@SessionAttributes("thought")
public class InventoryAdmin {

    private static final Logger logger = Logger.getLogger(InventoryAdmin.class);

    @Autowired
    private Common common;
    @Autowired
    private CustomersDao customersDao;
    @Autowired
    private CouponsDao couponsDao;
    @Autowired
    private PointsDao pointsDao;
    @Autowired
    private OrdersDao ordersDao;
    @Autowired
    private ProductsDao productsDao;
    @Autowired
    private ItemsDao itemsDao;
    @Autowired
    private SuppliersDao suppliersDao;
    @Autowired
    private SupplierOrdersDao supplierOrdersDao;
    @Autowired
    private  SupplierOrderItemDao supplierOrderItemDao;
    @Autowired
    private InventoryDao inventoryDao;
    @Autowired
    private InventoryItemDao inventoryItemDao;
    @Autowired
    private CategoriesDao categoriesDao;
    @Autowired
    private WpPostsDao wpPostsDao;
    @Autowired
    private WpPostmetaDao wpPostmetaDao;
    @Autowired
    private WpUsermetaDao wpUsermetaDao;
    @Autowired
    private WpOrderItemsDao wpOrderItemsDao;
    @Autowired
    private WorkersDao workersDao;
    @Autowired
    private GlobalsDao globalsDao;

    public final static String[] inventoryFilters = new String[]{"הכל", "ירקות", "פירות", "עלים ותבלינים", "פטריות ונבטים", "פירות יבשים", "קטניות וטבלינים", "לחמים ומאפים", "מיוחדים", "לפי מוצר", "לפי ספק"};

    @RequestMapping(value = "/admin/InventoryHub", method = RequestMethod.GET)
    public Object reportHub(@RequestParam(value = "filter", defaultValue = "0") Integer filter,
                            @RequestParam(value = "product_id", required = false) String productId,
                            @RequestParam(value = "supplier_id", required = false) String supplierId,
                            Model model,
                            HttpServletRequest request) {
        if (!U.var.parseBoolean(request.getSession().getAttribute("login"))) {
            return "redirect:/admin/login";
        }

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        List<Product> products;

        switch (filter) {
            case 0:
                products = productsDao.findAll();
                break;
            case 1:
                products = productsDao.findAllByCategory(1);
                break;
            case 2:
                products = productsDao.findAllByCategory(2);
                break;
            case 3:
                products = productsDao.findAllByCategory(3);
                break;
            case 4:
                products = productsDao.findAllByCategory(4);
                break;
            case 5:
                products = productsDao.findAllByCategory(5);
                break;
            case 6:
                products = productsDao.findAllByCategory(7);
                break;
            case 7:
                products = productsDao.findAllByCategory(8);
                break;
            case 8:
                products = productsDao.findAllByCategory(6);
                break;
            case 9:
                products = productsDao.findAllLikeProduct('%' + productId + '%');
                break;
            case 10:
                products = productsDao.findAllLikeSupplier('%' + supplierId + '%');
                break;
            default:
                products = productsDao.findAllByCategory(0);
                break;
        }

        boolean match = false;

        List<Map<String, Object>> columns = new LinkedList<>();
        for (Product product : products) {
            Map<String, Object> column = new HashMap<>();
            Inventory inventory = inventoryDao.findOneByProductID(product.getId());

            if (inventory != null) {
                Integer inventoryId = inventory.getId();
                column.put("id", inventoryId);
                column.put("product_name", inventory.getProductName());
                column.put("ordered", inventoryItemDao.sumOrderedByInventoryId(inventoryId));
                column.put("received", inventoryItemDao.sumReceivedByInventoryId(inventoryId));
                column.put("delivered", inventoryItemDao.sumDeliveredByInventoryId(inventoryId));
                column.put("inventory", inventoryItemDao.sumReceivedByInventoryId(inventoryId) - inventoryItemDao.sumDeliveredByInventoryId(inventoryId));
                column.put("product_threshold", inventory.getThreshold());
                column.put("last_update", dateFormat.format(inventory.getLastUpdate()));
                List <Integer> inventoryItemIds = inventoryItemDao.findAllItemIdByInventoryIdGroupByItemId(inventoryId);
                List<Map<String, Object>> inventoryRows = new LinkedList<>();
                for (Integer itemId : inventoryItemIds) {
                    Map<String, Object> inventoryRow = new HashMap<>();
                    inventoryRow.put("item_id", itemId);
                    inventoryRow.put("barcode", itemsDao.getOne(itemId).getBarcode());
                    inventoryRow.put("supplier", suppliersDao.findOne(itemsDao.getOne(itemId).getSupplierId()).getName());
                    inventoryRow.put("ordered", inventoryItemDao.sumOrderedByItemId(itemId));
                    inventoryRow.put("received", inventoryItemDao.sumReceivedByItemId(itemId));
                    inventoryRow.put("delivered", inventoryItemDao.sumDeliveredByItemId(itemId));
                    inventoryRows.add(inventoryRow);
                }
                column.put("inventory_rows", inventoryRows);
                columns.add(column);
            }
        }

        model.addAttribute("columns", columns);
        model.addAttribute("filter", inventoryFilters[filter]);
        model.addAttribute("filter_index", filter);

        Map<String, Object> globals = new HashMap<>();
        globals.put("modules_enable_coupons", globalsDao.findOne("modules_enable_coupons").getValue());
        globals.put("modules_enable_invoice", globalsDao.findOne("modules_enable_invoice").getValue());
        globals.put("modules_enable_logistics", globalsDao.findOne("modules_enable_logistics").getValue());
        globals.put("modules_enable_packing", globalsDao.findOne("modules_enable_packing").getValue());
        Object logedWorker = request.getSession().getAttribute("userId");
        Worker worker = workersDao.getOne(Integer.parseInt(logedWorker.toString()));
        globals.put("worker_name", worker.getFirstName() + " " + worker.getLastName());
        model.addAttribute("globals", globals);

        return "InventoryHub";
    }

    @RequestMapping(value = "/admin/inventory/itemEdit", method = RequestMethod.GET)
    public Object editInventoryItem(@RequestParam(value = "id") Integer itemId, @RequestParam(value = "filter") Integer filter, Model model, HttpServletRequest request) {

        InventoryItem inventoryItem = inventoryItemDao.findLastByItemID(itemId);
        Inventory inventory = inventoryDao.findOne(inventoryItem.getInventoryId());
        Product product = productsDao.findOne(inventory.getProductId());
        model.addAttribute("id", itemId);
        model.addAttribute("product_name", inventory.getProductName());
        model.addAttribute("barcode", itemsDao.findOne(itemId).getBarcode());
        model.addAttribute("category", categoriesDao.findOne(product.getCategoryId()).getName());
        model.addAttribute("supplier", suppliersDao.findOne(itemsDao.findOne(itemId).getSupplierId()).getName());
        model.addAttribute("item_received", 0);
        model.addAttribute("item_inventory", inventoryItemDao.sumReceivedByInventoryId(inventory.getId()) - inventoryItemDao.sumDeliveredByInventoryId(inventory.getId()));
        model.addAttribute("filter", filter);

        Map<String, Object> globals = new HashMap<>();
        globals.put("modules_enable_coupons", globalsDao.findOne("modules_enable_coupons").getValue());
        globals.put("modules_enable_invoice", globalsDao.findOne("modules_enable_invoice").getValue());
        globals.put("modules_enable_logistics", globalsDao.findOne("modules_enable_logistics").getValue());
        globals.put("modules_enable_packing", globalsDao.findOne("modules_enable_packing").getValue());
        Object logedWorker = request.getSession().getAttribute("userId");
        Worker worker = workersDao.getOne(Integer.parseInt(logedWorker.toString()));
        globals.put("worker_name", worker.getFirstName() + " " + worker.getLastName());
        model.addAttribute("globals", globals);

        return "InventoryItemEdit";
    }

    @RequestMapping(value = "/admin/inventory/item/edit/submit", method = RequestMethod.POST)
    public Object inventoryItemEditSubmit(@RequestParam(value = "filter") Integer filter,
                                          @RequestParam(value = "id") Integer itemId,
                                          @RequestParam(value = "item_inventory") Float itemInventory,
                                          @RequestParam(value = "item_received") Float itemReceived) {

        InventoryItem inventoryItem = inventoryItemDao.findLastByItemID(itemId);
        Inventory inventory = inventoryDao.findOne(inventoryItem.getInventoryId());

        Float currentInventory = inventoryItemDao.sumReceivedByInventoryId(inventory.getId()) - inventoryItemDao.sumDeliveredByInventoryId(inventory.getId());
        Float diff = currentInventory - itemInventory;
        inventoryItem.setReceived(inventoryItem.getReceived() - diff);

        inventoryItem.addToReceived(itemReceived);

        inventoryItemDao.save(inventoryItem);

        return "redirect:/admin/InventoryHub?filter=" + filter;
    }

    @RequestMapping(value = "/admin/inventory/item_history", method = RequestMethod.GET)
    public Object inventoryItemHistory(@RequestParam(value = "item_barcode") String barcode,
                                       Model model, HttpServletRequest request) {

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        Item item = itemsDao.findOneByBarcode(barcode);
        Product product = productsDao.findOne(item.getProductId());
        Supplier supplier = null;
        List<Map<String, Object>> columns = new LinkedList<>();
        List<InventoryItem> inventoryItems = inventoryItemDao.findAllByItemId(item.getId());

        for (InventoryItem inventoryItem : inventoryItems) {
            Map<String, Object> column = new HashMap<>();

            SupplierOrderItem supplierOrderItem = supplierOrderItemDao.findOne(inventoryItem.getSupplierOrderItemId());
            SupplierOrder supplierOrder = supplierOrdersDao.findOne(supplierOrderItem.getSupplierOrderId());
            if (supplier == null) {
                supplier = suppliersDao.findOne(supplierOrder.getSupplierId());
            }

            System.out.format("   ItemOrderId: %d   OrderId: %d   Barcode: %s%n", inventoryItem.getSupplierOrderItemId(), supplierOrderItem.getSupplierOrderId(), supplierOrder.getBarcode());
            column.put("order_number", common.createSupplierOrderSBID(supplier.getId(), supplierOrder.getId()));
            column.put("ordered", inventoryItem.getOrdered());
            column.put("received", inventoryItem.getReceived());
            column.put("delivered", inventoryItem.getDelivered());
            column.put("date", dateFormat.format(inventoryItem.getCreatedDate()));
            columns.add(column);
        }

        model.addAttribute("product_name", product.getNameHeb());
        model.addAttribute("supplier", supplier.getName());
        model.addAttribute("columns", columns);

        Map<String, Object> globals = new HashMap<>();
        globals.put("modules_enable_coupons", globalsDao.findOne("modules_enable_coupons").getValue());
        globals.put("modules_enable_invoice", globalsDao.findOne("modules_enable_invoice").getValue());
        globals.put("modules_enable_logistics", globalsDao.findOne("modules_enable_logistics").getValue());
        globals.put("modules_enable_packing", globalsDao.findOne("modules_enable_packing").getValue());
        Object logedWorker = request.getSession().getAttribute("userId");
        Worker worker = workersDao.getOne(Integer.parseInt(logedWorker.toString()));
        globals.put("worker_name", worker.getFirstName() + " " + worker.getLastName());
        model.addAttribute("globals", globals);

        return "InventoryItemHistory";
    }
}
