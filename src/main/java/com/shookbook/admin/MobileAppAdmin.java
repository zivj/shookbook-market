package com.shookbook.admin;

import com.shookbook.dao.shookbook.*;
import com.shookbook.dao.wordpress.WpUsermetaDao;
import com.shookbook.dao.wordpress.WpUsersDao;
import com.shookbook.entities.shookbook.*;
import com.shookbook.entities.wordpress.WpUsermeta;
import com.shookbook.utils.U;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

@Controller
@SessionAttributes("thought")
public class MobileAppAdmin {
    private static final Logger logger = Logger.getLogger(MobileAppAdmin.class);
    @Autowired
    private Common common;
    @Autowired
    private CustomersDao customersDao;
    @Autowired
    private OrdersDao ordersDao;
    @Autowired
    private WorkersDao workersDao;
    @Autowired
    private GlobalsDao globalsDao;
    @Autowired
    private WpUsermetaDao wpUsermetaDao;
    @Autowired
    private WpUsersDao wpUsersDao;
    @Autowired
    private MobileGroupsDao mobileGroupsDao;
    @Autowired
    private MobileNotificationsDao mobileNotificationsDao;
    @Autowired
    private MobileUsersDao mobileUsersDao;

    @RequestMapping(value = "/admin/mobileApp", method = RequestMethod.GET)
    public Object mobileApp2(Model model, HttpServletRequest request) {

        List<WpUsermeta> usersMeta = wpUsermetaDao.findAllAppUsers();
        List<Integer> wpUserIds = new ArrayList<Integer>();
        for (WpUsermeta userMeta : usersMeta) {
            wpUserIds.add(U.var.parseInt(userMeta.getUserId()));
        }

        List<Map<String, Object>> customerColumns = new LinkedList<>();
        for (WpUsermeta userMeta : usersMeta) {
            Map<String, Object> customerColumn = new HashMap<>();
            Integer wpUserId = U.var.parseInt(userMeta.getUserId());
            Customer customer = customersDao.findOneByWpID(wpUserId);
            customerColumn.put("customerId", customer.getId());
            customerColumn.put("customerWpId", customer.getWpId());
            customerColumn.put("customerName", customer.getFirstName() + " " + customer.getLastName());
            customerColumn.put("customerSession", userMeta.getMetaValue());
            customerColumns.add(customerColumn);
        }

        List<MobileGroup> mobileGroups = mobileGroupsDao.findAll();
        List<Map<String, Object>> groupColumns = new LinkedList<>();
        for (MobileGroup group : mobileGroups) {
            Map<String, Object> groupColumn = new HashMap<>();
            groupColumn.put("id", group.getId());
            groupColumn.put("name", group.getName());
            groupColumn.put("numberOfUsers", mobileUsersDao.findAllByGroup(group.getId()).size());
            groupColumns.add(groupColumn);
        }

        List<Map<String, Object>> notificationsColumns = new LinkedList<>();
        for (MobileNotification notification : mobileNotificationsDao.findAll()) {
            Map<String, Object> notificationColumn = new HashMap<>();
            notificationColumn.put("id", notification.getId());
            notificationColumn.put("type", notification.getType());
            notificationColumn.put("group", notification.getGroupId());
            notificationColumn.put("text", notification.getText());
            notificationsColumns.add(notificationColumn);
        }

        model.addAttribute("notificationsColumns", notificationsColumns);
        model.addAttribute("customerColumns", customerColumns);
        model.addAttribute("groupColumns", groupColumns);
        Map<String, Object> globals = new HashMap<>();
        globals.put("modules_enable_coupons", globalsDao.findOne("modules_enable_coupons").getValue());
        globals.put("modules_enable_invoice", globalsDao.findOne("modules_enable_invoice").getValue());
        globals.put("modules_enable_logistics", globalsDao.findOne("modules_enable_logistics").getValue());
        globals.put("modules_enable_packing", globalsDao.findOne("modules_enable_packing").getValue());
        Object logedWorker = request.getSession().getAttribute("userId");
        Worker worker = workersDao.getOne(Integer.parseInt(logedWorker.toString()));
        globals.put("worker_name", worker.getFirstName() + " " + worker.getLastName());
        model.addAttribute("globals", globals);
        model.addAttribute("admin", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.ADMIN.ordinal() ? 1 : 0);
        model.addAttribute("customer_support", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.CUSTOMER_SUPPORT.ordinal() ? 1 : 0);

        return "MobileApp";
    }

    @RequestMapping(value = "/admin/mobileApp/group", method = RequestMethod.GET)
    public Object mobileAppGroupDetails(@RequestParam(value = "id") Integer id, Model model, HttpServletRequest request) {
        if (!U.var.parseBoolean(request.getSession().getAttribute("login"))) {
            return "redirect:/admin/login";
        }

        MobileGroup group = mobileGroupsDao.findOne(id);

        Map<String, Object> column = new HashMap<>();
        column.put("id", group.getId());
        column.put("name", group.getName());

        List<MobileUser> users = mobileUsersDao.findAllByGroup(group.getId());
        column.put("numberOfUsers", users.size());

        List<Map<String, Object>> customers = new LinkedList<>();
        Customer customer;

        for (MobileUser user : users) {

            Map<String, Object> customerData = new HashMap<>();
            customer = customersDao.findOne(user.getCustomerId());

            customerData.put("customerId", customer.getId());
            customerData.put("email", customer.getEmail());
            customerData.put("fullName", customer.getFirstName() + " " + customer.getLastName());
            customerData.put("phone", customer.getPhoneExt() + "-" + customer.getPhoneNo());
            customers.add(customerData);
        }

        model.addAttribute("customers", customers);
        model.addAttribute("listedCustomers", customersDao.findAll());
        model.addAttribute("column", column);
        Map<String, Object> globals = new HashMap<>();
        globals.put("modules_enable_coupons", globalsDao.findOne("modules_enable_coupons").getValue());
        globals.put("modules_enable_invoice", globalsDao.findOne("modules_enable_invoice").getValue());
        globals.put("modules_enable_logistics", globalsDao.findOne("modules_enable_logistics").getValue());
        globals.put("modules_enable_packing", globalsDao.findOne("modules_enable_packing").getValue());
        Object logedWorker = request.getSession().getAttribute("userId");
        Worker worker = workersDao.getOne(Integer.parseInt(logedWorker.toString()));
        globals.put("worker_name", worker.getFirstName() + " " + worker.getLastName());
        model.addAttribute("globals", globals);


        return "MobileAppGroupDetails";
    }

    @RequestMapping(value = "/admin/mobileApp/createGroup", method = RequestMethod.GET)
    public Object mobileCreateGroup(Model model, HttpServletRequest request) {
        if (!U.var.parseBoolean(request.getSession().getAttribute("login"))) {
            return "redirect:/admin/login";
        }

        Map<String, Object> globals = new HashMap<>();
        globals.put("modules_enable_coupons", globalsDao.findOne("modules_enable_coupons").getValue());
        globals.put("modules_enable_invoice", globalsDao.findOne("modules_enable_invoice").getValue());
        globals.put("modules_enable_logistics", globalsDao.findOne("modules_enable_logistics").getValue());
        globals.put("modules_enable_packing", globalsDao.findOne("modules_enable_packing").getValue());
        Object logedWorker = request.getSession().getAttribute("userId");
        Worker worker = workersDao.getOne(Integer.parseInt(logedWorker.toString()));
        globals.put("worker_name", worker.getFirstName() + " " + worker.getLastName());
        model.addAttribute("globals", globals);
        model.addAttribute("admin", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.ADMIN.ordinal() ? 1 : 0);

        return "MobileAppCreateGroup";
    }

    @RequestMapping(value = "/admin/mobileApp/group/submit", method = RequestMethod.POST)
    public Object mobileAppGroupSubmit(@RequestParam(value = "groupName") String groupName, Model model, HttpServletRequest request) {
        System.out.format("Generate mobileApp group: %s%n", groupName);
        MobileGroup group = new MobileGroup();
        group.setName(groupName);
        group.setIsDeleted('0');

        mobileGroupsDao.save(group);

        return "redirect:/admin/mobileApp";
    }

    @RequestMapping(value = "/admin/mobileApp/group/submit/addUser", method = RequestMethod.POST)
    public Object addUser(@RequestParam(value = "customerId") Integer userId, @RequestParam(value = "groupId") Integer groupId, HttpServletRequest request) {
        MobileUser user = new MobileUser();
        user.setGroupId(groupId);
        user.setCustomerId(userId);
        user.setIsDeleted('0');

        mobileUsersDao.save(user);
        return "redirect:/admin/mobileApp/group?id=" + groupId;
    }

    @RequestMapping(value = "/admin/mobileApp/createNotification", method = RequestMethod.GET)
    public Object mobileCreateNotification(Model model, HttpServletRequest request) {
        if (!U.var.parseBoolean(request.getSession().getAttribute("login"))) {
            return "redirect:/admin/login";
        }

        List<MobileGroup> mobileGroups = mobileGroupsDao.findAll();
        model.addAttribute("mobileGroups", mobileGroups);

        Map<String, Object> globals = new HashMap<>();
        globals.put("modules_enable_coupons", globalsDao.findOne("modules_enable_coupons").getValue());
        globals.put("modules_enable_invoice", globalsDao.findOne("modules_enable_invoice").getValue());
        globals.put("modules_enable_logistics", globalsDao.findOne("modules_enable_logistics").getValue());
        globals.put("modules_enable_packing", globalsDao.findOne("modules_enable_packing").getValue());
        Object logedWorker = request.getSession().getAttribute("userId");
        Worker worker = workersDao.getOne(Integer.parseInt(logedWorker.toString()));
        globals.put("worker_name", worker.getFirstName() + " " + worker.getLastName());
        model.addAttribute("globals", globals);
        model.addAttribute("admin", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.ADMIN.ordinal() ? 1 : 0);

        return "MobileAppCreateNotification";
    }

    @RequestMapping(value = "/admin/mobileApp/notification/submit", method = RequestMethod.POST)
    public Object mobileAppNotificationSubmit(@RequestParam(value = "mobileGroup") Integer mobileGroup, @RequestParam(value = "notificationType") Integer notificationType, @RequestParam(value = "notificationText") String notificationText, Model model, HttpServletRequest request) {
        MobileNotification notification = new MobileNotification();
        notification.setGroupId(mobileGroup);
        notification.setText(notificationText);
        notification.setIsDeleted('0');

        switch (notificationType) {
            case 0: notification.setType(MobileNotification.notificationType.REMAINDER); break;
            case 1: notification.setType(MobileNotification.notificationType.PACKING); break;
            case 2: notification.setType(MobileNotification.notificationType.DELIVERY); break;
            case 3: notification.setType(MobileNotification.notificationType.GROUP); break;
            case 4: notification.setType(MobileNotification.notificationType.INFORMATION); break;
            case 5: notification.setType(MobileNotification.notificationType.GENERAL); break;
            case 6: notification.setType(MobileNotification.notificationType.REMAINDER); break;
            default: notification.setType(MobileNotification.notificationType.GENERAL); break;
        }

        mobileNotificationsDao.save(notification);

        return "redirect:/admin/mobileApp";
    }

    @RequestMapping(value = "/admin/mobileApp/csv/group", method = RequestMethod.GET)
    public Object csvCouponUsage(@RequestParam(value = "groupId") Integer groupId, Model model, HttpServletResponse response) {
        MobileGroup group = mobileGroupsDao.findOne(groupId);
        StringBuilder stringBuilder = new StringBuilder("\n");
        stringBuilder.append("Mobile group:").append(',').append('"').append(group.getName()).append('"').append("\n");
        List<MobileUser> users = mobileUsersDao.findAllByGroup(group.getId());

        String fullName = "";
        String phone = "";
        Customer customer;

        for (MobileUser user : users) {
            customer = customersDao.findOne(user.getCustomerId());

            fullName = customer.getFirstName() + " " + customer.getLastName();
            phone = customer.getPhoneExt() + "-" + customer.getPhoneNo();
            stringBuilder.append('"').append(fullName).append('"').append(',').append('"').append(customer.getEmail()).append('"').append("\n");
        }

        response.setHeader("Content-Disposition", "attachment; filename=\"coupon_details.csv\"");
        response.setContentLength(stringBuilder.length());

        response.setContentType("text/csv; charset=");
        return stringBuilder.toString();
    }

    @RequestMapping(value = "/admin/mobileApp/sendNotification", method = RequestMethod.GET)
    public Object sendNotification(@RequestParam(value = "notificationId") Integer notificationId, Model model, HttpServletRequest request) {

        MobileNotification notification = mobileNotificationsDao.findOne(notificationId);
        MobileGroup group = mobileGroupsDao.findOne(notification.getGroupId());
        List<MobileUser> users = mobileUsersDao.findAllByGroup(group.getId());
        String text = notification.getText();
        String deviceToken = "";
        for (MobileUser user : users) {
            Customer customer = customersDao.findOne(user.getCustomerId());
            System.out.format("   Customer: %s   Group: %d   Notification %d%n", user.getCustomerId(), group.getId(), notificationId);
            try {
                deviceToken = wpUsermetaDao.findOneByIdAndKey(U.var.parseLong(customer.getWpId()), "app_device_token").getMetaValue();
                common.sendMobileNotification(deviceToken, text);
            } catch (Exception ex) {
                deviceToken = "";
            }

            System.out.format("   Sending: %s   To: %s%n", text, deviceToken);
            // sendMobileNotification(deviceToken, text);
        }

        return "redirect:/admin/mobileApp";
    }

    @RequestMapping(value = "/admin/mobileApp/sendNotificationCron", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public void sendNotificationCron(@RequestParam(value = "notificationId") Integer notificationId, Model model, HttpServletRequest request) {

        MobileNotification notification = mobileNotificationsDao.findOne(notificationId);
        MobileGroup group = mobileGroupsDao.findOne(notification.getGroupId());
        List<MobileUser> users = mobileUsersDao.findAllByGroup(group.getId());
        String text = notification.getText();
        String deviceToken = "";
        for (MobileUser user : users) {
            Customer customer = customersDao.findOne(user.getCustomerId());
            System.out.format("   Customer: %s   Group: %d   Notification %d%n", user.getCustomerId(), group.getId(), notificationId);
            try {
                deviceToken = wpUsermetaDao.findOneByIdAndKey(U.var.parseLong(customer.getWpId()), "app_device_token").getMetaValue();
                common.sendMobileNotification(deviceToken, text);
            } catch (Exception ex) {
                deviceToken = "";
            }

            System.out.format("   Sending: %s   To: %s%n", text, deviceToken);
            // sendMobileNotification(deviceToken, text);
        }
    }
}
