package com.shookbook.admin;

import com.shookbook.dao.shookbook.*;
import com.shookbook.entities.shookbook.*;
import com.shookbook.types.HashSetIgnoresNulls;
import com.shookbook.utils.U;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by MorM on 4/20/16.
 */
@Controller
@SessionAttributes("thought")
public class SuppliersAdmin {
    private static final Logger logger = Logger.getLogger(SuppliersAdmin.class);
    @Autowired
    private Common common;
    @Autowired
    private ProductsDao productsDao;
    @Autowired
    private CustomersDao customersDao;
    @Autowired
    private OrdersDao ordersDao;
    @Autowired
    private OrderProductDao orderProductDao;
    @Autowired
    private ItemsDao itemsDao;
    @Autowired
    private SuppliersDao suppliersDao;
    @Autowired
    private SupplierOrdersDao supplierOrdersDao;
    @Autowired
    private SupplierOrderItemDao supplierOrderItemDao;
    @Autowired
    private InventoryDao inventoryDao;
    @Autowired
    private InventoryItemDao inventoryItemDao;
    @Autowired
    private WorkersDao workersDao;
    @Autowired
    private GlobalsDao globalsDao;

    @RequestMapping(value = "/admin/suppliers/add", method = RequestMethod.GET)
    public Object supplierAdd(Model model, HttpServletRequest request) {
        Map<String, Object> globals = new HashMap<>();
        globals.put("modules_enable_coupons", globalsDao.findOne("modules_enable_coupons").getValue());
        globals.put("modules_enable_invoice", globalsDao.findOne("modules_enable_invoice").getValue());
        globals.put("modules_enable_logistics", globalsDao.findOne("modules_enable_logistics").getValue());
        globals.put("modules_enable_packing", globalsDao.findOne("modules_enable_packing").getValue());
        Object logedWorker = request.getSession().getAttribute("userId");
        Worker worker = workersDao.getOne(Integer.parseInt(logedWorker.toString()));
        globals.put("worker_name", worker.getFirstName() + " " + worker.getLastName());
        model.addAttribute("globals", globals);

        return "SupplierAdd";
    }

    @RequestMapping(value = "/admin/suppliers", method = RequestMethod.GET)
    public Object suppliers(Model model, HttpServletRequest request) {
        if (!U.var.parseBoolean(request.getSession().getAttribute("login"))) {
            return "redirect:/admin/login";
        }
        List<Supplier> suppliers = suppliersDao.findAll();


        List<Map<String, Object>> columns = new LinkedList<>();
        for (Supplier supplier : suppliers) {
            Map<String, Object> column = new HashMap<>();
            column.put("supplierName", supplier.getName());
            column.put("supplierNumber", common.pedSupplierNumber(supplier.getId()));
            column.put("phone", supplier.getPhone());
            column.put("email", supplier.getEmail());
            column.put("joinDate", new SimpleDateFormat("dd-MM-yyyy").format(supplier.getCreatedDate()));
            column.put("ordersQuantity", supplierOrdersDao.getSupplierOrdersNo(supplier.getId()));
            columns.add(column);
        }

        model.addAttribute("columns", columns);
        Map<String, Object> globals = new HashMap<>();
        globals.put("modules_enable_coupons", globalsDao.findOne("modules_enable_coupons").getValue());
        globals.put("modules_enable_invoice", globalsDao.findOne("modules_enable_invoice").getValue());
        globals.put("modules_enable_logistics", globalsDao.findOne("modules_enable_logistics").getValue());
        globals.put("modules_enable_packing", globalsDao.findOne("modules_enable_packing").getValue());
        Object logedWorker = request.getSession().getAttribute("userId");
        Worker worker = workersDao.getOne(Integer.parseInt(logedWorker.toString()));
        globals.put("worker_name", worker.getFirstName() + " " + worker.getLastName());
        model.addAttribute("globals", globals);
        model.addAttribute("admin", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.ADMIN.ordinal() ? 1 : 0);
        return "Suppliers";
    }


    @RequestMapping(value = "/admin/suppliers/supplier", method = RequestMethod.GET)
    public Object suppliersDetails(@RequestParam(value = "id") Integer id, Model model, HttpServletRequest request) {
        if (!U.var.parseBoolean(request.getSession().getAttribute("login"))) {
            return "redirect:/admin/login";
        }

        SupplierOrder supplierOrder = supplierOrdersDao.findOne(id);
        Supplier supplier = suppliersDao.findOne(id);
        Map<String, Object> column = new HashMap<>();
        column.put("supplierName", supplier.getName());
        column.put("id", supplier.getId());
        column.put("supplierNumber", common.pedSupplierNumber(supplier.getId()));
        column.put("email", supplier.getEmail());
        column.put("licensedDealer", supplier.getCompanyId());
        column.put("contactMan", supplier.getContact());
        column.put("phone", supplier.getPhone());
        column.put("contactMan2", supplier.getContact2());
        column.put("phone2", supplier.getPhone2());
        column.put("fax", supplier.getFax());
        column.put("bankAccountNo", supplier.getBankAccountNo());
        column.put("bankAccountOwner", supplier.getBankAccountOwner());
        column.put("bankNo", supplier.getBankNo());
        column.put("bankBranchNo", supplier.getBankBranchNo());
        column.put("businessAddress", supplier.getStreet() + " " + (supplier.getHouseNo() != null ? supplier.getHouseNo() : ""));
        column.put("businessCity", supplier.getCity());
        column.put("zip", supplier.getZip());
        column.put("orderInventoryFactor", U.var.json2Map(supplier.getOrderInventoryFactorJsonAsJson()));
        column.put("orderWeightFactor", U.var.json2Map(supplier.getOrderWeightFactorJsonAsJson()));

        model.addAttribute("column", column);
        Map<String, Object> globals = new HashMap<>();
        globals.put("modules_enable_coupons", globalsDao.findOne("modules_enable_coupons").getValue());
        globals.put("modules_enable_invoice", globalsDao.findOne("modules_enable_invoice").getValue());
        globals.put("modules_enable_logistics", globalsDao.findOne("modules_enable_logistics").getValue());
        globals.put("modules_enable_packing", globalsDao.findOne("modules_enable_packing").getValue());
        Object logedWorker = request.getSession().getAttribute("userId");
        Worker worker = workersDao.getOne(Integer.parseInt(logedWorker.toString()));
        globals.put("worker_name", worker.getFirstName() + " " + worker.getLastName());
        model.addAttribute("globals", globals);
        model.addAttribute("admin", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.ADMIN.ordinal() ? 1 : 0);
        return "Supplier";
    }


    @RequestMapping(value = "/admin/suppliers/supplier/edit", method = RequestMethod.GET)
    public Object suppliersDetailsEdit(@RequestParam(value = "id") Integer id, Model model, HttpServletRequest request) {
        if (!U.var.parseBoolean(request.getSession().getAttribute("login"))) {
            return "redirect:/admin/login";
        }
        SupplierOrder supplierOrder = supplierOrdersDao.findOne(id);
        Supplier supplier = suppliersDao.findOne(id);
        Map<String, Object> column = new HashMap<>();
        column.put("supplierName", supplier.getName());
        column.put("id", supplier.getId());
        column.put("email", supplier.getEmail());
        column.put("licensedDealer", supplier.getCompanyId());
        column.put("contactMan", supplier.getContact());
        column.put("phone", supplier.getPhone());
        column.put("contactMan2", supplier.getContact2());
        column.put("phone2", supplier.getPhone2());
        column.put("fax", supplier.getFax());
        column.put("bankAccountNo", supplier.getBankAccountNo());
        column.put("bankAccountOwner", supplier.getBankAccountOwner());
        column.put("bankNo", supplier.getBankNo());
        column.put("bankBranchNo", supplier.getBankBranchNo());
        column.put("businessAddress", supplier.getStreet() + " " + supplier.getHouseNo());
        column.put("businessCity", supplier.getCity());
        column.put("zip", supplier.getZip());
        column.put("orderInventoryFactor", U.var.json2Map(supplier.getOrderInventoryFactorJsonAsJson()));
        column.put("orderWeightFactor", U.var.json2Map(supplier.getOrderWeightFactorJsonAsJson()));

        model.addAttribute("column", column);
        Map<String, Object> globals = new HashMap<>();
        globals.put("modules_enable_coupons", globalsDao.findOne("modules_enable_coupons").getValue());
        globals.put("modules_enable_invoice", globalsDao.findOne("modules_enable_invoice").getValue());
        globals.put("modules_enable_logistics", globalsDao.findOne("modules_enable_logistics").getValue());
        globals.put("modules_enable_packing", globalsDao.findOne("modules_enable_packing").getValue());
        Object logedWorker = request.getSession().getAttribute("userId");
        Worker worker = workersDao.getOne(Integer.parseInt(logedWorker.toString()));
        globals.put("worker_name", worker.getFirstName() + " " + worker.getLastName());
        model.addAttribute("globals", globals);
        model.addAttribute("admin", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.ADMIN.ordinal() ? 1 : 0);
        return "SupplierEdit";
    }

    @RequestMapping(value = "/admin/suppliers/supplier/submitEdit", method = RequestMethod.POST)
    public Object supplierEditSubmit(@RequestParam(value = "id") Integer id,
                                     @RequestParam(value = "supplierName") String supplierName,
                                     @RequestParam(value = "contactMan") String contactMan,
                                     @RequestParam(value = "contactMan2") String contactMan2,
                                     @RequestParam(value = "email") String email,
                                     @RequestParam(value = "phone") String phone,
                                     @RequestParam(value = "phone2") String phone2,
                                     @RequestParam(value = "fax") String fax,
                                     @RequestParam(value = "street") String street,
                                     @RequestParam(value = "city") String city,
                                     @RequestParam(value = "zip") String zip,
                                     @RequestParam(value = "bankAccountOwner") String bankAccountOwner,
                                     @RequestParam(value = "bankNo") String bankNo,
                                     @RequestParam(value = "bankBranchNo") String bankBranchNo,
                                     @RequestParam(value = "bankAccountNo") String bankAccountNo,
                                     @RequestParam(value = "orderInventoryFactorEnable", required = false) String orderInventoryFactorEnableValue,
                                     @RequestParam(value = "orderInventoryFactorDay1") Double orderInventoryFactorDay1,
                                     @RequestParam(value = "orderInventoryFactorDay2") Double orderInventoryFactorDay2,
                                     @RequestParam(value = "orderInventoryFactorDay3") Double orderInventoryFactorDay3,
                                     @RequestParam(value = "orderInventoryFactorDay4") Double orderInventoryFactorDay4,
                                     @RequestParam(value = "orderInventoryFactorDay5") Double orderInventoryFactorDay5,
                                     @RequestParam(value = "orderInventoryFactorDay6") Double orderInventoryFactorDay6,
                                     @RequestParam(value = "orderInventoryFactorDay7") Double orderInventoryFactorDay7,
                                     @RequestParam(value = "orderWeightFactorEnable", required = false) String orderWeightFactorEnableValue,
                                     @RequestParam(value = "orderWeightFactorDay1") Double orderWeightFactorDay1,
                                     @RequestParam(value = "orderWeightFactorDay2") Double orderWeightFactorDay2,
                                     @RequestParam(value = "orderWeightFactorDay3") Double orderWeightFactorDay3,
                                     @RequestParam(value = "orderWeightFactorDay4") Double orderWeightFactorDay4,
                                     @RequestParam(value = "orderWeightFactorDay5") Double orderWeightFactorDay5,
                                     @RequestParam(value = "orderWeightFactorDay6") Double orderWeightFactorDay6,
                                     @RequestParam(value = "orderWeightFactorDay7") Double orderWeightFactorDay7,
                                     HttpServletRequest request) {
        if (!U.var.parseBoolean(request.getSession().getAttribute("login"))) {
            return "redirect:/admin/login";
        }
        Supplier supplier = suppliersDao.findOne(id);

        boolean orderInventoryFactorEnable = "on".equals(orderInventoryFactorEnableValue);
        boolean orderWeightFactorEnable = "on".equals(orderWeightFactorEnableValue);

        supplier.setName(supplierName);
        supplier.setPhone(phone);
        supplier.setPhone2(phone2);
        supplier.setEmail(email);
        supplier.setFax(fax);
        supplier.setContact(contactMan);
        supplier.setContact2(contactMan2);
        supplier.setCity(city);
        supplier.setStreet(street);
        supplier.setZip(zip);
        supplier.setBankAccountOwner(bankAccountOwner);
        supplier.setBankNo(bankNo);
        supplier.setBankBranchNo(bankBranchNo);
        supplier.setBankAccountNo(bankAccountNo);
        if (!orderInventoryFactorEnable && orderInventoryFactorDay1 == 1d && orderInventoryFactorDay2 == 1d && orderInventoryFactorDay3 == 1d && orderInventoryFactorDay4 == 1d && orderInventoryFactorDay5 == 1d && orderInventoryFactorDay6 == 1d && orderInventoryFactorDay7 == 1d) {
            supplier.resetOrderInventoryFactorJson();
        } else {
            supplier.setOrderInventoryFactor("enable", orderInventoryFactorEnable);
            supplier.setOrderInventoryFactor("day1", orderInventoryFactorDay1);
            supplier.setOrderInventoryFactor("day2", orderInventoryFactorDay2);
            supplier.setOrderInventoryFactor("day3", orderInventoryFactorDay3);
            supplier.setOrderInventoryFactor("day4", orderInventoryFactorDay4);
            supplier.setOrderInventoryFactor("day5", orderInventoryFactorDay5);
            supplier.setOrderInventoryFactor("day6", orderInventoryFactorDay6);
            supplier.setOrderInventoryFactor("day7", orderInventoryFactorDay7);
        }

        if (!orderWeightFactorEnable && orderWeightFactorDay1 == 1d && orderWeightFactorDay2 == 1d && orderWeightFactorDay3 == 1d && orderWeightFactorDay4 == 1d && orderWeightFactorDay5 == 1d && orderWeightFactorDay6 == 1d && orderWeightFactorDay7 == 1d) {
            supplier.resetOrderWeightFactorJson();
        } else {
            supplier.setOrderWeightFactor("enable", orderWeightFactorEnable);
            supplier.setOrderWeightFactor("day1", orderWeightFactorDay1);
            supplier.setOrderWeightFactor("day2", orderWeightFactorDay2);
            supplier.setOrderWeightFactor("day3", orderWeightFactorDay3);
            supplier.setOrderWeightFactor("day4", orderWeightFactorDay4);
            supplier.setOrderWeightFactor("day5", orderWeightFactorDay5);
            supplier.setOrderWeightFactor("day6", orderWeightFactorDay6);
            supplier.setOrderWeightFactor("day7", orderWeightFactorDay7);
        }

        suppliersDao.save(supplier);

        logger.info("SB: " + request.getSession().getAttribute("userName") + ": update supplier " + supplier.getId());

        return "redirect:/admin/suppliers/supplier?id=" + id;
    }


    @RequestMapping(value = "/admin/suppliers/supplier/items", method = RequestMethod.GET)
    public Object supplierProducts(@RequestParam(value = "id") Integer id, Model model, HttpServletRequest request) {
        if (!U.var.parseBoolean(request.getSession().getAttribute("login"))) {
            return "redirect:/admin/login";
        }

        Supplier supplier = suppliersDao.findOne(id);
        Map<String, Object> supplierDetails = new HashMap<>();
        supplierDetails.put("supplierName", supplier.getName());
        supplierDetails.put("id", supplier.getId());
        supplierDetails.put("supplierNumber", common.pedSupplierNumber(supplier.getId()));
        supplierDetails.put("phone", supplier.getPhone());
        supplierDetails.put("email", supplier.getEmail());
        supplierDetails.put("joinDate", new SimpleDateFormat("dd-MM-yyyy").format(supplier.getCreatedDate()));
        List<Item> itemIds = itemsDao.findAllBySuppliersId(id);
        supplierDetails.put("itemsCount", itemIds.size());
        supplierDetails.put("fax", supplier.getFax());
        model.addAttribute("supplierDetails", supplierDetails);

        List<Integer> productIds = new LinkedList<>();
        for (Item item : itemIds) {
            productIds.add(item.getProductId());
        }

        List<Map<String, Object>> items = new LinkedList<>();
        for (Item item : itemsDao.findAllBySuppliersId(id)) {
            Map<String, Object> column = new HashMap<>();
            Product product = productsDao.findOne(item.getProductId());
            column.put("product_name", product.getNameHeb());
            column.put("barcode", item.getBarcode());
            column.put("buying_cost", item.getBuyingCost());
            column.put("inventory", item.getInventory());
            column.put("unit", product.getPackageType().heb);
            column.put("status", product.getStatus().heb);

            items.add(column);
        }

        model.addAttribute("items", items);
        model.addAttribute("admin", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.ADMIN.ordinal() ? 1 : 0);
        return "SupplierProducts";
    }

    @RequestMapping(value = "/admin/suppliers/add/submit", method = RequestMethod.POST)
    public Object supplierAddSubmit(@RequestParam(value = "supplierName") String supplierName,
                                    @RequestParam(value = "licensedDealer") String licensedDealer,
                                    @RequestParam(value = "contactMan") String contactMan,
                                    @RequestParam(value = "phone") String phone,
                                    @RequestParam(value = "fax") String fax,
                                    @RequestParam(value = "email") String email,
                                    @RequestParam(value = "contactMan2") String contactMan2,
                                    @RequestParam(value = "phone2") String phone2,
                                    @RequestParam(value = "street") String street,
                                    @RequestParam(value = "city") String city,
                                    @RequestParam(value = "zip") String zip,
                                    @RequestParam(value = "bankNo") String bankNo,
                                    @RequestParam(value = "bankBranchNo") String bankBranchNo,
                                    @RequestParam(value = "bankAccountNo") String bankAccountNo,
                                    @RequestParam(value = "bankAccountOwner") String bankAccountOwner,
                                    @RequestParam(value = "orderInventoryFactorEnable", required = false) String orderInventoryFactorEnableValue,
                                    @RequestParam(value = "orderInventoryFactorDay1") Double orderInventoryFactorDay1,
                                    @RequestParam(value = "orderInventoryFactorDay2") Double orderInventoryFactorDay2,
                                    @RequestParam(value = "orderInventoryFactorDay3") Double orderInventoryFactorDay3,
                                    @RequestParam(value = "orderInventoryFactorDay4") Double orderInventoryFactorDay4,
                                    @RequestParam(value = "orderInventoryFactorDay5") Double orderInventoryFactorDay5,
                                    @RequestParam(value = "orderInventoryFactorDay6") Double orderInventoryFactorDay6,
                                    @RequestParam(value = "orderInventoryFactorDay7") Double orderInventoryFactorDay7,
                                    @RequestParam(value = "orderWeightFactorEnable", required = false) String orderWeightFactorEnableValue,
                                    @RequestParam(value = "orderWeightFactorDay1") Double orderWeightFactorDay1,
                                    @RequestParam(value = "orderWeightFactorDay2") Double orderWeightFactorDay2,
                                    @RequestParam(value = "orderWeightFactorDay3") Double orderWeightFactorDay3,
                                    @RequestParam(value = "orderWeightFactorDay4") Double orderWeightFactorDay4,
                                    @RequestParam(value = "orderWeightFactorDay5") Double orderWeightFactorDay5,
                                    @RequestParam(value = "orderWeightFactorDay6") Double orderWeightFactorDay6,
                                    @RequestParam(value = "orderWeightFactorDay7") Double orderWeightFactorDay7,
                                    Model model, HttpServletRequest request) {
        Supplier supplier = new Supplier();

        supplier.setName(supplierName);
        supplier.setCompanyId(licensedDealer);
        supplier.setContact(contactMan);
        supplier.setPhone(phone);
        supplier.setFax(fax);
        supplier.setEmail(email);
        supplier.setContact2(contactMan2);
        supplier.setPhone2(phone2);
        supplier.setStreet(street);
        supplier.setCity(city);
        supplier.setZip(zip);
        supplier.setBankAccountOwner(bankAccountOwner);
        supplier.setBankNo(bankNo);
        supplier.setBankBranchNo(bankBranchNo);
        supplier.setBankAccountNo(bankAccountNo);
        supplier.setIsDeleted('0');
        supplier.setStatus('0');

        boolean orderInventoryFactorEnable = "on".equals(orderInventoryFactorEnableValue);

        if (!orderInventoryFactorEnable && orderInventoryFactorDay1 == 1d && orderInventoryFactorDay2 == 1d && orderInventoryFactorDay3 == 1d && orderInventoryFactorDay4 == 1d && orderInventoryFactorDay5 == 1d && orderInventoryFactorDay6 == 1d && orderInventoryFactorDay7 == 1d) {
            supplier.resetOrderInventoryFactorJson();
        } else {
            supplier.setOrderInventoryFactor("enable", orderInventoryFactorEnable);
            supplier.setOrderInventoryFactor("day1", orderInventoryFactorDay1);
            supplier.setOrderInventoryFactor("day2", orderInventoryFactorDay2);
            supplier.setOrderInventoryFactor("day3", orderInventoryFactorDay3);
            supplier.setOrderInventoryFactor("day4", orderInventoryFactorDay4);
            supplier.setOrderInventoryFactor("day5", orderInventoryFactorDay5);
            supplier.setOrderInventoryFactor("day6", orderInventoryFactorDay6);
            supplier.setOrderInventoryFactor("day7", orderInventoryFactorDay7);
        }

        boolean orderWeightFactorEnable = "on".equals(orderWeightFactorEnableValue);

        if (!orderWeightFactorEnable && orderWeightFactorDay1 == 1d && orderWeightFactorDay2 == 1d && orderWeightFactorDay3 == 1d && orderWeightFactorDay4 == 1d && orderWeightFactorDay5 == 1d && orderWeightFactorDay6 == 1d && orderWeightFactorDay7 == 1d) {
            supplier.resetOrderWeightFactorJson();
        } else {
            supplier.setOrderWeightFactor("enable", orderWeightFactorEnable);
            supplier.setOrderWeightFactor("day1", orderWeightFactorDay1);
            supplier.setOrderWeightFactor("day2", orderWeightFactorDay2);
            supplier.setOrderWeightFactor("day3", orderWeightFactorDay3);
            supplier.setOrderWeightFactor("day4", orderWeightFactorDay4);
            supplier.setOrderWeightFactor("day5", orderWeightFactorDay5);
            supplier.setOrderWeightFactor("day6", orderWeightFactorDay6);
            supplier.setOrderWeightFactor("day7", orderWeightFactorDay7);
        }

        supplier = suppliersDao.save(supplier);
        logger.info("SB: " + request.getSession().getAttribute("userName") + ": add supplier " + supplier.getId());
        Map<String, Object> globals = new HashMap<>();
        globals.put("modules_enable_coupons", globalsDao.findOne("modules_enable_coupons").getValue());
        globals.put("modules_enable_invoice", globalsDao.findOne("modules_enable_invoice").getValue());
        globals.put("modules_enable_logistics", globalsDao.findOne("modules_enable_logistics").getValue());
        globals.put("modules_enable_packing", globalsDao.findOne("modules_enable_packing").getValue());
        Object logedWorker = request.getSession().getAttribute("userId");
        Worker worker = workersDao.getOne(Integer.parseInt(logedWorker.toString()));
        globals.put("worker_name", worker.getFirstName() + " " + worker.getLastName());
        model.addAttribute("globals", globals);
        model.addAttribute("admin", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.ADMIN.ordinal() ? 1 : 0);
        return "redirect:/admin/suppliers";
    }

    @RequestMapping(value = "/admin/suppliers/supplier/orders", method = RequestMethod.GET)
    public Object supplierOrders(@RequestParam(value = "id") Integer id, Model model, HttpServletRequest request) {
        if (!U.var.parseBoolean(request.getSession().getAttribute("login"))) {
            return "redirect:/admin/login";
        }
        List<Map<String, Object>> columns = new LinkedList<>();

        Supplier supplier = suppliersDao.findOne(id);

        Map<String, Object> details = new HashMap<>();
        details.put("supplierName", supplier.getName());
        details.put("id", supplier.getId());
        columns.add(details);

        for (SupplierOrder supplierOrder : supplierOrdersDao.findAll()) {
            Map<String, Object> column = new HashMap<>();
            if (supplierOrder.getSupplierId() == id) {
                column.put("orderNumber", common.createSupplierOrderSBID(supplierOrder.getSupplierId(), supplierOrder.getId()));
                column.put("orderDate", new SimpleDateFormat("dd-MM-yyyy").format(supplierOrder.getCreatedDate()));
                column.put("orderSum", supplierOrder.getSum());
                column.put("orderStatus", supplierOrder.getStatus().heb);
            }
            columns.add(column);
        }
        model.addAttribute("columns", columns);
        model.addAttribute("admin", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.ADMIN.ordinal() ? 1 : 0);
        return "SupplierOrder";
    }


    @RequestMapping(value = "/admin/suppliers/orders/order/printSupplierOrderQRcode", method = RequestMethod.GET)
    public Object printQRcodeSuppliers(@RequestParam(value = "id") Integer id, Model model, HttpServletRequest request) {
        if (!U.var.parseBoolean(request.getSession().getAttribute("login"))) {
            return "redirect:/admin/login";
        }
        String barcode = ordersDao.findOne(id).getBarcode();
        SupplierOrder supplierOrder = supplierOrdersDao.getOne(id);
        int supplierId = supplierOrder.getSupplierId();
        Supplier supplier = suppliersDao.findOne(supplierId);

        Map<String, Object> column = new HashMap<>();

        column.put("supplierOrderId", supplierOrder.getId());
        column.put("supplierName", supplier.getName());
        column.put("supplierAddress", supplier.getStreet() + " " + supplier.getHouseNo() + " , " + supplier.getCity());

        Set<Integer> itemsIds = new HashSetIgnoresNulls<>();
        for (SupplierOrderItem supplierOrderItem : supplierOrderItemDao.findAllByOrderId(supplierOrder.getId())) {
            itemsIds.add(supplierOrderItem.getItemId());
        }
        Map<Integer, Item> itemsMap = new HashMap<>();
        for (Item item : itemsDao.findAll(itemsIds)) {
            itemsMap.put(item.getId(), item);
        }

        List<Map<String, Object>> items = new LinkedList<>();
        for (SupplierOrderItem supplierOrderItem : supplierOrderItemDao.findAllByOrderId(supplierOrder.getId())) {
            Map<String, Object> item = new HashMap<>();
            item.put("itemId", itemsMap.get(supplierOrderItem.getItemId()).getId());
            item.put("itemBarcode", itemsMap.get(supplierOrderItem.getItemId()).getBarcode());
            items.add(item);
        }


        model.addAttribute("qr", barcode);
        model.addAttribute("column", column);
        model.addAttribute("items", items);
        model.addAttribute("admin", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.ADMIN.ordinal() ? 1 : 0);

        return "PrintQRcodeSuppliers";
    }


    @RequestMapping(value = "/admin/suppliers/removeSupplier", method = RequestMethod.GET)
    public Object supplierRemove(@RequestParam(value = "id") Integer id, Model model, HttpServletRequest request) {
        if (!U.var.parseBoolean(request.getSession().getAttribute("login"))) {
            return "redirect:/admin/login";
        }
        Supplier supplier = suppliersDao.findOne(id);

        supplier.setIsDeleted('1');
        suppliersDao.save(supplier);
        logger.info("SB: " + request.getSession().getAttribute("userName") + ": remove supplier " + supplier.getId());
        return "redirect:/admin/suppliers";
    }


    @RequestMapping(value = "/admin/suppliers/supplier/notes", method = RequestMethod.GET)
    public Object supplierAdministrationNotes(@RequestParam(value = "id") Integer id, Model model, HttpServletRequest request) {

        Supplier supplier = suppliersDao.findOne(id);
        Map<String, Object> item = new HashMap<>();
        item.put("name", supplier.getName());
        item.put("date", new SimpleDateFormat("dd-MM-yyyy").format(supplier.getCreatedDate()));
        item.put("negative", supplier.getNegativeNotes());
        item.put("positive", supplier.getPositiveNotes());
        model.addAttribute("item", item);
        model.addAttribute("admin", Integer.parseInt(request.getSession().getAttribute("admin").toString()) == Worker.Types.ADMIN.ordinal() ? 1 : 0);

        return "SupplierAdministrationNotes";

    }
}
