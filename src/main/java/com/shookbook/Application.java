package com.shookbook;

import com.shookbook.utils.U;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.io.File;
import java.util.List;


@SpringBootApplication
public class Application
{
    //https://spring.io/guides/gs/rest-service/
    public static String imageRoot = "images";
    public static String csvRoot = "csv";

    public static void main(String[] args)
    {
        U.config.reload("application.properties");
        SpringApplication.run(Application.class, args);
        List list;

//        list = new ArrayList();
//        list.add("A string");
//        list.add(Integer.valueOf(12345));
//        list.add(Boolean.TRUE);
//        System.out.println(Pherialize.serialize(list));

    }

    @Bean
    CommandLineRunner init() {
        return (String[] args) -> {
            new File(imageRoot).mkdir();
            new File(csvRoot).mkdir();
        };
    }




}
