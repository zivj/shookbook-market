package com.shookbook.dao.shookbook;

import com.shookbook.entities.shookbook.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CategoriesDao extends JpaRepository<Category, Integer>
{
    @Query("FROM Category GROUP BY id")
    List<Category> getAllCategories();
}
