/**
 * Created by zivj on 25/9/17.
 */

package com.shookbook.dao.shookbook;

import com.shookbook.entities.shookbook.Refund;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface RefundsDao extends JpaRepository<Refund, Integer> {

    @Query(value = "SELECT * FROM refunds WHERE created_date > DATE_SUB(NOW(), INTERVAL 1 YEAR) AND type <> 6 ORDER BY id DESC", nativeQuery = true)
    List<Refund> findAllInYear();

    @Query(value = "SELECT * FROM coupons WHERE created_date > DATE_SUB(NOW(), INTERVAL 3 MONTH) AND type <> 6 ORDER BY id DESC", nativeQuery = true)
    List<Refund> findAllIn3Months();

    @Query(value = "SELECT * FROM refunds WHERE customer_id = :customerId ORDER BY id DESC", nativeQuery = true)
    List<Refund> findAllByCustomerIdDesc(@Param("customerId") Integer customerId);

    @Query(value = "SELECT * FROM refunds WHERE ((customer_id = :customerId) OR (customer_id = 0))", nativeQuery = true)
    List<Refund> findAllValidForCustomerId(@Param("customerId") Integer customerId);

    @Query(value = "SELECT * FROM refunds WHERE cause = :cause", nativeQuery = true)
    List<Refund> findAllByCause(@Param("cause") Integer cause);

    @Query(value = "SELECT * FROM refunds WHERE type = :type", nativeQuery = true)
    List<Refund> findAllByType(@Param("type") Integer type);

    @Query(value = "SELECT * FROM refunds WHERE created_date BETWEEN :from_date AND :to_date", nativeQuery = true)
    List<Refund> findAllBetweenDates(@Param("from_date") Date fromDate, @Param("to_date") Date toDate);
}