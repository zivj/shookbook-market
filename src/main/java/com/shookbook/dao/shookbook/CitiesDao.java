package com.shookbook.dao.shookbook;


import com.shookbook.entities.shookbook.City;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CitiesDao extends JpaRepository<City, Integer>
{
    @Query(value = "SELECT * FROM cities WHERE name = :city LIMIT 1", nativeQuery = true)
    City findOneByCity(@Param("city") String city);
}
