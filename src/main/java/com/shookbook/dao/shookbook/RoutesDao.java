package com.shookbook.dao.shookbook;

import com.shookbook.entities.shookbook.Routes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface RoutesDao extends JpaRepository<Routes, Integer> {
    @Query(value = "SELECT * FROM routes WHERE delivery_date = :date AND is_deleted = '0'", nativeQuery = true)
    List<Routes> findAllByDeliveryDate(@Param("date") Date date);

    @Query(value = "SELECT count(rs.id) FROM routes r INNER JOIN route_stop rs ON r.id = rs.route_id WHERE r.id = :id AND r.is_deleted = '0' AND rs.is_deleted = '0'", nativeQuery = true)
    Integer getNumberOfStops(@Param("id") Integer id);

    @Query(value = "SELECT count(rs.id) FROM routes r INNER JOIN route_stop rs ON r.id = rs.route_id WHERE r.id = :id AND rs.actual_delivery IS NULL AND r.is_deleted = '0' AND rs.is_deleted = '0'", nativeQuery = true)
    Integer getNumberOfOpenStops(@Param("id") Integer id);

    @Query(value = "SELECT * FROM routes WHERE ((delivery_date = :date AND estimated_start < '13:00') OR (delivery_date = DATE_SUB(:date ,INTERVAL 1 DAY) AND estimated_start > '13:00')) AND is_deleted = '0'", nativeQuery = true)
    List<Routes> findAllPendingByDeliveryDate(@Param("date") Date date);
}
