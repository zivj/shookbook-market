package com.shookbook.dao.shookbook;

import com.shookbook.entities.shookbook.Supplier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface SuppliersDao extends JpaRepository<Supplier, Integer>
{
    @Query("FROM Supplier WHERE is_deleted = '0' ORDER BY id DESC")
    List<Supplier> findAll();

    @Query(value = "SELECT DISTINCT s.*\n" +
            "FROM orders o INNER JOIN order_product op ON o.id = op.order_id INNER JOIN items i ON i.product_id = op.product_id INNER JOIN suppliers s ON s.id = i.supplier_id\n" +
            "WHERE o.status = 1 AND s.order_inventory_factor_json IS NOT NULL AND order_inventory_factor_json LIKE '%\"enable\":true%' AND i.inventory > 0 AND s.is_deleted = '0'", nativeQuery = true)
    List<Supplier> findAllWithFactorForNextOrder();
}
