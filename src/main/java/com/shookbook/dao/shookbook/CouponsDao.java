
/**
 * Created by zivj on 25/9/17.
 */

package com.shookbook.dao.shookbook;

import com.shookbook.entities.shookbook.Coupon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CouponsDao extends JpaRepository<Coupon, Integer> {

        @Query(value = "SELECT * FROM coupons WHERE is_deleted = '0' AND created_date > DATE_SUB(NOW(), INTERVAL 1 YEAR) AND type <> 6 ORDER BY id DESC", nativeQuery = true)
        List<Coupon> findAllInYear();

        @Query(value = "SELECT * FROM coupons WHERE is_deleted = '0' AND created_date > DATE_SUB(NOW(), INTERVAL 3 MONTH) AND type <> 6 ORDER BY id DESC", nativeQuery = true)
        List<Coupon> findAllIn3Months();

        @Query(value = "SELECT * FROM coupons WHERE is_deleted = '0' AND customer_id = :customerId ORDER BY id DESC", nativeQuery = true)
        List<Coupon> findAllByCustomerIdDesc(@Param("customerId") Integer customerId);

        @Query(value = "SELECT * FROM coupons WHERE ((customer_id = :customerId) OR (customer_id = 0)) AND is_deleted = '0'", nativeQuery = true)
        List<Coupon> findAllValidForCustomerId(@Param("customerId") Integer customerId);

        @Query(value = "SELECT * FROM coupons WHERE cause = :cause AND is_deleted = '0'", nativeQuery = true)
        List<Coupon> findAllByCause(@Param("cause") Integer cause);

        @Query(value = "SELECT * FROM coupons WHERE type = :type AND is_deleted = '0'", nativeQuery = true)
        List<Coupon> findAllByType(@Param("type") Integer type);

        @Query(value = "SELECT * FROM coupons WHERE title = :title AND is_deleted = '0' LIMIT 1", nativeQuery = true)
        Coupon findOneByTitle(@Param("title") String title);
}
