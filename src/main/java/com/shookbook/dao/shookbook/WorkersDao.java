package com.shookbook.dao.shookbook;

import com.shookbook.entities.shookbook.Worker;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface WorkersDao extends JpaRepository<Worker, Integer>
{
    @Query("FROM Worker WHERE username = :username AND password = PASSWORD(:password)" )
    Worker login(@Param("username") String username, @Param("password") String password);

    @Modifying
    @Transactional
    @Query(value = "UPDATE workers SET password = PASSWORD(:password) WHERE id = :worker_id", nativeQuery = true)
    void updatePassword(@Param("worker_id") Integer workerId, @Param("password") String password);

    @Query(value = "SELECT * FROM workers WHERE type = 0", nativeQuery = true)
    List<Worker> findAllPackingWorkers();

    @Query(value = "SELECT * FROM workers WHERE first_name = :name AND type = 7 LIMIT 1", nativeQuery = true)
    Worker findOneByName(@Param("name") String name);
}
