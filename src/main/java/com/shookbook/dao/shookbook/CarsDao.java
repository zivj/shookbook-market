package com.shookbook.dao.shookbook;

import com.shookbook.entities.shookbook.Car;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarsDao extends JpaRepository<Car, Integer> {
}
