package com.shookbook.dao.shookbook;

import com.shookbook.entities.shookbook.Global;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface GlobalsDao extends JpaRepository<Global, String>
{

    @Query("FROM Global WHERE key_name = 'email'")
    Global findEmail();

    @Query("FROM Global WHERE key_name = 'email_from'")
    Global findEmailFrom();
}
