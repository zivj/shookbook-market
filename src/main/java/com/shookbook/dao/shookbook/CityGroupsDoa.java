package com.shookbook.dao.shookbook;

import com.shookbook.entities.shookbook.CityGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CityGroupsDoa extends JpaRepository<CityGroup, Integer>
{
    @Query(value = "SELECT cg.* FROM city_groups cg INNER JOIN cities c on cg.id = c.city_group_id WHERE c.name = :city LIMIT 1", nativeQuery = true)
    CityGroup findOneByCity(@Param("city") String city);
}
