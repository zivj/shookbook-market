package com.shookbook.dao.shookbook;

import com.shookbook.entities.shookbook.Inventory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface InventoryDao extends JpaRepository<Inventory, Integer>
{
    @Query(value = "SELECT * FROM inventory WHERE product_id = :product_id", nativeQuery = true)
    Inventory findOneByProductID(@Param("product_id") Integer product_id);
}
