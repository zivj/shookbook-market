package com.shookbook.dao.shookbook;

import com.shookbook.entities.shookbook.MobileNotification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MobileNotificationsDao extends JpaRepository<MobileNotification, Integer> {
    @Query(value = "SELECT * FROM mobile_notifications WHERE group_id = :group_id AND is_deleted = '0' LIMIT 1", nativeQuery = true)
    MobileNotification findOneByGroupId(@Param("group_id") Integer group_id);
}
