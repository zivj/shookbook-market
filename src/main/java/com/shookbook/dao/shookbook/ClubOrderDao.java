package com.shookbook.dao.shookbook;

import com.shookbook.entities.shookbook.ClubOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ClubOrderDao extends JpaRepository<ClubOrder, Integer> {
    @Query(value = "SELECT * FROM club_order WHERE wp_order_id = :wpOrderId LIMIT 1", nativeQuery = true)
    ClubOrder findOneByWpOrderId(@Param("wpOrderId") Integer wpOrderId);
}
