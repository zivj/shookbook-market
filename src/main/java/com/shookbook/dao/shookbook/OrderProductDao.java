package com.shookbook.dao.shookbook;

import com.shookbook.entities.shookbook.OrderProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface OrderProductDao extends JpaRepository<OrderProduct, Integer>
{

    @Query("FROM OrderProduct where order_id IN (:order_ids)")
    List<OrderProduct> findAllByOrderId(@Param("order_ids") Iterable<Integer> orderIds);

    @Query(value = "SELECT op.*\n" +
            "FROM order_product op INNER JOIN orders o ON op.order_id = o.id\n" +
            "WHERE o.created_date > :date", nativeQuery = true)
    List<OrderProduct> findAllByGreaterDate(@Param("date") Date date);

    @Query(value = "SELECT op.*\n" +
            "FROM order_product op\n" +
            "INNER JOIN products p ON op.product_id = p.id\n" +
            "WHERE op.order_id = :order_id ORDER BY p.packing_weight_code", nativeQuery = true)
    List<OrderProduct> findAllByOrderId(@Param("order_id") int orderId);

    @Query(value = "SELECT * FROM order_product WHERE order_id = :order_id AND product_id = :product_id LIMIT 1", nativeQuery = true)
    OrderProduct findOneByOrderIdAndProductId(@Param("order_id") int orderId, @Param("product_id") int productId);


    @Query(value = "SELECT op.*\n" +
            "FROM order_product op\n" +
            "INNER JOIN products p ON op.product_id = p.id\n" +
            "WHERE op.order_id = :order_id AND is_missing = '0' ORDER BY p.packing_weight_code", nativeQuery = true)
    List<OrderProduct> findAllByOrderIdWithoutMissing(@Param("order_id") int orderId);

    @Query(value = "SELECT SUM(quantity) FROM order_product where item_id = :item_id group by item_id", nativeQuery = true)
    Integer quantity(@Param("item_id") int item_id);

    @Query(value = "SELECT DISTINCT COUNT(item_id) FROM order_product where :item_id = item_id", nativeQuery = true)
    Integer getItemCount(@Param("item_id") int item_id);


    @Query(value = "SELECT SUM(quantity)\n" +
            "FROM order_product \n" +
            "WHERE product_id = :product_id", nativeQuery = true)
    Integer getTotalQuantityOfProductFromAllOrders(@Param("product_id") int product_id);


    @Query(value = "SELECT sum(quantity*unit_price) \n" +
            "FROM orders o \n" +
            "INNER JOIN order_product op ON o.id = op.order_id \n" +
            "WHERE op.product_id = :product_id \n " +
            "AND o.customer_id = :customer_id \n" +
            "AND o.created_date BETWEEN  :from_date AND :to_date", nativeQuery = true)
    float getProductAveragePrice(@Param("customer_id") int customer_id, @Param("product_id") int productId, @Param("from_date") Date fromDate, @Param("to_date") Date toDate);

    @Query(value = "SELECT sum(quantity) \n" +
            "FROM orders o \n" +
            "INNER JOIN order_product op ON o.id = op.order_id \n" +
            "WHERE op.product_id = :product_id \n " +
            "AND o.customer_id = :customer_id \n" +
            "AND o.created_date BETWEEN  :from_date AND :to_date", nativeQuery = true)
    int getProductCountPerCustomer(@Param("customer_id") int customer_id, @Param("product_id") int productId, @Param("from_date") Date fromDate, @Param("to_date") Date toDate);

    @Query(value = "SELECT op.product_id, op.order_id, op.quantity_needed, op.quantity, o.dhl_package_number, p.packing_weight_code\n"+
            "FROM order_product op INNER JOIN orders o ON op.order_id = o.id INNER JOIN products p ON op.product_id = p.id\n"+
            "WHERE o.status = 3 AND (o.last_packing_timestamp BETWEEN :from_date AND :to_date) AND op.quantity_needed <> op.quantity\n" +
            "ORDER BY op.product_id", nativeQuery = true)
    List<Object[]> missingReport(@Param("from_date") Date fromDate, @Param("to_date") Date toDate);

    @Query(value = "SELECT op.*\n" +
            "FROM order_product op INNER JOIN orders o ON op.order_id = o.id\n" +
            "WHERE op.order_id IN (:order_ids) AND op.quantity_needed > op.quantity AND dhl_package_number LIKE :palette\n" +
            "ORDER BY o.dhl_package_number", nativeQuery = true)
    List<OrderProduct> findAllWithMissingItemsByOrders(@Param("order_ids") Iterable<Integer> orderIds, @Param("palette") String palette);

    @Query(value = "SELECT o.id, o.dhl_package_number, c.first_name, c.last_name, op.quantity_needed, op.quantity, w.sb_id  FROM orders o INNER JOIN customers c ON c.id = o.customer_id INNER JOIN order_product op ON o.id = op.order_id INNER JOIN workers w ON o.packing_worker_id = w.id\n" +
            "WHERE op.product_id = :product AND o.last_packing_timestamp BETWEEN :from_date AND :to_date", nativeQuery = true)
    List<Object[]> getPackingOrderProducts(@Param("product") Integer product, @Param("from_date") Date fromDate, @Param("to_date") Date toDate);
}
