package com.shookbook.dao.shookbook;

import com.shookbook.entities.shookbook.RouteStop;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RouteStopDao extends JpaRepository<RouteStop, Integer> {
    @Query(value = "SELECT * FROM route_stop WHERE order_id = :order_id AND is_deleted = '0'", nativeQuery = true)
    RouteStop findOneByOrderId(@Param("order_id") Integer order_id);

    @Query(value = "SELECT * FROM route_stop WHERE route_id = :route_id AND is_deleted = '0' ORDER BY estimated_delivery", nativeQuery = true)
    List<RouteStop> findAllByRouteId(@Param("route_id") Integer route_id);

    @Query(value = "SELECT rs.* FROM route_stop rs INNER JOIN orders o ON rs.order_id = o.id WHERE rs.route_id = :route_id AND rs.is_deleted = '0' ORDER BY o.dhl_package_number ASC LIMIT 1", nativeQuery = true)
    RouteStop getFirstInRoute(@Param("route_id") Integer route_id);

    @Query(value = "SELECT rs.* FROM route_stop rs INNER JOIN orders o ON rs.order_id = o.id WHERE rs.route_id = :route_id AND rs.is_deleted = '0' ORDER BY o.dhl_package_number DESC LIMIT 1", nativeQuery = true)
    RouteStop getLastInRoute(@Param("route_id") Integer route_id);
}
