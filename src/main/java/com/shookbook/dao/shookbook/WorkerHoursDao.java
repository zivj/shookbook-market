package com.shookbook.dao.shookbook;

import com.shookbook.entities.shookbook.WorkerHours;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface WorkerHoursDao extends JpaRepository<WorkerHours, Integer>
{
    @Query("FROM WorkerHours WHERE worker_id = :worker_id" )
    List<WorkerHours> findAllByWorker(@Param("worker_id") Integer workerId);

    @Modifying
    @Transactional
    @Query(value = "UPDATE worker_hours\n" +
            "SET end_time = NOW()\n" +
            "WHERE worker_id = :worker_id AND end_time IS NULL", nativeQuery = true)
    void closeAllByWorker(@Param("worker_id") Integer workerId);
}
