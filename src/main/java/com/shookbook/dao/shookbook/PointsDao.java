package com.shookbook.dao.shookbook;

import com.shookbook.entities.shookbook.Point;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface PointsDao extends JpaRepository<Point, Integer>
{
    @Query(value = "SELECT SUM(points) FROM points WHERE customer_id = :customer_id" , nativeQuery = true)
    Integer sum(@Param("customer_id") Integer customerId);

    @Query(value = "SELECT SUM(points) FROM points WHERE customer_id = :customer_id AND points > 0" , nativeQuery = true)
    Integer cumulative(@Param("customer_id") Integer customerId);
}
