package com.shookbook.dao.shookbook;

import com.shookbook.entities.shookbook.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

public interface ProductsDao extends JpaRepository<Product, Integer>
{
    @Query(value = "SELECT p.* FROM products p INNER JOIN order_product op on op.product_id = p.id INNER JOIN orders o ON o.id = op.order_id WHERE o.id = :order_id AND p.is_deleted = '0' ORDER BY p.packing_weight_code", nativeQuery = true)
    List<Product> findAllByOrderId(@Param("order_id") int order_id);

    @Query("FROM Product WHERE category_id = :category AND is_deleted = '0'")
    List<Product> findAllByCategory(@Param("category") int category);

    @Query("FROM Product WHERE product_id = :product_id AND is_deleted = '0'")
    List<Product> findAllById(@Param("product_id") int product_id);

    @Query(value = "SELECT * FROM products WHERE name_heb LIKE :product AND is_deleted = '0'", nativeQuery = true)
    List<Product> findAllLikeProduct(@Param("product") String product);

    @Query(value = "SELECT p.* FROM products p INNER JOIN items i ON p.id = i.product_id INNER JOIN suppliers s ON i.supplier_id = s.id WHERE s.name LIKE :supplier AND p.is_deleted = '0'", nativeQuery = true)
    List<Product> findAllLikeSupplier(@Param("supplier") String supplier);

    @Query("FROM Product WHERE is_missing = '1'")
    List<Product> findAllMissing();

    @Query("FROM Product WHERE wp_id = :wp_id AND is_deleted = '0'")
    Product findOneByWpId(@Param("wp_id") int wpId);

    @Modifying
    @Transactional
    @Query(value = "UPDATE products SET is_missing = '0'", nativeQuery = true)
    void clearAllMissing();

    @Query(value = "SELECT product_id, COUNT(*) order_count, SUM(quantity_needed) - SUM(quantity) quantity_sum\n" +
            "FROM order_product\n" +
            "WHERE order_id IN (SELECT id FROM orders WHERE quantity_needed > quantity AND ((start_packing_timestamp BETWEEN :from_date AND :to_date) OR (status = 2 AND dhl_package_number IS NOT NULL)))\n" +
            "GROUP BY product_id", nativeQuery = true)
    List<Integer[]> findAllCountAndSumInPackingFactory(@Param("from_date") Date fromDate, @Param("to_date") Date toDate);

    @Query(value = "SELECT MID(dhl_package_number, 1, 2) palette, SUM(boxes) boxes\n" +
            "FROM orders\n" +
            "WHERE start_packing_timestamp BETWEEN :from_date AND :to_date AND dhl_package_number IS NOT NULL AND status = 3\n" +
            "GROUP BY MID(dhl_package_number, 1, 2)", nativeQuery = true)
    List<Object[]> findAllPalletsInPackingFactory(@Param("from_date") Date fromDate, @Param("to_date") Date toDate);

    @Query(value = "SELECT op.product_id, p.name_heb, p.name_weight, p.packing_weight_code, SUM(op.quantity_needed) quantity_needed, p.supplier_names\n" +
            "FROM order_product op INNER JOIN orders o ON op.order_id = o.id INNER JOIN products p ON op.product_id = p.id\n" +
            "WHERE o.status = 2\n" +
            "GROUP BY op.product_id\n" +
            "ORDER BY p.packing_weight_code", nativeQuery = true)
    List<Object[]> weightReportPendingOnly();

    @Query(value = "SELECT op.product_id, p.name_heb, p.name_weight, p.packing_weight_code, SUM(op.quantity_needed) quantity_needed, p.supplier_names\n" +
            "FROM order_product op INNER JOIN orders o ON op.order_id = o.id INNER JOIN products p ON op.product_id = p.id\n" +
            "WHERE (o.status = 2 OR o.status = 5)\n" +
            "GROUP BY op.product_id\n" +
            "ORDER BY p.packing_weight_code", nativeQuery = true)
    List<Object[]> weightReportPendingOrHarvesting();

    @Query(value = "SELECT op.product_id, p.name_heb, p.name_weight, p.packing_weight_code, SUM(op.quantity_needed) quantity_needed, p.supplier_names\n" +
            "FROM order_product op INNER JOIN orders o ON op.order_id = o.id INNER JOIN products p ON op.product_id = p.id\n" +
            "WHERE o.status = 2 AND p.packing_action = '1'\n" +
            "GROUP BY op.product_id\n" +
            "ORDER BY p.packing_weight_code", nativeQuery = true)
    List<Object[]> weightReportPendingOnlyQuantization();

    @Query(value = "SELECT op.product_id, p.name_heb, p.name_weight, p.packing_weight_code, SUM(op.quantity_needed) quantity_needed, p.supplier_names\n" +
            "FROM order_product op INNER JOIN orders o ON op.order_id = o.id INNER JOIN products p ON op.product_id = p.id\n" +
            "WHERE (o.status = 2 OR o.status = 5) AND p.packing_action = '1'\n" +
            "GROUP BY op.product_id\n" +
            "ORDER BY p.packing_weight_code", nativeQuery = true)
    List<Object[]> weightReportPendingOrHarvestingQuantization();

    @Query(value = "SELECT op.product_id, p.name_heb, p.category_id, p.packing_action, p.packing_weight_code, p.supplier_names\n" +
            "FROM order_product op INNER JOIN orders o ON op.order_id = o.id INNER JOIN products p ON op.product_id = p.id\n" +
            "WHERE p.is_deleted = 0\n" +
            "GROUP BY op.product_id\n" +
            "ORDER BY p.packing_weight_code", nativeQuery = true)
    List<Object[]> weightReportAllProducts();

    @Query("FROM Product WHERE is_deleted = '0'")
    List<Product> findAll();

    @Query("FROM Product WHERE is_deleted = '0' AND status <> '0'")
    List<Product> findAllValid();

    @Query(value = "SELECT * FROM products WHERE is_deleted = 0 ORDER BY id DESC", nativeQuery = true)
    List<Product> findAllDesc();

    @Query(value = "SELECT DISTINCT p.*\n" +
            "FROM products p INNER JOIN order_product op ON p.id = op.product_id \n" +
            "    INNER JOIN orders o ON op.order_id = o.id\n" +
            "WHERE p.is_deleted = '0' AND o.status = 2 AND o.dhl_package_number IS NOT NULL\n" +
            "ORDER BY packing_weight_code", nativeQuery = true)
    List<Product> findAllInCurrentPackingOrderByWeight();

    @Query(value = "SELECT DISTINCT p.*\n" +
            "FROM products p INNER JOIN order_product op ON p.id = op.product_id \n" +
            "    INNER JOIN orders o ON op.order_id = o.id\n" +
            "WHERE p.is_deleted = '0' AND o.status = 2 AND o.dhl_package_number IS NOT NULL AND op.quantity <> op.quantity_needed AND p.is_missing = 0\n" +
            "ORDER BY packing_weight_code", nativeQuery = true)
    List<Product> findAllnotPackedOrderByWeight();

    @Query(value = "SELECT id FROM products where vat = '1' ", nativeQuery = true)
    List<Integer> findAllProductsIdsWithVAT();

}
