package com.shookbook.dao.shookbook;

import com.shookbook.entities.shookbook.OrderGeneration;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderGenerationsDao extends JpaRepository<OrderGeneration, Integer>
{
}
