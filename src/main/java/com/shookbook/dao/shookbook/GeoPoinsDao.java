package com.shookbook.dao.shookbook;

import com.shookbook.entities.shookbook.GeoPoint;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface GeoPoinsDao extends JpaRepository<GeoPoint, Integer> {
    @Query("FROM GeoPoint WHERE customer_id = :customer_id")
    List<GeoPoint> findAllByCustomerId(@Param("customer_id") int customerId);
}
