package com.shookbook.dao.shookbook;

import com.shookbook.entities.shookbook.Subscription;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SubscriptionsDao extends JpaRepository<Subscription, Integer> {
    @Query(value = "SELECT * FROM subscriptions WHERE customer_id = :id AND is_deleted = '0'", nativeQuery = true)
    List<Subscription> findAllByCustomerId(@Param("id") Integer id);
}
