package com.shookbook.dao.shookbook;

import com.shookbook.entities.shookbook.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

public interface CustomersDao extends JpaRepository<Customer, Integer>
{
    @Query(value = "SELECT * FROM customers WHERE is_deleted = '0' ORDER BY id DESC", nativeQuery = true)
    List<Customer> findAll();

    @Query(value = "SELECT * FROM customers WHERE is_deleted = '0' AND created_date > DATE_SUB(NOW(), INTERVAL 1 YEAR) ORDER BY id DESC", nativeQuery = true)
    List<Customer> findAllInYear();

    @Query(value = "SELECT * FROM customers WHERE is_deleted = '0' AND created_date > DATE_SUB(NOW(), INTERVAL 3 MONTH) ORDER BY id DESC", nativeQuery = true)
    List<Customer> findAllIn3Months();

    @Query(value = "SELECT * \n" +
            "FROM customers c INNER JOIN \n" +
            "   (SELECT customer_id FROM orders WHERE created_date > DATE_SUB(NOW(), INTERVAL 3 MONTH) GROUP BY customer_id) t ON c.id = t.customer_id \n" +
            "WHERE is_deleted = '0' ORDER BY id DESC", nativeQuery = true)
    List<Customer> findAllHadOrderIn3Months();

    @Query(value = "SELECT * \n" +
            "FROM customers c INNER JOIN \n" +
            "   (SELECT customer_id FROM orders WHERE created_date > DATE_SUB(NOW(), INTERVAL 1 MONTH) GROUP BY customer_id) t ON c.id = t.customer_id \n" +
            "WHERE is_deleted = '0' ORDER BY id DESC", nativeQuery = true)
    List<Customer> findAllHadOrderInLastMonth();

    @Query(value = "SELECT * \n" +
            "FROM customers c INNER JOIN \n" +
            "   (SELECT customer_id FROM orders GROUP BY customer_id HAVING COUNT(*) > 1) t ON c.id = t.customer_id \n" +
            "WHERE is_deleted = '0' ORDER BY id DESC", nativeQuery = true)
    List<Customer> findAllOrderMoreThan1Time();

    @Query(value = "SELECT * \n" +
            "FROM customers c INNER JOIN \n" +
            "   (SELECT customer_id FROM orders GROUP BY customer_id HAVING COUNT(*) > 5) t ON c.id = t.customer_id \n" +
            "WHERE is_deleted = '0' ORDER BY id DESC", nativeQuery = true)
    List<Customer> findAllOrderMoreThan5Times();

    @Query(value = "SELECT * \n" +
            "FROM customers c INNER JOIN \n" +
            "   (SELECT customer_id FROM orders GROUP BY customer_id HAVING COUNT(*) > 10) t ON c.id = t.customer_id \n" +
            "WHERE is_deleted = '0' ORDER BY id DESC", nativeQuery = true)
    List<Customer> findAllOrderMoreThan10Times();

    @Query(value = "SELECt * FROM customers WHERE environment_friendly = '1' AND is_deleted = '0' ORDER BY id DESC", nativeQuery = true)
    List<Customer> findAllEnvFriendly();

    public final static String[] customerFilters = new String[]{"הכל", "שנה", "3 חודשים", "הזמינו ב 3 חודשים האחרונים", "הזמינו בחודש האחרון", "הזמינו יותר מפעם אחת", "הזמינו יותר 5 פעמים", "הזמינו יותר 10 פעמים"};




//    @Query(value = "SELECT * FROM orders WHERE created_date > DATE_SUB(NOW(), INTERVAL 3 MONTH) ORDER BY id DESC", nativeQuery = true)


    @Query("FROM Customer WHERE is_deleted = '0' AND barcode IS NULL ORDER BY id DESC")
    List<Customer> findAllWithoutBarcode();

    @Query("FROM Customer WHERE created_date BETWEEN :from_date AND :to_date")
    List<Customer> findAllBetweenDates(@Param("from_date") Date fromDate, @Param("to_date") Date toDate);

    @Query("FROM Customer WHERE created_date BETWEEN :from_date AND :to_date AND attention_flag > 0 AND is_deleted = 0")
    List<Customer> findAllBetweenDatesWithAttentionFlag(@Param("from_date") Date fromDate, @Param("to_date") Date toDate);

    Customer findOneByBarcode(String customerId);

    @Query(value = "SELECT * FROM customers WHERE wp_id = :customer_wp_id", nativeQuery = true)
    Customer findOneByWpID(@Param("customer_wp_id") Integer customer_wp_id);

    @Query(value = "SELECT * FROM customers WHERE email = :customer_email AND is_deleted = 0 LIMIT 1", nativeQuery = true)
    Customer findOneByEmail(@Param("customer_email") String customer_email);

    @Query(value = "SELECT SUM(sum) FROM orders WHERE customer_id = :customer_id AND clearing_status = 1 AND created_date > :start_date", nativeQuery = true)
    Integer calcOrdersSum(@Param("customer_id") Integer customerId, @Param("start_date") Date startDate);

    @Query(value = "SELECT id FROM customers WHERE barcode = :customer_barcode", nativeQuery = true)
    Integer getCustomerIdByBarcode(@Param("customer_barcode") String customer_barcode);

    @Query(value = "SELECT customer_id, COUNT(customer_id) orders FROM orders GROUP BY customer_id", nativeQuery = true)
    List<Object[]> findAllOrderCount();

    @Query(value = "SELECT customer_id, COUNT(customer_id) orders FROM orders WHERE customer_id IN (:customer_ids) GROUP BY customer_id", nativeQuery = true)
    List<Object[]> findAllOrderCountByCustomerIds(@Param("customer_ids") Iterable<Integer> customerIds);

    @Query(value = "SELECT customer_id, phone, email FROM csv_customers WHERE customer_id= :customer_id", nativeQuery = true)
    List <Object[]> findOneCardcomCustomer(@Param("customer_id") Integer customerId);

    @Query(value = "SELECT c.id, c.wp_id, c.first_name, c.last_name, c.email, sum(o.sum) order_sum FROM orders o INNER JOIN customers c ON o.customer_id = c.id WHERE o.status = 3 AND o.clearing_status = 1 AND o.delivery_date >= :from_year AND o.delivery_date < :to_year GROUP BY customer_id", nativeQuery = true)
    List <Object[]> findAllWithStatusBetweenYears(@Param("from_year") String from_year, @Param("to_year") String to_year);

    @Query(value = "SELECT * FROM customers WHERE remarks IS NULL AND ((coupon_group = 0) OR (force_geotag = 0)) AND \n" +
                   "((city = 'מדרשת בן גוריון') OR (city = 'מדרשת בן-גוריון'))", nativeQuery = true)
    List <Customer> findAllMidrasha();

    @Modifying
    @Transactional
    @Query(value = "UPDATE customers SET type = '0'", nativeQuery = true)
    void clearAllType();
}
