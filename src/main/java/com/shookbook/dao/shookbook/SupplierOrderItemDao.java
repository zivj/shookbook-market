package com.shookbook.dao.shookbook;

import com.shookbook.entities.shookbook.SupplierOrderItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SupplierOrderItemDao extends JpaRepository<SupplierOrderItem, Integer>
{
    @Query(value = "SELECT soi.*\n" +
            "FROM supplier_order_item soi\n" +
            "INNER JOIN items i ON soi.item_id = i.id\n" +
            "INNER JOIN products p ON i.product_id = p.id\n" +
            "WHERE soi.supplier_order_id = :order_id ORDER BY p.id", nativeQuery = true)
    List<SupplierOrderItem> findAllByOrderId(@Param("order_id") int orderId);

    @Query(value = "SELECT SUM(quantity)\n" +
            "FROM supplier_order_item \n" +
            "WHERE item_id = :item_id \n ", nativeQuery = true )
    Integer getOrderedItemQuantity(@Param("item_id") Integer item_id);

}
