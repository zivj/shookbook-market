package com.shookbook.dao.shookbook;


import com.shookbook.entities.shookbook.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ItemsDao extends JpaRepository<Item, Integer>
{

    @Query("FROM Item WHERE is_deleted = '0'")
    List<Item> findAll();

    @Query("FROM Item WHERE is_deleted = '0' ORDER BY product_id")
    List<Item> findAllByProductId();

    @Query("FROM Item WHERE is_deleted = '0' ORDER BY product_id DESC")
    List<Item> findAllByProductIdDesc();

    @Query("FROM Item WHERE is_deleted = '0' AND product_id = :product_id")
    List<Item> findAllByProductIdWithParam(@Param("product_id") Integer product_id);

    @Query("FROM Item WHERE is_deleted = '0' AND product_id = :product_id AND inventory > 0")
    List<Item> findAllByProductIdWithInventory(@Param("product_id") Integer product_id);

    @Query(value = "SELECT * FROM items WHERE is_deleted = '0' AND product_id = :product_id AND inventory > 0 ORDER BY inventory", nativeQuery = true)
    List<Item> findAllByProductIdWithInventorySortedByInventory(@Param("product_id") Integer product_id);

    @Query(value = "SELECT * FROM items WHERE is_deleted = '0' AND product_id = :product_id AND inventory >= :quantity", nativeQuery = true)
    List<Item> findAllByProductIdWithQuantity(@Param("product_id") Integer product_id, @Param("quantity") Float quantity);

    @Query(value = "SELECT * FROM items WHERE is_deleted = '0' AND product_id IN (:product_ids) ORDER BY product_id", nativeQuery = true)
    List<Item> findAllByProductIds(@Param("product_ids") Iterable<Integer> productIds);

    @Query(value = "SELECT DISTINCT i.*\n" +
            "FROM items i INNER JOIN order_product op ON i.id = op.item_id INNER JOIN orders o ON op.order_id = o.id INNER JOIN products p ON i.product_id = p.id\n" +
            "WHERE o.status = 2\n" +
            "ORDER BY p.packing_weight_code, i.id;", nativeQuery = true)
    List<Item> findAllByOrderStatus2();

    @Query(value = "SELECT * FROM items WHERE is_deleted = '0' AND barcode = :barcode LIMIT 1", nativeQuery = true)
    Item findOneByBarcode(@Param("barcode") String barcode);

    @Modifying
    @Transactional
    @Query("DELETE FROM Item WHERE supplier_id = :supplier_id")
    Integer remove(@Param("supplier_id") Integer supplier_id);


    @Query("SELECT id FROM Supplier")
    List<Integer> getAllSuppliers();

    @Query(value = "SELECT COUNT(id) FROM items WHERE supplier_id = :supplier_id  group by supplier_id " , nativeQuery = true)
    Integer count(@Param("supplier_id") Integer supplier_id);

    //Added by Mor 21/4 8:36
    @Query("FROM Item WHERE supplier_id = :p_supplier_id")
    List<Item> findAllBySuppliersId(@Param("p_supplier_id") int id);

    //Added by Mor 15/7 9:22
    @Query("FROM Item WHERE is_deleted = '0' AND inventory < 50")
    List<Item> findAllByInventory();

    @Query(value = "SELECT * FROM items \n"+
            "WHERE product_id = :product_id \n"+
            "AND is_deleted = '0'"
            , nativeQuery = true)
    List<Item> findAllItemByProductId(@Param("product_id") Integer product_id);

}
