package com.shookbook.dao.shookbook;

import com.shookbook.entities.shookbook.MobileGroup;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MobileGroupsDao extends JpaRepository<MobileGroup, Integer> {
}
