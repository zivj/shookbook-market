package com.shookbook.dao.shookbook;

import com.shookbook.entities.shookbook.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface OrdersDao extends JpaRepository<Order, Integer>
{
    @Query(value = "SELECT * FROM orders WHERE wp_id = :order_wp_id AND type < 80", nativeQuery = true)
    Order findOneByWpID(@Param("order_wp_id") Integer order_wp_id);

    @Query(value = "SELECT * FROM orders WHERE barcode LIKE :barcode AND is_deleted = '0'", nativeQuery = true)
    Order findOneByBarcode(@Param("barcode") String barcode);

    @Query("FROM Order WHERE id BETWEEN :from_id AND :to_id AND is_deleted = '0'")
    List<Order> findAllBetweenIds(@Param("from_id") int fromId, @Param("to_id") int toId);

    @Query("FROM Order WHERE start_packing_timestamp BETWEEN :from_date AND :to_date")
    List<Order> findAllBetweenDates(@Param("from_date") Date fromDate, @Param("to_date") Date toDate);

    @Query(value="SELECT DISTINCT customer_id FROM orders WHERE delivery_date BETWEEN :from_date AND :to_date AND (status = 3 OR status = 7 OR status = 0)", nativeQuery = true)
    List<Integer> findAllCustomerIdsDeliveredBetweenDates(@Param("from_date") Date fromDate, @Param("to_date") Date toDate);

    @Query("FROM Order WHERE DATE(last_packing_timestamp) = DATE(NOW())")
    List<Order> findAllBetweenDatesRealTime();

    @Query("FROM Order WHERE delivery_date BETWEEN :from_date AND :to_date")
    List<Order> findAllDeliveredBetweenDatesGroupByCustomerId(@Param("from_date") Date fromDate, @Param("to_date") Date toDate);

    @Query("FROM Order WHERE start_packing_timestamp BETWEEN :from_date AND :to_date AND last_packing_timestamp IS NULL")
    List<Order> findAllPackingNow(@Param("from_date") Date fromDate, @Param("to_date") Date toDate);

    @Query("FROM Order WHERE start_packing_timestamp BETWEEN :from_date AND :to_date AND last_packing_timestamp IS NOT NULL ORDER BY dhl_package_number")
    List<Order> findAllPackingDone(@Param("from_date") Date fromDate, @Param("to_date") Date toDate);

    @Query("FROM Order WHERE ((status = 2) OR (start_packing_timestamp BETWEEN :from_date AND :to_date AND last_packing_timestamp IS NOT NULL)) AND dhl_package_number LIKE :palette ORDER BY status, dhl_package_number")
    List<Order> findAllPalettePackingDone(@Param("palette") String palette, @Param("from_date") Date fromDate, @Param("to_date") Date toDate);

    @Query("FROM Order WHERE customer_id = :customer_id")
    List<Order> findAllByCustomerId(@Param("customer_id") int customerId);

    @Query("FROM Order WHERE customer_id = :customer_id AND delivery_date BETWEEN :from_date AND :to_date")
    List<Order> findAllByCustomerIdWithDates(@Param("customer_id") int customer_id, @Param("from_date") Date from_date, @Param("to_date") Date to_date);

    @Query("FROM Order WHERE customer_id = :customer_id ORDER BY created_date DESC")
    List<Order> findAllByCustomerIdDesc(@Param("customer_id") int customerId);

    @Query("FROM Order WHERE customer_id = :customer_id AND status = 3 ORDER BY created_date DESC")
    List<Order> findAllDeliveredByCustomerIdDesc(@Param("customer_id") int customerId);

    @Query("FROM Order WHERE (status = '3' OR status = '7' OR status = '0') AND clearing_status = '0' AND certificated = '1' AND last_packing_timestamp BETWEEN :from_date AND :to_date")
    List<Order> findAllClearable(@Param("from_date") Date fromDate, @Param("to_date") Date toDate);

    @Query("FROM Order WHERE (status = '3' OR status = '7' OR status = '0') AND clearing_status = '0' AND certificated = '1' ORDER BY id DESC")
    List<Order> findAllClearableToDate();

    @Query("FROM Order WHERE (status = '3' OR status = '7' OR status = '0') AND clearing_status = '2' AND certificated = '1' AND id > 7442 ORDER BY id DESC")
    List<Order> findAllNotCleared();

    @Query("FROM Order WHERE is_deleted = '0'")
    List<Order> findAll();

    @Query("FROM Order WHERE is_deleted = '0' AND status = '1'")
    List<Order> findAllOpen();

    @Query("FROM Order WHERE is_deleted = '0' AND status = '1' AND delivery_date = :date")
    List<Order> findAllOpenWithDeliveryDate(@Param("date") Date date);

    @Query(value = "SELECT * FROM orders WHERE is_deleted = '0' AND status = :status AND delivery_date = :date \n" +
                   "ORDER BY  CASE WHEN :order_by = 'id' THEN id END DESC, \n" +
                   "          CASE WHEN :order_by = 'dhl_package_number' THEN delivery_date END, dhl_package_number, \n" +
                   "          CASE WHEN :order_by = 'status' THEN status END", nativeQuery = true)
    List<Order> findAllWithStatusWithDeliveryDateOrderByParam(@Param("status") Integer status, @Param("date") Date date, @Param("order_by") String order_by);

    @Query(value = "SELECT * FROM orders WHERE is_deleted = '0' AND status = :status AND delivery_date = :date \n" +
            "AND ((dhl_package_number NOT LIKE 'HH%') AND (dhl_package_number NOT LIKE 'RR%') AND (dhl_package_number NOT LIKE 'ZZ%')) \n" +
            "ORDER BY  CASE WHEN :order_by = 'id' THEN id END DESC, \n" +
            "          CASE WHEN :order_by = 'dhl_package_number' THEN delivery_date END,  dhl_package_number, \n" +
            "          CASE WHEN :order_by = 'status' THEN status END", nativeQuery = true)
    List<Order> findFilteredWithStatusWithDeliveryDateOrderByParam(@Param("status") Integer status, @Param("date") Date date, @Param("order_by") String order_by);

    @Query(value = "SELECT * FROM orders WHERE is_deleted = '0' AND status = :status AND delivery_date BETWEEN :from_date AND :to_date  \n" +
            "ORDER BY  CASE WHEN :order_by = 'id' THEN id END DESC, \n" +
            "          CASE WHEN :order_by = 'dhl_package_number' THEN delivery_date END, dhl_package_number, \n" +
            "          CASE WHEN :order_by = 'status' THEN status END", nativeQuery = true)
    List<Order> findAllWithStatusBetweenDeliveryDateOrderByParam(@Param("status") Integer status, @Param("from_date") Date from_date, @Param("to_date") Date to_date, @Param("order_by") String order_by);

    @Query(value = "SELECT * FROM orders WHERE is_deleted = '0' AND status = :status AND delivery_date BETWEEN :from_date AND :to_date  \n" +
            "AND ((dhl_package_number NOT LIKE 'HH%') AND (dhl_package_number NOT LIKE 'RR%') AND (dhl_package_number NOT LIKE 'ZZ%')) \n" +
            "ORDER BY  CASE WHEN :order_by = 'id' THEN id END DESC, \n" +
            "          CASE WHEN :order_by = 'dhl_package_number' THEN delivery_date END, dhl_package_number, \n" +
            "          CASE WHEN :order_by = 'status' THEN status END", nativeQuery = true)
    List<Order> findFilteredWithStatusBetweenDeliveryDateOrderByParam(@Param("status") Integer status, @Param("from_date") Date from_date, @Param("to_date") Date to_date, @Param("order_by") String order_by);

    @Query(value = "SELECT * FROM orders WHERE is_deleted = '0' AND delivery_date BETWEEN :from_date AND :to_date  \n" +
            "ORDER BY  CASE WHEN :order_by = 'id' THEN id END DESC, \n" +
            "          CASE WHEN :order_by = 'dhl_package_number' THEN dhl_package_number END, \n" +
            "          CASE WHEN :order_by = 'status' THEN status END", nativeQuery = true)
    List<Order> findAllBetweenDeliveryDateOrderByParam(@Param("from_date") Date from_date, @Param("to_date") Date to_date, @Param("order_by") String order_by);

    @Query(value = "SELECT * FROM orders o INNER JOIN customers c ON o.customer_id = c.id \n" +
            "WHERE o.is_deleted = '0' AND o.delivery_date BETWEEN :from_date AND :to_date AND c.attention_flag > 0 AND (o.status = 1 OR o.status = 2 OR o.status = 5) \n" +
            "ORDER BY  CASE WHEN :order_by = 'id' THEN o.id END DESC, \n" +
            "          CASE WHEN :order_by = 'dhl_package_number' THEN o.dhl_package_number END, \n" +
            "          CASE WHEN :order_by = 'status' THEN o.status END", nativeQuery = true)
    List<Order> findAllBetweenDeliveryDateWithCustomerAttentionOrderByParam(@Param("from_date") Date from_date, @Param("to_date") Date to_date, @Param("order_by") String order_by);


    @Query("FROM Order WHERE is_deleted = '0' AND status = '1' AND (delivery_date <= :delivery_date OR delivery_date IS NULL)")
    List<Order> findAllOpenLessThanDate(@Param("delivery_date") Date deliveryDate);

    @Query(value = "SELECT * FROM orders WHERE certificated = '0' AND (status = '3' OR status = '7')", nativeQuery = true)
    List<Order> findAllNotCertificated();

    @Query(value = "SELECT * FROM orders ORDER BY id DESC", nativeQuery = true)
    List<Order> findAllOrderByIdDesc();

    @Query(value = "SELECT * FROM orders WHERE created_date > DATE_SUB(NOW(), INTERVAL 1 YEAR) ORDER BY id DESC", nativeQuery = true)
    List<Order> findAllOrderLastYearByIdDesc();

    @Query(value = "SELECT * FROM orders WHERE created_date > DATE_SUB(NOW(), INTERVAL 3 MONTH) ORDER BY id DESC", nativeQuery = true)
    List<Order> findAllOrderLast3MonthsByIdDesc();

    @Query(value = "SELECT * FROM orders WHERE created_date > DATE_SUB(NOW(), INTERVAL 1 MONTH) ORDER BY id DESC", nativeQuery = true)
    List<Order> findAllOrderLastMonthByIdDesc();

    @Query(value = "SELECT * FROM orders WHERE status = :status ORDER BY id DESC", nativeQuery = true)
    List<Order> findAllOrderByStatusByIdDesc(@Param("status") Integer status);

    // @Query(value = "SELECT * FROM orders WHERE (status = 3 OR status = 7) AND delivery_date BETWEEN CURDATE() AND DATE_ADD(CURDATE(), INTERVAL 1 DAY) ORDER BY dhl_package_number", nativeQuery = true)
    @Query(value = "SELECT * FROM orders WHERE (status = 3 OR status = 7) AND ((delivery_date  = CURDATE() AND delivery_window = 0) OR (delivery_date = DATE_ADD(CURDATE(), INTERVAL 1 DAY) AND delivery_window = 1)) ORDER BY dhl_package_number", nativeQuery = true)
    List<Order> findAllCurrentAssignmentsByHdlPackageNumber();

    @Query(value = "SELECT LEFT(dhl_package_number,2) prefix, count(id) FROM orders WHERE (status = 3 OR status = 7) AND delivery_date BETWEEN CURDATE() AND DATE_ADD(CURDATE(), INTERVAL 1 DAY) GROUP BY prefix", nativeQuery = true)
    List<Object[]> findAllCurrentAssignmentsGroupByHdlPackageNumber();

    @Query(value = "SELECT * FROM orders WHERE status = :status AND delivery_date BETWEEN :from_date AND :to_date ORDER BY id DESC", nativeQuery = true)
    List<Order> findAllOrderByStatusBetweenDatesByIdDesc(@Param("status") Integer status, @Param("from_date") Date fromDate, @Param("to_date") Date toDate);

    @Query("FROM Order WHERE status = '2' AND dhl_package_number IS NULL")
    List<Order> findAllPendingOrder();

    @Query("FROM Order WHERE status = '5' AND is_deleted = 0")
    List<Order> findAllHarvestingOrder();

    @Query(value = "SELECT * FROM orders WHERE status = :status AND is_deleted = 0 AND delivery_window = :delivery_window", nativeQuery = true)
    List<Order> findAllHarvestingOrderByDeliveryWindow(@Param("delivery_window") Integer delivery_window, @Param("status") Integer status);

    @Query(value = "SELECT * FROM orders WHERE (status = '2' OR status = '3') AND delivery_window = :delivery_window AND delivery_date = :date AND ((certificated = '0' AND type = '0') OR (type <> '0')) AND LENGTH(dhl_package_number) > 0 ORDER BY dhl_package_number", nativeQuery = true)
    List<Order> findAllPendingOrderByDeliveryWindowReadyToDhlOrdered(@Param("delivery_window") Integer delivery_window, @Param("date") Date date);

    @Query("FROM Order WHERE status = '2' AND certificated = '0' AND LENGTH(dhl_package_number) > 0 ORDER BY dhl_package_number ASC")
    List<Order> findAllPendingOrderReadyToDhlOrdered();

    @Query("FROM Order WHERE status = '2' AND certificated = '0' AND LENGTH(dhl_package_number) > 0 ORDER BY dhl_package_number DESC")
    List<Order> findAllPendingOrderReadyToDhlOrderedDesc();

    @Query("FROM Order WHERE status = '2' AND delivery_window = '1' AND certificated = '0' AND LENGTH(dhl_package_number) > 0 ORDER BY dhl_package_number")
    List<Order> findAllEveningPendingOrderReadyToDhlOrdered();

    @Query("FROM Order WHERE status = '2' AND delivery_window = '0' AND certificated = '0' AND LENGTH(dhl_package_number) > 0 ORDER BY dhl_package_number DESC")
    List<Order> findAllMorningPendingOrderReadyToDhlOrderedDesc();

    @Query(value = "SELECT MID(dhl_package_number, 1, 2) prefix FROM orders WHERE status = '2' AND delivery_window = :delivery_window AND certificated = '0' AND LENGTH(dhl_package_number) > 0 GROUP BY prefix", nativeQuery = true)
    List<String> findAllPendingOrderPrifix(@Param("delivery_window") Integer delivery_window);

    @Query(value = "SELECT * FROM orders WHERE status = '2' AND certificated = '0' AND dhl_package_number LIKE :prefix ORDER BY dhl_package_number DESC", nativeQuery = true)
    List<Order> findAllPendingOrderByPrefixDesc(@Param("prefix") String prefix);

    @Query(value = "SELECT * FROM orders WHERE status = '2' AND certificated = '0' AND dhl_package_number LIKE :prefix ORDER BY dhl_package_number", nativeQuery = true)
    List<Order> findAllPendingOrderByPrefix(@Param("prefix") String prefix);

    @Query(value = "SELECT * FROM orders WHERE status <> '4' AND delivery_date = :date AND dhl_package_number LIKE :prefix ORDER BY dhl_package_number DESC", nativeQuery = true)
    List<Order> findAllDeliveredOrderByPrefixDesc(@Param("prefix") String prefix, @Param("date") Date date);

    @Query(value = "SELECT o.*\n" +
            "FROM orders o\n" +
            "INNER JOIN order_product op ON o.id = op.order_id\n" +
            "WHERE op.product_id = :product_id\n" +
            "AND o.created_date BETWEEN  :from_date AND :to_date\n" +
            "ORDER BY product_id", nativeQuery = true)
    List<Order> findAllBetweenDatesAndProductId(@Param("from_date") Date fromDate, @Param("to_date") Date toDate,@Param("product_id") int product_id);


    @Query(value = "SELECT *\n" +
            "FROM orders o\n" +
            "WHERE customer_id = :customer_id\n" +
            "AND created_date BETWEEN  :from_date AND :to_date\n" +
            "AND clearing_status LIKE '1'\n" +
            "ORDER BY created_date", nativeQuery = true)
    List<Order> findAllBetweenDatesAndCustomerId(@Param("from_date") Date fromDate, @Param("to_date") Date toDate, @Param("customer_id") int customer_id);


    @Query(value = "SELECT EXTRACT(DAY FROM start_packing_timestamp).m, SUM(sum)\n" +
            "FROM orders \n" +
            "WHERE customer_id = :customer_id\n" +
            "GROUP BY m", nativeQuery = true)
    List<Order> findAllPurchaseByCustomerId(@Param("customer_id") int customer_id);

    @Query(value = "SELECT status, COUNT(*)\n" +
            "FROM orders\n" +
            "GROUP BY status\n" +
            "ORDER BY status;", nativeQuery = true)
    List<Integer[]> findAllStatusCounts();

    @Query(value = "SELECT delivery_date, COUNT(*)\n" +
            "FROM orders\n" +
            "WHERE status = 1\n" +
            "GROUP BY delivery_date\n" +
            "ORDER BY delivery_date;", nativeQuery = true)
    List<String[]> findAllOpenDeliveryDateCounts();

    @Query(value = "SELECT delivery_date, COUNT(*)\n" +
            "FROM orders\n" +
            "WHERE status = 1 AND delivery_window = 0\n" +
            "GROUP BY delivery_date\n" +
            "ORDER BY delivery_date;", nativeQuery = true)
    List<String[]> findAllOpenDeliveryDateMorningCounts();

    @Query(value = "SELECT delivery_date, COUNT(*)\n" +
            "FROM orders\n" +
            "WHERE status = 1 AND delivery_window = 1\n" +
            "GROUP BY delivery_date\n" +
            "ORDER BY delivery_date;", nativeQuery = true)
    List<String[]> findAllOpenDeliveryDateEveningCounts();

    @Query(value = "SELECT count(*) FROM orders WHERE customer_id = :customer_id", nativeQuery = true)
    Integer getCustomerIdByBarcode(@Param("customer_id") int customer_id);

    @Query(value = "SELECT * FROM orders WHERE customer_id = :customer_id ORDER BY created_date DESC LIMIT 1", nativeQuery = true)
    Order findOneByCustomerIdDesc(@Param("customer_id") int customer_id);

    @Query(value = "SELECT * FROM orders WHERE status = 1 and customer_id IN (SELECT customer_id FROM orders WHERE status = 1 GROUP BY customer_id HAVING COUNT(customer_id) > 1)", nativeQuery = true)
    List<Order> findAllOpenWithSameCustomerId();

    @Query(value = "SELECT * FROM orders WHERE (status = 2 OR status = 5) and customer_id IN (SELECT customer_id FROM orders WHERE (status = 2 OR status = 5) GROUP BY customer_id HAVING COUNT(customer_id) > 1)", nativeQuery = true)
    List<Order> findAllPendingWithSameCustomerId();

    @Query(value = "SELECT customer_id, max(delivery_date) FROM orders GROUP BY customer_id", nativeQuery = true)
    List<Object[]> findLastDeliveredWithSameCustomerId();

    @Query(value = "SELECT delivery_date FROM orders WHERE (status = 2 OR status = 5) AND is_deleted = 0 ORDER BY delivery_date LIMIT 1", nativeQuery = true)
    Date findLastDeliveryDateByHarvesting();

    @Query(value = "SELECT * FROM orders WHERE (status = 5 AND delivery_date < DATE(NOW()+ INTERVAL 1 DAY)) OR (status = 2 and dhl_package_number IS NULL)", nativeQuery = true)
    List<Order> findAllDhlError();

    @Query(value = "select o.id, csv.invoice_number from orders o inner join customers c on o.customer_id = c.id inner join csv csv on csv.email = c.email  where o.status = 3 and o.clearing_invoice_number is null and o.sum = csv.sum", nativeQuery = true)
    List<Object[]> getOrders();

    @Query(value = "SELECT o.* FROM orders o INNER JOIN customers c ON c.id = o.customer_id WHERE c.attention_flag > 0 AND (o.status = 1 OR o.status = 2 OR o.status = 5)", nativeQuery = true)
    List<Order> findAllNeedAttention();

    @Query("FROM Order WHERE (status = '3' or status = '7') AND clearing_status = '6' AND certificated = '1' ORDER BY id DESC")
    List<Order> findAllCreditCheck();

    @Query(value = "SELECT * FROM orders \n" +
                   "WHERE status = 3 AND sms_code < 2 AND delivery_date = DATE_ADD(CURDATE(), INTERVAL :window DAY) AND delivery_window = :window \n" +
                   "AND estimated_delivery_time >= :time  AND estimated_delivery_time < ADDTIME(:time, :interval) \n" +
                   "AND dhl_package_number LIKE :prefix \n" +
                   "ORDER BY dhl_package_number", nativeQuery = true)
    List<Order> findNextOrdersToDeliver(@Param("prefix") String prefix, @Param("time") String time, @Param("interval") String interval, @Param("window") Integer window);

    @Query(value = "SELECT * FROM orders \n" +
            "WHERE status = 3 AND sms_code < 2 AND delivery_date = DATE_ADD(CURDATE(), INTERVAL :window DAY) AND delivery_window = :window \n" +
            "AND estimated_delivery_time <= :time  AND estimated_delivery_time > SUBTIME(:time, :interval) \n" +
            "AND dhl_package_number LIKE :prefix \n" +
            "ORDER BY dhl_package_number", nativeQuery = true)
    List<Order> findNextOrdersToDeliverSub(@Param("prefix") String prefix, @Param("time") String time, @Param("interval") String interval, @Param("window") Integer window);

    @Query(value = "SELECT * FROM orders \n" +
            "WHERE status = 3 AND sms_code < 2 AND delivery_date = CURDATE() AND delivery_window = 0 \n" +
            "AND estimated_delivery_time > :time  AND estimated_delivery_time < ADDTIME(:time, :interval) \n" +
            "AND dhl_package_number LIKE :prefix \n" +
            "ORDER BY dhl_package_number", nativeQuery = true)
    List<Order> findNextMorningOrdersToDeliver(@Param("prefix") String prefix, @Param("time") String time, @Param("interval") String interval);

    @Query(value = "SELECT * FROM orders \n" +
            "WHERE status = 3 AND sms_code < 2 AND delivery_date = CURDATE() AND dhl_package_number LIKE :prefix \n" +
            "AND estimated_delivery_time > :time  AND estimated_delivery_time < ADDTIME(:time, :interval) \n" +
            "ORDER BY dhl_package_number", nativeQuery = true)
    List<Order> findNextttOrdersToDeliver(@Param("prefix") String prefix, @Param("time") String time, @Param("interval") String interval);

    @Query(value = "SELECT SUM(sum), COUNT(sum) FROM orders WHERE customer_id = :customer_id AND delivery_date BETWEEN :from_date AND :to_date", nativeQuery = true)
    List<Object[]> getSumByCustomerIdBetweenDates(@Param("customer_id") Integer customerId, @Param("from_date") Date fromDate, @Param("to_date") Date toDate);
}
