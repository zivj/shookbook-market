package com.shookbook.dao.shookbook;

import com.shookbook.entities.shookbook.MobileUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MobileUsersDao extends JpaRepository<MobileUser, Integer> {
    @Query(value = "SELECT * FROM mobile_users WHERE is_deleted = '0' AND group_id = :group", nativeQuery = true)
    List<MobileUser> findAllByGroup(@Param("group") Integer group);
}
