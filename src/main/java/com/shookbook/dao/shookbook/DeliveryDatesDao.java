package com.shookbook.dao.shookbook;

import com.shookbook.entities.shookbook.DeliveryDate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface DeliveryDatesDao extends JpaRepository<DeliveryDate, Integer>
{
    @Query(value = "SELECT date FROM delivery_dates WHERE date > :delivery_date LIMIT 3", nativeQuery = true)
    List <Date> findNextThree(@Param("delivery_date") Date delivery_date);
}
