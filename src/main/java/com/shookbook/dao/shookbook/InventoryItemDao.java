package com.shookbook.dao.shookbook;

import com.shookbook.entities.shookbook.InventoryItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface InventoryItemDao extends JpaRepository<InventoryItem, Integer>
{

    @Query(value = "SELECT * FROM inventory_item WHERE inventory_id = :inventory_id", nativeQuery = true)
    List<InventoryItem> findAllByInventoryId(@Param("inventory_id") Integer inventory_id);

    @Query(value = "SELECT * FROM inventory_item WHERE item_id = :item_id", nativeQuery = true)
    List<InventoryItem> findAllByItemId(@Param("item_id") Integer item_id);

    @Query(value = "SELECT item_id FROM inventory_item WHERE inventory_id = :inventory_id GROUP BY item_id", nativeQuery = true)
    List<Integer> findAllItemIdByInventoryIdGroupByItemId(@Param("inventory_id") Integer inventory_id);

    @Query(value = "SELECT sum(ordered) FROM inventory_item WHERE inventory_id = :inventory_id", nativeQuery = true)
    Float sumOrderedByInventoryId(@Param("inventory_id") Integer inventory_id);

    @Query(value = "SELECT sum(received) FROM inventory_item WHERE inventory_id = :inventory_id", nativeQuery = true)
    Float sumReceivedByInventoryId(@Param("inventory_id") Integer inventory_id);

    @Query(value = "SELECT sum(delivered) FROM inventory_item WHERE inventory_id = :inventory_id", nativeQuery = true)
    Float sumDeliveredByInventoryId(@Param("inventory_id") Integer inventory_id);

    @Query(value = "SELECT sum(ordered) FROM inventory_item WHERE item_id = :item_id", nativeQuery = true)
    Float sumOrderedByItemId(@Param("item_id") Integer item_id);

    @Query(value = "SELECT sum(received) FROM inventory_item WHERE item_id = :item_id", nativeQuery = true)
    Float sumReceivedByItemId(@Param("item_id") Integer item_id);

    @Query(value = "SELECT sum(delivered) FROM inventory_item WHERE item_id = :item_id", nativeQuery = true)
    Float sumDeliveredByItemId(@Param("item_id") Integer item_id);

    @Query(value = "SELECT * FROM inventory_item WHERE inventory_id = :inventory_id", nativeQuery = true)
    InventoryItem findOneByInventoryID(@Param("inventory_id") Integer inventory_id);

    @Query(value = "SELECT * FROM inventory_item WHERE item_id = :item_id ORDER BY created_date DESC LIMIT 1", nativeQuery = true)
    InventoryItem findLastByItemID(@Param("item_id") Integer item_id);
}
