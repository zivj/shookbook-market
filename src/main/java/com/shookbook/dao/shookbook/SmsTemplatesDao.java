package com.shookbook.dao.shookbook;

import com.shookbook.entities.shookbook.SmsTemplate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface SmsTemplatesDao extends JpaRepository<SmsTemplate, Integer>
{
    @Query(value = "SELECT * FROM sms_templates st INNER JOIN city_groups cg ON st.city_group_id = cg.id INNER JOIN cities c ON c.city_group_id = cg.id \n"+
                   "WHERE c.name = :city AND st.type = :type LIMIT 1", nativeQuery = true)
    SmsTemplate findOneByCityAndType(@Param("city") String city, @Param("type") Integer type);

    @Query(value = "SELECT * FROM sms_templates st INNER JOIN city_groups cg ON st.city_group_id = cg.id INNER JOIN cities c ON c.city_group_id = cg.id \n"+
                   "WHERE c.name = :city AND st.type = :type AND st.coupon_group = :coupon_group LIMIT 1", nativeQuery = true)
    SmsTemplate findOneByCityAndTypeAndCouponGroup(@Param("city") String city, @Param("type") Integer type, @Param("coupon_group") Integer coupon_group);
    @Query(value = "SELECT * FROM sms_templates WHERE city_group_id = 0 AND type = :type LIMIT 1", nativeQuery = true)
    SmsTemplate findOneByType(@Param("type") Integer type);
}
