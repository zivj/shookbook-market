package com.shookbook.dao.wordpress;

import com.shookbook.entities.wordpress.WpUsermeta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface WpUsermetaDao extends JpaRepository<WpUsermeta, Long>
{
    @Query("FROM WpUsermeta WHERE user_id = :user_id AND meta_key = :meta_key")
    WpUsermeta findOneByIdAndKey(@Param("user_id") Long userId, @Param("meta_key") String metaKey);

    @Transactional
    @Modifying
    @Query(value = "UPDATE wp_usermeta SET meta_value = :meta_value WHERE user_id = :user_id AND meta_key = :meta_key", nativeQuery = true)
    void updateByIdAndKey(@Param("user_id") Long userId, @Param("meta_key") String metaKey, @Param("meta_value") String metaValue);

    @Query(value = "SELECT * FROM wp_usermeta WHERE meta_key = 'app_session_token'", nativeQuery = true)
    List<WpUsermeta> findAllAppUsers();
}
