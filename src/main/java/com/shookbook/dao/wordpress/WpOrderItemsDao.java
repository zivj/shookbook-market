package com.shookbook.dao.wordpress;

import com.shookbook.entities.wordpress.WpOrderItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface WpOrderItemsDao extends JpaRepository<WpOrderItem, Long> {
    @Query(value = "SELECT * FROM wp_woocommerce_order_items WHERE order_item_name = :order_item_name", nativeQuery = true)
    List<WpOrderItem> findAllByTitle(@Param("order_item_name") String order_item_name);

    @Query(value = "SELECT * FROM wp_woocommerce_order_items WHERE order_id = :order_id AND order_item_type = :key", nativeQuery = true)
    List<WpOrderItem> findAllByOrderIdAndKey(@Param("order_id") Long order_id, @Param("key") String key);

}
