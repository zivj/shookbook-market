package com.shookbook.dao.wordpress;

import com.shookbook.entities.wordpress.WpUser;

import org.springframework.data.jpa.repository.JpaRepository;

public interface WpUsersDao extends JpaRepository<WpUser, Long>
{
}
