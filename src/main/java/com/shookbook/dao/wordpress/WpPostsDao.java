package com.shookbook.dao.wordpress;

import com.shookbook.entities.wordpress.WpPost;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface WpPostsDao extends JpaRepository<WpPost, Long>
{
    @Query(value = "SELECT * FROM wp_posts wpp WHERE wpp.post_type = 'shop_coupon' ORDER BY wpp.ID DESC LIMIT 1", nativeQuery = true)
    WpPost lastCouponTitle();

    @Query(value = "SELECT * FROM wp_posts WHERE post_title = :title LIMIT 1", nativeQuery = true)
    WpPost findPostIdByTitle(@Param("title") String title);

    @Query(value = "SELECT wpm.meta_value, wp.post_title FROM wp_posts wp INNER JOIN wp_postmeta wpm ON wp.id = wpm.post_id WHERE wp.post_title LIKE :title AND wp.post_type = :type AND wpm.meta_key = '_used_by'", nativeQuery = true)
    List <Object[]> findUsageByTitleAndType(@Param("title") String title, @Param("type") String type);

    @Query(value = "SELECT * FROM wp_posts wpp INNER JOIN wp_postmeta wppm ON wpp.ID = wppm.post_id WHERE wpp.post_type = 'shop_order' AND wpp.post_status = 'wc-processing' AND wppm.meta_key = 'sb_platform' AND wppm.meta_value LIKE '%mobile%' AND wpp.post_date < NOW() AND wpp.post_date > DATE_SUB(NOW(), INTERVAL :delta minute)" , nativeQuery = true)
    List <WpPost> findAllMobilePending(@Param("delta") Float delta);

    @Query(value = "SELECT wpp.* FROM wp_posts wpp INNER JOIN wp_postmeta wpm1 ON wpp.id = wpm1.post_id \n" +
                   "INNER JOIN wp_postmeta wpm2 ON wpp.id = wpm2.post_id \n" +
                   "WHERE wpm1.meta_key = '_customer_user' AND wpm1.meta_value = :customer_id \n" +
                   "AND wpm2.meta_key = 'sb_platform' AND wpm2.meta_value = 'subscription' \n" +
                   "ORDER BY wpp.ID DESC LIMIT 1" , nativeQuery = true)
    WpPost findLastSubscriptionOrder(@Param("customer_id") Long customer_id);
}
