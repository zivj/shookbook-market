package com.shookbook.dao.wordpress;

import com.shookbook.entities.wordpress.WpOrderItemmeta;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WpOrderItemmetaDao extends JpaRepository<WpOrderItemmeta, Long> {
}
