package com.shookbook.dao.wordpress;

import com.shookbook.entities.wordpress.WpPostmeta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface WpPostmetaDao extends JpaRepository<WpPostmeta, Long>
{
    @Query("FROM WpPostmeta WHERE post_id = :post_id AND meta_key = :meta_key")
    WpPostmeta findOneByIdAndKey(@Param("post_id") Long postId, @Param("meta_key") String metaKey);

    @Transactional
    @Modifying
    @Query(value = "UPDATE wp_postmeta SET meta_value = :meta_value WHERE post_id = :post_id AND meta_key = :meta_key", nativeQuery = true)
    void updateByIdAndKey(@Param("post_id") Long postId, @Param("meta_key") String metaKey, @Param("meta_value") String metaValue);

    @Query(value = "SELECT count(meta_id) FROM wp_postmeta WHERE post_id = :post_id AND meta_key = '_used_by'", nativeQuery = true)
    Integer getPostUsage(@Param("post_id") Integer post_id);

    @Query(value = "SELECT count(meta_id) FROM wp_postmeta WHERE post_id = :post_id AND meta_key = '_used_by' and meta_value= :user_id", nativeQuery = true)
    Integer getPostUsageByUser(@Param("post_id") Integer post_id, @Param("user_id") String user_id);

    @Query(value = "SELECT meta_value FROM wp_postmeta WHERE post_id = :post_id AND meta_key = '_used_by'", nativeQuery = true)
    List<String> findAllUsage(@Param("post_id") Integer post_id);
}
