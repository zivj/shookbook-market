package com.shookbook.dao.wordpress;

import com.shookbook.entities.wordpress.WpTermRelationships;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WpTermRelationshipsDao extends JpaRepository<WpTermRelationships, Long>
{
}
